In this document, we will briefly introduce our framework 'MPress'. 

# What is MPress
 
MPress is a framework built on top of PyTorch, and tested with [PipeDream](https://github.com/msr-fiddle/pipedream)/[DAPPLE](https://dl.acm.org/doi/abs/10.1145/3437801.3441593). It aims to break the GPU memory wall of inter-operator parallel DNN training while minimizing extra cost. 


# MPress-PipeDream-based
We first implemented MPress based on PipeDream and did a lot of experiments. The experiments show that compared to PipeDream, MPress can capture more performance gains or train larger models by exploiting the NVLINK connections between GPUs by tapping into hardware features. See subfolder `mpress-pipedream-based` for more details including how to compile, test and reproduce the figures.

# MPress-DAPPLE-based
Limited by some unreasonable mechanisms of PipeDream, such as saving too many versions of parameters, data transfer between stages need to go through CPU, and computation and communication do not have good overlap, resulting in MPress-PipeDream-based cannot train extremely-large models or achieve very high performance. So we further implemented MPress based on using schedule applied in DAPPLE. This version of MPress can support training several tens of B-parameter models on an 8-card V100 machine, while achieving up to 55+ TFLOPs per card. See subfolder `mpress-dapple-based`  for more details including how to install, test and reproduce the figures.