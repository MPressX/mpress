
import torch
from torch import nn

from torchgpipe.checkpoint import Checkpoint, Recompute, Checkpointing, enable_checkpointing
from torchgpipe.microbatch import Batch
from typing import Deque, List, Tuple, Union, Optional
from torch import Tensor, ByteTensor
from collections import deque, OrderedDict
from torchgpipe.phony import get_phony
from torchgpipe.dependency import fork, join
from torch.utils.checkpoint import checkpoint
from torchgpipe.pipeline import Tensors, TensorOrTensors

class Module_with_checkpoint(nn.Module):
    def __init__(self, m:nn.Module):
        super(Module_with_checkpoint, self).__init__()
        self.module = m
    def forward(self, x:TensorOrTensors):
        atomic = False
        if isinstance(x, Tensor):
            x = (x,)
            atomic = True
        recomputed: Deque[Recomputed] = deque(maxlen=1)
        rng_states: Deque[RNGStates] = deque(maxlen=1)
        phony = get_phony(x[0].device, requires_grad=True)
        output = Checkpoint.apply(phony, recomputed, rng_states, self.module, atomic, *x)
        if isinstance(output, Tensor):
            output, phony = fork(output)
            phony = Recompute.apply(phony, recomputed, rng_states, self.module, atomic, *x)
            output = join(output, phony)
        else: # tuple of tensors
            output = list(output)
            output[0], phony = fork(output[0])
            phony = Recompute.apply(phony, recomputed, rng_states, self.module, atomic, *x)
            output[0] = join(output[0], phony)
            output = tuple(output)
        return output
    
def cast_sequential_with_recomputing_ratio(
    module: nn.Sequential,
    recomputing_ratio:float=1.0
):
    from math import ceil
    ckpt_layer_num = ceil(len(module) * recomputing_ratio)
    layers = OrderedDict()
    for name, layer in module.named_children():
        if ckpt_layer_num > 0:
            ckpt_layer_num -= 1
            layers[name]=Module_with_checkpoint(layer)
        else:
            layers[name]=layer
    return nn.Sequential(layers)
        