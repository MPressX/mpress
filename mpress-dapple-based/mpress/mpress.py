"""The GPipe interface."""
from collections import OrderedDict
from typing import TYPE_CHECKING, Any, Iterable, List, Optional, Tuple, Union, cast

import torch
from torch import Tensor, nn
import torch.autograd
import torch.cuda
import time

from torchgpipe import microbatch
from torchgpipe.batchnorm import DeferredBatchNorm
# from torchgpipe.pipeline import Pipeline
from torchgpipe.stream import AbstractStream, new_stream
from torchgpipe.gpipe import recommend_auto_balance, split_module, verify_module, BalanceError
from torchgpipe.gpipe import Device, Devices, Tensors, TensorOrTensors, Module, NamedModules
from deepspeed.ops.adam import DeepSpeedCPUAdam
from deepspeed.ops.adam import FusedAdam
from .appleline import Appleline

__all__ = ['MPress']

       
def generate_cpu_parameters(m:nn.Module):
    for param in m.parameters():
        yield param.detach().to(torch.device("cpu"))
        
        
def copy_gradient(m:nn.Module, optimizer:DeepSpeedCPUAdam):
    for param_id, param in enumerate(m.parameters()):
        if param.grad is not None:
            cpu_grad = param.grad.detach().to(torch.device("cpu"), non_blocking=True)
            optimizer.param_groups[0]['params'][param_id].grad = cpu_grad
def copy_parameter_back(m:nn.Module, optimizer:DeepSpeedCPUAdam):
    for param_id, param in enumerate(m.parameters()):
        if param.grad is not None:
            param.data = optimizer.param_groups[0]['params'][param_id].detach().to(param.device, non_blocking=True).data
        


class MPress(Module):
    balance: List[int] = []
    devices: List[torch.device] = []
    chunks: int = 1
    
    def __init__(self,
                 module: nn.Sequential,
                 balance: Iterable[int],
                 devices: Optional[Devices]=None,
                 chunks: int=chunks,
                 deferred_batch_norm: bool=False,
                 calc_loss: callable = None,
                 enable_swap_optimizer: bool=False,
                 checkpoint_strategy:str="never",
                 enable_fp16:bool=True
                 ) -> None:
        super().__init__()
        chunks = int(chunks)
        if chunks < 0:
            raise ValueError('number of chunks must be positive integer')
        verify_module(module)
        
        self.chunks = chunks
        
        if deferred_batch_norm:
            module = DeferredBatchNorm.convert_deferred_batch_norm(module, chunks)
        
        if devices is None:
            devices = range(torch.cuda.device_count())
        devices = [torch.device(d) for d in devices]
        devices = cast(List[torch.device], devices)
        
        try:
            self.partitions, self.balance, self.devices = split_module(module, balance, devices)
        except BalanceError as exc:
            raise ValueError(recommend_auto_balance(str(exc)))
        
        self.enable_fp16=enable_fp16    
        if self.enable_fp16:
            for partition in self.partitions:
                partition.half()
    
        self._copy_streams: List[List[AbstractStream]] = []
        
        self.calc_loss = calc_loss if calc_loss else lambda *args, **kwargs: None
        
        self.enable_swap_offload = enable_swap_optimizer
        self.optimizers:List[DeepSpeedCPUAdam] = []
        if enable_swap_optimizer:
            for i, partition in enumerate(self.partitions):
                optimizer = DeepSpeedCPUAdam(generate_cpu_parameters(partition))
                self.optimizers.append(optimizer)
        else:
            for i, partition in enumerate(self.partitions):
                optimizer = FusedAdam(partition.parameters())
                self.optimizers.append(optimizer)
        self.checkpoint_strategy = checkpoint_strategy
        
    def _ensure_copy_streams(self) -> List[List[AbstractStream]]:
        if not self._copy_streams:
            for device in self.devices:
                self._copy_streams.append([new_stream(device) for _ in range(self.chunks)])
        return self._copy_streams
    
    def train_for_input(self, 
                        input: TensorOrTensors, 
                        calc_loss:callable = None) -> None:
        enable_profiling = True
        simulate_better_overlap = False
        if not calc_loss:
            calc_loss = self.calc_loss
        
        # cast input
        if isinstance(input, torch.Tensor):
            input = input.to(device=self.devices[0])
            if self.enable_fp16 and input.dtype == torch.float32:
                input = input.half()
        else:
            input = list(input)
            for i in range(len(input)):
                input[i] = input[i].to(device=self.devices[0])
                if self.enable_fp16 and input[i].dtype == torch.float32:
                    input[i].half()
            input = tuple(input)
        
        if enable_profiling:
            torch.cuda.synchronize()
            t0 = time.time()
        microbatch.check(input)
        if not self.devices:
            return input
        
        batches = microbatch.scatter(input, self.chunks)
        
        copy_streams = self._ensure_copy_streams()


        appleline = Appleline(calc_loss=calc_loss,
                              batches=batches,
                              partitions=self.partitions,
                              checkpoint_strategy=self.checkpoint_strategy,
                              devices=self.devices,
                              copy_streams=copy_streams)
        appleline.run()
        
        if enable_profiling:
            torch.cuda.synchronize()
            t1 = time.time()
        
        if self.enable_swap_offload:
            for partition,optimizer in zip(self.partitions,self.optimizers):
                # simulate overlap with better scheduling
                if simulate_better_overlap:
                    if optimizer != self.optimizers[-1]:
                        continue
                copy_gradient(partition, optimizer)
            torch.cuda.synchronize()
            
            for partition,optimizer in zip(self.partitions,self.optimizers):
                # optimizer.step()
                pass
            # torch.cuda.synchronize()
            
            for partition, optimizer in zip(self.partitions, self.optimizers):
                if simulate_better_overlap:
                    if optimizer != self.optimizers[-1]:
                        continue
                copy_parameter_back(partition, optimizer)
            torch.cuda.synchronize()
        else:
            for partition,optimizer in zip(self.partitions, self.optimizers):
                optimizer.step()
                pass
                
        if enable_profiling:
            torch.cuda.synchronize()
            t2 = time.time()
        if enable_profiling:
            pipeline_time = t1 - t0
            offload_copy_time = t2 - t1
            print(f"pipeline time: {pipeline_time}, offload copy time: {offload_copy_time}")
            