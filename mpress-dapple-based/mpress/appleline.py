"""The pipeline parallelism of GPipe."""
from queue import Queue
from tkinter import E
from types import TracebackType
from typing import TYPE_CHECKING, Iterable, List, Optional, Tuple, Type, Union, cast

import torch
from torch import Tensor, nn

from torchgpipe.checkpoint import Checkpointing, enable_checkpointing
from torchgpipe.copy import Copy, Wait
from torchgpipe.dependency import fork, join
from torchgpipe.microbatch import Batch
from torchgpipe.stream import AbstractStream, current_stream, use_device, get_device, use_stream, record_stream, wait_stream
from torchgpipe.worker import Task, spawn_workers
from torchgpipe.pipeline import Tensors, TensorOrTensors, ExcInfo, InQueue, OutQueue, depend, copy, wait


from zq_tools import zq_tracing

import colorful as cf

__all__: List[str] = []

def batch_copy(prev_stream:AbstractStream, 
               next_stream:AbstractStream,
               batch:Batch)->Batch:
    output_stream = current_stream(get_device(next_stream))
    if batch.atomic:
        with use_stream(prev_stream), use_stream(next_stream):
            x = batch.tensor
            y = x.to(get_device(next_stream))
            record_stream(x, prev_stream)
            record_stream(y, output_stream)
        return Batch(y)
    else:
        with use_stream(prev_stream), use_stream(next_stream):
            output = []
            for x in batch.tensors:
                y = x.to(get_device(next_stream))
                output.append(y)
                record_stream(x, prev_stream)
                record_stream(y, output_stream)
        return Batch(tuple(output))

def clock_cycles(m: int, n: int) -> Iterable[List[Tuple[int, int, bool]]]:
    """Generates schedules for each clock cycle."""
    # m: number of micro-batches
    # n: number of partitions
    # i: index of micro-batch
    # j: index of partition
    # k: clock number
    # F: T/F: T:forward, F: backward
    #
    # k (i,j,F) (i,j,F) (i,j,F)
    # - ------- ------- -------
    # 0 (0,0,T)
    # 1 (1,0,T) (0,1,T)
    # 2         (0,1,F)
    # 3 (0,0,F) (1,1,T)
    # 4         (1,1,F)
    # 5 (1,0,F)
    total_clocks = m*2+(n-1)*2
    total_schedules = []
    
    for partition_id in range(n):
        schedule_for_partition = [None for _ in range(total_clocks)]
        warmup_iterations = min(m, n-partition_id)
        for batch_id in range(warmup_iterations):
            schedule_for_partition[batch_id+partition_id] = (batch_id, partition_id, True)
        for batch_id in range(warmup_iterations, m):
            schedule_for_partition[warmup_iterations+n+(batch_id-warmup_iterations)*2] = (batch_id, partition_id, True)
        for batch_id in range(m):
            schedule_for_partition[n+n-partition_id-1+2*batch_id] = (batch_id, partition_id, False)
        total_schedules.append(schedule_for_partition)
    
    for clock in range(total_clocks):
        ans = []
        for partition_id in range(n):
            schedule = total_schedules[partition_id][clock]
            if schedule:
                ans.append(schedule)
        yield ans



class Appleline:
    """The pipeline parallelism for GPipe."""

    def __init__(self,
                 calc_loss:callable, 
                 batches: List[Batch],
                 partitions: List[nn.Sequential],
                 checkpoint_strategy: str,
                 devices: Optional[List[torch.device]] = None,
                 copy_streams: Optional[List[List[AbstractStream]]] = None,
                 ) -> None:
        self.calc_loss = calc_loss 
        self.batches = batches
        self.partitions = partitions
        self.devices = devices
        self.copy_streams = copy_streams
        # self.input_datas[partition_id][chunk_id]
        self.input_datas:List[List[Batch]] = [[None for _ in range(len(batches))] for _ in range(len(partitions))] 
        self.output_datas:List[List[Batch]] = [[None for _ in range(len(batches))] for _ in range(len(partitions))] 
        self.grad_datas:List[List[Batch]] = [[None for _ in range(len(batches))] for _ in range(len(partitions))] 
        assert(checkpoint_strategy in ["except_last", "always", "never"])
        self.checkpoint_strategy = checkpoint_strategy
    
    def should_recompute(self, partition_id):
        if self.checkpoint_strategy=="always":
            return True
        elif self.checkpoint_strategy=="never":
            return False
        else:
            return not self.is_last_partition(partition_id)
        
    def is_last_partition(self, partition_id):
        return partition_id == len(self.partitions) - 1

    def run(self) -> None:
        """Runs pipeline parallelism.

        It modifies the given batches in place.

        """
        batches = self.batches
        partitions = self.partitions
        devices = self.devices

        m = len(batches)
        n = len(partitions)

        with spawn_workers(devices) as (in_queues, out_queues):
            for schedule in clock_cycles(m, n):
                self.fence(schedule)
                self.compute(schedule, in_queues, out_queues)

    def fence(self,
              schedule: List[Tuple[int, int, bool]]
              ) -> None:
        """Copies micro-batches after computation for the previous
        micro-batches.
        """
        batches = self.batches
        copy_streams = self.copy_streams

        for i, j, is_forward in schedule:
            # Ensure that batches[i-1] is executed after batches[i] in
            # backpropagation by an explicit dependency.
            # No need in DAPPLE !!!
            # if i != 0:
            #     depend(batches[i-1], batches[i])

            next_stream = copy_streams[j][i]
            if is_forward:
                if j == 0:
                    self.input_datas[j][i] = batches[i].detach()
                if j != 0:
                    # to check !!!
                    prev_stream = copy_streams[j-1][i]
                    self.input_datas[j][i] = batch_copy(prev_stream, next_stream, self.output_datas[j-1][i]).detach()
            else:
                # if not self.is_last_partition(j):
                #     prev_stream = copy_streams[j+1][i]
                #     # ?
                if self.is_last_partition(j):
                    pass
                else:
                    # to check stream !!!
                    # print(f"fencing: i,j,is_schedule={i,j,is_forward}")
                    prev_stream = copy_streams[j+1][i]
                    # import time
                    # time.sleep(1)
                    # BERT 与 GPT 中的第二个tensor都不参加grad计算
                    self.grad_datas[j][i] = batch_copy(prev_stream, next_stream, Batch(self.input_datas[j+1][i][0].grad)).detach()
                    

    def compute(self,
                schedule: List[Tuple[int, int, bool]],
                in_queues: List[InQueue],
                out_queues: List[OutQueue],
                ) -> None:
        """Runs tasks with synchronization to copy streams."""
        batches = self.batches
        partitions = self.partitions
        devices = self.devices
        copy_streams = self.copy_streams

        n = len(partitions)
        streams = [current_stream(d) for d in devices]
        exc_info: Optional[ExcInfo] = None

        # With checkpointing, the autograd graph looks like this diagram:
        # ┌─────┸──────┐
        # │    Copy    │
        # └─────┰──────┘   (fence)
        # ─ ─ ─ ╂ ─ ─ ─ ─ ─ ─ ─ ─ ─
        #       ┃          (compute)
        # ┌─────┸──────┐
        # │    Wait    │ [1] Synchronize the current stream with the copy stream.
        # └─────┰──────┘
        # ┌─────┸──────┐
        # │ Checkpoint │ [2] Compute a partition within checkpointing.
        # └─────┰──────┘
        # ┌─────┸──────┐
        # │    Wait    │ [3] Synchronize the copy stream with the current stream.
        # └─────┰──────┘
        #       ┠ ─ ─ ─ ┐
        #       ┃ ┌─────┴─────┐
        #       ┃ │ Recompute │ [4] Schedule the recomputation at backpropagation.
        #       ┃ └─────┬─────┘
        #       ┠ ─ ─ ─ ┘
        #       ┃
        # ─ ─ ─ ╂ ─ ─ ─ ─ ─ ─ ─ ─ ─
        # ┌─────┸──────┐   (fence)
        # │    Copy    │
        # └─────┰──────┘
        for i, j, is_forward in schedule:
            if is_forward:
                zq_tracing.record_begin_async(name=f"partition_{j}", id=j, cat="host", direction="fw", batch_id=i)
                input:Batch = self.input_datas[j][i]
                partition = partitions[j]

                # Synchronize with the copied input. ([1] in the diagram)
                if j != 0:
                    wait(input, copy_streams[j][i], streams[j])
                    input.set_requires_grad_retain_grad()
                    
                # print(cf.red(f"compute: {i},{j},Forward: input.requires_grad={input.value.requires_grad}"))

                should_recompute = self.should_recompute(j)
                if should_recompute:
                    def forward(input:TensorOrTensors,
                                partition: nn.Sequential=partition,
                                i=i,
                                j=j
                                )->TensorOrTensors:
                        zq_tracing.record_begin(name=f"forward_batch_{i}_partition_{j}", cat="device", tid=j)
                        output = partition(input)
                        zq_tracing.record_end(name=f"forward_batch_{i}_partition_{j}", cat="device", tid=j)
                        return output
                    chk = Checkpointing(forward, input)
                    task = Task(streams[j], compute=chk.checkpoint, finalize=chk.recompute)
                    del forward, chk
                else:
                    def forward(batch: Batch = input,
                                partition: nn.Sequential = partition,
                                i=i,
                                j=j
                                ) -> Batch:
                        zq_tracing.record_begin(name=f"forward_batch_{i}_partition_{j}", cat="device", tid=j)
                        output = batch.call(partition)
                        zq_tracing.record_end(name=f"forward_batch_{i}_partition_{j}", cat="device", tid=j)
                        return output
                    task = Task(streams[j], compute=forward, finalize=None)
                    del forward

                # Compute tasks in parallel. ([2] in the diagram)
                in_queues[j].put(task)
            else:
                zq_tracing.record_begin_async(name=f"partition_{j}", id=j, cat="host", direction="bw", batch_id=i)
                if self.is_last_partition(j):
                    output = self.output_datas[j][i]
                    input = self.input_datas[j][i]
                    def backward(output: Batch=output,
                                 input: Batch=input,
                                 i=i,
                                 j=j
                                 ) -> Batch:
                        
                        zq_tracing.record_begin(name=f"backward_batch_{i}_partition_{j}", cat="device", tid=j)
                        loss = self.calc_loss(output.value)
                        loss.backward()
                        zq_tracing.record_end(name=f"backward_batch_{i}_partition_{j}", cat="device", tid=j)
                        # print(f"loss={loss}")
                        # print(f"input.grad={input.value.grad}")
                        # print(f"input.requires_grad={input.value.requires_grad}")
                        # return input.grad()
                        return None
                    task = Task(streams[j], compute=backward, finalize=None)
                    del backward
                else:
                    grad = self.grad_datas[j][i]
                    output = self.output_datas[j][i]
                    wait(grad, copy_streams[j][i], streams[j])
                    def backward(tensors: Batch=output,
                                 grads: Batch=grad,
                                 i=i,j=j
                                 )->Batch:
                        # torch.autograd.backward(tensors.value, grads.value, retain_graph=True)
                        # if tensors.atomic:
                        #     torch.autograd.backward(tensors.value, grads.value)
                        # else:
                        #     torch.autograd.backward(tensors.value[0], grads.value[0])
                        # 因为bert/GPT中如果有两个参数，则第二个参数永远是不需要grad的
                        zq_tracing.record_begin(name=f"backward_batch_{i}_partition_{j}", cat="device", tid=j)
                        torch.autograd.backward(tensors[0], grads[0])
                        zq_tracing.record_end(name=f"backward_batch_{i}_partition_{j}", cat="device", tid=j)
                        
                        # return self.input_datas[j][i].grad()
                        return None
                    task = Task(streams[j], compute=backward, finalize=None)
                    del backward
                # print(cf.red(f"compute: {i},{j},Backward: input.requires_grad={input.value.requires_grad}, input={input}"))
                in_queues[j].put(task)

        zq_tracing.record_begin_async(name=f"wait", id=9, cat="host")
        for i, j, is_forward in schedule:
            ok, payload = out_queues[j].get()

            # Hold the first exception.
            if exc_info is not None:
                continue
            elif not ok:
                exc_info = cast(ExcInfo, payload)
                continue
            
            task, output = cast(Tuple[Task, Batch], payload)
            if is_forward:
                self.output_datas[j][i] = output

                # The copy stream synchronizes to copy the output. ([3] in the
                # diagram)
                if j != n-1:
                    # wait(output, streams[j], copy_streams[j][i])
                    wait_stream(streams[j], copy_streams[j][i])
                zq_tracing.record_end_async(name=f"partition_{j}", id=j, cat="host", direction="fw", batch_id=i)
                with use_device(devices[j]):
                    task.finalize(output)
            else:
                if j!=0:
                    wait_stream(streams[j], copy_streams[j][i])
                zq_tracing.record_end_async(name=f"partition_{j}", id=j, cat="host", direction="bw", batch_id=i)
                self.output_datas[j][i] = None
                self.grad_datas[j][i] = None
                if not self.is_last_partition(j):
                    self.input_datas[j+1][i] = None
                if j==0:
                    self.input_datas[j][i] = None

                if j==0:
                    # print(f"Finish microbatch: {i}")
                    pass
        zq_tracing.record_end_async(name=f"wait", id=9, cat="host")


        # Fail at the first exception.
        if exc_info is not None:
            raise exc_info[0].with_traceback(exc_info[1], exc_info[2])
