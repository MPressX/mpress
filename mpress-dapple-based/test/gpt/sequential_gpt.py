import model
from torch import nn
import torch

def json_to_config(json_path):
    import json
    with open(json_path, 'r') as f:
        j = json.load(f)
        config = GPT_Config(
            seq_len=j['seq_len'],
            n_heads=j['n_heads'],
            n_layers=j['n_layers'],
            vocab_size=j['vocab_size'],
            d_model=j['d_model'],
            d_ff=j['d_ff']
            
        )
        return config

class GPT_Config():
    vocab_size:int 
    seq_len:int 
    d_model:int 
    n_layers: int 
    n_heads:int 
    d_ff:int 
    embd_pdrop:float 
    attn_pdrop:float 
    resid_pdrop:float 
    pad_id:int 
    
    def __init__(
        self,
        vocab_size=50304, 
        seq_len=1024,
        d_model=768, # hidden size
        n_layers=12, # num layers
        n_heads=16, 
        d_ff=3072, # intermediate size?
        embd_pdrop=0.1,
        attn_pdrop=0.1,
        resid_pdrop=0.1,
        pad_id=0
    ):
        self.vocab_size=vocab_size
        self.seq_len=seq_len
        self.d_model=d_model
        self.n_layers=n_layers
        self.n_heads=n_heads
        self.d_ff=d_ff
        self.embd_pdrop=embd_pdrop
        self.attn_pdrop=attn_pdrop
        self.resid_pdrop=resid_pdrop
        self.pad_id=pad_id
        

class GPT_Pre(nn.Module):
    def __init__(self, config:GPT_Config):
        super(GPT_Pre, self).__init__()
        self.pad_id = config.pad_id
        self.embedding = nn.Embedding(config.vocab_size, config.d_model)
        self.dropout = nn.Dropout(config.embd_pdrop)
        self.pos_embedding = nn.Embedding(config.seq_len+1, config.d_model)
        
    
    def get_attention_padding_mask(self, q, k, pad_id):
        attn_pad_mask = k.eq(pad_id).unsqueeze(1).repeat(1, q.size(1), 1)
        return attn_pad_mask
    
    def get_attention_subsequent_mask(self, q):
        bs, q_len = q.size()
        subsequent_mask = torch.ones(bs, q_len, q_len).triu(diagonal=1)
        return subsequent_mask

    def forward(self, inputs):
        # |inputs| : (batch_size, seq_len), LongTensor
        positions = torch.arange(inputs.size(1), device=inputs.device, dtype=inputs.dtype).repeat(inputs.size(0),1)+1
        position_pad_mask = inputs.eq(self.pad_id)
        positions.masked_fill_(position_pad_mask, 0)
        # |positions| : (batch_size, seq_len), LongTensor
        
        outputs = self.dropout(self.embedding(inputs)) + self.pos_embedding(positions)
        # |outputs| : (batch_size, seq_len, d_model)
        
        attn_pad_mask = self.get_attention_padding_mask(inputs, inputs, self.pad_id)
        subsequent_mask = self.get_attention_subsequent_mask(inputs).to(device=attn_pad_mask.device)
        attn_mask = torch.gt((attn_pad_mask.to(dtype=subsequent_mask.dtype) + subsequent_mask), 0)
        # |attn_pad_mask| : (batch_size, seq_len, seq_len), BoolTensor
        # |subsequent_mask| : (batch_size, seq_len, seq_len), FloatTensor # it is a float type, not bool.
        # |attn_mask| : (batch_size, seq_len, seq_len), BoolTensor
        
        
        return (outputs, attn_mask)

class GPT_Transformer(nn.Module):
    def __init__(self, config:GPT_Config):
        super(GPT_Transformer, self).__init__()
        self.decoder = model.DecoderLayer(
            config.d_model, 
            config.n_heads,
            config.d_ff,
            config.attn_pdrop,
            config.resid_pdrop
        )
    def forward(self, inputs):
        x, attn_mask = inputs
        # |x| : (batch_size, seq_len, d_model)
        # |attn_mask| : (batch_size, seq_len, seq_len), BoolTensor
        x, _ = self.decoder(x, attn_mask)
        # _ as |attn_weights| : (batch_size, n_heads, seq_len, seq_len)
        return x, attn_mask

class GPT_Post(nn.Module):
    def __init__(self, config:GPT_Config, gpt_pre:GPT_Pre):
        super(GPT_Post, self).__init__()
        self.linear = nn.Linear(config.d_model, config.vocab_size, bias=False)
        # only for validate pipeline
        # self.linear.weight = gpt_pre.embedding.weight
    def forward(self, inputs):
        outputs, attn_mask = inputs
        # |outputs| : (batch_size, seq_len, d_model)
        # |attn_mask| : (batch_size, seq_len, seq_len), BoolTensor
        lm_logits = self.linear(outputs)
        # |lm_logits| : (batch_size, seq_len, vocab_size)
        return lm_logits
      
def build_GPTSequential(config:GPT_Config):
    """rebuild like model for pretrain"""
    layers = [('GPT_Pre', GPT_Pre(config))]
    for i in range(config.n_layers):
        layers.append(
            (f"Transformer_{i}", GPT_Transformer(config))
        )
    layers.append(('GPT_Post', GPT_Post(config, layers[0][1])))
    from collections import OrderedDict
    return nn.Sequential(OrderedDict(layers))
    