from logging import critical
import torch
from torch import nn
from sequential_gpt import build_GPTSequential, GPT_Config

# config = GPT_Config(n_layers=20, d_model=2048, n_heads=12, d_ff=4096*2)
config = GPT_Config(n_layers=2, d_model=768, n_heads=12, d_ff=1024)

gpt = build_GPTSequential(config)

def generate_inputs(batchsize:int, config:GPT_Config):
    inputs=torch.randint(low=0, high=config.vocab_size, size=(batchsize, config.seq_len))
    # targets=torch.randint(low=0, high=config.vocab_size, size=(batchsize, config.seq_len-1))
    targets=inputs[:,1:].contiguous()
    return inputs, targets

def getLoss(lm_logits, targets):
    criterion = nn.CrossEntropyLoss(ignore_index = config.pad_id)
    lm_logits = lm_logits[:, :-1].contiguous()
    loss = criterion(lm_logits.view(-1, config.vocab_size), targets.view(-1))
    return loss

def str_shape_and_dtype(t):
    if type(t) is torch.Tensor:
        return "[shape={}, dtype={}]".format(t.shape, t.dtype)
    t = list(t)
    t = [str_shape_and_dtype(v) for v in t]
    return f"({', '.join(t)})"

batchsize=1
gpt = gpt.cuda().half()
for i in range(1):
    print(f"training iteration : {i}")
    inputs, targets = generate_inputs(batchsize=batchsize, config=config)
    inputs=inputs.cuda()
    targets=targets.cuda()
    # lm_logits = gpt(inputs)
    for i,m in enumerate(gpt):
        print(f"layer {i} inputs: {str_shape_and_dtype(inputs)}")
        inputs = m(inputs)
        print(f"layer {i} outputs: {str_shape_and_dtype(inputs)}")
    lm_logits = inputs
    
    loss = getLoss(lm_logits, targets)
    print(f"loss={loss}")
    
    