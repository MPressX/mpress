import torch
from torch import Tensor
from torch import nn
from sequential_gpt import build_GPTSequential, GPT_Config, json_to_config, GPT_Pre, GPT_Transformer
import argparse
import sys
sys.path.append("../../")
from mpress.mpress import MPress
import time

# config = json_to_config("gpt_configs/gpt-max-1080Ti.json")
config = json_to_config("gpt_configs/gpt-full.json")
batchsize = 1

def generate_inputs(batchsize:int, config:GPT_Config):
    inputs=torch.randint(low=0, high=config.vocab_size, size=(batchsize, config.seq_len))
    # targets=torch.randint(low=0, high=config.vocab_size, size=(batchsize, config.seq_len-1))
    targets=inputs[:,1:].contiguous()
    return inputs, targets



head_input = generate_inputs(batchsize, config)
head = GPT_Pre(config)
mid_input = head(head_input[0])
print(mid_input[0].shape, mid_input[1].shape)
x, attn_mask = mid_input
x = x.cuda().half().detach()
x.requires_grad=True
attn_mask = attn_mask.cuda()

v = (x, attn_mask)
num_layers = 1
layers = []
for _ in range(num_layers):
    layers.append(GPT_Transformer(config).cuda().half())
torch.cuda.synchronize()
print(f"memory_summary: {torch.cuda.memory_summary(abbreviated=True)}")


elapsed_times = []
start_events = []
stop_events = []
with torch.no_grad():
    # for layer in layers:
    for i in range(10):
        layer = layers[0]
        start = torch.cuda.Event(enable_timing=True)
        stop = torch.cuda.Event(enable_timing=True)
        start.record()
        v = layer(v)
        stop.record()
        start_events.append(start)
        stop_events.append(stop)

torch.cuda.synchronize()

for start, stop in zip(start_events, stop_events):
    elapsed_times.append(start.elapsed_time(stop))
print(elapsed_times)
print(f"memory_summary: {torch.cuda.memory_summary(abbreviated=True)}")






