from functools import lru_cache
from typing import List, Tuple
head = 0
tail = 0.38
layer = 0.34
num_layers = 300
num_nodes = 8

tfs = [head] + [layer for _ in range(num_layers)] + [tail]
ckp_strategy = [True for _ in range(num_nodes-1)] + [False]

@lru_cache(maxsize=None)
def search(left_layers:int, left_nodes:int)->Tuple[List[int], float]:
    enable_ckp = ckp_strategy[left_nodes-1]
    amplify_ratio = 4 if enable_ckp else 3
    if left_nodes == 1:
        return [left_layers], amplify_ratio*sum(tfs[:left_layers])
    if left_layers == 0:
        return [0 for _ in range(left_nodes)], 0
        
    minimum_computation = float('inf')
    layer_distribution = []
    for layers_in_last_node in range(0, left_layers+1):
        comp_of_last_node = sum(tfs[left_layers-layers_in_last_node:left_layers])*amplify_ratio
        dist, comp_of_rest_nodes = search(left_layers-layers_in_last_node, left_nodes-1)
        if max(comp_of_last_node, comp_of_rest_nodes) < minimum_computation:
            minimum_computation = max(comp_of_last_node, comp_of_rest_nodes)
            layer_distribution = dist + [layers_in_last_node]
    return layer_distribution, minimum_computation

def dist_to_comp(dist:List[int])->List[float]:
    layer_start = 0
    ans = []
    for i in range(num_nodes):
        enable_ckp = ckp_strategy[i]
        amplify_ratio = 4 if enable_ckp else 3
        ans.append(sum(tfs[layer_start:layer_start+dist[i]])*amplify_ratio)
        layer_start += dist[i]
    return ans

dist, comp = search(num_layers+2, num_nodes)
print(f"dist: {dist}")
print(f"comp: {comp}")
print(f"list of comp: {dist_to_comp(dist)}")