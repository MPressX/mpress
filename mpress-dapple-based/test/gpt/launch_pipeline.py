import torch
from torch import nn
from sequential_gpt import build_GPTSequential, GPT_Config, json_to_config
import argparse
import sys
sys.path.append("../../")
from mpress.mpress import MPress
from mpress.mpress import MPress
import time
from deepspeed.ops.adam import FusedAdam
from zq_tools import zq_tracing

# batchsize = 256
enable_print_params=True
enable_print_tflop=False
enable_print_mem_used=False
TERA_FLOPS=0

bs_per_chunk = 2

def print_mem_used():
    def print_in_16_width_to_right(x):
        print("{:>16}".format(x), end="")
    p = print_in_16_width_to_right
    num_devices=torch.cuda.device_count()
    for i in range(num_devices):
        print(torch.cuda.memory_summary(i, abbreviated=True))
    

def get_parameters(m:nn.Module):
    total_params = sum(p.numel() for p in m.parameters())
    # total_trainable_params = sum(p.numel() for p in m.parameters() if p.requires_grad)
    return total_params

def print_num_params(n:int):
    return f"[{n}={n/1e9:.2f}B={n*4/1024/1024/1024:.2f}GiB]"

def generate_inputs(batchsize:int, config:GPT_Config):
    inputs=torch.randint(low=0, high=config.vocab_size, size=(batchsize, config.seq_len))
    # targets=torch.randint(low=0, high=config.vocab_size, size=(batchsize, config.seq_len-1))
    targets=inputs[:,1:].contiguous()
    return inputs, targets

def getLoss(lm_logits, targets, config:GPT_Config):
    criterion = nn.CrossEntropyLoss(ignore_index = config.pad_id)
    lm_logits = lm_logits[:, :-1].contiguous()
    loss = criterion(lm_logits.view(-1, config.vocab_size), targets.view(-1))
    return loss

def get_nbytes(x):
    if type(x) == torch.Tensor:
        return x.numel() * x.element_size()
    return sum([get_nbytes(v) for v in list(x)])


def balance2devices(balance):
    devices = []
    for i in range(len(balance)):
        for j in range(balance[i]):
            devices.append(i)
    return devices

def print_parmas(gpt:nn.Sequential, config:GPT_Config):
    # print #params
    print("=============== counting params ===============")
    num_params_head = get_parameters(gpt[0])
    num_params_tail = get_parameters(gpt[-1])
    num_params_single_layer = get_parameters(gpt[1])
    num_params_all = num_params_head+num_params_tail+num_params_single_layer*(config.n_layers)
    assert(num_params_all==get_parameters(gpt))
    print(f"num_params_head: {print_num_params(num_params_head)}")
    print(f"num_params_tail: {print_num_params(num_params_tail)}")
    print(f"num_params_layer(single): {print_num_params(num_params_single_layer)}")
    print(f"num_params_all: {print_num_params(num_params_all)}")

def print_tflops(gpt:nn.Sequential, config:GPT_Config)->float:
    # For the exact same model, the TFLOP calculated in DeepSpeed is about 3  times higher than that calculated via fvcore.     
    # Manually Align the values
    print("=============== counting tflop & activation checkpoints ===============")
    from fvcore.nn import FlopCountAnalysis
    inputs, targets = generate_inputs(batchsize=1, config=config)
    flops_head = FlopCountAnalysis(gpt[0], (inputs,))
    x = gpt[0](inputs)
    flops_single_layer = FlopCountAnalysis(gpt[1], (x, ))
    x = gpt[1](x)
    activation_checkpoint_nbytes = get_nbytes(x)
    print(f"activation checkpoint size(single version): "
        f"{activation_checkpoint_nbytes} bytes = {activation_checkpoint_nbytes/1024/1024:.2f} MiB")
    flops_tail = FlopCountAnalysis(gpt[-1], (x,))
    
    # For the exact same model, the TFLOP calculated in DeepSpeed is about 3  times higher than that calculated via fvcore.     
    # Manually Align the values
    flops_head_total = flops_head.total()*3
    flops_single_layer_total = flops_single_layer.total()*3
    flops_tail_total = flops_tail.total()*3
    print(f"flops head: {flops_head_total} = {flops_head_total/1e12:.2f} teraFLOP per sample")
    print(f"flops single layer: {flops_single_layer_total} = {flops_single_layer_total/1e12:.2f} teraFLOP per sample")
    print(f"flops tail: {flops_tail_total} = {flops_tail_total/1e12:.2f} teraFLOP per sample")
    
    total_flops_count = flops_head_total + flops_single_layer_total*config.n_layers + flops_tail_total
    forward_flops = total_flops_count 
    print(f"flops (only forward): {forward_flops} = {forward_flops/1e12:.2f} teraFLOP per sample")
    backward_flops = forward_flops*2
    recompute_flops = forward_flops
    total_flops = forward_flops + backward_flops + recompute_flops
    # if training speed is A samples/second, then TFlops = total_flops * A
    tera_flops = total_flops/1e12
    print(f"flops (Forward+Backwrd+Recompute): {total_flops} = {total_flops/1e12:.2f} teraFLOP per sample")
    global TERA_FLOPS
    TERA_FLOPS=tera_flops

from typing import List
def train_with_mpress(model:nn.Sequential,
                      balance: List[int],
                      num_chunks: int,
                      args: argparse.ArgumentParser,
                      config: GPT_Config
                      ):
    init_start = time.time()
    model = MPress(model,
                balance=balance,
                chunks=num_chunks,
                enable_swap_optimizer=args.enable_swap_optimizer,
                checkpoint_strategy=args.checkpoint_strategy,
                enable_fp16=args.fp16)
        
    
    init_end = time.time()
    model.train()
    print(f"init_time={init_end-init_start:.2f} sec")
    
    last_time = time.time()
    for i in range(args.num_iterations):
        print(f"training iteration: {i}")
        inputs, targets = generate_inputs(batchsize=args.batch_size, config=config)
        inputs = inputs.cuda(0, non_blocking=True)
        targets = targets.cuda(torch.cuda.device_count()-1, non_blocking=True)
        targets = targets.chunk(num_chunks)
        class CalcLoss():
            chunk_id = -1    
            @staticmethod
            def calc_loss(outputs):
                CalcLoss.chunk_id+=1
                return getLoss(outputs, targets[CalcLoss.chunk_id], config)
        
        model.train_for_input(inputs, calc_loss=CalcLoss.calc_loss)
        
        current_time = time.time()
        elapsed_time = current_time - last_time
        last_time = current_time
        speed = args.batch_size / elapsed_time
        import colorful as cf
        print(cf.red("training speed: {:.2f} samples/sec".format(speed)))
        if enable_print_tflop:
            print(cf.red(f"Compute speed: {speed*TERA_FLOPS:.2f} TeraFLOPS/sec"))
        if enable_print_mem_used:
            print_mem_used()
            
def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("--gpt_config_path", default="gpt_configs/gpt-demo.json", type=str, help="Path of configuration file for GPT")
    parser.add_argument("--batch_size", default=32, type=int, help="Total batch size for training a minibatch")
    parser.add_argument("--fp16", action="store_true", help="Use fp16")
    parser.add_argument("--enable_swap_optimizer", action="store_true", help="Enable swapping optimizer")
    parser.add_argument("--checkpoint_strategy", type=str, default="never",
                        help='options: ["except_last", "always", "never"]')
    parser.add_argument("--num_iterations", default=3, type=int, help="number of iterations")

    args = parser.parse_args()
    config = json_to_config(args.gpt_config_path)


    balance = [config.n_layers//torch.cuda.device_count() for _ in range(torch.cuda.device_count())]
    balance[0]+=1 # for Pre
    balance[-1] = config.n_layers + 2 - sum(balance[:-1])
    # balance[-1]-=torch.cuda.device_count()-1
    # for i in range(torch.cuda.device_count()-1):
    #     balance[i]+=1

    # devices = balance2devices(balance)
    # balance = [1 for _ in devices]
    devices = [i for i in range(torch.cuda.device_count())]

    print(f"balance={balance}")
    print(f"devices={devices}")
    print("=========================== build gpt sequential ===========================")
    gpt = build_GPTSequential(config)

    if enable_print_params:
        print_parmas(gpt, config)
    if enable_print_tflop:
        print_tflops(gpt, config)
    
    train_with_mpress(gpt, balance, args.batch_size//bs_per_chunk, args, config)
    
    zq_tracing.record_thread_name("CPU")
    zq_tracing.record_thread_sort_index(999999999)
    zq_tracing.record_dump(f"./tmp/gpt-{args.pipe_end}.json")

if __name__ == "__main__":
    main()