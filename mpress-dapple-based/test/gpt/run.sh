Z_LEVEL=fatal python launch_pipeline.py \
    --gpt_config_path=gpt_configs/gpt-demo.json \
    --batch_size 32 \
    --num_iterations 100000 \
    --checkpoint_strategy never 
    --enable_swap_optimizer \
    # --fp16 \ # upgrade pytorch version to support fp16 (GeLU Problem)
