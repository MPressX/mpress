import torch
from torch import Tensor
from torch import nn
from sequential_gpt import build_GPTSequential, GPT_Config, json_to_config
import argparse
import sys
sys.path.append("../../")
from mpress.mpress import MPress
import time

config = json_to_config("gpt_configs/gpt-max-1080Ti.json")

batchsize = 256
enable_offload_optimizer=True
checkpoint_strategy="except_last"
num_iterations=100
enable_print_params=True
enable_print_tflop=True
enable_print_mem_used=True
enable_print_analysis=True

def print_mem_used():
    def print_in_16_width_to_right(x):
        print("{:>16}".format(x), end="")
    p = print_in_16_width_to_right
    num_devices=torch.cuda.device_count()
    for i in range(num_devices):
        print(torch.cuda.memory_summary(i, abbreviated=True))
    

def get_parameters(m:nn.Module):
    total_params = sum(p.numel() for p in m.parameters())
    # total_trainable_params = sum(p.numel() for p in m.parameters() if p.requires_grad)
    return total_params

def print_num_params(n:int):
    return f"[{n}={n/1e9:.2f}B={n*4/1024/1024/1024:.2f}GiB]"

def generate_inputs(batchsize:int, config:GPT_Config):
    inputs=torch.randint(low=0, high=config.vocab_size, size=(batchsize, config.seq_len))
    # targets=torch.randint(low=0, high=config.vocab_size, size=(batchsize, config.seq_len-1))
    targets=inputs[:,1:].contiguous()
    return inputs, targets

def getLoss(lm_logits, targets):
    criterion = nn.CrossEntropyLoss(ignore_index = config.pad_id)
    lm_logits = lm_logits[:, :-1].contiguous()
    loss = criterion(lm_logits.view(-1, config.vocab_size), targets.view(-1))
    return loss

def get_nbytes(x:Tensor, enable_fp16=False):
    if type(x) == torch.Tensor:
        size = x.numel() * x.element_size()
        if enable_fp16 and x.is_floating_point():
            size//=2
        return size
    return sum([get_nbytes(v, enable_fp16) for v in list(x)])

NUM_CHUNKS=batchsize
balance = [config.n_layers//torch.cuda.device_count() for _ in range(torch.cuda.device_count())]
balance[0]+=1 # for Pre
balance[-1] = config.n_layers + 2 - sum(balance[:-1])
print(f"balance={balance}")
print("=========================== build gpt sequential ===========================")
print(f"n_layers = {config.n_layers}")
from copy import deepcopy
config2 = deepcopy(config)
config2.n_layers=1
gpt = build_GPTSequential(config2)


# print #params
if enable_print_params:
    print("=============== counting params ===============")
    num_params_head = get_parameters(gpt[0])
    num_params_tail = get_parameters(gpt[-1])
    num_params_single_layer = get_parameters(gpt[1])
    num_params_all = num_params_head+num_params_tail+num_params_single_layer*(config.n_layers)
    # assert(num_params_all==get_parameters(gpt))
    print(f"num_params_head: {print_num_params(num_params_head)}")
    print(f"num_params_tail: {print_num_params(num_params_tail)}")
    print(f"num_params_layer(single): {print_num_params(num_params_single_layer)}")
    print(f"num_params_all: {print_num_params(num_params_all)}")

# print tflop
if enable_print_tflop:
    print("=============== counting tflop & activation checkpoints ===============")
    from fvcore.nn import FlopCountAnalysis
    inputs, targets = generate_inputs(batchsize=1, config=config)
    flops_head = FlopCountAnalysis(gpt[0], (inputs,))
    x = gpt[0](inputs)
    flops_single_layer = FlopCountAnalysis(gpt[1], (x, ))
    x = gpt[1](x)
    activation_checkpoint_nbytes_fp32 = get_nbytes(x, False)
    activation_checkpoint_nbytes_fp16 = get_nbytes(x, True)
    print(f"activation checkpoint size(fp32): "
          f"{activation_checkpoint_nbytes_fp32} bytes = {activation_checkpoint_nbytes_fp32/1024/1024:.2f} MiB")
    print(f"activation checkpoint size(fp16): "
          f"{activation_checkpoint_nbytes_fp16} bytes = {activation_checkpoint_nbytes_fp16/1024/1024:.2f} MiB")
    flops_tail = FlopCountAnalysis(gpt[-1], (x,))

    # For the exact same model, the TFLOP calculated in DeepSpeed is about 3  times higher than that calculated via fvcore.     
    # Manually Align the values
    
    flops_head_total = flops_head.total()*3
    flops_single_layer_total = flops_single_layer.total()*3
    flops_tail_total = flops_tail.total()*3
    print(f"flops head: {flops_head_total} = {flops_head_total/1e12:.2f} teraFLOP per sample")
    print(f"flops single layer: {flops_single_layer_total} = {flops_single_layer_total/1e12:.2f} teraFLOP per sample")
    print(f"flops tail: {flops_tail_total} = {flops_tail_total/1e12:.2f} teraFLOP per sample")
    
    total_flops_count = flops_head_total + flops_single_layer_total*config.n_layers + flops_tail_total
    forward_flops = total_flops_count 
    print(f"flops (only forward): {forward_flops} = {forward_flops/1e12:.2f} teraFLOP per sample")
    backward_flops = forward_flops*2
    recompute_flops = forward_flops
    total_flops = forward_flops + backward_flops + recompute_flops
    # if training speed is A samples/second, then TFlops = total_flops * A
    tera_flops = total_flops/1e12
    print(f"flops (Forward+Backwrd+Recompute): {total_flops} = {total_flops/1e12:.2f} teraFLOP per sample")

if enable_print_analysis:
    print("=============== analysis computation cost and mem consumption ===============")
    head = dict()
    layer = dict()
    tail = dict()
    head['embedding']={
        "comp":0,
        "mem":config.vocab_size*config.d_model
    }
    head['pos_embedding']={
        "comp":0,
        "mem":(config.seq_len+1)*config.d_model
    }
    layer['mha.WQ'] = layer["mha.WK"] = layer["mha.WV"] = {
        "comp": config.seq_len*config.d_model*config.d_model,
        "mem": config.d_model*config.d_model
    }
    layer["mha.scaled_dot_product_attn.matmul1"] = {
        "comp": config.n_heads*config.seq_len*(config.d_model//config.n_heads)*config.seq_len,
        "mem":0
    }
    layer["mha.scaled_dot_product_attn.matmul2"] = {
        "comp": config.n_heads*config.seq_len*config.seq_len*(config.d_model//config.n_heads),
        "mem":0
    }
    layer["fnn.linear1"] = {
        "comp": config.seq_len*config.d_model*config.d_ff,
        "mem": config.d_model*config.d_ff
    }
    layer["fnn.linear2"] = {
        "comp": config.seq_len*config.d_ff*config.d_model,
        "mem": config.d_ff * config.d_model
    }
    tail['linear1'] = {
        "comp": config.seq_len*config.d_model*config.vocab_size,
        "mem": config.d_model*config.vocab_size
    }
    
    from prettytable import PrettyTable
    table = PrettyTable(["op", "comp", "mem"])
    for op in head:
        table.add_row([f"head.{op}", head[op]['comp'], head[op]['mem']])
    for op in layer:
        table.add_row([f"layer.{op}", layer[op]['comp'], layer[op]['mem']])
    for op in tail:
        table.add_row([f"tail.{op}", tail[op]['comp'], tail[op]['mem']])
        
    table.add_row(["head", sum([head[op]['comp'] for op in head]), sum([head[op]['mem'] for op in head])])
    table.add_row(["layer", sum([layer[op]['comp'] for op in layer]), sum([layer[op]['mem'] for op in layer])])
    table.add_row(["tail", sum([tail[op]['comp'] for op in tail]), sum([tail[op]['mem'] for op in tail])])
    
    
    print(table)    
