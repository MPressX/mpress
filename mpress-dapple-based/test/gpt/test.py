import torch
from torch import nn
from model import GPT

def get_parameters(m:nn.Module):
    total_params = sum(p.numel() for p in m.parameters())
    # total_trainable_params = sum(p.numel() for p in m.parameters() if p.requires_grad)
    return total_params

def print_num_params(n:int):
    return f"[{n}={n/1e9:.2f}B={n*4/1024/1024/1024:.2f}GiB]"

# n_layers=20

gpt = GPT(10000, n_layers=2).cuda().half()
batchsize = 1
max_seq_len = 512
inputs = torch.randint(low=0, high=10000, size=(batchsize, max_seq_len)).cuda()
outputs = gpt(inputs)
print(f"outputs={outputs}")
# print(f"num_params: {print_num_params(get_parameters(gpt))}")