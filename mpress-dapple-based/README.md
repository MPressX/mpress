# What is MPress
 
MPress is a framework built on top of PyTorch, and tested with PipeDream/GPipe. It aims to break the GPU memory wall of inter-operator parallel DNN training while minimizing extra cost. This folder contains DAPPLE-based MPress implementation and its corresponding test demos.


# Getting started with docker

MPress has been fully tested on AWS EC2 p3dn.24xlarge instance. For ease of use, we provide a pre-compiled docker image to get started quickly. 

`nvidia-docker` (version 2.0+) is recommended for launching docker images.

## Pull, run and exec
```bash
docker pull mpressx/mpress:latest
nvidia-docker run -itd --name=mpress_test --privileged mpressx/mpress:latest
nvidia-docker exec -it mpress_test bash
```
## Test GPT
1. enter the directory
```bash
cd /workspace/mpress/mpress-dapple-based
cd test/gpt
```

2. modify the config
```bash
vim ./run.sh
```

3. run GPT
```bash
bash ./run.sh
```
