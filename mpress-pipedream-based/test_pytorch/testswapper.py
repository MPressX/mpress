import torch
import time
from torch.swapper import wait_until_ok_by_layers
images = torch.randn(64,3,224,224,dtype=torch.float32).cuda(0)
torch.ones
for _ in range(10):
    torch.swapper.swap_out_by_targets(targets=[images])
    torch.swapper.swap_in_by_targets(targets=[images])
    torch.swapper.wait_until_ok(targets=[images], status=0)
    time.sleep(1)
