import torch

tensor1 = torch.rand(1024,1024,dtype=torch.float32,requires_grad=True).cuda(1)
tensor2 = torch.rand(1024,1024,dtype=torch.float32,requires_grad=True).cuda(1)


tensor_sum = torch.sum(tensor1 * tensor2)

tensor_sum.backward()

