import torch
import torch.nn.functional as F
import torch.nn as nn


class demo0(nn.Module):
    def __init__(self) -> None:
        super().__init__()
    def forward(self, inputx, inputy):
        output=torch.add(inputx,inputy)
        return output

class demo1(nn.Module):
    def __init__(self) -> None:
        super().__init__()
        self.layer0 = demo0()
        self.layer1 = demo0()
        self.layer2 = nn.Conv2d(2,2,(2,2))
        self.layer2.register_swap_in(0)
    def forward(self, inputx, inputy):
        out = self.layer0(inputx, inputy)
        out = self.layer1(inputx, out)
        out = self.layer2(out)
        return out

def print_parameters(module, inputs_grad, outputs_grad):
    print("gradient inputs", inputs_grad)
    print("gradient outputs", outputs_grad)


net = demo1().cuda()
# for each in net.children():
#     print(each)

hook_handler = net

x = torch.randn(2,2,2,2, requires_grad=True).cuda()
y = torch.randn(2,2,2,2, requires_grad=True).cuda()

out = net(x,y)


out = torch.sum(out)
out.backward()
