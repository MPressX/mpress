import torch


host_data = torch.ones(2,2)
cuda_data = (torch.ones(2,2)*2).cuda()


print("host_data: ",host_data)
print("cuda_data: ",cuda_data)
print("cuda memory cost: ",torch.cuda.memory_allocated()\
    ,"cuda memory cached: ",torch.cuda.memory_cached())

# torch.autograd.
torch.autograd.test_dataptr(host_data,cuda_data)
print("="*10)
print("cuda->host memory cost: ",torch.cuda.memory_allocated()\
    ,"cuda memory cached: ",torch.cuda.memory_cached())
# print("cuda data: ",cuda_data)
print("="*10)
# cuda_data_back = host_data.cuda()
cuda_data_new = (torch.ones(2,2)*3).cuda()

torch.autograd.test_dataptr(cuda_data_new,cuda_data)
print("cuda data: ",cuda_data) 
print("alloc a new cuda tensor & host->cuda memory cost: ",torch.cuda.memory_allocated()\
    ,"cuda memory cached: ",torch.cuda.memory_cached())
torch.autograd.test_dataptr(host_data,cuda_data)


cuda_data_new=None
print("="*10)
print("cuda->host memory cost: ",torch.cuda.memory_allocated()\
    ,"cuda memory cached: ",torch.cuda.memory_cached())
# print(cuda_data)
# print(host_data)
# print("host->cuda memory cost: ",torch.cuda.memory_allocated())
# print("cuda data: ", cuda_data_back)
