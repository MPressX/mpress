import os 
import shutil

def launch_task(cmd:str):
    r = os.popen(cmd)
    text = r.read()
    r.close()
    return text

def extract_bandwidth(output:str):
    '''outputs are like this:
    [CUDA Bandwidth Test] - Starting...
Running on...

 Device 0: NVIDIA GeForce GTX 1080 Ti
 Quick Mode

 Host to Device Bandwidth, 1 Device(s)
 PINNED Memory Transfers
   Transfer Size (Bytes)        Bandwidth(MB/s)
   33554432                     11734.0

 Device to Host Bandwidth, 1 Device(s)
 PINNED Memory Transfers
   Transfer Size (Bytes)        Bandwidth(MB/s)
   33554432                     12781.3

 Device to Device Bandwidth, 1 Device(s)
 PINNED Memory Transfers
   Transfer Size (Bytes)        Bandwidth(MB/s)
   33554432                     346299.8

Result = PASS
    '''
    output = output.split('\n')
    for i in range(len(output)):
        if "Host to Device Bandwidth" in output[i]:
            break
    H2D = float(output[i+3].split()[1])
    H2D /= 1024
    for i in range(len(output)):
        if "Device to Host Bandwidth" in output[i]:
            break
    D2H = float(output[i+3].split()[1])
    D2H /= 1024
    print(H2D, D2H)
    return {
        "H2D": H2D,
        "D2H": D2H
    }

def extract_p2pBandwidthLatency(output:str):
    '''output is like this:
[P2P (Peer-to-Peer) GPU Bandwidth Latency Test]
Device: 0, NVIDIA GeForce GTX 1080 Ti, pciBusID: 3, pciDeviceID: 0, pciDomainID:0
Device: 1, NVIDIA GeForce GTX 1080 Ti, pciBusID: 4, pciDeviceID: 0, pciDomainID:0
Device: 2, NVIDIA GeForce GTX 1080 Ti, pciBusID: 83, pciDeviceID: 0, pciDomainID:0
Device: 3, NVIDIA GeForce GTX 1080 Ti, pciBusID: 84, pciDeviceID: 0, pciDomainID:0
Device=0 CAN Access Peer Device=1
Device=0 CANNOT Access Peer Device=2
Device=0 CANNOT Access Peer Device=3
Device=1 CAN Access Peer Device=0
Device=1 CANNOT Access Peer Device=2
Device=1 CANNOT Access Peer Device=3
Device=2 CANNOT Access Peer Device=0
Device=2 CANNOT Access Peer Device=1
Device=2 CAN Access Peer Device=3
Device=3 CANNOT Access Peer Device=0
Device=3 CANNOT Access Peer Device=1
Device=3 CAN Access Peer Device=2

***NOTE: In case a device doesn't have P2P access to other one, it falls back to normal memcopy procedure.
So you can see lesser Bandwidth (GB/s) and unstable Latency (us) in those cases.

P2P Connectivity Matrix
     D\D     0     1     2     3
     0       1     1     0     0
     1       1     1     0     0
     2       0     0     1     1
     3       0     0     1     1
Unidirectional P2P=Disabled Bandwidth Matrix (GB/s)
   D\D     0      1      2      3 
     0 353.70  11.33  10.68  11.15 
     1  11.48 354.34  10.83  11.22 
     2  11.07  10.73 354.53  11.25 
     3  10.86  10.99  11.33 354.83 
Unidirectional P2P=Enabled Bandwidth (P2P Writes) Matrix (GB/s)
   D\D     0      1      2      3 
     0 354.06  10.26  10.92  11.12 
     1  10.25 353.98  10.89  11.20 
     2  11.05  11.05 354.87  10.25 
     3  11.08  10.93  10.26 353.67 
Bidirectional P2P=Disabled Bandwidth Matrix (GB/s)
   D\D     0      1      2      3 
     0 355.63  17.90  20.32  17.90 
     1  17.94 354.74  20.45  17.78 
     2  20.33  20.30 354.26  19.39 
     3  17.58  17.44  19.48 354.75 
Bidirectional P2P=Enabled Bandwidth Matrix (GB/s)
   D\D     0      1      2      3 
     0 354.46  19.18  20.20  17.78 
     1  19.20 354.11  20.31  17.69 
     2  20.34  20.51 354.74  19.16 
     3  17.75  17.64  19.17 353.11 
P2P=Disabled Latency Matrix (us)
   GPU     0      1      2      3 
     0   1.28  11.92  12.37  10.96 
     1  13.58   1.23  10.83  11.40 
     2  12.11  10.93   1.25  12.22 
     3  11.52  10.66  13.37   1.21 

   CPU     0      1      2      3 
     0   3.46   9.58   9.50   9.43 
     1  10.16   3.34   9.24   9.45 
     2   9.46   9.40   4.11  10.62 
     3   9.81   9.63   9.30   3.25 
P2P=Enabled Latency (P2P Writes) Matrix (us)
   GPU     0      1      2      3 
     0   1.27   1.01  11.99  12.23 
     1   1.07   1.23  10.70  11.72 
     2  10.68  10.58   1.22   0.97 
     3  10.55  11.94   0.95   1.21 

   CPU     0      1      2      3 
     0   3.82   2.55   9.44   9.47 
     1   2.75   3.67   9.69   9.51 
     2  11.72  10.08   3.26   2.49 
     3   9.72   9.53   2.60   3.56     
    '''
    output = output.split('\n')
    num_cards = 0
    for line in output:
        if line.startswith("Device:"):
            num_cards+=1
    print(num_cards)
    
    def get_matrix(title:str):
        matrix = []
        for i in range(len(output)):
            if title in output[i]:
                break
        for j in range(num_cards):
            line = output[i+2+j]
            from_j = line.split()
            from_j.pop(0)
            from_j = [float(v) for v in from_j]
            matrix.append(from_j)
        return matrix

    return {
        "uni p2p matrix": get_matrix('Unidirectional P2P=Enabled Bandwidth'),
        "bi p2p matrix": get_matrix('Bidirectional P2P=Enabled Bandwidth'),
        "connectivity matrix": get_matrix('P2P Connectivity Matrix'),
        "p2p latency": get_matrix('P2P=Enabled Latency'),
    }
    
        
            

if __name__ == '__main__':
    current_dir, _ = os.path.split(__file__)
    output_dir = os.path.join(current_dir,'output')
    if not os.path.exists(output_dir): os.makedirs(output_dir)
    bandwidthTest = os.path.join(current_dir, 'bandwidthTest/bandwidthTest')
    p2pBandwidthLatencyTest = os.path.join(current_dir, 
                                        'p2pBandwidthLatencyTest/p2pBandwidthLatencyTest')
    print(bandwidthTest, p2pBandwidthLatencyTest)
    res_b = launch_task(bandwidthTest)
    res_p = launch_task(p2pBandwidthLatencyTest)
    with open(os.path.join(output_dir,"bandwidthTest.txt"),'w') as f:
        f.write(res_b)
    with open(os.path.join(output_dir,"p2pBandwidthLatency.txt"),'w') as f:
        f.write(res_p)
    j = extract_bandwidth(res_b)
    j.update(extract_p2pBandwidthLatency(res_p))
    print(j)
    
    import json
    with open(os.path.join(output_dir, 'network_profiling.json'), 'w') as f:
        json.dump(j, f, indent=4)

    
           