# What is used for 
This is a diff patch file to show the differences between our work and PyTorch(commit id: 8554416a1). Many python files or scripts are not included for simplicity.


# Brief discription of diff files

1. `CMakeLists.txt`: for CMake
2. `*.cpp/*.h/*.hpp`: C/C++ code for implementing swap function
3. `torch/swapper/__init__.py`: provide swap APIs
4. `torch/swapper/optim/*.py`: layer wise optimizer implementation, for better overlap and lower memory occupation
5. `others`: modifications required by PipeDream