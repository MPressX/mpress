#ifndef COPYENGINE
#define COPYENGINE

#include "Common.h"


#include <torch/csrc/autograd/edge.h>
#include <torch/csrc/autograd/function.h>
#include <torch/csrc/autograd/functions/basic_ops.h>

#include <functional>
#include <thread>
#include <condition_variable>
#include <mutex>
#include <unistd.h>
#include <list>
#include <unordered_set>
#include <unordered_map>
#include <vector>
#include <time.h>




namespace torch{
namespace swapper{




struct CopyTask{
    taskType task_type_;
    c10::DataPtr* old_ptr_;
    size_t nbytes_;
    P_Reply reply_;
    CopyTask():task_type_(taskType::SHUTDOWN), old_ptr_(nullptr), nbytes_(0),
        reply_(nullptr){};
    CopyTask(taskType task_type,c10::DataPtr* old_ptr,
        size_t nbytes, P_Reply reply):
        task_type_(task_type), old_ptr_(old_ptr), 
        nbytes_(nbytes), reply_(reply){};
};

struct C10_API ReadyQueue{
    std::list<CopyTask> list_;
    std::condition_variable not_empty_;
    std::mutex mutex_;
    void push(CopyTask task);
    void push_shutdown_task();
    CopyTask pop();
    size_t left_tasks();
};

struct TORCH_API CopyEngine_
{
private:
    /* data */
    std::vector<cudaEvent_t> freed;
    std::unordered_set<cudaEvent_t> allocated;
public:
    int default_device_id; 
    std::vector<std::vector<cudaStream_t>> streams;
    std::unordered_map<int, ReadyQueue> ready_queues;
    std::unordered_map<int, std::thread> threads;
    std::vector<int> candidates;
    // std::unordered_map<void*, std::vector<c10::DataPtr>> record_tails;
    std::unordered_map<void*, 
        std::pair<std::vector<c10::DataPtr>, P_Reply> > record_tails;
    void set_default_device_id(int id){default_device_id=id;}
    void set_candidates(std::vector<int>&c){candidates=c;}
    void _init_streams(){
        AT_ASSERT(candidates.size()>0, "Candidates not inited!");
        for (int is_send = 0; is_send <= 1; is_send++){
            streams.emplace_back(std::vector<cudaStream_t>());
            auto&s = streams[is_send];
            for (auto&c:candidates){
                int i = c+1;
                while (s.size()<i+1) s.emplace_back(nullptr);
                CudaSafeCall(cudaStreamCreateWithFlags(&s[i], cudaStreamNonBlocking));
            }
        }
    }
    void destroy_streams(){
        for (int is_send = 0; is_send<=1;is_send++){
            for (auto&stream:streams[is_send]){
                if (stream){
                    // CudaSafeCall(cudaStreamDestroy(stream)); // driver shutting down
                }
            }
        }
    }
    void init_self(){
        _init_streams();
        for (int is_send = 0; is_send <= 1; is_send++) ready_queues[is_send]; // map will automatically init Queue
        start_loop();
    }
    cudaStream_t& get_stream(int is_send, int device_id){return streams[is_send][device_id+1];}
    
    void start_loop(){
        for (int is_send = 0; is_send<=1; is_send++){
            threads[is_send] = std::thread(&CopyEngine_::loop_copy_engine, this, is_send);
            threads[is_send].detach();
        }
    }
    CopyEngine_(/* args */){};

    void loop_copy_engine(int is_send);
    void evaluate_function_send(CopyTask& task);
    void evaluate_function_recv(CopyTask& task);
    void evaluate_function(CopyTask& task);
    cudaEvent_t alloc_event(){
        cudaEvent_t event;
        if (freed.size()==0){
            cudaEventCreate(&event);
            allocated.insert(event);
        }else{
            event =freed.back();
            freed.pop_back();
        }
        return event;
    }
    void free_event(cudaEvent_t event){
        if (allocated.find(event) != allocated.end()){
            allocated.erase(event);
        }
        freed.emplace_back(event);
    }
    void memcpy_sync(void* dst, void* src, size_t nbytes, cudaMemcpyKind cpy_kind,
                      cudaStream_t stream, std::function<void()>fn);
    void push_task_swap_out(c10::DataPtr new_ptr, c10::DataPtr old_ptr, size_t nbytes,
                                cudaStream_t stream);
    void terminate_loop(){
        for (int is_send = 0; is_send<=1; is_send++) ready_queues[is_send].push_shutdown_task();
    }
    ~CopyEngine_(){
        terminate_loop();
        for (auto&event:freed){
            CudaSafeCall(cudaEventDestroy(event));
        }
        freed.clear();
        destroy_streams();
    };
};

C10_API CopyEngine_* get_copy_engine();
}
}



#endif