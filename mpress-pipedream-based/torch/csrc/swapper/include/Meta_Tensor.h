#ifndef __META_TENSOR_H__
#define __META_TENSOR_H__

#include <c10/core/Allocator.h>
#include <aten/src/ATen/core/Tensor.h>
#include <string>
#include <utility>


namespace torch{
namespace swapper{
/**
 * @param  {at::Tensor}
 * @return {at::DataPtr*}: a pointer to Tensor's DataPtr. If not exists, return nullptr;
 */
C10_API at::DataPtr* get_ptr_DataPtr(at::Tensor&);
/**
 * 
 * @param  {at::Tensor}  
 * @return {size_t}: return the Tensors nbytes. If not defined, return 0.
 */
C10_API size_t get_nbytes(at::Tensor&);
typedef std::pair<at::DataPtr*, size_t> Meta_Tensor;
C10_API std::string to_string(Meta_Tensor meta_tensor);
C10_API Meta_Tensor make_meta_tensor(unsigned long ptr, unsigned long nbyte);
C10_API Meta_Tensor make_meta_tensor(void* ptr, unsigned long nbyte);
C10_API Meta_Tensor make_meta_tensor(at::Tensor&);
C10_API Meta_Tensor make_meta_tensor(); // return (nullptr, 0)
}
}


#endif