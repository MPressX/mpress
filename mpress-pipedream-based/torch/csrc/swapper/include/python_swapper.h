#ifndef __PYTHON_SWAPPER_H__
#define __PYTHON_SWAPPER_H__
#include <torch/csrc/swapper/include/Swapper.h>
#include <torch/csrc/swapper/include/MemTracker.h>
#include <torch/csrc/swapper/include/Meta_Tensor.h>
#include <torch/csrc/swapper/include/CopyEngine.h>
#include <torch/csrc/python_headers.h>
#include <torch/csrc/utils/python_strings.h>

#include <torch/csrc/DynamicTypes.h>
#include <torch/csrc/PtrWrapper.h>
#include <torch/csrc/THP.h>
#include <torch/csrc/autograd/edge.h>
#include <torch/csrc/autograd/function.h>
#include <torch/csrc/autograd/python_anomaly_mode.h>
#include <torch/csrc/autograd/python_function.h>
#include <torch/csrc/utils/auto_gil.h>
#include <torch/csrc/autograd/functions/basic_ops.h>
#include <torch/csrc/autograd/generated/Functions.h>
#include <torch/csrc/autograd/python_variable.h>

#include <c10/core/Allocator.h>

#include <string>
#include <unordered_map>

using namespace torch::swapper;
namespace torch{
namespace swapper{
    
C10_API Meta_Tensor meta_tensor_py2c(PyObject*);
C10_API PyObject* meta_tensor_c2py(Meta_Tensor);

bool THPSwapper_initModule(PyObject *module);


}
}






#endif