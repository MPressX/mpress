#ifndef __COMMON_H__
#define __COMMON_H__

#include <torch/csrc/swapper/include/utils/z_debug.h>
#include <torch/csrc/swapper/include/utils/check_cuda.h>

#include <torch/csrc/WindowsTorchApiMacro.h>
#include <ATen/Tensor.h>
#include <caffe2/core/common_gpu.h>


#include <vector>
#include <memory>
#include <numeric>
#include <unordered_map>
#include <queue>

#include <cuda.h>
#include <cuda_runtime_api.h>

namespace torch{
namespace swapper{




typedef struct TORCH_API Reply{
    std::vector<size_t> size_list;
    std::vector<c10::Device> device_list;
} Reply;
using P_Reply = std::shared_ptr<Reply>;



enum class taskType: int8_t{
  SHUTDOWN = 0,
  SWAP_OUT = 1, 
  SWAP_IN = 2
};


} // swapper
} // torch


#endif