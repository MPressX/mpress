#ifndef SWAPPER
#define SWAPPER
#include <torch/csrc/swapper/include/Meta_Tensor.h>
#include <torch/csrc/swapper/include/MemTracker.h>
#include <torch/csrc/swapper/include/CopyEngine.h>
#include <torch/csrc/swapper/include/Advisor.h>
#include <torch/csrc/swapper/include/utils/z_debug.h>

#include <torch/csrc/autograd/edge.h>
#include <torch/csrc/autograd/function.h>
#include <torch/csrc/autograd/functions/basic_ops.h>
#include <torch/csrc/autograd/generated/Functions.h>

#include <c10/core/Allocator.h>

#include <string>
#include <unordered_map>
#include <queue>
#include <set>
#include <unordered_set>
#include <utility>
#include <unistd.h>

#include <cuda.h>
#include <cuda_runtime_api.h>





using namespace torch::swapper;
namespace torch{
namespace swapper{
    
typedef std::unordered_set<void*> SET_OF_PTR;
using VarList=std::vector<torch::autograd::Variable>;
using opt_f = std::function<void(torch::autograd::Variable&)>;

C10_API void record_state_of_weight(torch::autograd::Variable& weight, torch::autograd::Variable& state);
C10_API std::vector<at::Tensor>& get_states_of_weight(torch::autograd::Variable& weight);
C10_API opt_f& get_optimize();
C10_API void set_optimize(opt_f optimize);
C10_API void search_tensors_by_range(VarList& start_vars, VarList& end_vars, std::vector<at::Tensor>& tensors_found);

C10_API int swap_out_by_meta(Meta_Tensor meta_tensor);
C10_API int swap_out_by_target(at::Tensor& tensor);
C10_API int swap_out_by_node(torch::autograd::Node* node, SET_OF_PTR& excluded_set);
C10_API int check_and_swap_out(at::Tensor& tensor, SET_OF_PTR& excluded_set);

C10_API int swap_out_by_meta(Meta_Tensor meta_tensor, std::vector<int>& destinations, std::vector<float>& ratios);
C10_API int swap_out_by_target(at::Tensor& tensor, std::vector<int>& destinations, std::vector<float>& ratios);
C10_API int swap_out_by_node(torch::autograd::Node* node, SET_OF_PTR& excluded_set, std::vector<int>& destinations, std::vector<float>& ratios);
C10_API int check_and_swap_out(at::Tensor& tensor, SET_OF_PTR& excluded_set, std::vector<int>& destinations, std::vector<float>& ratios);
C10_API int swap_in_by_meta(Meta_Tensor meta_tensor);
C10_API int swap_in_by_target(at::Tensor& tensor);
C10_API int swap_in_by_node(torch::autograd::Node* node, SET_OF_PTR& excluded_set);
C10_API int swap_in_by_node(torch::autograd::Node* node);
C10_API int check_and_swap_in(at::Tensor& tensor, SET_OF_PTR& excluded_set);
C10_API int wait_until_ok_by_target(at::Tensor& tensor, int status=0);
C10_API int wait_until_ok_by_node(torch::autograd::Node* node, int status=0);

enum class VarType{
    Variable = 1,
    SavedVariable = 2,
    Unkonwn = 0
};

class VariableContainingTensor{
public:
    void* ptr_;
    VarType type_;
    at::Tensor t_;
    bool t_inited_;
    VariableContainingTensor(){
        ptr_ = nullptr;
        type_ = VarType::Unkonwn;
    }
    VariableContainingTensor(torch::autograd::Variable& data){
        ptr_ = &data;
        type_ = VarType::Variable;
        t_inited_ = false;
    }
    VariableContainingTensor(torch::autograd::SavedVariable& data){
        ptr_ = &data;
        type_ = VarType::SavedVariable;
    }
    bool is_variable(){return type_ == VarType::Variable;};
    bool is_saved_variable(){return type_ == VarType::SavedVariable;};
    torch::autograd::Variable* get_VariablePtr(){
        return (torch::autograd::Variable*) ptr_;
    }
    torch::autograd::SavedVariable* get_SavedVariablePtr(){
        return (torch::autograd::SavedVariable*) ptr_;
    }
    at::Tensor& get_tensor(){
        switch (type_){
        case VarType::Variable:
            {
            if (!t_inited_){
                t_ = get_VariablePtr()->tensor_data();
                t_inited_ = true;
            }
            return t_;
            }
            break;
        case VarType::SavedVariable:
            return get_SavedVariablePtr()->get_data();
            break;
        default:
            AT_ERROR("unexpected VarType");
        }
    }
};


C10_API std::vector<VariableContainingTensor> get_Vars(torch::autograd::Node* node);

struct TORCH_API Swapper
{
private:
    std::string name;
    int default_device_id;
public:
    MemTracker* mem_tracker;
    CopyEngine_* copy_engine;
    Advisor* advisor;
    unsigned long swap_out_bytes;
    

    void set_default_device_id(int id){default_device_id=id;};
    int get_default_device_id(){return default_device_id;};
    Swapper(std::string name_):name(name_),default_device_id(0){
        mem_tracker = get_mem_tracker();
        copy_engine = get_copy_engine();
        advisor = get_advisor();
        swap_out_bytes=0;
    };
    ~Swapper(){
        mem_tracker->free_cpu_cached_list();
    };
    std::string get_name();
    
    std::unordered_map<void*, unsigned int> ptr2devices; // 0~3: CPU/GPU/xxx/...;  4~8: index

    int is_swapped(void* ptr){
        return ptr2devices.find(ptr)!=ptr2devices.end();
    }
    void set_swap_out_bytes(unsigned long bytes){
        swap_out_bytes = bytes;
    }
    unsigned long get_swap_out_bytes(){
        return swap_out_bytes;
    }
    unsigned long add_swap_out_bytes(unsigned long bytes){
        swap_out_bytes += bytes;
        return swap_out_bytes;
    }
};

C10_API Swapper* get_swapper();

}
}






#endif