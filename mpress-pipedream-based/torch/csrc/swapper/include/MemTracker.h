#ifndef MEMTRACKER
#define MEMTRACKER

#define ENABLE_PINNED_MEM

#ifndef ENABLE_PINNED_MEM
#define DISABLE_PINNED_MEM

#endif
#include <torch/csrc/WindowsTorchApiMacro.h>
#include <c10/core/Allocator.h>
#include <c10/cuda/CUDACachingAllocator.h>
#include <caffe2/core/common_gpu.h>
#include <caffe2/core/context_gpu.h>

#include <iomanip>
#include <iostream> 
#include <unordered_map>
#include <vector>
#include <mutex>

// prevent overflow
#define GiB *1024ull*1024*1024
#define MiB *1024ull*1024
#define KiB *1024ull
#define REGISTER_CPU_ALLOCATOR \
        set_cpu_allocator(c10::GetAllocator(c10::DeviceType::CPU))
#define REGISTER_CUDA_ALLOCATOR \
        set_cuda_allocator(c10::cuda::CUDACachingAllocator::get())
namespace torch{
namespace swapper{

typedef struct Block{
    size_t size;
    c10::DataPtr data;
} Block;
using BlockList = std::vector<Block>;
using Ptr2Block = std::unordered_map<void*, Block>;
using Size2BlockList = std::unordered_map<size_t, BlockList>;


typedef struct CPUMemInfo{
    // Bytes
    size_t allocated_mem;
    size_t cached_mem;
    size_t allocated_block_num;
    size_t cached_block_num;
    std::string to_string();
} CPUMemInfo;
typedef struct CUDAMemInfo{
    int device_id;
    size_t currentMemoryAllocated;
    size_t maxMemoryAllocated;
    size_t currentMemoryCached;
    size_t maxMemoryCached;
    std::string to_string();
} CUDAMemInfo;

C10_API std::ostream& operator << (std::ostream& out, CPUMemInfo& info);
C10_API std::ostream& operator << (std::ostream& out, CUDAMemInfo& info);




struct TORCH_API MemTracker
{
private:
    /* data */
    at::Allocator* cpu_allocator=nullptr;
    at::Allocator* cuda_allocator=nullptr;
    Ptr2Block cpu_allocated_list;
    Size2BlockList cpu_cached_list;
    Ptr2Block cuda_allocated_list;
    std::mutex& mutex(bool is_local);
    CPUMemInfo cpu_mem_info;
    CUDAMemInfo cuda_mem_info;
    // cuda_memory_threshold_:
    // -1: disable the check
    // >0: check if the memory is enough
    size_t cuda_memory_threshold_;
    size_t default_device_id_;
    // static std::mutex l;
public:
    MemTracker(/* args */){
        if(!cpu_allocator || !cuda_allocator){
            REGISTER_CPU_ALLOCATOR;
            REGISTER_CUDA_ALLOCATOR;
        }
        cpu_mem_info = {0,0,0,0};
        cuda_memory_threshold_ = 0 GiB;
        default_device_id_ = 0;
    };
    ~MemTracker(){};
    // the following api is just used to initialize the mem tracker
    void set_cpu_allocator(c10::Allocator* allocator);
    void set_cuda_allocator(c10::Allocator* allocator);
    
    void close_memory_threshold(){
        c10::cuda::CUDACachingAllocator::set_memory_threshold(0, default_device_id_);
    }
    void restart_memory_threshold(){
        c10::cuda::CUDACachingAllocator::set_memory_threshold(cuda_memory_threshold_, default_device_id_);
    }
    void set_runtime_args(size_t memory_threshold, size_t default_device_id){
        default_device_id_ = default_device_id;
        cuda_memory_threshold_ = memory_threshold;
        c10::cuda::CUDACachingAllocator::set_memory_threshold(cuda_memory_threshold_, default_device_id_);
    }
    // the following api can be used to allocate or free memory in a row way
    void* allocate(size_t nbytes, c10::Device device);
    void free_cpu(void* ptr);
    void free_gpu(void* ptr);
    const CPUMemInfo& get_cpu_mem_info();
    const CUDAMemInfo& get_cuda_mem_info(int device_index);
    void free_cpu_cached_list();

    // the following api is just used for unit testing
    size_t get_cpu_allocated_list_length();
    size_t get_cpu_cached_list_length();
    size_t get_cuda_allocated_list_length();
};


// export to 
C10_API MemTracker* get_mem_tracker();
C10_API c10::DataPtr allocate(size_t nbytes, c10::Device device);
}
}


#endif