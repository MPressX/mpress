#ifndef __Z_DEBUG_H__
#define __Z_DEBUG_H__

#include <stdio.h>
#include <vector>
#include <string>
#include <unordered_set>
#include <sstream>
#include <torch/csrc/swapper/include/utils/check_cuda.h>
#include <iostream>
#include <cuda_runtime_api.h>
#include <thread>

#define DISABLE_PRINT_COMPILE_TIME 0

#define Z_LEVEL_DEBUG 0
#define Z_LEVEL_INFO 1
#define Z_LEVEL_WARN 2
#define Z_LEVEL_ERROR 3
#define Z_LEVEL_FATAL 4

template<typename T>
std::string get_pid(){
    auto tid = std::this_thread::get_id();
    std::ostringstream os;
    os << tid;
    return os.str();
}


class Z_ENV{
public:
    int Z_LEVEL;
    int Z_COLOR;
    std::string prefix;
    void set_prefix(std::string p_){prefix = p_;}
    Z_ENV(){
        Z_LEVEL=Z_LEVEL_DEBUG;
        Z_COLOR=0;
        prefix="";
        std::string s = getenv("Z_LEVEL")?getenv("Z_LEVEL"):"";
        for(auto&v:s)v=tolower(v);
        if (s.find("debug")!=std::string::npos){
            Z_LEVEL=Z_LEVEL_DEBUG;
        }
        else if (s.find("info")!=std::string::npos){
            Z_LEVEL=Z_LEVEL_INFO;
        }
        else if (s.find("warn")!=std::string::npos){
            Z_LEVEL=Z_LEVEL_WARN;
        }
        else if (s.find("error")!=std::string::npos){
            Z_LEVEL=Z_LEVEL_ERROR;
        }
        else if (s.find("fatal")!=std::string::npos){
            Z_LEVEL=Z_LEVEL_FATAL;
        }
        else{
        }
        if (getenv("Z_COLOR")){
            Z_COLOR=1;
        }
    };
    ~Z_ENV(){};
};

static auto z_env = Z_ENV();

#if (DISABLE_PRINT_COMPILE_TIME == 0)
#define Z_DEBUG(format,...) \
    if (z_env.Z_LEVEL <= Z_LEVEL_DEBUG) Z_PALE("[DEBUG] " format, ##__VA_ARGS__) else {};
#define Z_INFO(format,...) \
    if (z_env.Z_LEVEL <= Z_LEVEL_INFO) Z_WHITE("[INFO ] " format, ##__VA_ARGS__) else {};
#define Z_WARN(format,...) \
    if (z_env.Z_LEVEL <= Z_LEVEL_WARN) Z_YELLOW("[WARN ] " format, ##__VA_ARGS__) else {};
#define Z_ERROR(format,...) \
    if (z_env.Z_LEVEL <= Z_LEVEL_ERROR) Z_RED("[ERROR] " format, ##__VA_ARGS__) else {};
#define Z_FATAl(format,...) \
    if (z_env.Z_LEVEL <= Z_LEVEL_FATAL) Z_RED("[FATAL] " format, ##__VA_ARGS__) else {};
#define Z_TMP(format,...) \
    Z_RED("[%s] " format, z_env.prefix.c_str(), ##__VA_ARGS__);
#else
#define Z_DEBUG(format, ...) 
#define Z_INFO(format, ...) 
#define Z_WARN(format,...) 
#define Z_ERROR(format,...) 
#define Z_FATAl(format,...) 
#define Z_TMP(format,...)
#endif

#define Z_RED(format,...) \
    if (z_env.Z_COLOR) printf("[TID:%s]" __FILE__ ":%d: \033[31;1m" format "\033[0m", get_pid<int>().c_str(), __LINE__, ##__VA_ARGS__); \
    else printf("[TID:%s]" __FILE__ ":%d: " format, get_pid<int>().c_str(), __LINE__, ##__VA_ARGS__);
#define Z_GREEN(format,...) \
    if (z_env.Z_COLOR) printf("[TID:%s]" __FILE__ ":%d: \033[32;1m" format "\033[0m", get_pid<int>().c_str(), __LINE__, ##__VA_ARGS__); \
    else printf("[TID:%s]" __FILE__ ":%d: " format, get_pid<int>().c_str(), __LINE__, ##__VA_ARGS__);
#define Z_YELLOW(format,...) \
    if (z_env.Z_COLOR) printf("[TID:%s]" __FILE__ ":%d: \033[33;1m" format "\033[0m", get_pid<int>().c_str(), __LINE__, ##__VA_ARGS__); \
    else printf("[TID:%s]" __FILE__ ":%d: " format, get_pid<int>().c_str(), __LINE__, ##__VA_ARGS__);
#define Z_BLUE(format,...) \
    if (z_env.Z_COLOR) printf("[TID:%s]" __FILE__ ":%d: \033[34;1m" format "\033[0m", get_pid<int>().c_str(), __LINE__, ##__VA_ARGS__); \
    else printf("[TID:%s]" __FILE__ ":%d: " format, get_pid<int>().c_str(), __LINE__, ##__VA_ARGS__);
#define Z_PURPLE(format,...) \
    if (z_env.Z_COLOR) printf("[TID:%s]" __FILE__ ":%d: \033[35;1m" format "\033[0m", get_pid<int>().c_str(), __LINE__, ##__VA_ARGS__); \
    else printf("[TID:%s]" __FILE__ ":%d: " format, get_pid<int>().c_str(), __LINE__, ##__VA_ARGS__);
#define Z_PALE(format,...) \
    if (z_env.Z_COLOR) printf("[TID:%s]" __FILE__ ":%d: \033[37;2m" format "\033[0m", get_pid<int>().c_str(), __LINE__, ##__VA_ARGS__); \
    else printf("[TID:%s]" __FILE__ ":%d: " format, get_pid<int>().c_str(), __LINE__, ##__VA_ARGS__);
#define Z_WHITE(format,...) \
    if (z_env.Z_COLOR) printf("[TID:%s]" __FILE__ ":%d: \033[37;1m" format "\033[0m", get_pid<int>().c_str(), __LINE__, ##__VA_ARGS__); \
    else printf("[TID:%s]" __FILE__ ":%d: " format, get_pid<int>().c_str(), __LINE__, ##__VA_ARGS__);



template<typename T>
std::string set2string(std::unordered_set<T> a){
    if (a.empty()) return "";
    std::ostringstream os;
    for (auto it=a.begin(); it!=a.end(); it++){
        os << *it << "\t";
    }
    auto ans = os.str();
    return ans.substr(0, ans.size()-1);
}
template<typename T>
std::string vector2string(std::vector<T> a, char sep='\t'){
    if (a.empty()) return "";
    std::ostringstream os;
    for (auto it=a.begin(); it!=a.end(); it++){
        os << *it << sep;
    }
    auto ans = os.str();
    return ans.substr(0, ans.size()-1);
}


template <typename T>
std::string print_data_in_gpu(void* ptr, int cnt, char sep='\t'){
    if (cnt==0) return "";
    void* host;
    host = malloc(sizeof(T)*cnt);
    cudaStream_t stream;
    CudaSafeCall(cudaStreamCreateWithFlags(&stream, cudaStreamNonBlocking));
    CudaSafeCall(cudaMemcpyAsync(host, ptr, sizeof(T)*cnt, cudaMemcpyDeviceToHost, stream));
    CudaSafeCall(cudaStreamSynchronize(stream));
    CudaSafeCall(cudaStreamDestroy(stream));
    T* t_p = (T*)host;
    std::ostringstream os;
    for (int i = 0; i < cnt; i++){
      os << t_p[i] << sep;
    }
    free(host);
    auto ans = os.str();
    return ans.substr(0, ans.size()-1);
}

#endif