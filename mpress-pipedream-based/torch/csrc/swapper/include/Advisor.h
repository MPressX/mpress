#ifndef __ADVISOR_H__
#define __ADVISOR_H__

#include "MemTracker.h"
#include "Common.h"







namespace torch{
namespace swapper{
using VoidPtrList=std::vector<void*>;
using DataPtrList=std::vector<c10::DataPtr>;

enum class TORCH_API SWAP_MODE {
    FIXED=1, // only support swapping between default device and candidate device!
    AUTO=2, // make strategy in runtime
    STAY=3, // do not swap
    DEFAULT=0 // same as STAY
};


typedef struct Record{
    size_t index;
    size_t nbytes;
    float priority;
    bool operator <(const Record& b) const{
        return priority < b.priority;
    }
} Record;

typedef struct DeviceStatus{
    int32_t index;
    unsigned long long free_memory;
    float bandwidth;
    // bool operator <(const DeviceStatus& b) const{
    //     return bandwidth < b.bandwidth;
    // }
} DeviceStatus;

class TORCH_API Advisor
{
private:
    /* data */

    SWAP_MODE mode_;
    int default_device_id_;
    int num_cards;

    float alpha = 1e-4;
    float beta = 1e2;
    int current_iteration=-1;
    int sample_point=0;
    size_t current_index = 0;
    size_t current_index_v2 = 0;
    bool enable_hybrid_swap=false;
    
    // std::unordered_map<void*, P_Reply> reply_recorder;
    std::vector<std::vector<int>> connectivity_matrix;
    std::vector<std::vector<float>> speed_matrix;
    std::vector<int> candidates; // devices to send tensors
    std::priority_queue<Record> priority_queue;
    std::map<size_t, timespec> record_table;
    std::map<void*, size_t> ptr_idx_table;
    std::map<size_t, P_Reply> reply_table;

    std::vector<DeviceStatus> spare_device_list;

    P_Reply swap_by_fixed(size_t nbytes);
    P_Reply swap_by_auto(at::DataPtr& dptr, size_t nbytes, taskType task_type);
    P_Reply swap_by_auto_v2(size_t nbytes, taskType task_type);
    P_Reply swap_by_default();
    float heuristic_priority(int size, float interval);
    float random_priority();
    std::vector<DeviceStatus> get_device_status();
    bool hybrid_swap(
                    size_t tensor_index, 
                    size_t nbytes, 
                    std::vector<DeviceStatus>& device_status_list);

public:
    float get_link_speed(int src_id, int dst_id){return speed_matrix[src_id+1][dst_id+1];}
    float total_candidates_speed;
    Advisor():mode_(SWAP_MODE::DEFAULT){
        default_device_id_ = 0;
    };
    ~Advisor(){};
    void update_iteration(){current_iteration++;current_index=0;}
    void reset_idx(){
        Z_DEBUG("Reset advisor index\n");
        current_index_v2=0;

    }
    void set_sample_point(int sample_point_){sample_point = sample_point_;}
    void set_default_device_id(int default_device_id){default_device_id_ = default_device_id;}
    void set_connectivity_matrix(std::vector<std::vector<int>>& m){connectivity_matrix=m;}
    void set_speed_matrix(std::vector<std::vector<float>>& m){speed_matrix=m;}
    void set_candidates(std::vector<int>&c){candidates=c;}
    void set_num_cards(int n){num_cards=n;}
    void set_swap_mode(SWAP_MODE m){mode_=m;};
    void init_advisor_after_send_settings(){
        total_candidates_speed = 0;
        for (auto&candidate:candidates){
            total_candidates_speed += get_link_speed(default_device_id_, candidate);
        }
    }
    void open_hybrid_swap(){
        Z_DEBUG("Enable hybrid swap\n");
        enable_hybrid_swap=true;

    }

    // user only need to set gpu spare space through set_device_status
    // device2size = {
        // device_index : useful spare memory size
    // }
    void set_device_status(std::unordered_map<int, unsigned long long> device2size);

    void print_settings();
    // void detect_device_link();
    P_Reply how_to_swap(at::DataPtr& dptr, size_t nbytes, taskType task_type);


    // P_Reply how_to_swap_in(at::DataPtr& dptr);
    // P_Reply how_to_swap_out(at::DataPtr& dptr, size_t nbytes);
    // size_t get_recorder_size(){
    //     return reply_recorder.size();
    // }
};

C10_API P_Reply generate_reply(size_t nbytes, std::vector<int>& destinations, std::vector<float>& ratios);

C10_API Advisor* get_advisor();
    
}   // namespace swapper
}   // namespace torch






#endif