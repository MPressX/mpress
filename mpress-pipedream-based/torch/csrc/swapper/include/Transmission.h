#ifndef __TRANSMISSION__H__
#define __TRANSMISSION__H__

// #include <torch/csrc/WindowsTorchApiMacro.h>

#include <cuda.h>
#include <cuda_runtime_api.h>

#include <cstdint>
#include <utility>
#include <vector>
#include <algorithm>
#include <cmath>
#include <numeric>
#include <iostream>
#include <assert.h> 
#include <c10/macros/Export.h>

namespace torch{
namespace swapper{

using PtrList = std::vector<void*>;
using IntList = std::vector<int>;
using StreamList = std::vector<cudaStream_t>;
using SizeList = std::vector<size_t>;

namespace transmission{
    C10_API void init(int num_cards_);
    C10_API int is_connected(int i, int j);
    C10_API int enable_p2p(int i, int j);
}

// D H & P2P(1->1) hostID=-1
C10_API void GeneralMemcpyAsync(
    void* dst,
    int dst_id,
    void* src,
    int src_id,
    cudaStream_t stream,
    size_t nbytes
);

// D D (1->n)
C10_API void GeneralMemcpyAsync(
    PtrList dsts,
    IntList dst_ids,
    void* src,
    int src_id,
    SizeList dst_nbytes,
    StreamList streams,
    size_t nbytes
);

// D D (n->1)
C10_API void GeneralMemcpyAsync(
    void* dst,
    int dst_id,
    PtrList srcs,
    IntList src_ids,
    SizeList src_nbytes,
    StreamList streams,
    size_t nbytes
);
}
}
#endif