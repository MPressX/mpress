#include <torch/csrc/swapper/include/Swapper.h>
#include <torch/csrc/swapper/include/Meta_Tensor.h>
#include <torch/csrc/swapper/include/Advisor.h>
#include <torch/csrc/swapper/include/hash_string.h>
#include <torch/csrc/autograd/functions/accumulate_grad.h>
#include <torch/csrc/autograd/functions/tensor.h>

using namespace torch::swapper;

namespace torch{
namespace swapper{

auto empty_int_vector = std::vector<int>(0);
auto empty_float_vector = std::vector<float>(0);

static std::unordered_map<void*, std::vector<at::Tensor>> weight2states;
static std::vector<at::Tensor> empty_states(0);
static opt_f optimize=[](torch::autograd::Variable&varialbe){};

void record_state_of_weight(
    torch::autograd::Variable& weight,
    torch::autograd::Variable& state){
    auto id = weight.get_id();
    void* key = (void*)id;
    if (weight2states.find(key) == weight2states.end()){
        weight2states[key] = std::vector<at::Tensor>(0);
    }
    auto&states = weight2states[key];
    states.emplace_back(state.tensor_data());
}
std::vector<at::Tensor>& get_states_of_weight(torch::autograd::Variable& weight){
    auto id = weight.get_id();
    void* key = (void*) id;
    auto it = weight2states.find(key);
    if (it==weight2states.end()){
        return empty_states;
    }
    else{
        return it->second;
    }
}

C10_API opt_f& get_optimize(){
    return optimize;
};
C10_API void set_optimize(opt_f opt){
    optimize=opt;
};

static Swapper swapper = Swapper("haiqwa");

std::string Swapper::get_name(){
    return name;
}

std::vector<VariableContainingTensor> get_Vars(torch::autograd::Node* node){
    using namespace torch::autograd::generated;
    using namespace torch::swapper::hash_string;
    std::vector<VariableContainingTensor> ans;
    auto name = node->name();
    switch (hash_run_time(name.c_str())){
    case "PowBackward0"_hash:
        ans.push_back({((PowBackward0*)node)->self_});
        break;
    case "MaxPool2DWithIndicesBackward"_hash:
        {
            ans.push_back({((MaxPool2DWithIndicesBackward*)node)->self_});
            ans.push_back({((MaxPool2DWithIndicesBackward*)node)->result1_});
            auto& self = ((MaxPool2DWithIndicesBackward*)node)->self_;
            auto& result = ((MaxPool2DWithIndicesBackward*)node)->result1_;
            if(self.get_data().is_contiguous()){
                Z_DEBUG("MaxPool2D self is contiguous \n");
            }
            else{
                Z_DEBUG("MaxPool2D self is not contiguous \n");
            }
            if(result.get_data().is_contiguous()){
                Z_DEBUG("MaxPool2D result is contiguous \n");
            }
            else{
                Z_DEBUG("MaxPool2D result is not contiguous \n");
            }
            break;
        }
    case "ThnnConv2DBackward"_hash:
        ans.push_back({((ThnnConv2DBackward*)node)->self_});
        ans.push_back({((ThnnConv2DBackward*)node)->finput_});
        ans.push_back({((ThnnConv2DBackward*)node)->fgrad_input_});
        ans.push_back({((ThnnConv2DBackward*)node)->weight_});
        break;
    case "NllLossBackward"_hash:
        // To Check: 会触发Segmentation Fault，不清楚为什么
        break; //error....
        ans.push_back({((NllLossBackward*)node)->self_});
        ans.push_back({((NllLossBackward*)node)->target_});
        ans.push_back({((NllLossBackward*)node)->weight_});
        ans.push_back({((NllLossBackward*)node)->total_weight_});
        break;
    case "LogSoftmaxBackward"_hash:
        ans.push_back({((LogSoftmaxBackward*)node)->self_});
        ans.push_back({((LogSoftmaxBackward*)node)->result_});
        break;
    case "AddmmBackward"_hash:
        ans.push_back({((AddmmBackward*)node)->mat1_});
        ans.push_back({((AddmmBackward*)node)->mat2_});
        break;
    case "torch::autograd::AccumulateGrad"_hash:
        ans.push_back({((torch::autograd::AccumulateGrad*)node)->variable});
        break;
    case "ReluBackward1"_hash:
        // in pytorch1.2, it is the backward op for nn.ReLU(inplace=True), the result_ is alias of input/output.
        // ans.push_back({((ReluBackward1*)node)->result_});
        break;
    case "ReluBackward0"_hash:
        ans.push_back({((ReluBackward0*)node)->self_});
        break;
    case "FusedDropoutBackward"_hash:
        ans.push_back({((FusedDropoutBackward*)node)->result1_});
        break;
    case "CudnnConvolutionBackward"_hash:
        ans.push_back({((CudnnConvolutionBackward*)node)->self_});
        ans.push_back({((CudnnConvolutionBackward*)node)->weight_});
        break;
    case "MulBackward0"_hash:
        ans.push_back({((MulBackward0*)node)->self_});
        ans.push_back({((MulBackward0*)node)->other_});
        break;
    case "DivBackward0"_hash:
        ans.push_back({((DivBackward0*)node)->self_});
        ans.push_back({((DivBackward0*)node)->other_});
        break;
    case "EmbeddingBackward"_hash:
        ans.push_back({((EmbeddingBackward*)node)->indices_});
        break;
    case "CudnnBatchNormBackward"_hash:
        ans.push_back({((CudnnBatchNormBackward*)node)->input_});
        ans.push_back({((CudnnBatchNormBackward*)node)->weight_});
        ans.push_back({((CudnnBatchNormBackward*)node)->running_mean_});
        ans.push_back({((CudnnBatchNormBackward*)node)->running_var_});
        ans.push_back({((CudnnBatchNormBackward*)node)->result1_});
        ans.push_back({((CudnnBatchNormBackward*)node)->result2_});
        break;
    case "CudnnRnnBackward"_hash:
        ans.push_back({((CudnnRnnBackward*)node)->input_});
        ans.push_back({((CudnnRnnBackward*)node)->hx_});
        ans.push_back({((CudnnRnnBackward*)node)->cx_});
        // ans.push_back({((CudnnRnnBackward*)node)->dropout_state_}); // in gnmt bug, but not bert.
        ans.push_back({((CudnnRnnBackward*)node)->result0_});
        ans.push_back({((CudnnRnnBackward*)node)->result3_});
        ans.push_back({((CudnnRnnBackward*)node)->result4_});
        for (auto&saved_variable:((CudnnRnnBackward*)node)->weight_){
            ans.push_back({saved_variable});
        }
        break;
    case "MmBackward"_hash:
        ans.push_back({((MmBackward*)node)->self_});
        ans.push_back({((MmBackward*)node)->mat2_});
        break;
    case "NormBackward0"_hash:
        ans.push_back({((NormBackward0*)node)->self_});
        ans.push_back({((NormBackward0*)node)->result_});
        break;
    case "GatherBackward"_hash:
        // ans.push_back({((GatherBackward*)node)->self_}); # segmentation fault error in Bert 
        // ans.push_back({((GatherBackward*)node)->index_}); # segmentation fault error in Bert 
        break;
    case "IndexBackward"_hash:
        ans.push_back({((IndexBackward*)node)->self_});
        for (auto&saved_variable:((IndexBackward*)node)->indices_){
            ans.push_back({saved_variable});
        }
        break;
    case "TanhBackward"_hash:
        ans.push_back({((TanhBackward*)node)->result_});
        break;
    case "Revert_varlenBackward"_hash:
        // it is compiled in PipeDream's provided code.
        // Can not find this op in source code, but it appears when training GNMT in PipeDream
        break;
    case "BmmBackward"_hash:
        ans.push_back({((BmmBackward*)node)->self_});
        ans.push_back({((BmmBackward*)node)->mat2_});
        break;
    case "SoftmaxBackward"_hash:
        ans.push_back({((SoftmaxBackward*)node)->self_});
        ans.push_back({((SoftmaxBackward*)node)->result_});
        break;
    case "SqrtBackward"_hash:
        ans.push_back({((SqrtBackward*)node)->result_});
        break;
    case "ErfBackward"_hash:
        ans.push_back({((ErfBackward*)node)->self_});
        break;
    case "SplitBackward"_hash:
        ans.push_back({((SplitBackward*)node)->self_});
        break;
    case "BroadcastBackward"_hash:
        // source code not found BroadcastBackward, but it appears when running bert
        break;
    case "SigmoidBackward"_hash:
        ans.push_back({((SigmoidBackward*)node)->result_});
        break;
    case "StackBackward"_hash:
        ((StackBackward*)node);
    case "UnbindBackward"_hash:
        ((UnbindBackward*)node);
        break;
    case "MeanBackward0"_hash:
        ((MeanBackward0*)node);
        break;
    case "PermuteBackward"_hash:
        ((PermuteBackward*)node);
        break;
    case "SubBackward0"_hash:
        ((SubBackward0*)node);
        break;
    case "ViewBackward"_hash:
        ((ViewBackward*)node);
        break;
    case "NegBackward"_hash:
        ((NegBackward*)node);
        break;
    case "AsStridedBackward"_hash:
        ((AsStridedBackward*)node);
        break;
    case "AliasBackward"_hash:
        ((AliasBackward*)node);
        break;
    case "torch::autograd::CopyBackwards"_hash:
        ((torch::autograd::CopyBackwards*)node);
        break;
    case "TransposeBackward0"_hash:
        ((TransposeBackward0*)node);
        break;
    case "SqueezeBackward1"_hash:
        ((SqueezeBackward1*)node);
        break;
    case "TBackward"_hash:
        ((TBackward*)node);
        break;
    case "CloneBackward"_hash:
        ((CloneBackward*)node);
        break;
    case "UnsqueezeBackward0"_hash:
        ((UnsqueezeBackward0*)node);
        break;
    case "MeanBackward1"_hash:
        ((MeanBackward1*)node);
        break;
    case "UnsafeViewBackward"_hash:
        ((UnsafeViewBackward*)node);
        break;
    case "ExpandBackward"_hash:
        ((ExpandBackward*)node);
        break;
    case "CatBackward"_hash:
        ((CatBackward*)node);
        break;
    case "AddBackward0"_hash:
        ((AddBackward0*)node);
        break;
    case "torch::autograd::GraphRoot"_hash:
        ((torch::autograd::GraphRoot*)node);
        break;
    case "SumBackward0"_hash:
        ((SumBackward0*)node);
        break;
    case "CheckpointFunctionBackward"_hash:
        break;
    default:
        Z_WARN("Swapper cannot recognize grad function: %s ; Please check if it can be ignored\n", name.c_str());
        break;
    }
    return std::move(ans);
}


void search_tensors_by_range(
                        VarList& start_vars, 
                        VarList& end_vars, 
                        std::vector<at::Tensor>& tensors_found){
    /*
        use BFS algorithm scan through all tensors from start_vars to end_vars; However, 
        after taking the torch autograd feature into consideration, BFS algorithm must 
        start from end_vars!
    
    */
    Z_DEBUG("Enter search_tensors_by_range\n");
    std::queue<std::shared_ptr<torch::autograd::Node>> wait_to_visit;
    std::set<std::shared_ptr<torch::autograd::Node>> visited_nodes;
    for(auto& var : end_vars){
        wait_to_visit.push(std::move(var.grad_fn()));
    }
    
    while(!wait_to_visit.empty()){
        // pop first grad_fn from the queue
        auto cur_grad_fn = wait_to_visit.front();
        wait_to_visit.pop();
        // check if grad_fn valid
        if((!cur_grad_fn) || 
            (visited_nodes.find(cur_grad_fn)!=visited_nodes.end())) continue;
        visited_nodes.insert(cur_grad_fn);
        auto cur_vars = get_Vars(cur_grad_fn.get());
        Z_DEBUG("cur_grad_fn = %s and find saved variables = %d\n",cur_grad_fn->name().c_str(), cur_vars.size());

        for(auto& var : cur_vars){
            // check if the tensor has been found
            auto tensor = var.get_tensor();
            if (!tensor.defined()){
                continue;
            }
            bool found = false;
            for(auto& tensor_found : tensors_found){
                auto meta = make_meta_tensor(tensor);
                // Z_DEBUG("dptr: %p\n", meta.first);
                if(tensor_found.is_alias_of(tensor)){
                    found = true;
                    break;
                }
            }
            if(!found){
                // push the tensor into tensors_found, if the tensor has not been found 
                tensors_found.push_back(std::move(tensor));

            }
        } 
        // push next grad_fn into wait_to_list
        // only if the next grad_fn is not the start_vars' grad_fn

        auto next_edges = cur_grad_fn->next_edges();
        Z_DEBUG("grad function [%s] has [%d] edges\n", cur_grad_fn->name().c_str(), next_edges.size());
        // for(auto& next_edge : next_edges){
        for (int i = 0lu; i < next_edges.size(); i++){
            // Z_DEBUG("i=%d\n", i);
            auto &next_edge = next_edges[i];
            // Z_DEBUG("next_edge=%p\n", &next_edge);
            // Z_DEBUG("next_edge.function=%p\n", next_edge.function.get());
            if (!next_edge.function.get()){ // no function
                continue;
            }
            auto is_start_grad_fn = false;
            for(auto& start_var : start_vars){
                if(next_edge.function.get() == start_var.grad_fn().get()){
                    Z_DEBUG("Next grad function [%s] is the same as start var\n", next_edge.function->name().c_str());
                    is_start_grad_fn = true;
                    break;
                }
            }
            // if the next edge is start variable's grad_fn, no need to visit it
            if(is_start_grad_fn) continue;
            
            // push directly, no check, because it will be checked when poped
            Z_DEBUG("Push grad function [%s] to `wait_to_visit`\n", next_edge.function->name().c_str());
            wait_to_visit.push(std::move(next_edge.function));
        }

    }
}
int swap_out_by_meta(Meta_Tensor meta_tensor){
    return swap_out_by_meta(meta_tensor, empty_int_vector, empty_float_vector);
}

int swap_out_by_meta(Meta_Tensor meta_tensor, std::vector<int>& destinations, std::vector<float>& ratios){
    /*
        ret:    0: 成功swap
                1: 已经swap过了，直接返回

    */
    // should check tensor on local GPU
    auto nbytes = meta_tensor.second;
    auto& old_ptr = *((at::DataPtr*)meta_tensor.first);
    Z_DEBUG("swap_out: check the necessity to swap ptr [%p][deviceType=%d][deviceIndex=%d][status=%d]\n",  old_ptr.get(),
        old_ptr.device().type(), old_ptr.device().index(), old_ptr.get_status());
    auto current_status = old_ptr.get_status();
    switch (current_status) {
        case c10::DataStatus::SWAPPED:
        case c10::DataStatus::SWAPPING_OUT:
            return 1;
            break;
        case c10::DataStatus::OK:
            break;
        case c10::DataStatus::SWAPPING_IN:
            old_ptr.wait_status_until(c10::DataStatus::OK);
            break;
        default:
            AT_ASSERT(false, "Unexpected DataStatus ", (int)current_status);
    };
    auto reply = destinations.size()==0?
        get_advisor()->how_to_swap(old_ptr, nbytes, taskType::SWAP_OUT):
        generate_reply(nbytes, destinations, ratios);
    // auto reply = get_advisor()->how_to_swap(old_ptr, nbytes, taskType::SWAP_OUT);
    if (!reply) return 0;
    AT_ASSERT(old_ptr.device().index()==swapper.get_default_device_id(), 
        "Tensor device does not match default device");
    Z_DEBUG("swap_out: [%p] \n",  old_ptr.get());
    Z_DEBUG("Swap out tensor info: size=[%.4f]MB\n", (float)nbytes/(1024*1024));
    swapper.add_swap_out_bytes(nbytes);
    old_ptr.set_status(c10::DataStatus::SWAPPING_OUT);
    swapper.copy_engine->ready_queues[1].push(
        CopyTask(
            taskType::SWAP_OUT,
            &old_ptr,
            nbytes,
            reply
        )
    );
    return 0;
}

int swap_in_by_meta(Meta_Tensor meta_tensor){
    /*
        ret:    0: 成功swap in
                1: 已经swap in过了/没被swap out过，直接返回
                2: 非法数据

    */
    // should check tensor not on local GPU
    if (!meta_tensor.first){
        return 2;
    }
    auto nbytes = meta_tensor.second;
    auto& old_ptr = *((at::DataPtr*)meta_tensor.first);
    Z_DEBUG("swap_in: check the necessity to swap ptr [%p][deviceType=%d][deviceIndex=%d][status=%d]\n",  old_ptr.get(),
        old_ptr.device().type(), old_ptr.device().index(), old_ptr.get_status());
    auto current_status = old_ptr.get_status();
    switch (current_status){
        case c10::DataStatus::OK:
        case c10::DataStatus::SWAPPING_IN:
            return 1;
            break;
        case c10::DataStatus::SWAPPED:
            break;
        case c10::DataStatus::SWAPPING_OUT:
            old_ptr.wait_status_until(c10::DataStatus::SWAPPED);
            break;
        default:
            AT_ASSERT(false, "Unexpected DataStatus ", (int)current_status);
            
    }
    Z_DEBUG("swap_in: ready to swap ptr %p\n",  old_ptr.get());
    old_ptr.set_status(c10::DataStatus::SWAPPING_IN);
    // get_advisor()->how_to_swap(old_ptr, nbytes, taskType::SWAP_IN);
    swapper.copy_engine->ready_queues[0].push(
        CopyTask(
            taskType::SWAP_IN,
            &old_ptr,
            nbytes,
            nullptr
        )
    );
    return 0;
}

int swap_out_by_target(at::Tensor& tensor){
    return swap_out_by_target(tensor, empty_int_vector, empty_float_vector);
}
int swap_out_by_target(at::Tensor& tensor, std::vector<int>& destinations, std::vector<float>& ratios){
    return swap_out_by_meta(make_meta_tensor(tensor), destinations, ratios);
}

int swap_in_by_target(at::Tensor& tensor){
    return swap_in_by_meta(make_meta_tensor(tensor));
}
int check_and_swap_out(at::Tensor& tensor, SET_OF_PTR& excluded_set){
    check_and_swap_out(tensor, excluded_set, empty_int_vector, empty_float_vector);
}

int check_and_swap_out(at::Tensor& tensor, SET_OF_PTR& excluded_set,
    std::vector<int>& destinations, std::vector<float>& ratios){
    auto ptr = tensor.data_ptr();
    for(auto excluded_ptr : excluded_set){
        Z_DEBUG("[%p] is in excluded_set\n", excluded_ptr);

    }
    if (excluded_set.find(ptr) == excluded_set.end()){
        Z_DEBUG("[%p] is not in excluded_set so swap it\n", ptr);
        swap_out_by_target(tensor, destinations, ratios);
    } else {
        Z_DEBUG("[%p] is in excluded_set so do not swap it\n", ptr);

    }
    return 0;
};
int swap_out_by_node(torch::autograd::Node* node, SET_OF_PTR& excluded_set){
    return swap_out_by_node(node, excluded_set, empty_int_vector, empty_float_vector);
}
int swap_out_by_node(torch::autograd::Node* node, SET_OF_PTR& excluded_set,
    std::vector<int>& destinations, std::vector<float>& ratios){
    Z_DEBUG("swap_out_by_node: excluded_set: %s\n", vector2string<void*>(std::vector<void*>(excluded_set.begin(), excluded_set.end())).c_str());
    auto vars = get_Vars(node);
    for (auto&v:vars){
        Z_DEBUG("swap_out_by_node: v.get()=%p\n", v.get_tensor().data_ptr());
        check_and_swap_out(v.get_tensor(), excluded_set);
    }
    return 0;
}
int check_and_swap_in(at::Tensor& tensor, SET_OF_PTR& excluded_set){
    auto ptr = tensor.data_ptr();
    if (excluded_set.find(ptr) == excluded_set.end()){
        swap_in_by_target(tensor);
    }
    return 0;
};
int swap_in_by_node(torch::autograd::Node* node, SET_OF_PTR& excluded_set){
    Z_INFO("swap_in_by_node: %s\n", node->name().c_str());
    auto vars = get_Vars(node);
    for (auto&v: vars){
        check_and_swap_in(v.get_tensor(), excluded_set);
    }
    return 0;
}

int swap_in_by_node(torch::autograd::Node* node){
    Z_DEBUG("swap_in_by_node: %s\n", node->name().c_str());
    auto vars = get_Vars(node);
    for (auto&v: vars){
        swap_in_by_target(v.get_tensor());
    }
    return 0;
}
int wait_until_ok_by_target(at::Tensor& tensor, int status){
    if (tensor.defined()){
    auto t0 = tensor.unsafeGetTensorImpl();
    // Z_DEBUG("t0: %p\n", t0);
    // Z_DEBUG("has storage: %d\n", t0->has_storage());
    auto t2 = t0->get_storage();
    auto& t3 = t2.get_data_ptr();
        tensor.unsafeGetTensorImpl()->get_storage().get_data_ptr().wait_status_until(
            c10::DataStatus(status)
        );
    }
    return 0;
}

int wait_until_ok_by_node(torch::autograd::Node* node, int status){
    Z_DEBUG("start wait until ok by node (%s)\n", node->name().c_str());
    auto vars = get_Vars(node);
    for (auto i = 0lu; i<vars.size(); i++){
        auto&v  = vars[i];
        if (!v.get_tensor().defined()){
            continue;
        }
        bool need_to_wait = false;
        if ((int)v.get_tensor().unsafeGetTensorImpl()->get_storage().get_data_ptr().get_status()>0){
            need_to_wait = true;
        }
        auto start_time = std::chrono::system_clock::now();
        wait_until_ok_by_target(v.get_tensor(), status);
        auto end_time = std::chrono::system_clock::now();
        auto elapsed_time = std::chrono::duration_cast<std::chrono::milliseconds>(end_time - start_time).count();
        if (need_to_wait){
            Z_INFO("Node[%s], %lu-th tensor [%s] waits for (ms)=%ld\n",
                node->name().c_str(), i, to_string(make_meta_tensor(v.get_tensor())).c_str(), elapsed_time);
        }
        Z_DEBUG("after wait: ptr: [%p], device_type: %d, device_index: %d, status=%d\n", 
            v.get_tensor().data_ptr(), (int)(v.get_tensor().device().type()), (int)v.get_tensor().device().index(), 
            (int)v.get_tensor().unsafeGetTensorImpl()->get_storage().get_data_ptr().get_status());
    }
    if (node->name()=="torch::autograd::AccumulateGrad"){
        auto&weight = ((torch::autograd::AccumulateGrad*)node)->variable;
        auto states = get_states_of_weight(weight);
        for (auto&state:states){
            wait_until_ok_by_target(state, status);
        }
    }
    Z_DEBUG("all tensors are ready in node (%s)\n", node->name().c_str());
    return 0;
}

Swapper* get_swapper(){
    return &swapper;
}


}
}