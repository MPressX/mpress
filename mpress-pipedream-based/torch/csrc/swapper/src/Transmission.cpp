#include "Transmission.h"
#include <torch/csrc/swapper/include/utils/z_debug.h>
#include <torch/csrc/swapper/include/utils/check_cuda.h>

using namespace torch::swapper;

namespace torch{
namespace swapper{

namespace transmission{
    bool inited=false;
    int num_cards;
    std::vector<std::vector<int>> peer_connections;
    int enabled_p2p[8][8] = {0};
    void init(int num_cards_){
        assert(inited==false); inited = true;
        num_cards = num_cards_;
        peer_connections = std::vector<std::vector<int>>(num_cards, std::vector<int>(num_cards));
        for (int i = 0; i < num_cards; i++){
            for (int j = 0; j < num_cards; j++){
                CudaSafeCall(cudaDeviceCanAccessPeer(&peer_connections[i][j], i, j));
            }
        }
    }
    int is_connected(int i, int j){
        // user should gurantee the inputs i and j
        if (inited) return peer_connections[i][j];
        int t;
        CudaSafeCall(cudaDeviceCanAccessPeer(&t, i, j));
        return t;
    }
    int enable_p2p(int i, int j){
        if (enabled_p2p[i][j]) {
            return 0;
        }
        if (!is_connected(i,j)) {
            return -1; // 无法启用p2p
        }
        CudaSafeCall(cudaSetDevice(i));
        CudaSafeCall(cudaDeviceEnablePeerAccess(j,0));
        CudaSafeCall(cudaSetDevice(j));
        CudaSafeCall(cudaDeviceEnablePeerAccess(i,0));
        enabled_p2p[i][j] = 1;
        enabled_p2p[j][i] = 1;
        return 0;
    }
}

// D H & P2P(1->1) hostID=-1
void GeneralMemcpyAsync(
    void* dst,
    int dst_id,
    void* src,
    int src_id,
    cudaStream_t stream,
    size_t nbytes
){
    // 不考虑两者均是host端内存的情况
    // CudaSafeCall(cudaStreamSynchronize(stream));
    // timespec start_time, end_time;
    // clock_gettime(CLOCK_MONOTONIC, &start_time);
    if (dst_id == -1){
        CudaSafeCall(cudaMemcpyAsync(dst, src, nbytes, cudaMemcpyDeviceToHost, stream));
    }
    else if (src_id == -1){
        CudaSafeCall(cudaMemcpyAsync(dst, src, nbytes, cudaMemcpyHostToDevice, stream));
    }
    else{
        if(transmission::is_connected(src_id, dst_id)){
            transmission::enable_p2p(src_id, dst_id);
            CudaSafeCall(cudaMemcpyPeerAsync(dst, dst_id, src, src_id, nbytes, stream));
        }
        else{
            CudaSafeCall(cudaMemcpyPeerAsync(dst, dst_id, src, src_id, nbytes, stream));
        }
    }
    // CudaSafeCall(cudaStreamSynchronize(stream));
    // clock_gettime(CLOCK_MONOTONIC, &end_time);
    // float time_cost = (end_time.tv_sec - start_time.tv_sec) * 1e3 + (end_time.tv_nsec - start_time.tv_nsec) * 1e-6;
    // size_t size = nbytes;
    // std::cout<<  (float)size*1000/(1024*1024*1024)/time_cost <<std::endl;
}

// D D (1->n)
void GeneralMemcpyAsync(
    PtrList dsts,
    IntList dst_ids,
    void* src,
    int src_id,
    SizeList dst_nbytes,
    StreamList streams,
    size_t nbytes
){
    // for (auto&s:streams) CudaSafeCall(cudaStreamSynchronize(s));
    // timespec start_time, end_time;
    // clock_gettime(CLOCK_MONOTONIC, &start_time);
    for(int i =0 ; i< dsts.size() ; i++){
        if (i > 0) src = src + dst_nbytes[i-1];
        if(dst_ids[i]!=-1){
            if(transmission::is_connected(src_id, dst_ids[i])){
                transmission::enable_p2p(src_id, dst_ids[i]);
                CudaSafeCall(cudaMemcpyPeerAsync(dsts[i],dst_ids[i],src,src_id,dst_nbytes[i],streams[i]));
            }
            else{
                CudaSafeCall(cudaMemcpyPeerAsync(dsts[i],dst_ids[i],src,src_id,dst_nbytes[i],streams[i]));
            }
        } else{
            // 多目标中可能出现一个CPU，但是不考虑起点是CPU。
            CudaSafeCall(cudaMemcpyAsync(dsts[i], src, dst_nbytes[i], cudaMemcpyDeviceToHost, streams[i]));
        }
    }
    // for (auto&s:streams) CudaSafeCall(cudaStreamSynchronize(s));
    // clock_gettime(CLOCK_MONOTONIC, &end_time);
    // float time_cost = (end_time.tv_sec - start_time.tv_sec) * 1e3 + (end_time.tv_nsec - start_time.tv_nsec) * 1e-6;
    // size_t size = std::accumulate(dst_nbytes.begin(), dst_nbytes.end(), (size_t)0);
    // std::cout<<  (float)size*1000/(1024*1024*1024)/time_cost <<std::endl;
}

// D D (n->1)
void GeneralMemcpyAsync(
    void* dst,
    int dst_id,
    PtrList srcs,
    IntList src_ids,
    SizeList src_nbytes,
    StreamList streams,
    size_t nbytes
){
    for(int i =0 ; i< srcs.size() ; i++){
        if (i > 0) dst = dst + src_nbytes[i-1];
        if (src_ids[i]!=-1){
            if(transmission::is_connected(src_ids[i], dst_id)){
                transmission::enable_p2p(src_ids[i], dst_id);
                CudaSafeCall(cudaMemcpyPeerAsync(dst,dst_id,srcs[i],src_ids[i],src_nbytes[i],streams[i]));
            }
            else{
                CudaSafeCall(cudaMemcpyPeerAsync(dst,dst_id,srcs[i],src_ids[i],src_nbytes[i],streams[i]));
            }
        } else{
            CudaSafeCall(cudaMemcpyAsync(dst, srcs[i], src_nbytes[i], cudaMemcpyHostToDevice, streams[i]));
        }
        
    }
}
}
}