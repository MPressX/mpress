#include "Advisor.h"
#include <limits>

#include <torch/csrc/swapper/include/utils/z_debug.h>
#include <torch/csrc/swapper/include/utils/likely.h>

#include <string>



namespace torch
{
namespace swapper
{
constexpr size_t minimum_swap_bytes = 1*1024*1024; // 1MiB
constexpr size_t kMinBlockSize = 512;
c10::Device index2device(int index){
    return index<0 ? c10::Device(c10::DeviceType::CPU, index) : c10::Device(c10::DeviceType::CUDA, index);
}

size_t round_size(size_t size) {
    if (size < kMinBlockSize) {
      return kMinBlockSize;
    } else {
      return kMinBlockSize * ((size + kMinBlockSize - 1) / kMinBlockSize);
    }
  }

P_Reply get_hybrid_swap_reply(std::vector<size_t> size_list, std::vector<c10::Device> device_list){
    auto reply = std::make_shared<Reply>();
    reply->device_list = device_list;
    reply->size_list = size_list;
    return reply;
}

std::vector<size_t> split_cake(size_t cake_size, std::vector<float> weights){
    
    auto sum_weights = std::accumulate(weights.begin(), weights.end(), (float)0.0);
    size_t left_cake = cake_size;
    auto per_size = floor(cake_size / sum_weights);
    std::vector<size_t> result;
    for (auto weight : weights){
        size_t one_piece = (size_t)floor(weight * per_size);
        if(unlikely(one_piece > left_cake)){
            one_piece = left_cake;
        }
        left_cake-=one_piece;
        result.push_back(one_piece);
    }
    if(left_cake > 0){
        // there are still cake left
        result[0]+=left_cake;
    }
    if(cake_size!=std::accumulate(result.begin(), result.end(), (size_t)0)){
        Z_ERROR("There's an size error during split cake phase: nbytes=%lu, however reply.size=%lu \n",cake_size,std::accumulate(result.begin(), result.end(), (size_t)0));
        AT_ERROR("There's an size error during split cake phase\n");
    }
    return result;
}

bool Advisor::hybrid_swap(
                    size_t tensor_index, 
                    size_t nbytes, 
                    std::vector<DeviceStatus>& device_status_list){
    // fixed point algorithm
    std::vector<int> valid_candidates = candidates;
    std::vector<size_t> size_list;
    bool found = false;
    while ((!found) && (!valid_candidates.empty()))
    {
        found = true;
        std::vector<float> weights;
        for (auto valid_candidate : valid_candidates){
            weights.push_back(get_link_speed(default_device_id_, valid_candidate));
        }
        size_list = split_cake(nbytes, weights);
        // if(nbytes!=std::accumulate(size_list.begin(), size_list.end(), (size_t)0)){
        //     Z_ERROR("There's an size error in Reply table during generating phase: nbytes=%lu, however reply.size=%lu \n",nbytes,std::accumulate(size_list.begin(), size_list.end(), (size_t)0));

        // }

        auto candidate_it = valid_candidates.begin();
        auto device_it = device_status_list.begin();
        auto size_it = size_list.begin();
        for(
            ;
            candidate_it!=valid_candidates.end()&&device_it!=device_status_list.end();
            ){
            if(*candidate_it == device_it->index){
                if(round_size(*size_it) < device_it->free_memory){
                    candidate_it++;
                    size_it++;
                    device_it++;
                } else {
                    valid_candidates.erase(candidate_it);
                    size_list.erase(size_it);
                    device_it++;
                    found = false;
                }
            } else{
                device_it++;
            }
        }
    }
    if(found){
    // update device status
        std::vector<c10::Device> device_list;
        auto candidate_it = valid_candidates.begin();
        auto device_it = device_status_list.begin();
        auto size_it = size_list.begin(); 
        Z_DEBUG("Hybrid find swap target devices: [%s] for tensor-%d\n", vector2string<int32_t>(valid_candidates,' ').c_str(), tensor_index);
        for(
            ;
            candidate_it!=valid_candidates.end()&&device_it!=device_status_list.end();
            ){
            if(*candidate_it == device_it->index){
                device_it->free_memory -= round_size(*size_it);
                Z_DEBUG("Device-%d has memory %llu left after allocate for tensor-%lu\n", device_it->index, device_it->free_memory, tensor_index)
                device_list.push_back(index2device(device_it->index));
                candidate_it++;
                size_it++;
                device_it++;
            } else{
                device_it++;
            }
        }
        reply_table[tensor_index] = get_hybrid_swap_reply(size_list, device_list);
        return true;
    }    
    else{
        Z_DEBUG("Hybrid found is false\n")
    }
    return false;
}


void Advisor::set_device_status(std::unordered_map<int, unsigned long long> device2size){
    if(mode_!=SWAP_MODE::AUTO){
        AT_ERROR("Please set device status after setting swapping mode into AUTO");
    }
    spare_device_list.clear();
    // candidates.clear();
    std::vector<size_t> candidates_;
    Z_INFO("There're %d spare devices set by users\n", device2size.size());
    for(auto it=device2size.begin(); it!=device2size.end(); it++){
        Z_DEBUG("Device-%d has %lu free memory space\n", it->first, it->second);
        DeviceStatus device_status;
        device_status.index = it->first;
        candidates_.push_back(it->first);
        device_status.free_memory = it->second;
        device_status.bandwidth = get_link_speed(default_device_id_, it->first);
        spare_device_list.push_back(device_status);
    }
    DeviceStatus cpu;
    cpu.index=-1;
    candidates_.push_back(-1);
    cpu.free_memory=std::numeric_limits<unsigned long long>::max();
    cpu.bandwidth=get_link_speed(default_device_id_, -1);
    spare_device_list.push_back(cpu);

    for(auto i : candidates_){
        bool exist=false;
        for(auto j : candidates){
            if(i==j){
                exist=true;
                break;
            }
        }
        if(exist){
        } else{
            AT_ERROR("Candidates set error in advisor!");
        }
    }
    candidates.clear();
    candidates.assign(candidates_.begin(),candidates_.end());
}


std::vector<DeviceStatus> Advisor::get_device_status(){
    // only get candidates' status specified by users
    std::vector<DeviceStatus> device_status_list;
    for(auto& candidate:candidates){
        DeviceStatus device_status;
        device_status.bandwidth = get_link_speed(default_device_id_, candidate);
        device_status.index = candidate;
        if (candidate == -1){
            device_status.free_memory = std::numeric_limits<unsigned long long>::max();
        } else {
            int current_device = caffe2::CaffeCudaGetDevice();
            caffe2::CaffeCudaSetDevice(candidate);
            size_t available, total;
            cudaMemGetInfo(&available, &total);
            device_status.free_memory = available/5;
            caffe2::CaffeCudaSetDevice(current_device);
        }
        Z_DEBUG("Find device-%d free_memory: %llu\n",device_status.index, device_status.free_memory);
        device_status_list.push_back(device_status);
        
    }
    return device_status_list;
}

float Advisor::heuristic_priority(int size, float interval){
    return alpha*size + beta/interval;
}

float Advisor::random_priority(){
    srand(time(0));
    return random();
}

P_Reply Advisor::swap_by_auto_v2(size_t nbytes, taskType task_type){
    if(task_type==taskType::SWAP_OUT){
        if (nbytes < minimum_swap_bytes){
            Z_DEBUG("Tensor size less than 1MB; So Advisor decide not to swap it!\n");
            return nullptr;
        }
        auto tensor_index = current_index_v2;
        current_index_v2++;
        Z_DEBUG("The [%d]-th swapped-out tensor during single iteration!\n", tensor_index);
        auto it = reply_table.find(tensor_index);
        if (it == reply_table.end() && !enable_hybrid_swap){
            // return host swap if cannot found in reply table
            Z_DEBUG("Not enable the hybrid swap, so swap to host memory by default\n");
            return get_hybrid_swap_reply({nbytes},{index2device(-1)});
        } else if(it == reply_table.end() && enable_hybrid_swap){
            if(hybrid_swap(tensor_index, nbytes, spare_device_list)){
                return reply_table.find(tensor_index)->second;
            } else {
                AT_ERROR("Hybrid swap failed! Cannot find available target device.\n");
                return nullptr;
            }
        } else {
            if(nbytes!=std::accumulate(it->second->size_list.begin(), it->second->size_list.end(), (size_t)0)){
                Z_ERROR("There's an size error in Reply table during using phase: nbytes=%lu, reply_table.size=%lu \n", nbytes,
                std::accumulate(it->second->size_list.begin(), it->second->size_list.end(), (size_t)0));
                AT_ERROR("There's an size error in Reply table during using phase \n");
            }
            return it->second;
        }
    } else {
        return nullptr;
    }

}
P_Reply Advisor::swap_by_auto(at::DataPtr& dptr, size_t nbytes, taskType task_type){
    if (nbytes < 1*1024*1024){
        Z_DEBUG("Tensor size less than 1MB; So Advisor decide not to swap it!\n");
        return nullptr;
    }
    void* tensor_ptr = &dptr;
    size_t tensor_index;
    if (task_type==taskType::SWAP_OUT){
        tensor_index = current_index;
        current_index++;
        Z_DEBUG("Tensor-[%p] is the [%d]-th swapped-out tensor during single iteration!\n", &dptr, tensor_index);
    }


    if (current_iteration == sample_point && task_type == taskType::SWAP_OUT){
        // sampling in specific iteration
        auto ptr_idx_table_it = ptr_idx_table.find(tensor_ptr);
        if (ptr_idx_table_it!=ptr_idx_table.end()){
            AT_ERROR("Repeat swapping out the same tensor and its dataptr is [%p]!", tensor_ptr);
        } else {
            ptr_idx_table[tensor_ptr] = tensor_index;
        }

        auto it = record_table.find(tensor_index);
        if (it==record_table.end()){
            timespec start_time;
            clock_gettime(CLOCK_MONOTONIC, &start_time);
            record_table[tensor_index] = start_time;
            Z_DEBUG("Record timestamp when swapping out tensors\n");
        } else {
            AT_ERROR("Unknown problem raised in record phase!\n");
        }
    } else if(current_iteration >= sample_point && task_type == taskType::SWAP_IN && reply_table.empty()){
        // capture swap in task and compute priority

        auto ptr_idx_table_it = ptr_idx_table.find(tensor_ptr);
        if (ptr_idx_table_it==ptr_idx_table.end()){
            AT_ERROR("tensor ptr [%p] not found in ptr_idx_table!\n", tensor_ptr);
        } else {
            tensor_index = ptr_idx_table[tensor_ptr];
        }
        auto it = record_table.find(tensor_index);
        if (it!=record_table.end()) {
            timespec end_time;
            clock_gettime(CLOCK_MONOTONIC, &end_time);
            timespec start_time = record_table[tensor_index];
            float interval = (end_time.tv_sec - start_time.tv_sec) * 1e3 + (end_time.tv_nsec - start_time.tv_nsec) * 1e-6;
            float priority = heuristic_priority(nbytes, interval);
            Record record = {tensor_index, nbytes, priority};
            priority_queue.push(record);
            Z_DEBUG("Tensor-[%d]'s priority is %f: size[%d] interval[%f]\n", tensor_index, priority, nbytes, interval);

            // remove useless record from record_table and ptr_idx_table
            record_table.erase(it);
            auto iterator = ptr_idx_table.find(tensor_ptr);
            ptr_idx_table.erase(iterator);
        } else {
            AT_ERROR("Cannot find tensor index [%d] in record_table!\n", tensor_index);
        }


    } else if (current_iteration > sample_point && record_table.empty()) {
        // make hybrid swap only after sample_point is passed and record table is cleared
        if (!priority_queue.empty()){
            auto device_status_list = get_device_status();
            while (!priority_queue.empty())
            {
                auto record = priority_queue.top();
                priority_queue.pop();
                if(hybrid_swap(record.index, record.nbytes, device_status_list)){
                } else {
                    AT_ERROR("Hybrid swap failed! Cannot find available target device.\n");
                }
            }
            
        }
    }
    
    
    if (task_type==taskType::SWAP_OUT){
        auto it = reply_table.find(tensor_index);
        if (it == reply_table.end()){
            // return host swap if cannot found in reply table
            return get_hybrid_swap_reply({nbytes},{index2device(-1)});
        } else {
            return it->second;
        }
    } else {
        return nullptr;
    }
}

P_Reply Advisor::swap_by_fixed(size_t nbytes){
    auto ans = std::make_shared<Reply>();
    double per = (double)nbytes / total_candidates_speed;
    size_t left = nbytes;
    for (int i = 0; i < candidates.size()-1; i++){
        auto candidate = candidates[i];
        auto quantity = (size_t)(per*get_link_speed(default_device_id_, candidate));
        if (unlikely(quantity>left)){
            quantity = left;
        }
        ans->size_list.emplace_back(quantity);
        left -= quantity;
        ans->device_list.emplace_back(std::move(index2device(candidate)));
    }
    ans->size_list.emplace_back(left);
    ans->device_list.emplace_back(std::move(index2device(candidates[candidates.size()-1])));
    return ans;
}

P_Reply Advisor::swap_by_default(){
    return nullptr;
}

P_Reply Advisor::how_to_swap(at::DataPtr& dptr, size_t nbytes, taskType task_type){
    if(candidates.empty()){
        AT_ERROR("Please set candidates to Advisor");
    }
    if (nbytes<(1<<20)) { // <= 1MiB
        Z_DEBUG("The nbytes(%lu) is less than 1MiB, skip swapping it!\n", nbytes);
        return nullptr; 
    }
    P_Reply reply;
    switch (mode_)
    {
    case SWAP_MODE::AUTO:
        reply = swap_by_auto_v2(nbytes, task_type);
        break;
    
    case SWAP_MODE::FIXED:
        reply = swap_by_fixed(nbytes);
        break;
    case SWAP_MODE::STAY:
    case SWAP_MODE::DEFAULT:
        reply = swap_by_default();
        break;
    default:
        AT_ERROR("Unknown SWAP_MODE");
        break;
    }
    return reply;
}



void Advisor::print_settings(){
    Z_INFO("mode_=%d\n", mode_);
    Z_INFO("default_device_id_=%d\n", default_device_id_);
    Z_INFO("sample_point=%d\n", sample_point);
    Z_INFO("speed_matrix:\n");
    for (auto&line:speed_matrix){
        for (auto&v:line){
            printf("%.2f\t", v);
        }
        printf("\n");
    }
    Z_INFO("connectivity_matrix:\n");
    for (auto&line:connectivity_matrix){
        for (auto&v:line){
            printf("%d\t", v);
        }
        printf("\n");
    }
    std::string s_candidates;
    for (auto&c:candidates) s_candidates = s_candidates+std::to_string(c)+" ";
    Z_INFO("candidates: %s\n", s_candidates.c_str());
}

P_Reply generate_reply(size_t nbytes, std::vector<int>& destinations, std::vector<float>& ratios){
    if (nbytes <= minimum_swap_bytes || destinations.empty()){
        return nullptr;
    }
    auto reply = std::make_shared<Reply>();
    reply->size_list.resize(destinations.size());
    size_t left_size = nbytes;
    for(size_t i=0; i<destinations.size()-1; i++){
        reply->size_list[i] = nbytes * ratios[i];
        left_size -= reply->size_list[i];
    }
    reply->size_list[destinations.size()-1] = left_size;
    for (size_t i=0; i<destinations.size(); i++){
        reply->device_list.emplace_back(index2device(destinations[i]));
    }
    return reply;
}

static Advisor advisor;

Advisor* get_advisor(){
    return &advisor;
}

} // namespace swapper 
} // namespace torch
