#include "CopyEngine.h"
#include "Transmission.h"
#include "MemTracker.h"
using namespace torch::swapper;

namespace torch{
namespace swapper{


void ReadyQueue::push(CopyTask task){
    {
        std::lock_guard<std::mutex> lock(mutex_);
        list_.emplace_back(std::move(task));
    }
    not_empty_.notify_one();
}
void ReadyQueue::push_shutdown_task(){
    {
        std::lock_guard<std::mutex> lock(mutex_);
        list_.emplace_back(CopyTask());
    }
    not_empty_.notify_one();
}

CopyTask ReadyQueue::pop(){
    std::unique_lock<std::mutex> lock(mutex_);
    not_empty_.wait(lock, [this]{return !list_.empty();});
    auto task = std::move(const_cast<CopyTask&>(list_.front()));
    list_.pop_front();
    return task;
}
size_t ReadyQueue::left_tasks(){
    return list_.size();
}

void CopyEngine_::evaluate_function_send(CopyTask& task){
    auto reply = task.reply_;
    if (!reply) return; // just stay.
    auto&size_list = reply->size_list;
    auto&device_list = reply->device_list;
    void* key = (void*)task.old_ptr_;
    auto head_dataptr = allocate(size_list[0], device_list[0]);
    auto&pair = record_tails[key];
    auto&tails = pair.first;
    pair.second = reply;
    PtrList dsts = {head_dataptr.get()};
    IntList dst_ids = {head_dataptr.device().index()};
    StreamList ss = {get_stream(1, dst_ids[0])};


    if (size_list.size()>1){
        for (int i = 1; i < size_list.size(); i++){
            tails.emplace_back(allocate(
                size_list[i], device_list[i]
            ));
            dsts.emplace_back(tails[i-1].get()); // tail不包括第一个
            dst_ids.emplace_back(device_list[i].index());
            ss.emplace_back(get_stream(1, dst_ids[i]));
        }
        GeneralMemcpyAsync(
            dsts,
            dst_ids,
            task.old_ptr_->get(),
            (int)task.old_ptr_->device().index(),
            size_list,
            ss,
            task.nbytes_
        );
    }
    else{
        GeneralMemcpyAsync(
            head_dataptr.get(),
            (int) head_dataptr.device().index(),
            task.old_ptr_->get(),
            (int)task.old_ptr_->device().index(),
            get_stream(1,(int)head_dataptr.device().index()),
            task.nbytes_
        );
    }
    Z_DEBUG("Send: pointer:\t[%p] --> [%s]\n", task.old_ptr_->get(), vector2string<void*>(dsts).c_str());
    Z_DEBUG("Send: devices:\t[%d] --> [%s]\n", (int)task.old_ptr_->device().index(), vector2string<int>(dst_ids).c_str());
    CudaSafeCall(cudaStreamSynchronize(nullptr));
    for (auto&s:ss) CudaSafeCall(cudaStreamSynchronize(s));


    // Z_DEBUG("Send: old_device [(type)%d : (index)%d]\n", (int)task.old_ptr_->device().type(), (int)task.old_ptr_->device().index());
    // Z_DEBUG("Send: set_data_ptr: [%p] --> [%p]\n", task.old_ptr_->get(), head_dataptr.get());
    task.old_ptr_->set_data_ptr_v2(std::move(head_dataptr));
    task.old_ptr_->set_status(c10::DataStatus::SWAPPED);
    // Z_DEBUG("Send: new_device [(type)%d : (index)%d]\n", (int)task.old_ptr_->device().type(), (int)task.old_ptr_->device().index());
}

void CopyEngine_::evaluate_function_recv(CopyTask& task){
    timespec start_time, end_time;
    clock_gettime(CLOCK_MONOTONIC, &start_time);
    void* key = (void*)task.old_ptr_;
    auto it = record_tails.find(key);
    AT_ASSERT(it!=record_tails.end(), "The dataptr is not recorded");
    PtrList srcs = {task.old_ptr_->get()};
    IntList src_ids = {task.old_ptr_->device().index()};
    StreamList ss = {get_stream(0, src_ids[0])};
    auto&pair = it->second;
    auto&tails = pair.first;
    auto&reply = pair.second;
    auto new_dptr = allocate(task.nbytes_, 
                             c10::Device(c10::DeviceType::CUDA,
                                         default_device_id));
    // cudaStreamSynchronize(nullptr);
    if (tails.size()>0){
        for (auto&dptr:tails){
            srcs.emplace_back(dptr.get());
            src_ids.emplace_back(dptr.device().index());
            ss.emplace_back(get_stream(0,dptr.device().index()));
        }
        GeneralMemcpyAsync(
            new_dptr.get(),
            default_device_id,
            srcs,
            src_ids,
            reply->size_list,
            ss,
            task.nbytes_
        );
    }
    else{
        GeneralMemcpyAsync(
            new_dptr.get(),
            default_device_id,
            task.old_ptr_->get(),
            (int)task.old_ptr_->device().index(),
            get_stream(0, task.old_ptr_->device().index()),
            task.nbytes_
        );
    }
    Z_DEBUG("Recv: pointer:\t[%p] <-- [%s]\n", new_dptr.get(), vector2string<void*>(srcs).c_str());
    Z_DEBUG("Recv: devices:\t[%d] <-- [%s]\n", (int)new_dptr.device().index(), vector2string<int>(src_ids).c_str());
    for (auto&s:ss) CudaSafeCall(cudaStreamSynchronize(s));
    clock_gettime(CLOCK_MONOTONIC, &end_time);
    float time_cost = (end_time.tv_sec - start_time.tv_sec) * 1e3 + (end_time.tv_nsec - start_time.tv_nsec) * 1e-6;

    Z_DEBUG("Swap in task completes in %.4f ms\n", time_cost);

    Z_DEBUG("Recv: set_data_ptr: [%p] --> [%p]\n", task.old_ptr_->get(), new_dptr.get());
    task.old_ptr_->set_data_ptr_v2(std::move(new_dptr));
    record_tails.erase(it);
    task.old_ptr_->set_status(c10::DataStatus::OK);
}

void CopyEngine_::evaluate_function(CopyTask& task){
    switch (task.task_type_){
    case taskType::SWAP_OUT:
        evaluate_function_send(task);
        break;
    case taskType::SWAP_IN:
        evaluate_function_recv(task);
        break;
    default:
        AT_ERROR("Unknown taskType");
        break;
    }
}

void CopyEngine_::loop_copy_engine(int is_send){
    cudaSetDevice(default_device_id);
    auto& queue = ready_queues[is_send];
    while (true){
        // Z_DEBUG("waiting for new task...\n");
        auto task = queue.pop();
        if (task.task_type_ == taskType::SHUTDOWN){
            Z_DEBUG("receive shutdown task, exiting loop...\n");
            break;
        }
        // Z_DEBUG("get a new task with type [%d] and data size [%d]!\n", (int)task.task_type_, (int)task.nbytes_);
        evaluate_function(task);
        auto left_tasks = queue.left_tasks();
        Z_DEBUG("Remaining %lu tasks in queue, is_send=%d\n", left_tasks, is_send);
    }
}

void CopyEngine_::memcpy_sync(void* dst, void* src, size_t nbytes, cudaMemcpyKind cpy_kind,
                    cudaStream_t stream, std::function<void()>fn){
    auto event = alloc_event();
    cudaMemcpyAsync(dst, src, nbytes, cpy_kind, stream);
    cudaEventRecord(event, stream);
    cudaEventSynchronize(event);
    fn();
    free_event(event);
}

static CopyEngine_ copy_engine;

CopyEngine_* get_copy_engine(){
    return &copy_engine;
}

}
}