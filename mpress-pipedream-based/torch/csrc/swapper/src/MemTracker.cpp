#include <MemTracker.h>
#include <torch/csrc/swapper/include/utils/check_cuda.h>
#include <torch/csrc/swapper/include/utils/z_debug.h>
// #define DEBUG

// #define LOCK l->lock()
// #define UNLOCK l->unlock()

using namespace torch::swapper;
namespace torch{
namespace swapper{

std::string CPUMemInfo::to_string(){
    std::stringstream str;
    str << *this;
    return str.str();
}

std::string CUDAMemInfo::to_string(){
    std::stringstream str;
    str << *this;
    return str.str();
}

std::ostream& operator << (std::ostream& out, CPUMemInfo& info) {
        float MB = 1024*1024;

        // setting precision for float number
        out.setf(std::ios::fixed, std::ios::floatfield);
        out.flags(std::ios::left);
        out << std::setprecision(2) << std::fixed;
        out << std::endl
            << "[CPU Memory Information]"
            << std::endl;
        out.width(25);
        out << "Memory Allocated";
        out << info.allocated_mem/MB << "MB" << std::endl;
        out.width(25);
        out << "Memory Cached";
        out << info.cached_mem/MB << "MB" << std::endl;
        return out;
}


std::ostream& operator << (std::ostream& out, CUDAMemInfo& info) {
        float MB = 1024*1024;

        // setting precision for float number
        out.setf(std::ios::fixed, std::ios::floatfield);
        out.flags(std::ios::left);
        out <<std::setprecision(2) << std::fixed;
        out << std::endl
            << "[CUDA Memory Information Device-"<< info.device_id <<"]"
            << std::endl;
        out.width(25);
        out << "Memory Allocated" << info.currentMemoryAllocated/MB << "MB" << std::endl;
        out.width(25);
        out << "Memory Cached" << info.currentMemoryCached/MB << "MB" << std::endl;
        out.width(25);
        out << "Max Memory Allocated" << info.maxMemoryAllocated/MB << "MB" << std::endl;
        out.width(25);
        out << "Max Memory Cached" << info.maxMemoryCached/MB << "MB" << std::endl;
        return out;
}   


    
void free_pinned_mem(void* ptr);
std::mutex& MemTracker::mutex(bool is_local) {
  static std::mutex m;
  static std::mutex n;
  if (is_local){
    return m;
  } else{
    return n;
  }
}
void* MemTracker::allocate(size_t nbytes, c10::Device device){
    // only one thread can allocate or free memory space at a time
    time_t seconds = 0;
    unsigned long wait_second = 0;
    while(
        device.type()==c10::DeviceType::CUDA && 
        device.index()==default_device_id_){
      if(cuda_memory_threshold_ > 0){
        // size_t free_memory = cuda_memory_threshold_ - 
        auto memory_allocated = c10::cuda::CUDACachingAllocator::currentMemoryAllocated(default_device_id_);
        if (cuda_memory_threshold_ < memory_allocated || cuda_memory_threshold_ - memory_allocated < nbytes){
          nanosleep(&c10::req,&c10::rem);
          if (seconds == 0){
            seconds = time(NULL);
          }
          else{
            auto current = time(NULL);
            if (current >= seconds+1){
              seconds = current;
              wait_second++;
              Z_WARN("The malloc in MemTracker has been wait for %lu second(s); "
                     "nbytes=%lu, device_index=%d, cuda_memory_threshold_=%lu, memory_allocated=%lu\n", 
                     wait_second, nbytes, (int)device.index(), cuda_memory_threshold_, memory_allocated);
              if (wait_second >= 1){
                  Z_WARN("The thread is blocked more than 1 seconds, continue allocating memory regardless of memory threshold\n");
                  break;
              }
            }
          }
        } else {
          break;
        }
      } else {
        //   std::cout<<"MemTracker memory threshold is closed!" << std::endl;
          break;
      }
    }
    if (device.index()==default_device_id_){
        std::lock_guard<std::mutex> lock_guard(mutex(true));
    } else{
        std::lock_guard<std::mutex> lock_guard(mutex(false));
    }
    // HELLO;
    c10::Allocator* allocator;
    std::unordered_map<void*, Block>* allocated_list;
    c10::DeleterFnPtr free_fn;
    
    
    int device_id;
    Block block;
    bool block_empty = true;

    switch (device.type())
    {
    case c10::DeviceType::CPU:
        {
            allocator = cpu_allocator;
            allocated_list = &cpu_allocated_list;
            // as a flag which suggests that the device is cpu
            device_id = -1;

            auto it = cpu_cached_list.find(nbytes);
            #ifdef DEBUG
            std::cout<<"cpu free is finding ..."<<std::endl;
            #endif
            if(it!=cpu_cached_list.end()){
                auto& blocklist = it->second;
                #ifdef DEBUG
                std::cout<<"there is available size in cpu free list"<<std::endl;
                #endif
                if(!blocklist.empty()){
                    #ifdef DEBUG
                    std::cout<<"there is available block in cpu free list"<<std::endl;
                    #endif
                    // if found available block in cpu free list 
                    block = std::move(blocklist.back());
                    block_empty = false;
                    // remove from cpu free list
                    blocklist.pop_back();
                    // update cpu mem info after dealing with free list
                    cpu_mem_info.cached_block_num-=1;
                    cpu_mem_info.cached_mem-=nbytes;
                }
            }
        }

        break;
    case c10::DeviceType::CUDA:
        {
            allocator = cuda_allocator;
            allocated_list = &cuda_allocated_list;
            device_id = caffe2::CaffeCudaGetDevice();
            caffe2::CaffeCudaSetDevice(device.index());
        }
        break;
    default:
        AT_ERROR("failed to allocate memory; unsupported device type");
        break;
    }

    if (!block_empty){
        
    } else {
        c10::DataPtr data = allocator->allocate(nbytes);
        if(device.type()==c10::DeviceType::CPU){
            #ifdef ENABLE_PINNED_MEM
            cudaPointerAttributes data_attr;
            if (cudaPointerGetAttributes(&data_attr, data.get()) == cudaErrorInvalidValue) {
                //  cudaGetLastError(); // clear out the previous API error
                CudaSafeCall(cudaHostRegister(data.get(), nbytes, cudaHostRegisterPortable));
            }

            // re-build data; replace original deleter fn with free_pinned memory
            auto data_ptr = data.get();
            auto ctx_ptr = data.get_context();
            c10::DeleterFnPtr fn = &free_pinned_mem;
            data.release_context();
            data = c10::DataPtr(data_ptr,ctx_ptr,fn,device);
            #endif
            block = {nbytes, std::move(data)};
            auto it = allocated_list->find(data.get());
            if(it != allocated_list->end()){
                AT_ERROR("the memory which is allocated currently exists in the allocated list!");
            }
        } else{
            // set device back; only cuda device need this operation
            if(device_id > -1){
                caffe2::CaffeCudaSetDevice(device_id);
            }
            auto data_ptr = data.get();
            data.release_context();
            return data_ptr;
        }
        
    }
    // cpu device
    allocated_list->insert(std::pair<void*, Block>(block.data.get(),std::move(block)));
    cpu_mem_info.allocated_block_num+=1;
    cpu_mem_info.allocated_mem+=nbytes;
    return block.data.get();

    
        
    // UNLOCK;
}

void MemTracker::free_cpu_cached_list(){
    cpu_cached_list.clear();
    cpu_mem_info.cached_block_num=0;
    cpu_mem_info.cached_mem=0;
}

void MemTracker::free_gpu(void* ptr){
    cuda_allocator->raw_deleter()(ptr);
}
void MemTracker::free_cpu(void* ptr){

    // LOCK;
    std::lock_guard<std::mutex> lock_guard(mutex(false));
    auto it = cpu_allocated_list.find(ptr);
    if(it == cpu_allocated_list.end())
        AT_ERROR("raise an error when free data ptr: ",ptr, " in gpu device");
    // remove item from allocated list! and the data block will be solved by memory pooling of pytorch
    auto block=std::move(it->second);
    cpu_allocated_list.erase(it);
    cpu_mem_info.allocated_block_num-=1;
    cpu_mem_info.allocated_mem-=block.size;

    auto it_free = cpu_cached_list.find(block.size);
    // here insert the block into cpu free list
    if(it_free != cpu_cached_list.end()){
        it_free->second.push_back(std::move(block));
    } else{
        // need to initialize a new list structure and insert into cpu free list
        BlockList blocklist;
        blocklist.push_back(std::move(block));
        cpu_cached_list.insert(
            std::pair<size_t,BlockList>(
                block.size, std::move(blocklist)
            )
        );
    }
    cpu_mem_info.cached_block_num+=1;
    cpu_mem_info.cached_mem+=block.size;
    // UNLOCK;
}
const CPUMemInfo& MemTracker::get_cpu_mem_info(){
    return cpu_mem_info;
}
const CUDAMemInfo& MemTracker::get_cuda_mem_info(int device_index){
    cuda_mem_info.device_id=device_index;
    cuda_mem_info.currentMemoryAllocated = c10::cuda::CUDACachingAllocator::currentMemoryAllocated(device_index);
    cuda_mem_info.maxMemoryAllocated = c10::cuda::CUDACachingAllocator::maxMemoryAllocated(device_index);
    cuda_mem_info.currentMemoryCached = c10::cuda::CUDACachingAllocator::currentMemoryCached(device_index);
    cuda_mem_info.maxMemoryCached = c10::cuda::CUDACachingAllocator::maxMemoryCached(device_index);
    return cuda_mem_info;
}
size_t MemTracker::get_cpu_allocated_list_length(){
    return cpu_allocated_list.size();
}
size_t MemTracker::get_cpu_cached_list_length(){
    size_t length = 0;
    for(auto it = cpu_cached_list.begin();it != cpu_cached_list.end();it++){
        length += it->second.size();
    }
    return length;
}
size_t MemTracker::get_cuda_allocated_list_length(){
    return cuda_allocated_list.size();
}

void MemTracker::set_cpu_allocator(c10::Allocator* allocator){
    cpu_allocator = allocator;
}
void MemTracker::set_cuda_allocator(c10::Allocator* allocator){
    cuda_allocator = allocator;
}

void free_pinned_mem(void* ptr){
    Z_DEBUG("free pinned memory, ptr=%p\n", ptr);
    CudaSafeCall(cudaHostUnregister(ptr));
    free(ptr);
}

// only one mem_tracker in global
static MemTracker mem_tracker;

// get deleter function poiter through the methods below
static void Delete_cpu(void* ptr){
    mem_tracker.free_cpu(ptr);
}
static void Delete_gpu(void* ptr){
    mem_tracker.free_gpu(ptr);
}

c10::DataPtr allocate(size_t nbytes, c10::Device device){
    void* data = mem_tracker.allocate(nbytes,device);
    c10::DeleterFnPtr free_fn;
 
    switch (device.type())
    {
    case c10::DeviceType::CPU:
        free_fn = &Delete_cpu;
        break;
    case c10::DeviceType::CUDA:
        free_fn = &Delete_gpu;
        break;
    default:
        AT_ERROR("failed to allocate memory; unsupported device type");
        break;
    }
    return {data,data,free_fn,device};


}

MemTracker* get_mem_tracker(){
    return &mem_tracker;
}



}
}