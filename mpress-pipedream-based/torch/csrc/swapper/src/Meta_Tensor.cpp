#include <utility>
#include <torch/csrc/swapper/include/Meta_Tensor.h>


using namespace torch::swapper;

namespace torch{
namespace swapper{
std::string to_string(Meta_Tensor meta_tensor){
    std::ostringstream os;
    os << "DataPtr[" << meta_tensor.first << "], ptr_[" << meta_tensor.first->get() << "], nbytes["
        << meta_tensor.second << "], status[" << int(meta_tensor.first->get_status()) << "]";
    return os.str();
}

at::DataPtr* get_ptr_DataPtr(at::Tensor& tensor){
    if (!tensor.defined()){
        return nullptr;
    }
    auto& ptr=tensor.unsafeGetTensorImpl()->get_storage().get_data_ptr();
    return &ptr;
}
inline size_t get_nbytes(at::Tensor& tensor){
    return tensor.defined()?tensor.nbytes():0;
}

Meta_Tensor make_meta_tensor(){
    return std::pair<at::DataPtr*, size_t>(nullptr, 0);
}
Meta_Tensor make_meta_tensor(unsigned long ptr, unsigned long nbyte){
    return std::pair<at::DataPtr*, size_t>((at::DataPtr*)ptr, nbyte);
}
Meta_Tensor make_meta_tensor(void* ptr, unsigned long nbyte){
    return std::pair<at::DataPtr*, size_t>((at::DataPtr*)ptr, nbyte);
}
Meta_Tensor make_meta_tensor(at::Tensor& tensor){
    return std::pair<at::DataPtr*, size_t>(get_ptr_DataPtr(tensor), get_nbytes(tensor));
}

}
}