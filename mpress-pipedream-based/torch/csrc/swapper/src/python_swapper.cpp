#include <torch/csrc/swapper/include/python_swapper.h>
#include <torch/csrc/swapper/include/Swapper.h>
#include <torch/csrc/swapper/include/Meta_Tensor.h>
#include <torch/csrc/swapper/include/Advisor.h>

#include <torch/csrc/DynamicTypes.h>
#include <torch/csrc/PtrWrapper.h>
#include <torch/csrc/THP.h>
#include <torch/csrc/autograd/edge.h>
#include <torch/csrc/autograd/function.h>
#include <torch/csrc/autograd/python_anomaly_mode.h>
#include <torch/csrc/autograd/python_function.h>
#include <torch/csrc/utils/auto_gil.h>
#include <torch/csrc/autograd/functions/basic_ops.h>
#include <torch/csrc/autograd/generated/Functions.h>
#include <torch/csrc/autograd/python_variable.h>
#include <torch/csrc/autograd/python_function.h>
#include <torch/csrc/autograd/python_cpp_function.h>
#include <torch/csrc/autograd/python_hook.h>

#include <torch/csrc/autograd/functions/accumulate_grad.h>

#include <cuda.h>
#include <cuda_runtime_api.h>

#include <queue>
#include <set>
#include <unordered_set>
#include <utility>

#include <unistd.h>

#include <torch/csrc/swapper/include/utils/z_debug.h>


using namespace torch::swapper;

namespace torch{
namespace swapper{

struct THPSwapper {
    PyObject_HEAD
};

Meta_Tensor meta_tensor_py2c(PyObject* obj){
    void* ptr = (void*)PyLong_AsUnsignedLong(PyTuple_GetItem(obj, 0));
    size_t nbyte = PyLong_AsUnsignedLong(PyTuple_GetItem(obj, 1));
    return std::pair<at::DataPtr*, size_t>((at::DataPtr*)ptr, nbyte);
}
PyObject* meta_tensor_c2py(Meta_Tensor meta_tensor){
    return PyTuple_Pack(
        2,
        PyLong_FromUnsignedLong((unsigned long)(meta_tensor.first)),
        PyLong_FromUnsignedLong(meta_tensor.second)
    );
}

auto get_1D_list_int = [](PyObject* m){
    std::vector<int> ans;
    Py_ssize_t lens = PyList_GET_SIZE(m);
    for (int i = 0; i < lens; i++){
        PyObject* item = PyList_GET_ITEM(m, i);
        int v = PyInt_AsLong(item);
        ans.emplace_back(v);
    }
    return ans;
};
auto get_2D_matrix_int = [](PyObject* m){
    std::vector<std::vector<int>> ans;
    Py_ssize_t lens = PyList_GET_SIZE(m);
    for (int i = 0; i < lens; i++){
        PyObject* line = PyList_GET_ITEM(m, i);
        ans.emplace_back(get_1D_list_int(line));
    }
    return ans;
};
auto get_1D_list_float = [](PyObject* m){
    std::vector<float> ans;
    Py_ssize_t lens = PyList_GET_SIZE(m);
    for (int i = 0; i < lens; i++){
        PyObject* item = PyList_GET_ITEM(m, i);
        float v = PyFloat_AsDouble(item);
        ans.emplace_back(v);
    }
    return ans;
};
auto get_2D_matrix_float = [](PyObject* m){
    std::vector<std::vector<float>> ans;
    Py_ssize_t lens = PyList_GET_SIZE(m);
    for (int i = 0; i < lens; i++){
        PyObject* line = PyList_GET_ITEM(m, i);
        ans.emplace_back(get_1D_list_float(line));
    }
    return ans;
};

// *****************************
// 在下面扩展c++暴露给python端的接口
// *****************************
PyObject *THPSwapper_get_name(){
    return THPUtils_packString(get_swapper()->get_name());
}
PyObject* THPForward_begin(){
    get_mem_tracker()->restart_memory_threshold();
    get_advisor()->update_iteration();
    Py_RETURN_NONE;
}
PyObject* THPReset_idx(){
    get_advisor()->reset_idx();
    Py_RETURN_NONE;
}
PyObject* THPEnable_hybrid_swap(){

    get_advisor()->open_hybrid_swap();
    Py_RETURN_NONE;
}

PyObject* THPBackward_begin(){
    get_mem_tracker()->close_memory_threshold();    
    Py_RETURN_NONE;
}
PyObject* THPSwapper_wait_until_ok(THPSwapper *self, PyObject* args, PyObject* kwargs){
    PyObject* targets = nullptr;
    int status;
    const char* accepted_kwargs[] = {
        "targets", "status", nullptr
    };
    if(!PyArg_ParseTupleAndKeywords(args, kwargs, "Oi", (char**)accepted_kwargs,
        &targets, &status)){
        return nullptr;
    }
    THPUtils_assert(PyList_Check(targets), "targets argument is expected to "
        "be a list, but got %s", THPUtils_typename(targets));
    Py_ssize_t num_targets = PyList_GET_SIZE(targets);
    std::vector<torch::autograd::Variable> targets_list;
    for (int i = 0; i < num_targets; i++){
        PyObject* target = PyList_GET_ITEM(targets, i);
        auto& variable = ((THPVariable*)target)->cdata;
        auto tensor = variable.tensor_data();
        wait_until_ok_by_target(tensor, status);
    }
    Py_RETURN_NONE;
}

PyObject* THPSwapper_wait_until_ok_by_layers(THPSwapper *self, PyObject* args, PyObject* kwargs){
    PyObject* targets = nullptr;
    int status;
    const char* accepted_kwargs[] = {
        "targets", "status", nullptr
    };
    if(!PyArg_ParseTupleAndKeywords(args, kwargs, "Oi", (char**)accepted_kwargs,
        &targets)){
        return nullptr;
    }
    THPUtils_assert(PyList_Check(targets), "targets argument is expected to "
        "be a list, but got %s", THPUtils_typename(targets));
    Py_ssize_t num_targets = PyList_GET_SIZE(targets);
    std::vector<torch::autograd::Variable> targets_list;
    for (int i = 0; i < num_targets; i++){
        PyObject* target = PyList_GET_ITEM(targets, i);
        auto fn = ((torch::autograd::THPCppFunction*)target)->cdata;
        wait_until_ok_by_node(fn.get(), status);
    }
    Py_RETURN_NONE;
}

PyObject* THPSwapper_search_metas_by_range(THPSwapper *self, PyObject* args, PyObject* kwargs){
    PyObject* start_pytensors = nullptr;
    PyObject* end_pytensors = nullptr;
    PyObject* excludes = nullptr; // 这些不做swap
    const char* accepted_kwargs[] = {
        "start_pytensors",
        "end_pytensors", 
        "excludes",
        nullptr
    };
    if(!PyArg_ParseTupleAndKeywords(args, kwargs, "OOO", (char**)accepted_kwargs,
        &start_pytensors, &end_pytensors, &excludes)){
        return nullptr;
    }
    Py_ssize_t num_excludes = PyList_GET_SIZE(excludes);
    SET_OF_PTR excluded_set;
    for (int i = 0; i < num_excludes; i++){
        PyObject* exclude = PyList_GET_ITEM(excludes, i);
        auto& variable = ((THPVariable*)exclude)->cdata;
        excluded_set.emplace(variable.data_ptr());
    }

    size_t start_pytensors_size = PyList_GET_SIZE(start_pytensors);
    size_t end_pytensors_size = PyList_GET_SIZE(end_pytensors);

    Z_DEBUG("start tensor size = %d\n", start_pytensors_size);
    Z_DEBUG("end tensor size = %d\n", end_pytensors_size);

    if(end_pytensors_size==0){
        Z_DEBUG("error: end tensor size is zero\n");
        return nullptr;
    }

    
    std::vector<at::Tensor> tensors_found;
    std::vector<torch::autograd::Variable> start_vars;
    std::vector<torch::autograd::Variable> end_vars;

    for(int i=0;i<start_pytensors_size;i++){
        PyObject *pytensor = PyList_GET_ITEM(start_pytensors, i);
        auto variable = ((THPVariable*)pytensor)->cdata;
        start_vars.push_back(std::move(variable));
    }
    
    for(int i=0;i<end_pytensors_size;i++){
        PyObject *pytensor = PyList_GET_ITEM(end_pytensors, i);
        auto variable = ((THPVariable*)pytensor)->cdata;
        end_vars.push_back(std::move(variable));
    }

    search_tensors_by_range(start_vars,end_vars,tensors_found);
    Z_DEBUG("Find tensor num = %d\n", tensors_found.size());

    auto ans = PyList_New(0);
    for (auto i = 0lu; i < tensors_found.size(); i++){
        auto &tensor = tensors_found[i];
        if (excluded_set.find(tensor.data_ptr())==excluded_set.end()){
            PyList_Append(
                ans,
                meta_tensor_c2py(make_meta_tensor(tensor))
            );
        }
    }
    return ans;
}
PyObject* THPSwapper_swap_out_by_range(THPSwapper *self, PyObject* args, PyObject* kwargs){
    // cudaStreamSynchronize(nullptr);
    PyObject* metas = THPSwapper_search_metas_by_range(self, args, kwargs);
    auto size = PyList_Size(metas);
    for (auto i = 0lu; i < size; i++){
        auto meta_py = PyList_GET_ITEM(metas, i);
        auto meta = meta_tensor_py2c(meta_py);
        swap_out_by_meta(meta);
    }
    return metas;
}

PyObject* THPSwapper_swap_out_by_metas(THPSwapper *self, PyObject* args, PyObject* kwargs){
    // cudaStreamSynchronize(nullptr);
    PyObject* targets = nullptr;
    PyObject* destinations = nullptr; 
    PyObject* ratios = nullptr; 
    const char* accepted_kwargs[] = {
        "targets", "destinations", "ratios", nullptr
    };
    if(!PyArg_ParseTupleAndKeywords(args, kwargs, "OOO", (char**)accepted_kwargs,
        &targets, &destinations, &ratios)){
        return nullptr;
    }
    
    auto v_destinations = get_1D_list_int(destinations);
    auto v_ratios = get_1D_list_float(ratios);
    if (v_ratios.size()>0){
        THPUtils_assert(v_ratios.size() == v_destinations.size(), "ratios and destinations should have the same size");
        auto sum = std::accumulate(v_ratios.begin(), v_ratios.end(), 0.0);
        for (auto &v:v_ratios){
            v /= sum;
        }
    }
    
    THPUtils_assert(PyList_Check(targets), "targets argument is expected to "
        "be a list, but got %s", THPUtils_typename(targets));
    Py_ssize_t num_targets = PyList_GET_SIZE(targets);
    std::vector<torch::autograd::Variable> targets_list;
    for (int i = 0; i < num_targets; i++){
        PyObject* target = PyList_GET_ITEM(targets, i);
        auto meta = meta_tensor_py2c(target);
        swap_out_by_meta(meta, v_destinations, v_ratios);
    }
    Py_RETURN_NONE;
}
PyObject* THPSwapper_swap_in_by_metas(THPSwapper *self, PyObject* args, PyObject* kwargs){
    PyObject* targets = nullptr;
    const char* accepted_kwargs[] = {
        "targets", nullptr
    };
    if(!PyArg_ParseTupleAndKeywords(args, kwargs, "O", (char**)accepted_kwargs,
        &targets)){
        return nullptr;
    }
    THPUtils_assert(PyList_Check(targets), "targets argument is expected to "
        "be a list, but got %s", THPUtils_typename(targets));
    Py_ssize_t num_targets = PyList_GET_SIZE(targets);
    std::vector<torch::autograd::Variable> targets_list;
    for (int i = 0; i < num_targets; i++){
        PyObject* target = PyList_GET_ITEM(targets, i);
        auto meta = meta_tensor_py2c(target);
        swap_in_by_meta(meta);
    }
    Py_RETURN_NONE;
}
PyObject* THPSwapper_tensor_to_meta(THPSwapper *self, PyObject* args, PyObject* kwargs){
    PyObject* target = nullptr;
    const char* accepted_kwargs[] = {
        "target", nullptr
    };
    if(!PyArg_ParseTupleAndKeywords(args, kwargs, "O", (char**)accepted_kwargs,
        &target)){
        return nullptr;
    }
    auto &variable = ((THPVariable*)target)->cdata;
    auto tensor = variable.tensor_data();
    auto meta = make_meta_tensor(tensor);
    return meta_tensor_c2py(make_meta_tensor(tensor));
}

PyObject* THPSwapper_set_debug_prefix(THPSwapper *self, PyObject* args, PyObject* kwargs){
    char* prefix_c;
    const char* accepted_kwargs[] = {
        "prefix", nullptr
    };
    if(!PyArg_ParseTupleAndKeywords(args, kwargs, "s", (char**)accepted_kwargs,
        &prefix_c)){
        return nullptr;
    }
    std::string prefix(prefix_c);
    z_env.set_prefix(prefix);
    Z_TMP("prefix=%s\n", prefix.c_str());
    Py_RETURN_NONE;
}

PyObject* THPSwapper_print_str(THPSwapper *self, PyObject* args, PyObject* kwargs){
    char* str_c;
    const char* accepted_kwargs[] = {
        "str", nullptr
    };
    if(!PyArg_ParseTupleAndKeywords(args, kwargs, "s", (char**)accepted_kwargs,
        &str_c)){
        return nullptr;
    }
    printf("%s\n", str_c);
    Py_RETURN_NONE;
}
PyObject* THPSwapper_set_swap_out_bytes(THPSwapper *self, PyObject* args, PyObject* kwargs){
    unsigned long bytes;
    const char* accepted_kwargs[] = {
        "bytes", nullptr
    };
    if(!PyArg_ParseTupleAndKeywords(args, kwargs, "k", (char**)accepted_kwargs,
        &bytes)){
        return nullptr;
    }
    get_swapper()->set_swap_out_bytes(bytes);
    Py_RETURN_NONE;
}
PyObject* THPSwapper_get_swap_out_bytes(THPSwapper *self, PyObject* args, PyObject* kwargs){
    auto bytes = get_swapper()->get_swap_out_bytes();
    return PyLong_FromUnsignedLong(bytes);
}

PyObject *wrap_variable(torch::autograd::Variable& variable)
{
    THPObjectPtr var(THPVariable_Wrap(variable));
    if (!var) throw python_error();
    return var.release();
}

PyObject* THPSwapper_register_optimize_func(THPSwapper *self, PyObject* args, PyObject* kwargs){
    PyObject* target = nullptr;
    const char* accepted_kwargs[] = {
        "target", nullptr
    };
    if(!PyArg_ParseTupleAndKeywords(args, kwargs, "O", (char**)accepted_kwargs,
        &target)){
        return nullptr;
    }
    std::function<void(torch::autograd::Variable&)> f = [=](torch::autograd::Variable& variable){
        auto obj = variable.pyobj();
        Py_INCREF(obj);
        THPObjectPtr var(obj);
        PyObject* t = var.release();
        AutoGIL gil;
        THPObjectPtr res2(PyObject_CallFunctionObjArgs(target, t, nullptr));
        Py_DECREF(obj);
    };
    set_optimize(f);
    Z_DEBUG("set optimize done!\n");
    Py_RETURN_NONE;
}



PyObject* THPSwapper_swap_out_by_targets(THPSwapper *self, PyObject* args, PyObject* kwargs){
    // cudaStreamSynchronize(nullptr);
    PyObject* targets = nullptr;
    const char* accepted_kwargs[] = {
        "targets", nullptr
    };
    if(!PyArg_ParseTupleAndKeywords(args, kwargs, "O", (char**)accepted_kwargs,
        &targets)){
        return nullptr;
    }
    THPUtils_assert(PyList_Check(targets), "targets argument is expected to "
        "be a list, but got %s", THPUtils_typename(targets));
    Py_ssize_t num_targets = PyList_GET_SIZE(targets);
    std::vector<torch::autograd::Variable> targets_list;
    for (int i = 0; i < num_targets; i++){
        PyObject* target = PyList_GET_ITEM(targets, i);
        auto& variable = ((THPVariable*)target)->cdata;
        auto tensor = variable.tensor_data();
        swap_out_by_target(tensor);
    }
    Py_RETURN_NONE;
}
PyObject* THPSwapper_swap_in_by_targets(THPSwapper *self, PyObject* args, PyObject* kwargs){
    PyObject* targets = nullptr;
    const char* accepted_kwargs[] = {
        "targets", nullptr
    };
    if(!PyArg_ParseTupleAndKeywords(args, kwargs, "O", (char**)accepted_kwargs,
        &targets)){
        return nullptr;
    }
    THPUtils_assert(PyList_Check(targets), "targets argument is expected to "
        "be a list, but got %s", THPUtils_typename(targets));
    Py_ssize_t num_targets = PyList_GET_SIZE(targets);
    std::vector<torch::autograd::Variable> targets_list;
    for (int i = 0; i < num_targets; i++){
        PyObject* target = PyList_GET_ITEM(targets, i);
        auto& variable = ((THPVariable*)target)->cdata;
        auto tensor = variable.tensor_data();
        swap_in_by_target(tensor);
    }
    Py_RETURN_NONE;
}

PyObject* THPSwapper_swap_out_by_layers(THPSwapper *self, PyObject* args, PyObject* kwargs){
    using namespace torch::autograd;
    // cudaStreamSynchronize(nullptr);
    PyObject* targets = nullptr; // layers的输出
    PyObject* excludes = nullptr; // 这些不做swap
    PyObject* destinations = nullptr; 
    PyObject* ratios = nullptr; 
    const char* accepted_kwargs[] = {
        "targets", "excludes", "destinations", "ratios", nullptr
    };
    if(!PyArg_ParseTupleAndKeywords(args, kwargs, "OOOO", (char**)accepted_kwargs,
        &targets, &excludes, &destinations, &ratios)){
        return nullptr;
    }
    Py_ssize_t num_targets = PyList_GET_SIZE(targets);
    Py_ssize_t num_excludes = PyList_GET_SIZE(excludes);
    auto v_destinations = get_1D_list_int(destinations);
    auto v_ratios = get_1D_list_float(ratios);
    if (v_ratios.size()>0){
        THPUtils_assert(v_ratios.size() == v_destinations.size(), "ratios and destinations should have the same size");
        auto sum = std::accumulate(v_ratios.begin(), v_ratios.end(), 0.0);
        for (auto &v:v_ratios){
            v /= sum;
        }
    }

    SET_OF_PTR excluded_set;
    for (int i = 0; i < num_excludes; i++){
        PyObject* exclude = PyList_GET_ITEM(excludes, i);
        auto& variable = ((THPVariable*)exclude)->cdata;
        excluded_set.emplace(variable.data_ptr());
    }

    for (int i = 0; i < num_targets; i++){
        PyObject* target = PyList_GET_ITEM(targets, i);
        auto fn = ((torch::autograd::THPCppFunction*)target)->cdata;
        Z_INFO("swap_out_by_node: %s\n", fn->name().c_str());
        swap_out_by_node(fn.get(), excluded_set, v_destinations, v_ratios);
    }
    Py_RETURN_NONE;
}


PyObject* THPSwapper_swap_in_by_layers(THPSwapper *self, PyObject* args, PyObject* kwargs){
    using namespace torch::autograd;
    PyObject* targets = nullptr; // layers的输出
    PyObject* excludes = nullptr; // 这些不做swap
    const char* accepted_kwargs[] = {
        "targets", "excludes", nullptr
    };
    if(!PyArg_ParseTupleAndKeywords(args, kwargs, "OO", (char**)accepted_kwargs,
        &targets, &excludes)){
        return nullptr;
    }
    THPUtils_assert(PyList_Check(targets), "targets argument is expected to "
        "be a list, but got %s", THPUtils_typename(targets));
    THPUtils_assert(PyList_Check(excludes), "excludes argument is expected to "
        "be a list, but got %s", THPUtils_typename(excludes));
    Py_ssize_t num_targets = PyList_GET_SIZE(targets);
    Py_ssize_t num_excludes = PyList_GET_SIZE(excludes);

    SET_OF_PTR excluded_set;
    for (int i = 0; i < num_excludes; i++){
        PyObject* exclude = PyList_GET_ITEM(excludes, i);
        auto& variable = ((THPVariable*)exclude)->cdata;
        excluded_set.emplace(variable.data_ptr());
    }

    for (int i = 0; i < num_targets; i++){
        PyObject* target = PyList_GET_ITEM(targets, i);
        auto fn = ((torch::autograd::THPCppFunction*)target)->cdata;
        Z_INFO("swap_in_by_node: %s\n", fn->name().c_str());
        swap_in_by_node(fn.get(), excluded_set);
    }
    Py_RETURN_NONE;
}


PyObject* THPSwapper_pass_grad_fn(THPSwapper *self, PyObject* args, PyObject* kwargs){
    PyObject* targets = nullptr;
    const char* accepted_kwargs[] = {
        "targets", nullptr
    };
    if(!PyArg_ParseTupleAndKeywords(args, kwargs, "O", (char**)accepted_kwargs,
        &targets)){
        return nullptr;
    }
    THPUtils_assert(PyList_Check(targets), "targets argument is expected to "
        "be a list, but got %s", THPUtils_typename(targets));
    Py_ssize_t num_targets = PyList_GET_SIZE(targets);
    std::vector<torch::autograd::Variable> targets_list;
    for (int i = 0; i < num_targets; i++){
        PyObject* target = PyList_GET_ITEM(targets, i);
        auto fn = ((torch::autograd::THPCppFunction*)target)->cdata;
        Z_DEBUG("fn->name()=%s\n", fn->name().c_str());
        // auto fn = ((THPCppFunction*)target)->cdata;
        // auto& variable = ((THPVariable*)target)->cdata;
        // auto tensor = variable.tensor_data();
        // swap_in_by_target(tensor);
    }
    Py_RETURN_NONE;
}

PyObject* THPSwapper_set_device_status(THPSwapper *self, PyObject* args, PyObject* kwargs){
    PyObject* device_list;
    PyObject* spare_memory_list;
    const char* accepted_kwargs[] = {
        "device_list",
        "spare_memory_list",
        nullptr
    };
    if(!PyArg_ParseTupleAndKeywords(args, kwargs, "OO", (char**)accepted_kwargs,
        &device_list, &spare_memory_list)){
        return nullptr;
    }
    auto device_list_size = PyList_GET_SIZE(device_list);
    auto spare_memory_list_size = PyList_GET_SIZE(spare_memory_list);
    if(device_list_size != spare_memory_list_size){
        AT_ERROR("Set device status error: device list length not equal to spare memory list length\n");
    }
    std::unordered_map<int, unsigned long long> spare_memory_table;
    for(int i=0;i<device_list_size;i++){
        spare_memory_table.insert(
            std::pair<int, unsigned long long> (
                PyInt_AsLong(PyList_GetItem(device_list,i)),
                (unsigned long long)PyLong_AsLong(PyList_GetItem(spare_memory_list,i))
            )
        );
    }
    get_advisor()->set_device_status(spare_memory_table);
    Py_RETURN_NONE;
}

PyObject* THPSwapper_init_swapper(THPSwapper *self, PyObject* args, PyObject* kwargs){
    // how to pass integers/strings from Python to C? 
    // reference: https://ishantheperson.github.io/posts/python-c-ext-2/
    int default_device_id;
    unsigned long cuda_memory_threshold;
    int num_cards;
    int swap_mode;
    int sample_point;
    PyObject* candidates;
    PyObject* connectivity_matrix;
    PyObject* speed_matrix;
    const char* accepted_kwargs[] = {
        "default_device_id", "cuda_memory_threshold", "num_cards", "swap_mode", 
        "sample_point", "candidates", "connectivity_matrix", "speed_matrix",
        nullptr
    };
    if(!PyArg_ParseTupleAndKeywords(args, kwargs, "ikiiiOOO", (char**)accepted_kwargs,
        &default_device_id, &cuda_memory_threshold, &num_cards, &swap_mode, &sample_point,
        &candidates, &connectivity_matrix, &speed_matrix)){
        return nullptr;
    }
    cudaSetDevice(default_device_id);
    auto con_matrix = get_2D_matrix_int(connectivity_matrix); // pass it to advisor later
    auto spd_matrix = get_2D_matrix_float(speed_matrix);
    auto candidates_ = get_1D_list_int(candidates);

    cudaSetDevice(default_device_id);

    get_swapper()->set_default_device_id(default_device_id);

    get_mem_tracker()->set_runtime_args(cuda_memory_threshold, default_device_id);

    get_copy_engine()->set_default_device_id(default_device_id);
    get_copy_engine()->set_candidates(candidates_);
    get_copy_engine()->init_self();

    get_advisor()->set_connectivity_matrix(con_matrix);
    get_advisor()->set_speed_matrix(spd_matrix);
    get_advisor()->set_num_cards(num_cards);
    get_advisor()->set_candidates(candidates_);
    get_advisor()->set_default_device_id(default_device_id);
    get_advisor()->set_swap_mode(SWAP_MODE(swap_mode));
    get_advisor()->set_sample_point(sample_point);
    get_advisor()->init_advisor_after_send_settings();
    get_advisor()->print_settings();
    Py_RETURN_NONE;
}

PyObject* THPSwapper_print_info(THPSwapper *self, PyObject* args, PyObject* kwargs){
    PyObject* targets = nullptr;
    const char* accepted_kwargs[] = {
        "targets", nullptr
    };
    if(!PyArg_ParseTupleAndKeywords(args, kwargs, "O", (char**)accepted_kwargs,
        &targets)){
        return nullptr;
    }
    THPUtils_assert(PyList_Check(targets), "targets argument is expected to "
        "be a list, but got %s", THPUtils_typename(targets));
    Py_ssize_t num_targets = PyList_GET_SIZE(targets);
    std::vector<torch::autograd::Variable> targets_list;
    for (int i = 0; i < num_targets; i++){
        PyObject* target = PyList_GET_ITEM(targets, i);
        auto& variable = ((THPVariable*)target)->cdata;
        auto tensor = variable.tensor_data();
        printf("Tensor %d, pointer=%p, tensor.device:[%d:%d], dataptr.device:[%d:%d]\n", i, tensor.data_ptr(), (int)tensor.device().type(), (int)tensor.device().index(),
            (int)tensor.unsafeGetTensorImpl()->get_storage().get_data_ptr().device().type(),
            (int)tensor.unsafeGetTensorImpl()->get_storage().get_data_ptr().device().index()
            );
    }

    Py_RETURN_NONE;
}

PyObject* THPSwapper_get_true_device_index(THPSwapper* self, PyObject* args, PyObject* kwargs){
    PyObject* target = nullptr;
    const char* accepted_kwargs[] = {
        "target", nullptr
    };
    if (!PyArg_ParseTupleAndKeywords(args, kwargs, "O", (char**)accepted_kwargs, &target)){
        return nullptr;
    }
    auto&variable = ((THPVariable*)target)->cdata;
    auto tensor = variable.tensor_data();
    long device_index = (long)tensor.unsafeGetTensorImpl()->get_storage().get_data_ptr().device().index();
    return PyInt_FromLong(device_index);
}



std::string tharray2string(at::IntArrayRef a){
    std::ostringstream os;
    for (int i = 0; i < a.size()-1; i++){
        os << a[i] << ",";
    }
    if (a.size()>0) os << a[a.size()-1];
    return os.str();
}
void analyze_maxpooling(torch::autograd::Node* grad_fn){
    auto& self = ((torch::autograd::generated::MaxPool2DWithIndicesBackward*)grad_fn)->self_.get_data();
    auto& result = ((torch::autograd::generated::MaxPool2DWithIndicesBackward*)grad_fn)->result1_.get_data();


    Z_DEBUG("Tensor self information: \n");
    
    Z_DEBUG("dimension = %s size = %d device_index = %d type = %s\n", tharray2string(self.sizes()).c_str(), self.numel(), self.device().index(), self.toString().c_str());
    Z_DEBUG("self value = ")
    for(int i=0; i<self.sizes()[0];i++){
        for(int j=0; j<self.sizes()[1];j++){
            for(int k=0; k<self.sizes()[2];k++){
                for(int l=0; l<self.sizes()[3];l++){
                    printf("%f  ", self[i][j][k][l].item().toFloat());
                }
            }
        }
    }
    printf("\n");
    

    Z_DEBUG("Tensor result information: \n");
    Z_DEBUG("dimension = %s size = %d device_index = %d type = %s\n", tharray2string(result.sizes()).c_str(), result.numel(), result.device().index(), result.toString().c_str());

    Z_DEBUG("result value = ")
    for(int i=0; i<result.sizes()[0];i++){
        for(int j=0; j<result.sizes()[1];j++){
            for(int k=0; k<result.sizes()[2];k++){
                for(int l=0; l<result.sizes()[3];l++){
                    printf("%ld  ", result[i][j][k][l].item().toLong());
                }
                
            }
        }
    }
    printf("\n");
            // ans.push_back({((MaxPool2DWithIndicesBackward*)node)->result1_});
}


std::map<std::string, void (*) (torch::autograd::Node*)> analyze_function_map={
    {"MaxPool2DWithIndicesBackward", analyze_maxpooling}
}; 
PyObject* THPSwapper_analyze_operator(THPSwapper* self, PyObject* args, PyObject* kwargs){
    PyObject* target = nullptr;
    const char* accepted_kwargs[] = {
        "target", nullptr
    };
    if (!PyArg_ParseTupleAndKeywords(args, kwargs, "O", (char**)accepted_kwargs, &target)){
        return nullptr;
    }
    auto& grad_fn = ((torch::autograd::THPCppFunction*)target)->cdata;
    Z_DEBUG("Swapper is analyzing grad function: %s\n", grad_fn->name().c_str());
    
    auto it = analyze_function_map.find(grad_fn->name());
    if(it != analyze_function_map.end()){
        it->second(grad_fn.get());
    } else {
        Z_WARN("Swapper do not support to analyze grad function: %s\n", grad_fn->name().c_str());
    }

    // auto fn = analyze_function_map[grad_fn->name()];
    Py_RETURN_NONE;
}

PyObject* get_infos_about_tensor(at::Tensor& tensor){
    auto& dptr = tensor.unsafeGetTensorImpl()->get_storage().get_data_ptr();
    auto ans = PyList_New(0);
    PyList_Append(ans, PyLong_FromUnsignedLong((unsigned long)tensor.data_ptr()));
    PyList_Append(ans, PyLong_FromUnsignedLong(tensor.nbytes()));
    PyList_Append(ans, PyInt_FromLong((unsigned long)dptr.device().type()));
    PyList_Append(ans, PyInt_FromLong((unsigned long)dptr.device().index()));
    PyList_Append(ans, PyInt_FromLong((unsigned long)dptr.get_status()));
    return ans;
}

PyObject* get_id(at::Tensor& tensor){
    auto &dptr = tensor.unsafeGetTensorImpl()->get_storage().get_data_ptr();
    return PyInt_FromLong((unsigned long)&dptr);
}
PyObject* THPSwapper_get_id_of_tensor(THPSwapper* self, PyObject* args, PyObject* kwargs){
    PyObject* target = nullptr;
    const char* accepted_kwargs[] = {
        "target", nullptr
    };
    if (!PyArg_ParseTupleAndKeywords(args, kwargs, "O", (char**)accepted_kwargs, &target)){
        return nullptr;
    }
    // auto&variable=((THPVariable*)target)->cdata;
    unsigned long id =((THPVariable*)target)->cdata.get_id();
    return PyInt_FromLong(id);
}
/*
    return [ptr, nbytes, device_type, device_index, stauts]
*/
PyObject* THPSwapper_get_infos_about_tensor(THPSwapper* self, PyObject* args, PyObject* kwargs){
    PyObject* target = nullptr;
    const char* accepted_kwargs[] = {
        "target", nullptr
    };
    if (!PyArg_ParseTupleAndKeywords(args, kwargs, "O", (char**)accepted_kwargs, &target)){
        return nullptr;
    }
    auto&variable=((THPVariable*)target)->cdata;
    auto tensor = variable.tensor_data();
    return get_infos_about_tensor(tensor);
}
/**
 * @return {PyObject*} :  [ptr, name, [var1, var2, ..]]
 */
PyObject* THPSwapper_get_infos_about_node(THPSwapper* self, PyObject* args, PyObject* kwargs){
    PyObject* target = nullptr;
    const char* accepted_kwargs[] = {
        "target", nullptr
    };
    if (!PyArg_ParseTupleAndKeywords(args, kwargs, "O", (char**)accepted_kwargs, &target)){
        return nullptr;
    }
    auto& grad_fn = ((torch::autograd::THPCppFunction*)target)->cdata;

    auto ans = PyList_New(0);
    PyList_Append(ans, PyLong_FromUnsignedLong((unsigned long)grad_fn.get()));
    PyList_Append(ans, PyBytes_FromString(grad_fn->name().c_str()));
    auto saved_variables = get_Vars(grad_fn.get());
    auto vars = PyList_New(0);
    for (auto saved_variable: saved_variables){
        PyList_Append(vars, get_infos_about_tensor(saved_variable.get_tensor()));
    }
    PyList_Append(ans, vars);
    return ans;
}


PyObject* THPSwapper_record_state(THPSwapper* self, PyObject* args, PyObject* kwargs){
    PyObject* weight = nullptr;
    PyObject* state = nullptr;
    const char* accepted_kwargs[] = {
        "weight", "state", nullptr
    };
    if (!PyArg_ParseTupleAndKeywords(args, kwargs, "OO", (char**)accepted_kwargs, 
        &weight, &state)){
        return nullptr;
    }
    // auto&variable=((THPVariable*)target)->cdata;
    // unsigned long id =((THPVariable*)target)->cdata
    auto&var_weight = ((THPVariable*)weight)->cdata;
    auto&var_state = ((THPVariable*)state)->cdata;
    record_state_of_weight(var_weight, var_state);
    Py_RETURN_NONE;
}

PyObject* THPSwapper_launch_swap_in(THPSwapper* self, PyObject* args, PyObject* kwargs){
    HANDLE_TH_ERRORS
    PyObject* end_tensors = nullptr;
    PyObject* start_tensors = nullptr;
    const char *accepted_kwargs[]={
        "end_tensors", "start_tensors", nullptr
    };
    if (!PyArg_ParseTupleAndKeywords(args, kwargs, "OO", (char**)accepted_kwargs,
        &end_tensors, &start_tensors))
        return nullptr;
    Py_ssize_t num_end_tensors = PyList_GET_SIZE(end_tensors);
    if (num_end_tensors==0){
        Py_RETURN_NONE;
    }
    torch::autograd::edge_list roots;
    roots.reserve(num_end_tensors);
    for (int i = 0; i < num_end_tensors; i++){
        PyObject* _tensor = PyList_GET_ITEM(end_tensors, i);
        auto&variable = ((THPVariable*)_tensor)->cdata;
        auto gradient_edge = variable.gradient_edge();
        roots.push_back(std::move(gradient_edge));
    }

    // compute dependencies
    std::unordered_map<torch::autograd::Node*, int> dependencies_;
    std::unordered_set<torch::autograd::Node*> seen;
    std::vector<torch::autograd::Node*> queue;
    auto compute_edge = [&](const torch::autograd::Edge& edge){
        if (auto next_ptr = edge.function.get()){
            dependencies_[next_ptr] += 1;
            const bool was_inserted = seen.insert(next_ptr).second;
            if (was_inserted) queue.push_back(next_ptr);
        }
    };
    for (const auto&edge: roots){
        compute_edge(edge);
    }
    while (!queue.empty()){
        auto fn = queue.back(); queue.pop_back();
        int cnt = 0;
        for (const auto&edge:fn->next_edges()){
            compute_edge(edge);
        }
    }
    
    
    // traverse graph
    std::vector<torch::autograd::Node*> node_in_backward_order;
    queue.clear();

    auto traverse_edge = [&](const torch::autograd::Edge& next){
        auto ptr = next.function.get();
        if (!ptr) return;
        auto it = dependencies_.find(ptr);
        if (it==dependencies_.end()){
            auto name = next.function->name();
            throw std::runtime_error(std::string("dependency not found for ")+name);
        }
        else if (--it->second==0){
            dependencies_.erase(it);
            queue.emplace_back(next.function.get());
        }
    };
    for (const auto&next: roots){
        traverse_edge(next);
    }
    while (!queue.empty()){
        auto fn = queue.back(); queue.pop_back();
        node_in_backward_order.emplace_back(fn);
        for (const auto&edge: fn->next_edges()){
            traverse_edge(edge);    
        }
    }
    Z_DEBUG("launch_swap_in automatically finds %d tensors waited to be swapped in!\n", node_in_backward_order.size());
    // launch swap in
    for (int i = 0; i < node_in_backward_order.size(); i++){
        auto&node = node_in_backward_order[i];
        // Z_DEBUG("launch swap in for node: %s\n", node->name().c_str());
        if (node->name()=="torch::autograd::AccumulateGrad"){
            auto&weight = ((torch::autograd::AccumulateGrad*)node)->variable;
            auto states = get_states_of_weight(weight);
            for (auto&state:states){
                swap_in_by_target(state);
            }
            auto tensor_of_weight = weight.tensor_data();
            swap_in_by_target(tensor_of_weight);
        }
        else{
            auto vars = get_Vars(node);
            for (auto&var:vars){
                auto& t = var.get_tensor();
                swap_in_by_target(t);
            }
        }
    }

    Py_RETURN_NONE;
    END_HANDLE_TH_ERRORS
}


static struct PyMethodDef THPEngine_methods[] = {
    {(char*)"name", (PyCFunction)THPSwapper_get_name, METH_NOARGS, nullptr},
    {(char*)"forward_begin", (PyCFunction)THPForward_begin, METH_NOARGS, nullptr},
    {(char*)"backward_begin", (PyCFunction)THPBackward_begin, METH_NOARGS, nullptr},
    {(char*)"reset_advisor_index", (PyCFunction)THPReset_idx, METH_NOARGS, nullptr},
    {(char*)"enable_hybrid_swap", (PyCFunction)THPEnable_hybrid_swap, METH_NOARGS, nullptr},
    {(char*)"wait_until_ok_by_layers", (PyCFunction)THPSwapper_wait_until_ok_by_layers, METH_VARARGS|METH_KEYWORDS, nullptr},
    {(char*)"wait_until_ok", (PyCFunction)THPSwapper_wait_until_ok, METH_VARARGS|METH_KEYWORDS, nullptr},
    {(char*)"tensor_to_meta", (PyCFunction)THPSwapper_tensor_to_meta, METH_VARARGS|METH_KEYWORDS, nullptr},
    {(char*)"search_metas_by_range", (PyCFunction)THPSwapper_search_metas_by_range, METH_VARARGS|METH_KEYWORDS, nullptr},
    // {(char*)"swap_out_by_range", (PyCFunction)THPSwapper_swap_out_by_range, METH_VARARGS|METH_KEYWORDS, nullptr},
    {(char*)"swap_out_by_metas", (PyCFunction)THPSwapper_swap_out_by_metas, METH_VARARGS|METH_KEYWORDS, nullptr},
    {(char*)"swap_out_by_targets", (PyCFunction)THPSwapper_swap_out_by_targets, METH_VARARGS|METH_KEYWORDS, nullptr},
    {(char*)"swap_in_by_metas", (PyCFunction)THPSwapper_swap_in_by_metas, METH_VARARGS|METH_KEYWORDS, nullptr},
    {(char*)"swap_in_by_targets", (PyCFunction)THPSwapper_swap_in_by_targets, METH_VARARGS|METH_KEYWORDS, nullptr},
    {(char*)"swap_out_by_layers", (PyCFunction)THPSwapper_swap_out_by_layers, METH_VARARGS|METH_KEYWORDS, nullptr},
    {(char*)"swap_in_by_layers", (PyCFunction)THPSwapper_swap_in_by_layers, METH_VARARGS|METH_KEYWORDS, nullptr},
    {(char*)"pass_grad_fn", (PyCFunction)THPSwapper_pass_grad_fn, METH_VARARGS|METH_KEYWORDS, nullptr},
    {(char*)"init_swapper", (PyCFunction)THPSwapper_init_swapper, METH_VARARGS|METH_KEYWORDS, nullptr},
    {(char*)"print_info", (PyCFunction)THPSwapper_print_info, METH_VARARGS|METH_KEYWORDS, nullptr},
    {(char*)"get_true_device_index", (PyCFunction)THPSwapper_get_true_device_index, METH_VARARGS|METH_KEYWORDS, nullptr},
    {(char*)"analyze_operator", (PyCFunction)THPSwapper_analyze_operator, METH_VARARGS|METH_KEYWORDS, nullptr},
    {(char*)"get_infos_about_tensor", (PyCFunction)THPSwapper_get_infos_about_tensor, METH_VARARGS|METH_KEYWORDS, nullptr},
    {(char*)"get_id_of_tensor", (PyCFunction)THPSwapper_get_id_of_tensor, METH_VARARGS|METH_KEYWORDS, nullptr},
    {(char*)"get_infos_about_node", (PyCFunction)THPSwapper_get_infos_about_node, METH_VARARGS|METH_KEYWORDS, nullptr},
    {(char*)"set_debug_prefix", (PyCFunction)THPSwapper_set_debug_prefix, METH_VARARGS|METH_KEYWORDS, nullptr},
    {(char*)"print_str", (PyCFunction)THPSwapper_print_str, METH_VARARGS|METH_KEYWORDS, nullptr},
    {(char*)"register_optimize_func", (PyCFunction)THPSwapper_register_optimize_func, METH_VARARGS|METH_KEYWORDS, nullptr},
    {(char*)"launch_swap_in", (PyCFunction)THPSwapper_launch_swap_in, METH_VARARGS|METH_KEYWORDS, nullptr},
    {(char*)"record_state", (PyCFunction)THPSwapper_record_state, METH_VARARGS|METH_KEYWORDS, nullptr},
    {(char*)"set_swap_out_bytes", (PyCFunction)THPSwapper_set_swap_out_bytes, METH_VARARGS|METH_KEYWORDS, nullptr},
    {(char*)"get_swap_out_bytes", (PyCFunction)THPSwapper_get_swap_out_bytes, METH_VARARGS|METH_KEYWORDS, nullptr},
    {(char*)"set_device_status", (PyCFunction)THPSwapper_set_device_status, METH_VARARGS|METH_KEYWORDS, nullptr},
    {nullptr}
};

// *****************************
// 下面是用来初始化模块的
// *****************************

PyObject *THPSwapper_new(PyTypeObject *type, PyObject *args, PyObject *kwargs)
{
  return type->tp_alloc(type, 0);
}
PyTypeObject THPSwapperType = {
  PyVarObject_HEAD_INIT(nullptr, 0)
  "torch._C._SwapperBase",                /* tp_name */
  sizeof(THPSwapper),                     /* tp_basicsize */
  0,                                     /* tp_itemsize */
  nullptr,                                     /* tp_dealloc */
  0,                                     /* tp_print */
  nullptr,                                     /* tp_getattr */
  nullptr,                                     /* tp_setattr */
  nullptr,                                     /* tp_reserved */
  nullptr,                                     /* tp_repr */
  nullptr,                                     /* tp_as_number */
  nullptr,                                     /* tp_as_sequence */
  nullptr,                                     /* tp_as_mapping */
  nullptr,                                     /* tp_hash  */
  nullptr,                                     /* tp_call */
  nullptr,                                     /* tp_str */
  nullptr,                                     /* tp_getattro */
  nullptr,                                     /* tp_setattro */
  nullptr,                                     /* tp_as_buffer */
  Py_TPFLAGS_DEFAULT | Py_TPFLAGS_BASETYPE, /* tp_flags */
  nullptr,                               /* tp_doc */
  nullptr,                                     /* tp_traverse */
  nullptr,                                     /* tp_clear */
  nullptr,                                     /* tp_richcompare */
  0,                                     /* tp_weaklistoffset */
  nullptr,                                     /* tp_iter */
  nullptr,                                     /* tp_iternext */
  THPEngine_methods,                     /* tp_methods */
  nullptr,                                     /* tp_members */
  nullptr,                                     /* tp_getset */
  nullptr,                                     /* tp_base */
  nullptr,                                     /* tp_dict */
  nullptr,                                     /* tp_descr_get */
  nullptr,                                     /* tp_descr_set */
  0,                                     /* tp_dictoffset */
  nullptr,                                     /* tp_init */
  nullptr,                                     /* tp_alloc */
  THPSwapper_new                          /* tp_new */
};

bool THPSwapper_initModule(PyObject *module){
    if(PyType_Ready(&THPSwapperType) < 0){
        return false;
    }
    Py_INCREF(&THPSwapperType);
    PyModule_AddObject(module, "_Swapper", (PyObject *)&THPSwapperType);
    return true;
}
}
}