#include <torch/csrc/autograd/python_engine.h>

#include <torch/csrc/DynamicTypes.h>
#include <torch/csrc/PtrWrapper.h>
#include <torch/csrc/THP.h>
#include <torch/csrc/autograd/edge.h>
#include <torch/csrc/autograd/engine.h>
#include <torch/csrc/autograd/function.h>
#include <torch/csrc/autograd/python_variable.h>
#include <torch/csrc/autograd/generated/Functions.h>
#include <torch/csrc/autograd/python_anomaly_mode.h>
#include <torch/csrc/autograd/python_function.h>
#include <torch/csrc/utils/auto_gil.h>
#include <torch/csrc/autograd/custom_function.h>
#include <torch/csrc/autograd/functions/accumulate_grad.h>
#ifndef _WIN32
#include <pthread.h>
#endif

#include <unordered_set>
#include <memory> // for unique_ptr
#include "cuda_runtime.h"
using namespace torch::autograd;

struct THPEngine {
    PyObject_HEAD
};

static torch::autograd::python::PythonEngine engine;

static Engine& get_python_engine() {
  return engine;
}

namespace torch { namespace autograd { namespace python {

void PythonEngine::thread_init(int device) {
  // Create a PyThreadState, but release the GIL. This lets AutoGIL calls
  // inside thread_main acquire the GIL without having to create a new
  // PyThreadState each time.
  AutoGIL gil;
  AutoNoGIL no_gil;
  Engine::thread_init(device);
}

void PythonEngine::thread_on_exception(NodeTask& task, std::exception& e) {
  auto python_err = dynamic_cast<python_error*>(&e);
  if (python_err) {
    python_err->persist();
  }
  Engine::thread_on_exception(task, e);
}

std::unique_ptr<AnomalyMetadata> PythonEngine::make_anomaly_metadata() {
  return std::unique_ptr<AnomalyMetadata>(new PyAnomalyMetadata());
}

variable_list PythonEngine::execute(
    const edge_list& roots,
    const variable_list& inputs,
    bool keep_graph,
    bool create_graph,
    const edge_list& outputs) {
  try {
    return Engine::execute(roots, inputs, keep_graph, create_graph, outputs);
  } catch (python_error& e) {
    e.restore();
    throw;
  }
}

}}} // namespace torch::autograd::python

PyObject *THPEngineClass = nullptr;

static bool _reinitialize_engine = false;

static void _maybe_reinitialize_engine_after_fork() {
  // This is "probably" thread-safe because the flag is set in a fork handler
  // before any threads are created, and this function is only called with the
  // GIL held. However, using fork + threads is playing with fire so this is
  // more of a "best effort" thing. For example, if the fork occurs while the
  // backwards threads hold a lock, we'll probably deadlock in the engine
  // destructor.
  if (_reinitialize_engine) {
    engine.~PythonEngine();
    new (&engine) torch::autograd::python::PythonEngine();
    _reinitialize_engine = false;
  }
}

// std::string AccumulateGrad = "torch::autograd::AccumulateGrad";

#define PRINT_HELLO_WORLD \
        std::cout<<"hello world!"<<std::endl;

#define VECTOR_MUL(VEC) \
        std::accumulate(VEC.begin(),\
                        VEC.end(),\
                        1,\
                        std::multiplies<int64_t>())

#define PUSH_BACK(OP_NAME, SAVED_VAR) \
        { \
          auto fn = (torch::autograd::generated::OP_NAME*)(grad_fn.get()); \
          auto VAR=fn->SAVED_VAR.unpack(grad_fn); \
          {saved_var_list.push_back(VAR);} \
        }


std::vector<Variable> get_saved_var_list(std::shared_ptr<torch::autograd::Node> grad_fn){
  
  std::vector<Variable> saved_var_list;
  if(grad_fn->name()=="NllLossBackward"){
    PUSH_BACK(NllLossBackward,self_)
    PUSH_BACK(NllLossBackward,target_)
    PUSH_BACK(NllLossBackward,weight_)
    PUSH_BACK(NllLossBackward,total_weight_)
  } else if(grad_fn->name()=="LogSoftmaxBackward"){
    PUSH_BACK(LogSoftmaxBackward,self_)
    PUSH_BACK(LogSoftmaxBackward,result_)
  } else if(grad_fn->name()=="AddmmBackward"){
    PUSH_BACK(AddmmBackward,mat1_)
    PUSH_BACK(AddmmBackward,mat2_)
  } else if(grad_fn->name()=="MulBackward0"){
    PUSH_BACK(MulBackward0,self_)
    PUSH_BACK(MulBackward0,other_)
  } else if(grad_fn->name()=="FusedDropoutBackward"){
    PUSH_BACK(FusedDropoutBackward,result1_)
  } else if(grad_fn->name()=="AdaptiveAvgPool2DBackward"){
    PUSH_BACK(AdaptiveAvgPool2DBackward,self_)
  } else if(grad_fn->name()=="MaxPool2DWithIndicesBackward"){
    PUSH_BACK(MaxPool2DWithIndicesBackward,self_)
    PUSH_BACK(MaxPool2DWithIndicesBackward,result1_)
  } else if(grad_fn->name()=="ReluBackward1"){
    PUSH_BACK(ReluBackward1,result_)
  } else if(grad_fn->name()=="ThnnConv2DBackward"){
    PUSH_BACK(ThnnConv2DBackward,self_)
    PUSH_BACK(ThnnConv2DBackward,weight_)
    PUSH_BACK(ThnnConv2DBackward,finput_)
    PUSH_BACK(ThnnConv2DBackward,fgrad_input_)
  } else if(grad_fn->name()=="DivBackward0"){
    PUSH_BACK(DivBackward0,self_)
    PUSH_BACK(DivBackward0,other_)
  } else if(grad_fn->name()=="MmBackward"){
    PUSH_BACK(MmBackward,self_)
    PUSH_BACK(MmBackward,mat2_)
  } else if(grad_fn->name()=="GeluBackward"){
    PUSH_BACK(GeluBackward,self_)
  } else if(grad_fn->name()=="EmbeddingBackward"){
    PUSH_BACK(EmbeddingBackward,indices_)
  } else if(grad_fn->name()=="BmmBackward"){
    PUSH_BACK(BmmBackward,self_)
    PUSH_BACK(BmmBackward,mat2_)
  } else if(grad_fn->name()=="SoftmaxBackward"){
    PUSH_BACK(SoftmaxBackward,self_)
    PUSH_BACK(SoftmaxBackward,result_)
  } else if(grad_fn->name()=="SplitBackward"){
    PUSH_BACK(SplitBackward,self_)
  } else if(grad_fn->name()=="BaddbmmBackward"){
    PUSH_BACK(BaddbmmBackward,batch2_)
    PUSH_BACK(BaddbmmBackward,batch1_)
  } else if(grad_fn->name()=="torch::autograd::AccumulateGrad"){
    auto accu_grad = dynamic_cast<torch::autograd::AccumulateGrad*>(grad_fn.get());
    saved_var_list.push_back(accu_grad->variable);
  } else{
    /*
      following ops have no need to save variable:
        2. TBackward
        3. CloneBackward
        4. AsStridedBackward
        5. SumBackward0
        6. ViewBackward
        7. UnsafeViewBackward
        8. torch::autograd::CopyBackwards
        9. TransposeBackward0
        10. AddBackward0
        11. ExpandBackward
        12. PermuteBackward
        13. torch::autograd::CopySlices

      发现自定义的function：
        1. FusedLayerNormAffineFunctionBackward
        2. _CopyToModelParallelRegionBackward
        3. _VocabParallelCrossEntropyBackward
        4. 
        5. 
        6. _ReduceFromModelParallelRegionBackward

    */
    // std::cout<<"unsupported gradient function: ["<<grad_fn->name()<<"]"<<std::endl;


    // 处理用户自定义算子
    auto custom_func = dynamic_cast<torch::autograd::PyNode*>(grad_fn.get());
    if(custom_func){
      // std::cout<<"detect customer operator:"<<custom_func->name()<<std::endl;
      THPFunction* ctx = (THPFunction*)(custom_func->obj);
      auto saved_variables_num = ctx->saved_variables.size();
      for(int i=0;i<saved_variables_num;i++){
        saved_var_list.push_back(ctx->saved_variables[i].unpack(grad_fn));
      }
    }
  }
  return saved_var_list;
}

PyObject *THPEngine_test_dataptr(THPEngine *self, PyObject *args, PyObject *kwargs){

  PyObject *target_tensor;
  PyObject *src_tensor;
  const char *accepted_kwargs[] = {
    "target_tensor","src_tensor",nullptr
  };

  if(!PyArg_ParseTupleAndKeywords(args, kwargs, "OO|", (char**)accepted_kwargs,
    &target_tensor,&src_tensor)){
      std::cout<<"[haiqwa c++] raise an error while parsing arguments"<<std::endl;
      return nullptr;
    }

  // .get_data_ptr();
  auto target_storage = (((THPVariable*)target_tensor)->cdata).variable_data().unsafeGetTensorImpl()->get_storage();
  auto src_storage = (((THPVariable*)src_tensor)->cdata).variable_data().unsafeGetTensorImpl()->get_storage();
  std::cout<<"[haiqwa c++] target tensor address="<<target_storage.get_data_ptr().get()<<std::endl;
  std::cout<<"[haiqwa c++] src tensor address="<<src_storage.get_data_ptr().get()<<std::endl;
  
  // src_storage.get_data_ptr().get_deleter()(src_storage.get_data_ptr().get());
  
  src_storage.get_data_ptr().set_data_ptr_v1(target_storage.get_data_ptr());
  int count;
  cudaGetDeviceCount(&count);
  std::cout<<"cuda device count = "<< count <<std::endl;
  
  // std::cout<<"[haiqwa c++] successfully free cuda data!"<<std::endl;
  // auto temp = c10::detail::UniqueVoidPtr(src_storage.get_data_ptr().get());
  
  // auto cuda_data = ((THPVariable*)src_tensor)->cdata;
  // std::cout<<"[haiqwa c++] cuda_data type = "<<cuda_data.device().type()<<std::endl;
  
  // auto cuda_data_ptr = (((THPVariable*)src_tensor)->cdata).get_impl()->get_storage();
  // target_tensor_var.set_name("host");
  // src_tensor_var.set_name("cuda");




  Py_RETURN_NONE;
}


// Implementation of torch._C._EngineBase.data_summary
PyObject *THPEngine_data_summary(THPEngine *self, PyObject *args, PyObject *kwargs){
  HANDLE_TH_ERRORS
  PyObject *tensors = nullptr;
  const char *accepted_kwargs[] = {
    "tensors",nullptr
  };
  // parse tensor from input arguments
  if(!PyArg_ParseTupleAndKeywords(args, kwargs, "O|", (char**)accepted_kwargs,
    &tensors)){
      return nullptr;
    }
  THPUtils_assert(PyList_Check(tensors), "tensors argument is expected to "
      "be a list, but got %s", THPUtils_typename(tensors));
  Py_ssize_t num_tensors = PyList_GET_SIZE(tensors);
  // std::cout<<num_tensors<<std::endl;
  int inter_node_size=0, leaf_node_size=0;
  // variable in visiting_queue is ready
  
  std::queue<std::shared_ptr<Node>> visiting_queue;
  std::set<std::shared_ptr<Node>> visited_set;
  std::vector<torch::autograd::Variable> var_list;


  for(int i=0;i<num_tensors;i++){
    PyObject *tensor = PyList_GET_ITEM(tensors, i);
    auto& variable = ((THPVariable*)tensor)->cdata;
    visiting_queue.push(std::move(variable.grad_fn()));
    var_list.push_back(variable);
    // 优先将output的大小加上去
    // inter_node_size+=VECTOR_MUL(variable.sizes());
  }
  

  
  while(!visiting_queue.empty()){
    auto current_function = visiting_queue.front();
    visiting_queue.pop();
    if(!current_function or visited_set.find(current_function)!=visited_set.end()) continue;
    visited_set.insert(current_function);
    // std::cout<<"current function name="<<current_function->name()<<std::endl;

    auto result=get_saved_var_list(current_function);

    // std::cout<<"result size="<<std::endl;
    std::vector<torch::autograd::Variable> result_valid;

    // 检查是否有重复
    
    for(auto new_ : result){
      bool is_new=true;
      if(!new_.defined()) continue;
      for(auto old_ : var_list){
        // new_.tensor_data().getIntrusivePtr()->storage().is_alias_of(old_.tensor_data().getIntrusivePtr()->storage())
        
        if(new_.is_alias_of(old_)) {is_new=false;break;}
        else ;
      }
      if(is_new) result_valid.push_back(new_);
    }

    
    // std::cout<<"size of result_valid="<<result_valid.size()<<std::endl;
    if(var_list.size()>0)
      var_list.insert(var_list.end(),result_valid.begin(),result_valid.end());
    else 
      var_list.insert(var_list.end(),result.begin(),result.end());

    // PRINT_HELLO_WORLD
    // 在这里单独统计出来权重参数的数量
    if(current_function->name()=="torch::autograd::AccumulateGrad"){
      for(int i =0;i<current_function->num_inputs();i++){
        leaf_node_size += VECTOR_MUL(current_function->input_metadata(i).shape());
      }
    }
    // PRINT_HELLO_WORLD



    // 将后续的grad_fn入队
    auto next_edges = current_function->next_edges();
    for(auto next_edge : next_edges){
      if(next_edge.function and visited_set.find(next_edge.function)==visited_set.end())
        // std::cout<<"function name: "<<next_edge.function->name()<<std::endl;
        visiting_queue.push(next_edge.function);
    }
  }


  // std::cout<<var_list.size()<<std::endl;
  for(auto each : var_list){
    // std::cout<<VECTOR_MUL(each.sizes())<<std::endl;
    // if(each.is_leaf() && each.requires_grad()){
    //   std::cout<<"find a parameter!"<<std::endl;
    // } else {
    //   std::cout<<"find activation!"<<std::endl;
    // }
    inter_node_size+=VECTOR_MUL(each.sizes());
  }
  inter_node_size-=leaf_node_size;
  PyObject* return_value = PyTuple_New(2);
  PyObject* arg1 = PyInt_FromLong(leaf_node_size);
  PyObject* arg2 = PyInt_FromLong(inter_node_size);
  PyTuple_SetItem(return_value, 0, arg1);
  PyTuple_SetItem(return_value, 1, arg2);
  return return_value;
  END_HANDLE_TH_ERRORS
}

// Implementation of torch._C._EngineBase.run_backward
PyObject *THPEngine_run_backward(THPEngine *self, PyObject *args, PyObject *kwargs)
{
  HANDLE_TH_ERRORS
  _maybe_reinitialize_engine_after_fork();
  PyObject *tensors = nullptr;
  PyObject *grad_tensors = nullptr;
  unsigned char keep_graph = 0;
  unsigned char create_graph = 0;
  PyObject *inputs = nullptr;
  unsigned char allow_unreachable = 0;
  const char *accepted_kwargs[] = {
      "tensors", "grad_tensors", "keep_graph", "create_graph", "inputs",
      "allow_unreachable", nullptr
  };
  if (!PyArg_ParseTupleAndKeywords(args, kwargs, "OObb|Ob", (char**)accepted_kwargs,
        &tensors, &grad_tensors, &keep_graph, &create_graph, &inputs, &allow_unreachable))
    return nullptr;

  THPUtils_assert(PyTuple_Check(tensors), "tensors argument is expected to "
      "be a tuple, but got %s", THPUtils_typename(tensors));
  THPUtils_assert(PyTuple_Check(grad_tensors), "grad_tensors argument is "
      "expected to be a tuple, but got %s", THPUtils_typename(grad_tensors));

  Py_ssize_t num_tensors = PyTuple_GET_SIZE(tensors);
  Py_ssize_t num_gradients = PyTuple_GET_SIZE(grad_tensors);
  THPUtils_assert(num_tensors == num_gradients, "got %ld tensors and %ld "
      "gradients", num_tensors, num_gradients);

  edge_list roots;
  roots.reserve(num_tensors);
  variable_list grads;
  grads.reserve(num_tensors);
  for (int i = 0; i < num_tensors; i++) {
    PyObject *_tensor = PyTuple_GET_ITEM(tensors, i);
    THPUtils_assert(THPVariable_Check(_tensor), "element %d of tensors "
        "tuple is not a Tensor", i);
    auto& variable = ((THPVariable*)_tensor)->cdata;
    auto gradient_edge = variable.gradient_edge();
    THPUtils_assert(gradient_edge.function,
        "element %d of tensors does not require grad and does not have a grad_fn", i);
    roots.push_back(std::move(gradient_edge));

    PyObject *grad = PyTuple_GET_ITEM(grad_tensors, i);
    if (THPVariable_Check(grad)) {
      grads.push_back(((THPVariable*)grad)->cdata);
    } else {
      THPUtils_assert(grad == Py_None,
          "element %d of gradients tuple is not a Tensor or None", i);
      THPUtils_assert(!variable.requires_grad(),
          "element %d of gradients tuple is None, but the corresponding Tensor requires grad");
    }
  }

  std::vector<Edge> output_edges;
  if (inputs != nullptr) {
    int num_inputs = PyTuple_GET_SIZE(inputs);
    output_edges.reserve(num_inputs);
    for (int i = 0; i < num_inputs; ++i) {
      PyObject *input = PyTuple_GET_ITEM(inputs, i);
      THPUtils_assert(THPVariable_Check(input),
          "all inputs have to be Tensors, but got %s", THPUtils_typename(input));
      THPVariable *input_var = (THPVariable*)input;
      const auto output_nr = input_var->cdata.output_nr();
      auto grad_fn = input_var->cdata.grad_fn();
      if (!grad_fn) {
          grad_fn = input_var->cdata.try_get_grad_accumulator();
      }
      THPUtils_assert(input_var->cdata.requires_grad(),
          "One of the differentiated Tensors does not require grad");
      if (!grad_fn) {
        output_edges.emplace_back();
      } else {
        output_edges.emplace_back(grad_fn, output_nr);
      }
    }
  }

  variable_list outputs;
  {
    AutoNoGIL no_gil;
    outputs = engine.execute(roots, grads, keep_graph, create_graph, output_edges);
  }

  if (inputs != nullptr) {
    int num_inputs = PyTuple_GET_SIZE(inputs);
    THPObjectPtr py_outputs {PyTuple_New(num_inputs)};
    if (!py_outputs) return nullptr;
    for (int i = 0; i < num_inputs; i++) {
      THPUtils_assert(allow_unreachable || outputs[i].defined(), "One of the "
                      "differentiated Tensors appears to not have been used "
                      "in the graph. Set allow_unused=True if this is the "
                      "desired behavior.");
      PyTuple_SET_ITEM(py_outputs.get(), i, THPVariable_Wrap(outputs[i]));
    }
    return py_outputs.release();
  } else {
    Py_RETURN_NONE;
  }
  END_HANDLE_TH_ERRORS
}

PyObject* THPEngine_queue_callback(PyObject *self, PyObject *_callback) {
  HANDLE_TH_ERRORS
  _maybe_reinitialize_engine_after_fork();
  std::shared_ptr<PyObject> callback(_callback, [](PyObject *obj) { AutoGIL gil; Py_DECREF(obj); });
  Py_INCREF(_callback);
  engine.queue_callback([callback]() {
    AutoGIL gil;
    THPObjectPtr result {PyObject_CallFunctionObjArgs(callback.get(), nullptr)};
    if (!result) throw python_error();
  });
  Py_RETURN_NONE;
  END_HANDLE_TH_ERRORS
}

PyObject* THPEngine_is_checkpoint_valid(PyObject *self) {
  HANDLE_TH_ERRORS
  if(engine.is_checkpoint_valid()) {
    Py_RETURN_TRUE;
  } else {
    Py_RETURN_FALSE;
  }
  END_HANDLE_TH_ERRORS
}

PyObject *THPEngine_new(PyTypeObject *type, PyObject *args, PyObject *kwargs)
{
  return type->tp_alloc(type, 0);
}

static struct PyMethodDef THPEngine_methods[] = {
  {(char*)"run_backward", (PyCFunction)THPEngine_run_backward, METH_VARARGS | METH_KEYWORDS, nullptr},
  {(char*)"queue_callback", (PyCFunction)THPEngine_queue_callback, METH_O, nullptr},
  {(char*)"is_checkpoint_valid", (PyCFunction)THPEngine_is_checkpoint_valid, METH_NOARGS, nullptr},
  {(char*)"data_summary", (PyCFunction)THPEngine_data_summary, METH_VARARGS | METH_KEYWORDS, nullptr},
  {(char*)"test_dataptr", (PyCFunction)THPEngine_test_dataptr, METH_VARARGS | METH_KEYWORDS, nullptr},
  {nullptr}
};


PyTypeObject THPEngineType = {
  PyVarObject_HEAD_INIT(nullptr, 0)
  "torch._C._EngineBase",                /* tp_name */
  sizeof(THPEngine),                     /* tp_basicsize */
  0,                                     /* tp_itemsize */
  nullptr,                                     /* tp_dealloc */
  0,                                     /* tp_print */
  nullptr,                                     /* tp_getattr */
  nullptr,                                     /* tp_setattr */
  nullptr,                                     /* tp_reserved */
  nullptr,                                     /* tp_repr */
  nullptr,                                     /* tp_as_number */
  nullptr,                                     /* tp_as_sequence */
  nullptr,                                     /* tp_as_mapping */
  nullptr,                                     /* tp_hash  */
  nullptr,                                     /* tp_call */
  nullptr,                                     /* tp_str */
  nullptr,                                     /* tp_getattro */
  nullptr,                                     /* tp_setattro */
  nullptr,                                     /* tp_as_buffer */
  Py_TPFLAGS_DEFAULT | Py_TPFLAGS_BASETYPE, /* tp_flags */
  nullptr,                               /* tp_doc */
  nullptr,                                     /* tp_traverse */
  nullptr,                                     /* tp_clear */
  nullptr,                                     /* tp_richcompare */
  0,                                     /* tp_weaklistoffset */
  nullptr,                                     /* tp_iter */
  nullptr,                                     /* tp_iternext */
  THPEngine_methods,                     /* tp_methods */
  nullptr,                                     /* tp_members */
  nullptr,                                     /* tp_getset */
  nullptr,                                     /* tp_base */
  nullptr,                                     /* tp_dict */
  nullptr,                                     /* tp_descr_get */
  nullptr,                                     /* tp_descr_set */
  0,                                     /* tp_dictoffset */
  nullptr,                                     /* tp_init */
  nullptr,                                     /* tp_alloc */
  THPEngine_new                          /* tp_new */
};

static void child_atfork() {
  _reinitialize_engine = true;
}

bool THPEngine_initModule(PyObject *module)
{
#ifndef _WIN32
  if (pthread_atfork(nullptr, nullptr, child_atfork) != 0) {
    throw std::runtime_error("unable to set pthread_atfork handler");
  }
#endif
  if (PyType_Ready(&THPEngineType) < 0)
    return false;
  Py_INCREF(&THPEngineType);
  PyModule_AddObject(module, "_ImperativeEngine", (PyObject *)&THPEngineType);
  set_default_engine_stub(get_python_engine);
  return true;
}
