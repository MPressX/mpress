import torch
from torch._C import _Swapper
from typing import List, Tuple

# def get_swap_engine():
#     return _Swapper()
__all__ = ["name", "init_swapper"]
_swapper = _Swapper()

def name():
    return _swapper.name()

def reset_advisor_index():
    return _swapper.reset_advisor_index()

def enable_hybrid_swap():
    return _swapper.enable_hybrid_swap()

def set_device_status(device_list:list, memory_list:list):
    return _swapper.set_device_status(device_list,memory_list)

def init_swapper(
    default_device_id=0,
    cuda_memory_threshold=0,
    num_cards=4,
    swap_mode = 1,
    sample_point = 0,
    candidates = [-1],
    connectivity_matrix=[],
    speed_matrix=[]
    ):
    """
    Args:
        default_device_id: 
            -1: not using GPU, only CPU;  
            0~N: using corresponding GPU
        cuda_memory_threshold:
            @Haiqwa to implement
        candidates: 
            which devices to receive
        swap_mode:
            FIXED=1, // only support swapping between default device and candidate device!
            AUTO=2, // make strategy in runtime
            STAY=3, // do not swap
            DEFAULT=0 // same as STAY
        connectivity_matrix: matrix[i][j] means the connectivity of device i-1 and device j-1
            device -1 means host; example
            [[0,0,0,0,0],
             [0,1,1,0,0],
             [0,1,1,0,0],
             [0,0,0,1,1],
             [0,0,0,1,1]]
        speed_matrix: matrix[i][j] represents the p2p speed(unidirectional) of device i-1 and device j-1
            device -1 menas host; example
            [[11.7,  11.7,   11.7,   11.7,   11.7],
             [12.8,  356.1,  10.2,   10.8,   11.0],
             [12.8,  10.25,  354.62, 10.79,  11.00],
             [12.8,  11.18,  11.09,  353.26, 10.25],
             [12.8,  11.20,  11.13,  10.25,  352.67]]
    """
    if connectivity_matrix == []:
        connectivity_matrix = [[0 if i!=j else 1 for i in range(-1, num_cards)] for j in range(-1, num_cards)]
    if speed_matrix == []:
        speed_matrix = [[12 for i in range(-1, num_cards)]for j in range(-1, num_cards)]
    # TODO: should check the legality of matrixs (the height and wdith should be equal to num_cards+1). 
    # Leave it now. 
    print("ready to call _swapper.init_swapper()")
    return _swapper.init_swapper(default_device_id, cuda_memory_threshold, num_cards,
                                 swap_mode, sample_point, candidates, connectivity_matrix, speed_matrix)



'''
    target --> Tensor
    meta --> Tuple(void*, size_t)
    layers --> grad_fn
'''


'''
    ATTENTION: Below code would trigger memory leak(increase)!
    The code is saved for further studying, because we do not understand the reason, 
    therefore we may meet similar memory leak. 
    
    NOTE: similar operation appears in `torch/nn/modules/module.py::Module::register_swap_in` !!!
    NOTE: 上述出现的问题只有在is_register_self=True的时候才会触发，但是这个条件一般不成立，所以问题可能还没出现
'''
# def search_metas_by_range(start_tensors=[], end_tensors=[], excludes=[], exclude_end_tensors=True):
#     if exclude_end_tensors:
#         excludes.extend(end_tensors)
#     return _swapper.search_metas_by_range(start_pytensors=start_tensors,
#                                           end_pytensors=end_tensors,
#                                           excludes=end_tensors)
    

def swap_out_by_metas(targets=[], destinations=[], ratios=[]):
    """give metas, swap then

    Args:
        targets (List[Tuple[int,int]], optional): list of metas. Defaults to [].

    Returns:
        None
    """
    return _swapper.swap_out_by_metas(targets, destinations, ratios)

def swap_in_by_metas(targets=[]):
    return _swapper.swap_in_by_metas(targets)

def swap_out_by_targets(targets=[]):
    """give tensors, swap then

    Args:
        targets (List[torch.Tensor], optional): list of torch.Tensors. Defaults to [].

    Returns:
        None: [description]
    """
    return _swapper.swap_out_by_targets(targets)

def swap_in_by_targets(targets=[]):
    return _swapper.swap_in_by_targets(targets)

def swap_out_by_layers(targets=[], excludes=[], destinations=[], ratios=[]):
    """give a gradient function, swap tensors they stored

    Args:
        targets (List[torch.xx.grad_fn], optional): list of grad_fn. Defaults to [].
        excludes (List[torch.Tensor], optional): list of torch.Tensor, tell engine that do not swap tensors in it. Defaults to [].

    Returns:
        None: [description]
    """
    return _swapper.swap_out_by_layers(targets, excludes, destinations, ratios)

def swap_in_by_layers(targets=[], excludes=[]):
    return _swapper.swap_in_by_layers(targets, excludes)

def search_metas_by_range(start_tensors=[], end_tensors=[], excludes=[]):
    """find tensors which are between `start_tensors` and `end_tensors` in computation graph

    Args:
        start_tensors (list): [usually be the input of one layer].
        end_tensors (list, necessary): [usually be the output of one layer]. Must be passed.
        excludes (list, optional): [description]. Defaults to [].

    Returns:
        List[Tuple[int,int]]: lists of meta tensors found in computation graph.
        
    Note:
        The search usually does not return ans containing `end_tensors`, because the search is 
        do not record the source nodes. 
        If necessary, use `excludes` to ensure the `end_tensors` not appear in returning result
    """
    return _swapper.search_metas_by_range(start_pytensors=start_tensors,
                                          end_pytensors=end_tensors,
                                          excludes=excludes)
    

def swap_out_by_range(start_tensors=[], end_tensors=[], excludes=[],  destinations=[], ratios=[]):
    """
    find and swap tensors which are between `start_tensors` and `end_tensors` in computation graph

    Args:
        start_tensors (list): [usually be the input of one layer].
        end_tensors (list, necessary): [usually be the output of one layer]. Must be passed.
        excludes (list, optional): [description]. Defaults to [].

    Returns:
        List[Tuple[int,int]]: lists of meta tensors found in computation graph.
    """
    metas = search_metas_by_range(start_tensors, end_tensors, excludes)
    swap_out_by_metas(metas, destinations, ratios)
    return metas


def wait_until_ok(targets=[], status=0):
    """
    Args:
        targets: list of tensors
        status:
            OK = 0,
            SWAPPING_OUT = 1,
            SWAPPED = 2,
            SWAPPING_IN = 3
    """
    return _swapper.wait_until_ok(targets, status)

def wait_until_ok_by_layers(targets=[], status=0):
    """
    Args:
        targets: list of grad_fn
        status: same with wait_until_ok()
    """
    return _swapper.wait_until_ok_by_layers(targets, status)
def forward_begin():
    _swapper.forward_begin()

def backward_begin():
    _swapper.backward_begin()
    
def print_info(targets:List[torch.Tensor]):
    _swapper.print_info(targets)
    
def get_true_device_index(target:torch.Tensor):
    return _swapper.get_true_device_index(target)

def analyze_operator(target):
    _swapper.analyze_operator(target)
    
def tensor_to_meta(t:torch.Tensor):
    return _swapper.tensor_to_meta(target=t)


def tensor_infos_2_string(infos:List):
    """give the infos get b `_swapper.get_infos_about_tensor`, return string for print

    Args:
        info (List): List[ptr, nbytes, true_device_type, true_device_index, status]

    Returns:
        str: 
    """
    return f"ptr[{hex(infos[0])}] nbytes[{infos[1]}], Device.Type[{infos[2]}], "\
        f"Device.Index[{infos[3]}], DataStatus[{infos[4]}]"

def str_infos_about_tensor(t:torch.Tensor):
    """give a tensor, return detailed infos in C level

    Args:
        t (torch.Tensor): Given Tensor

    Returns:
        str: List[ptr, nbytes, true_device_type, true_device_index, status]
    """
    infos = _swapper.get_infos_about_tensor(target=t)
    return tensor_infos_2_string(infos)
        
def str_infos_about_node(node):
    """give a gradient function, return detailed infos in C level

    Args:
        node (xxxBackward): a gradient function

    Returns:
        str: List[ptr_to_this_grad_fn_in_C, name]
    """
    infos = _swapper.get_infos_about_node(target=node)
    saved_variable_infos = infos[2]
    str_vars = f"Saved Variables:"
    if saved_variable_infos:
        for i, saved_variable_info in enumerate(saved_variable_infos):
            str_vars += f"\n\t[i] {tensor_infos_2_string(saved_variable_info)}"
    else:
        str_vars += "\n\tNone"
    return f"ptr[{hex(infos[0])}] name[{infos[1]}]\n{str_vars}"




def serialize(t):
    """serialize a Tuple/List to a list containing Tensors

    Args:
        t (Tuple or List): contianing complex items, e.g. the output of LSTM

    Returns:
        List[Tensor]: return every Tensor in t
    """
    ans = []
    if type(t) is torch.Tensor:
        return [t]
    elif type(t) is list or type(t) is tuple:
        for v in t:
            ans.extend(serialize(v))
    return ans

def get_shape_and_dtype(t:torch.Tensor):
    """
    returning string of infos about t.shape and t.dtype

    Args:
        t (torch.Tensor): [description]

    Returns:
        str: [shape:{t.shape}, dtype:{t.dtype}]
    """
    if type(t) == torch.Tensor:
        return f"[shape:{t.shape}, dtype:{t.dtype}]"
    elif type(t) is list:
        l = []
        for v in t:
            l.append(get_shape_and_dtype(v))
        return f"List[{','.join(l)}]"
    elif type(t) is tuple:
        l = []
        for v in t:
            l.append(get_shape_and_dtype(v))
        return f"Tuple[{','.join(l)}]"
    else:
        return f"[not tensor, type:{type(t)}]"
    
def set_debug_prefix(prefix: str):
    _swapper.set_debug_prefix(prefix)
    

def count_model_parameters(model):
    """count the parameters of model, return a tuple containing #params, #trainable_params, #nontrainable_params

    Args:
        model ([type]): a trainable model

    Returns:
        Tuple[int, int, int]: (total_params, trainable_params, nontrainable_params)
    """
    total_params = 0
    trainable_params = 0
    nontrainable_params = 0
    for param in model.parameters():
        import numpy as np
        mult_value = np.prod(param.size())
        total_params+=mult_value
        if param.requires_grad:
            trainable_params+=mult_value
        else:
            nontrainable_params+=mult_value
    return total_params, trainable_params, nontrainable_params


def str_mem_in_MB():
    memory_allocated = torch.cuda.memory_allocated()/1024/1024
    max_memory_allocated = torch.cuda.max_memory_allocated()/1024/1024
    memory_cached = torch.cuda.memory_cached()/1024/1024
    return f"{memory_allocated}[{max_memory_allocated}]({memory_cached})"

def print_str(s:str):
    _swapper.print_str(s)
    
def default_optimizer(parameter):
    parameter += parameter.grad
    
def register_optimize_func(f):
    _swapper.register_optimize_func(f)
    
def get_id(t:torch.Tensor):
    return _swapper.get_id_of_tensor(t)

def launch_swap_in(srcs, dsts=[]):
    """launch swap in tensors from srcs to dsts (srcs --> dsts, in backward order)

    Args:
        srcs (List[Tensor]): the srcs in backward graph
        dsts (List[Tensor]): the dsts in backward graph
    """
    _swapper.launch_swap_in(end_tensors=srcs, start_tensors=[dsts])

def record_state(weight:torch.Tensor, state:torch.Tensor):
    """tell C that the `weight` has one state `state`

    Args:
        weight (torch.Tensor): weight
        state (torch.Tensor): state of weight, for example: 'next_weight'(for pipedream), 'next_v'(in Adam)
    """
    _swapper.record_state(weight=weight, state=state)

def set_swap_out_bytes(bytes):
    _swapper.set_swap_out_bytes(bytes)

def get_swap_out_bytes():
    return _swapper.get_swap_out_bytes()

def clear_swap_out_bytes():
    set_swap_out_bytes(0)

if __name__ == '__main__':
    a = torch.rand(2,3)
    b = torch.rand(3,2)
    l = [a,b,None,[b,a, None]]
    print(get_shape_and_dtype(l))
    