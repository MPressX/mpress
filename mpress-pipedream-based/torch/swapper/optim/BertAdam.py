from torch.nn.utils.clip_grad import clip_grad_norm_
from . import LayerWiseOptimizer
import math
import torch
from collections import defaultdict
from torch import swapper

# TODO:
# weight_decay 根据不同的parameter有不同： 对于bias类、LayerNorm类weight_decay为0
# 之后再完善，先测试下是否能正常训练。

def warmup_cosine(x, warmup=0.002):
    if x < warmup:
        return x/warmup
    return 0.5 * (1.0 + torch.cos(math.pi * x))

def warmup_constant(x, warmup=0.002):
    if x < warmup:
        return x/warmup
    return 1.0

def warmup_linear(x, warmup=0.002):
    if x < warmup:
        return x/warmup
    return 1.0 - x

SCHEDULES = {
    'warmup_cosine':warmup_cosine,
    'warmup_constant':warmup_constant,
    'warmup_linear':warmup_linear,
}

class BertAdam(LayerWiseOptimizer):
    def __init__(self, lr, warmup=-1, t_total=-1, schedule='warmup_linear',
                 b1=0.9, b2=0.999, e=1e-6, weight_decay=0.01,
                 max_grad_norm = 1.0,
                 weight_specific_params=defaultdict(dict)):
        super(BertAdam, self).__init__()
        if lr < 0.0:
            raise ValueError("Invalid learning rate: {} - should be >= 0.0".format(lr))
        if schedule not in SCHEDULES:
            raise ValueError("Invalid schedule parameter: {}".format(schedule))
        if not 0.0 <= warmup < 1.0 and not warmup == -1:
            raise ValueError("Invalid warmup: {} - should be in [0.0, 1.0[ or -1".format(warmup))
        if not 0.0 <= b1 < 1.0:
            raise ValueError("Invalid b1 parameter: {} - should be in [0.0, 1.0[".format(b1))
        if not 0.0 <= b2 < 1.0:
            raise ValueError("Invalid b2 parameter: {} - should be in [0.0, 1.0[".format(b2))
        if not e >= 0.0:
            raise ValueError("Invalid epsilon value: {} - should be >= 0.0".format(e))
        self.lr = lr
        self.warmup = warmup
        self.t_total = t_total
        self.b1 = b1
        self.b2 = b2
        self.e = e
        self.weight_decay = weight_decay
        self.max_grad_norm = max_grad_norm
        self.schedule_fct = SCHEDULES[schedule]
        self.weight_specific_params = weight_specific_params
        self.state = defaultdict(dict)
        self.weight_needed_swap = defaultdict(lambda:False)
        
    def set_needed_swap(self, param):
        self.weight_needed_swap[param] = True

    def get_metas_in_states(self, param):
        state = self.state[param]
        if len(state)==0:
            state['step'] = 0
            state['next_m'] = torch.zeros_like(param.data)
            state['next_v'] = torch.zeros_like(param.data)
        meta_m = swapper.tensor_to_meta(state['next_m'])
        meta_v = swapper.tensor_to_meta(state['next_v'])
        return [meta_m, meta_v]
        
    @staticmethod
    def optimize(self, param):
        weight_specific_params = self.weight_specific_params[param]
        if not weight_specific_params:
            print(f"not found in weight_specific_params, return")
            return
        weight_decay = weight_specific_params['weight_decay']
        weight = param.data
        grad = param.grad.data
        state = self.state[param]

        if len(state)==0:
            state['step'] = 0
            state['next_m'] = torch.zeros_like(weight)
            state['next_v'] = torch.zeros_like(weight)
        next_m = state['next_m']
        next_v = state['next_v']
        beta1 = self.b1
        beta2 = self.b2

        if self.max_grad_norm > 0:
            clip_grad_norm_(param, self.max_grad_norm)
        
        next_m.mul_(beta1).add_(1-beta1, grad)
        next_v.mul_(beta2).addcmul_(1-beta2, grad, grad)
        update = next_m / (next_v.sqrt() + self.e)
        
        if weight_decay > 0.0:
            update += weight_decay*weight
        if self.t_total != -1:
            lr_scheduled = self.lr * self.schedule_fct(state['step']/self.t_total , self.warmup)
        else:
            lr_scheduled = self.lr
        
        update_with_lr = lr_scheduled * update
        weight.add_(-update_with_lr)
        state['step'] += 1
        
        if self.weight_needed_swap[param]:
            swapper.swap_out_by_targets(targets=[next_m, next_v])

class BertAdamPlaceHolder(LayerWiseOptimizer):
    def __init__(self):
        super(BertAdamPlaceHolder, self).__init__()
        
    def get_metas_in_states(self, param):
        return []
        
    @staticmethod
    def optimize(self, param):
        pass