from typing import overload
import torch
from collections import defaultdict
import traceback
import math

class LayerWiseOptimizer():
    def __init__(self):
        pass
    
    @staticmethod
    def optimize(self, param):
        raise NotImplementedError
    def get_optimize(self, zero_grad=True):
        """return a layer-wise optimizer

        Returns:
            function: a wrapper of optimize
            
        Notes!!!:
            `swapper.register_optimize_func(self.get_optimize())` does not work.
            Instead, split it into two lines:
            ```
            optimizer = self.get_optimize()
            swapper.register_optimize_func(optimizer)
            ```
        """
        def wrapped_optimizer(params):
            try:
                self.optimize(self, params)
                if zero_grad:
                    params.grad = None
                    # params.grad.detach_()
                    # params.grad.zero_()
            except:
                print(f"Layer-wise optimize failed, Error msg={traceback.format_exc()}")
        return wrapped_optimizer
    
        
        
    

class SGD(LayerWiseOptimizer):
    def __init__(self, lr_=0.01):
        super(SGD, self).__init__()
        self.lr = lr_

    @staticmethod
    def optimize(self, param):
        param -= self.lr*param.grad
    
    
class Adam(LayerWiseOptimizer):
    def __init__(self, lr=1e-3, betas=(0.9, 0.999), eps=1e-8,
                 weight_decay=0):
        super(Adam, self).__init__()
        if not 0.0 <= lr:
            raise ValueError("Invalid learning rate: {}".format(lr))
        if not 0.0 <= eps:
            raise ValueError("Invalid epsilon value: {}".format(eps))
        if not 0.0 <= betas[0] < 1.0:
            raise ValueError("Invalid beta parameter at index 0: {}".format(betas[0]))
        if not 0.0 <= betas[1] < 1.0:
            raise ValueError("Invalid beta parameter at index 1: {}".format(betas[1]))
        self.state = defaultdict(dict)
        self.betas = betas
        self.lr = lr
        self.eps = eps
        self.weight_decay = weight_decay
    
    @staticmethod
    def optimize(self, param):
        weight = param.data
        grad = param.grad.data
        state = self.state[param]
        if len(state)==0:
            state['step'] = 0
            state['exp_avg'] = torch.zeros_like(weight)
            state['exp_avg_sq'] = torch.zeros_like(weight)
        exp_avg, exp_avg_sq = state['exp_avg'], state['exp_avg_sq']
        beta1, beta2 = self.betas
        state['step'] += 1
        
        if self.weight_decay!=0:
            grad.add_(self.weight_decay, weight)
        exp_avg.mul_(beta1).add_(1-beta1, grad)
        exp_avg_sq.mul_(beta2).addcmul_(1-beta2, grad, grad)

        denom = exp_avg_sq.sqrt().add_(self.eps)
        bias_correction1 = 1-beta1**state['step']
        bias_correction2 = 1-beta2**state['step']
        step_size = self.lr*math.sqrt(bias_correction2)/bias_correction1
        weight.addcdiv_(-step_size, exp_avg, denom)
        