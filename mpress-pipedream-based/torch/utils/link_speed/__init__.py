import json
import os
def get_speed_matrix(config):
    if not config:
        return []
    
    if not os.path.exists(config):
        raise RuntimeError(f"{config} not found!")
    
    with open(config, 'r') as f:
        s = f.read()
    j = json.loads(s)
    t = j['uni p2p matrix']
    N = len(t)
    ans = [[0 for _ in range(N+1)] for _ in range(N+1)]
    for i in range(N+1):
        ans[0][i] = j['H2D']
        ans[i][0] = j['D2H']
    for i in range(N):
        for j in range(N):
            ans[i+1][j+1] = t[i][j]
    return ans
