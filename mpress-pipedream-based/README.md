<!-- Project name and Description  -->
In this document, we will briefly introduce our framework 'MPress' (implemented based on PipeDream source code) and mainly focus on the instructions for making it run.

# What is MPress
 
MPress is a framework built on top of PyTorch, and tested with PipeDream/GPipe. It aims to break the GPU memory wall of inter-operator parallel DNN training while minimizing extra cost. This folder contains PipeDream-based MPress implementation and its corresponding test demos.

# Getting started with docker

MPress has been fully tested on AWS EC2 p3dn.24xlarge instance. For ease of use, we provide a pre-compiled docker image to get started quickly. 

`nvidia-docker` (version 2.0+) is recommended for launching docker images.

## Pull, run and exec
```bash
docker pull mpressx/mpress:latest
nvidia-docker run -itd --name=mpress_test --privileged mpressx/mpress:latest
nvidia-docker exec -it mpress_test bash
```

# Getting started with compiling 
## Software requirements
- cuda 10.0/10.1
- cudnn 7.6
- python 3.6+
- gcc/g++: 7.5.0+

## Installation
1. clone the repo
```
git clone https://gitlab.com/MPressX/mpress
```
2. enter the directory
```
cd /workspace/mpress # or enter /path/to/mpress if you do not use docker
cd mpress-pipedream-based
```
3. compile PyTorch
```
bash compile.sh
```
4. install packaged required by `PipeDream`
```
cd test/swapper/pipedream
pip install -r requirements.txt
```


<!-- ### Optimize with device placing -->
## Test BERT
1. enter the directory
```bash
cd /path/to/mpress-pipedream-based
cd test/swapper/pipedream/runtime/bert
bash init_config.sh # init config for network connections
``` 
2. view the configs of each bert-model
```bash
vim ./auto_gen_config.py
```
| **model-name** | **number of params** |
| - | - |
| bert_large_balance/bert_large_memory | 0.35B |
| bert_48_1_balance/bert_48_1_memory | 0.64B |
| bert_48_2_balance/bert_48_2_memory/bert_48_2_balance_optimized | 1.67B |
| bert_64_2_balance | 4.0B |
| bert_6B_balance | 6.2B |

3. modify the config
```bash
vim ./run.sh
# test_list(list of test-cases)

```

4. run Bert
```bash
bash ./run.sh
```

5. check the results
```bash
cd /output/${model_name} 
tail -f stage7.log
```


# Diff files
We exported the diff files of our work and the PyTorch native code in `patch_files/mpress.patch` and made a note in `patch_files/README.md`, including the native code version(commit id) and the prupose of the diff files included in the diff.