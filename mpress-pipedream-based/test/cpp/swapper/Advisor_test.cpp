#include <torch/csrc/swapper/include/Advisor.h>
#include <torch/csrc/swapper/include/MemTracker.h>
#include <torch/csrc/swapper/include/utils/z_debug.h>
#include <gtest/gtest.h>
#include <torch/torch.h>

using namespace torch::swapper;


TEST(AdvisorTest, TEST_AUTO_MODE){
    auto instance = get_advisor();
    
    std::vector<std::vector<int>> connectivity_matrix = {
                                                        {0,0,0,0,0},
                                                        {0,1,1,0,0},
                                                        {0,1,1,0,0},
                                                        {0,0,0,1,1},
                                                        {0,0,0,1,1}};
    std::vector<std::vector<float>> speed_matrix = {
        {11.7,  11.7,   11.7,   11.7,   11.7},
        {12.8,  356.1,  25,   25,   50},
        {12.8,  25,  354.62, 50,  25},
        {12.8,  25,  50,  353.26, 50},
        {12.8,  50,  25,  50,  352.67}
    };                                                
    int default_device_id = 1;
    std::vector<int> candidates = {-1, 2, 3};
    std::unordered_map<int, unsigned long long> device2size;
    device2size.insert(std::pair<int, unsigned long long>(2,5ull*1024*1024*1024));
    device2size.insert(std::pair<int, unsigned long long>(3,5ull*1024*1024*1024));
    instance->set_default_device_id(default_device_id);
    instance->set_candidates(candidates);
    instance->set_connectivity_matrix(connectivity_matrix);
    instance->set_speed_matrix(speed_matrix);
    instance->set_swap_mode(SWAP_MODE::AUTO);
    instance->init_advisor_after_send_settings();
    instance->update_iteration();
    instance->set_device_status(device2size);
    
    std::vector<c10::DataPtr> data_ptr_list;
    for(int i=0;i<10;i++){
        data_ptr_list.push_back(
            allocate(10, c10::Device(c10::DeviceType::CUDA,default_device_id))
        );
    }
    size_t nbytes = 1<<30;
    instance->reset_idx();
    for(auto& dptr : data_ptr_list){
        instance->how_to_swap(dptr, nbytes, taskType::SWAP_OUT);
    }
    instance->open_hybrid_swap();
    instance->reset_idx();
    for(auto& dptr : data_ptr_list){
        instance->how_to_swap(dptr, nbytes, taskType::SWAP_OUT);
    }
    


    // auto dptr = allocate(10, c10::Device(c10::DeviceType::CUDA,default_device_id));
    // size_t nbytes = 1<<30;
    // auto reply = instance->how_to_swap(dptr, nbytes, taskType::SWAP_OUT);
    // for(auto& dptr: data_ptr_list){
    //     instance->how_to_swap(dptr, nbytes, taskType::SWAP_IN);
    // }



    // instance->update_iteration();
    // for(auto& dptr: data_ptr_list){
    //     auto reply = instance->how_to_swap(dptr, nbytes, taskType::SWAP_OUT);
    //     std::cout<< "tensor size: " << nbytes <<" target devices: ";
    //     for(auto device : reply->device_list){
    //         std::cout<<device.index() << " ";
    //     }
    //     std::cout<<std::endl;
    // }


}




int main(int argc, char **argv){
    testing::InitGoogleTest(&argc, argv);
    // init_advisor();
    std::cout<<"===Advisor_Test Begine==="<<std::endl;
    // get_mem_tracker();
    RUN_ALL_TESTS();
    return 0;
}