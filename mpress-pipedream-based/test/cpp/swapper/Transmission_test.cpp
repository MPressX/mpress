#include <iostream>
#include <torch/csrc/swapper/include/Transmission.h>
#include <torch/csrc/swapper/include/MemTracker.h>
#include <torch/csrc/swapper/include/utils/z_debug.h>
#include <gtest/gtest.h>
#include <cuda.h>
#include <cuda_runtime_api.h>
#include <thread>
#include <unistd.h>
#include <c10/core/Allocator.h>

using namespace torch::swapper;
using namespace std;

size_t N = 128 MiB;

void clear_host(void* p, size_t N){
    memset(p, 0, N);
}
void init_host(void* p, size_t N){
    uint8_t* a = (uint8_t*)p;
    for (size_t i = 0; i < N; i++){
        a[i] = i % 256;
    }
}
int check_host(void* p, size_t N){
    // 1: ok!
    // 0: not ok!
    uint8_t* a = (uint8_t*) p;
    for (size_t i = 0; i<N; i++){
        if (a[i] != (i%256)){
            cout<<"FAIL: i="<<i<<endl;
            return 0;
        }
    }
    return 1;
}


TEST(TransmissionTest, D2HandH2D){
    cudaStream_t stream;
    auto * mem_tracker = get_mem_tracker();
    auto device = allocate(N, c10::Device("cuda:0"));
    auto host = allocate(N, c10::Device(c10::DeviceType::CPU));
    auto result = allocate(N, c10::Device(c10::DeviceType::CPU));
    init_host(host.get(), N);
    GeneralMemcpyAsync(
        device.get(),
        0,
        host.get(),
        -1,
        stream,
        N
    );
    GeneralMemcpyAsync(
        result.get(),
        -1,
        device.get(),
        0,
        stream,
        N
    );
    clear_host(result.get(), 0);
    cudaStreamSynchronize(stream);
    EXPECT_EQ(1, check_host(result.get(), N));
}
TEST(TransmissionTest, D2D){
    cudaStream_t stream1, stream0;
    cudaSetDevice(1);
    cudaStreamCreate(&stream1);
    cudaSetDevice(0);
    cudaStreamCreate(&stream0);
    auto * mem_tracker = get_mem_tracker();
    auto device0 = allocate(N, c10::Device("cuda:0"));
    auto device1 = allocate(N, c10::Device("cuda:1"));
    auto host = allocate(N, c10::Device(c10::DeviceType::CPU));
    auto result = allocate(N, c10::Device(c10::DeviceType::CPU));
    init_host(host.get(), N);
    GeneralMemcpyAsync(
        device0.get(),
        0,
        host.get(),
        -1,
        stream0,
        N
    );
    // stream在发送端
    GeneralMemcpyAsync(
        device1.get(),
        1,
        device0.get(),
        0,
        stream0,
        N
    );
    clear_host(result.get(), N);
    cudaStreamSynchronize(stream0);
    cudaMemset(device0.get(), 0, N);
    cudaMemcpy(result.get(), device1.get(), N, cudaMemcpyDeviceToHost);
    EXPECT_EQ(1, check_host(result.get(), N));
    // stream在接收端
    GeneralMemcpyAsync(
        device0.get(),
        0,
        device1.get(),
        1,
        stream0,
        N
    );
    clear_host(result.get(), N);
    cudaStreamSynchronize(stream0);
    cudaMemcpy(result.get(), device0.get(), N, cudaMemcpyDeviceToHost);
    check_host(result.get(), N);
}
TEST(TransmissionTest, Multi2Single){
    transmission::init(4);
    vector<size_t> bytes = {N, N/3, N/3, N-(N/3)*2};
    vector<cudaStream_t> streams(4);
    vector<c10::DataPtr> devices;
    for (int i = 0; i < 4; i++){
        cudaSetDevice(i); cudaStreamCreate(&streams[i]);
        devices.emplace_back(allocate(bytes[i],c10::Device("cuda:"+to_string(i))));
    }
    auto host = allocate(N, c10::Device(c10::DeviceType::CPU));
    auto result = allocate(N, c10::Device(c10::DeviceType::CPU));
    auto *mem_tracker = get_mem_tracker();
    init_host(host.get(), N);
    clear_host(result.get(), N);
    cudaMemcpy(devices[0].get(), host.get(), N, cudaMemcpyHostToDevice);
    GeneralMemcpyAsync(
        {devices[1].get(), devices[2].get(), devices[3].get()},
        {1,2,3},
        devices[0].get(),
        0,
        {bytes[1], bytes[2], bytes[3]},
        {streams[1], streams[2], streams[3]},
        N
    );
    for (int i = 0; i < 4; i++){
        cudaSetDevice(i);
        cudaDeviceSynchronize();
    }
    cudaMemcpy(devices[0].get(), result.get(), N, cudaMemcpyHostToDevice);
    GeneralMemcpyAsync(
        devices[0].get(),
        0,
        {devices[1].get(), devices[2].get(), devices[3].get()},
        {1,2,3},
        {bytes[1], bytes[2], bytes[3]},
        {streams[1], streams[2], streams[3]},
        N
    );
    for (int i = 0; i < 4; i++){
        cudaSetDevice(i);
        cudaDeviceSynchronize();
    }
    cudaMemcpy(result.get(), devices[0].get(), N, cudaMemcpyDeviceToHost);
    check_host(result.get(), N);
}
TEST(TransmissionTest, HostInMultiDevices){
    vector<size_t> bytes = {N, N/4, N/4, N/4};
    size_t host_bytes = N - (N/4)*3;
    vector<cudaStream_t> streams{4};
    vector<c10::DataPtr> devices;
    for (int i = 0; i < 4; i++){
        cudaSetDevice(i); cudaStreamCreate(&streams[i]);
        devices.emplace_back(allocate(bytes[i],c10::Device("cuda:"+to_string(i))));
    }
    auto host = allocate(host_bytes, c10::Device(c10::DeviceType::CPU));
    auto result = allocate(N, c10::Device(c10::DeviceType::CPU));
    auto *mem_tracker = get_mem_tracker();
    init_host(result.get(), N);
    cudaMemcpy(devices[0].get(), result.get(), N, cudaMemcpyHostToDevice);
    GeneralMemcpyAsync(
        {devices[1].get(), devices[2].get(), host.get(), devices[3].get()},
        {1,2,-1,3},
        devices[0].get(),
        0,
        {bytes[1], bytes[2], host_bytes, bytes[3]},
        {streams[1], streams[2], streams[0], streams[3]},
        N
    );
    for (int i = 0; i < 4; i++){
        cudaSetDevice(i);
        cudaDeviceSynchronize();
    }
    memset(result.get(),0,N);
    cudaMemcpy(devices[0].get(), result.get(), N, cudaMemcpyHostToDevice);
    GeneralMemcpyAsync(
        devices[0].get(),
        0,
        {devices[1].get(), devices[2].get(), host.get(), devices[3].get()},
        {1,2,-1,3},
        {bytes[1], bytes[2], host_bytes, bytes[3]},
        {streams[1], streams[2], streams[0], streams[3]},
        N
    );
    for (int i = 0; i < 4; i++){
        cudaSetDevice(i);
        cudaDeviceSynchronize();
    }
    cudaMemcpy(result.get(), devices[0].get(), N, cudaMemcpyDeviceToHost);
    check_host(result.get(), N);
    
}
int main(int argc, char** argv){
    testing::InitGoogleTest(&argc, argv);
    cout<<"==Transmission test begin==="<<endl;
    RUN_ALL_TESTS();
    return 0;
}