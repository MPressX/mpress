#include<iostream>
#include<torch/csrc/swapper/include/MemTracker.h>
#include<gtest/gtest.h>

#include "caffe2/core/numa.h"

#include <unistd.h>
using namespace torch::swapper;

TEST(MemTrackerTest, TorchMemInfo){
    auto cur_type = caffe2::GetCudaMemoryPoolType();
    if (cur_type == caffe2::CudaMemoryPoolType::NONE){
        std::cout<<"[Info Print] Memory Type is NONE"<<std::endl;
    } else if(cur_type == caffe2::CudaMemoryPoolType::CUB){
        std::cout<<"[Info Print] Memory Type is CUB"<<std::endl;
    } else if(cur_type == caffe2::CudaMemoryPoolType::THC){
        std::cout<<"[Info Print] Memory Type is THC"<<std::endl;
    }


}

TEST(MemTrackerTest, CUDAMemTrackerLoopAllocate){
    auto* instance = get_mem_tracker();
    auto cuda = c10::Device(c10::DeviceType::CUDA,0);
    auto cpu = c10::Device(c10::DeviceType::CPU);
    
    c10::DataPtr mem1 = allocate(128 MiB,cuda);


    for(int i=0;i<10;i++){
        c10::DataPtr host_mem = allocate(128 MiB, cpu);
        mem1.set_data_ptr_v2(std::move(host_mem));

        c10::DataPtr cuda_mem = allocate(128 MiB, cuda);
        mem1.set_data_ptr_v2(std::move(cuda_mem));

        sleep(1);
    }
}


TEST(MemTrackerTest, CPUListInsertRemove){
    auto* instance = get_mem_tracker();
    instance->free_cpu_cached_list();
    EXPECT_EQ(0,instance->get_cpu_allocated_list_length());
    EXPECT_EQ(0,instance->get_cpu_cached_list_length());
    auto device = c10::Device(c10::DeviceType::CPU);
    {
        c10::DataPtr mem1 = allocate(128,device);
        EXPECT_EQ(1,instance->get_cpu_allocated_list_length());
        c10::DataPtr mem2 = allocate(256,device);
        EXPECT_EQ(2,instance->get_cpu_allocated_list_length());
        c10::DataPtr mem3 = allocate(256,device);
        EXPECT_EQ(3,instance->get_cpu_allocated_list_length());
    }
    EXPECT_EQ(0,instance->get_cpu_allocated_list_length());
    EXPECT_EQ(3,instance->get_cpu_cached_list_length());
    {
        c10::DataPtr mem1 = allocate(256,device);
        EXPECT_EQ(1,instance->get_cpu_allocated_list_length());
        EXPECT_EQ(2,instance->get_cpu_cached_list_length());
    }
    instance->free_cpu_cached_list();
    EXPECT_EQ(0,instance->get_cpu_allocated_list_length());
    EXPECT_EQ(0,instance->get_cpu_cached_list_length());
}

TEST(MemTrackerTest, CUDAListInsertRemove){
    auto* instance = get_mem_tracker();
    // instance->free_cpu_cached_list();
    EXPECT_EQ(0,instance->get_cuda_allocated_list_length());
    
    auto device = c10::Device(c10::DeviceType::CUDA,0);
    {
        c10::DataPtr mem1 = allocate(128,device);
        EXPECT_EQ(1,instance->get_cuda_allocated_list_length());
        c10::DataPtr mem2 = allocate(256,device);
        EXPECT_EQ(2,instance->get_cuda_allocated_list_length());
        c10::DataPtr mem3 = allocate(256,device);
        EXPECT_EQ(3,instance->get_cuda_allocated_list_length());
    }
    EXPECT_EQ(0,instance->get_cpu_allocated_list_length());
}

TEST(MemTrackerTest, MemAvailable){
    auto* instance = get_mem_tracker();
    c10::Device cpu(c10::DeviceType::CPU);
    c10::Device cuda(c10::DeviceType::CUDA,0);
    EXPECT_EQ(0,instance->get_cuda_allocated_list_length());
    EXPECT_EQ(0,instance->get_cpu_allocated_list_length());
    EXPECT_EQ(0,instance->get_cpu_cached_list_length());
    {
        auto host_mem = allocate(128,cpu);
        auto cuda_mem = allocate(128,cuda);
        memset(host_mem.get(),0,128);
        int re_code = 
        cudaMemcpy(cuda_mem.get(),host_mem.get(),128,cudaMemcpyHostToDevice);
        EXPECT_EQ(0,re_code);
    }
    EXPECT_EQ(0,instance->get_cuda_allocated_list_length());
    EXPECT_EQ(0,instance->get_cpu_allocated_list_length());
    EXPECT_EQ(1,instance->get_cpu_cached_list_length());
    instance->free_cpu_cached_list();
    
    EXPECT_EQ(0,instance->get_cpu_allocated_list_length());
    EXPECT_EQ(0,instance->get_cpu_cached_list_length());
}

TEST(MemTrackerTest, PinnedCPU){
    auto* instance = get_mem_tracker();

    c10::Device cpu(c10::DeviceType::CPU);
    auto block = allocate(128,cpu);
    cudaPointerAttributes data_attr;
    #ifdef ENABLE_PINNED_MEM
    EXPECT_TRUE(cudaPointerGetAttributes(&data_attr,block.get())!=cudaErrorInvalidValue);
    #elif DISABLE_PINNED_MEM
    EXPECT_TRUE(cudaPointerGetAttributes(&data_attr,block.get())==cudaErrorInvalidValue);
    #endif
    if(c10::IsNUMAEnabled()){
        std::cout<<"[Info Print] NUMA enabled"<<std::endl;
    } else {
        std::cout<<"[Info Print] NUMA disabled" <<std::endl;
    }
    block.clear();
    EXPECT_EQ(1,instance->get_cpu_cached_list_length());
    instance->free_cpu_cached_list();
    
}

TEST(MemTrackerTest, CUDAMemInfo){
    auto* instance = get_mem_tracker();
    c10::Device gpu0(c10::DeviceType::CUDA,0);
    c10::Device gpu1(c10::DeviceType::CUDA,1);
    c10::Device gpu2(c10::DeviceType::CUDA,2);
    c10::Device gpu3(c10::DeviceType::CUDA,3);
    auto block0 = allocate(128*1024*1024,gpu0);
    // auto block1 = allocate(128,gpu1);
    // caffeGetCudaMemoryPoolType();
    std::cout << "[Info Print] Allocate a 128MB block in GPU0" <<std::endl;
    auto cuda_mem_info = instance->get_cuda_mem_info(0);
    std::cout << cuda_mem_info;
    cuda_mem_info = instance->get_cuda_mem_info(1);
    std::cout << cuda_mem_info;

    block0.clear();
    // block1.clear();
}


TEST(MemTrackerTest, CPUMemInfo){
    auto* instance = get_mem_tracker();
    c10::Device cpu(c10::DeviceType::CPU,0);
    
    
    auto block0 = allocate(128 MiB,cpu);
    auto block1 = allocate(256 MiB,cpu);

    EXPECT_EQ(384 MiB, instance->get_cpu_mem_info().allocated_mem);
    EXPECT_EQ(0 MiB, instance->get_cpu_mem_info().cached_mem);

    block0.clear();
    EXPECT_EQ(256 MiB, instance->get_cpu_mem_info().allocated_mem);
    EXPECT_EQ(128 MiB, instance->get_cpu_mem_info().cached_mem);

    // auto block1 = allocate(128,gpu1);
    // caffeGetCudaMemoryPoolType();
    std::cout << "[Info Print] Allocate 128MB(freed) and 256MB block in cpu" <<std::endl;
    auto cuda_mem_info = instance->get_cpu_mem_info();
    std::cout << cuda_mem_info;

    block1.clear();
    instance->free_cpu_cached_list();
    // block1.clear();
}

TEST(MemTrackerTest, PinnedMemPressureTest){
    auto* instance = get_mem_tracker();
    c10::Device cpu(c10::DeviceType::CPU,0);
    int size = 10;
    auto large_block0 = allocate(size GiB, cpu);
    auto large_block1 = allocate(size GiB, cpu);
    auto large_block2 = allocate(size GiB, cpu);

    EXPECT_EQ(3*size GiB, instance->get_cpu_mem_info().allocated_mem);
    auto cpu_mem_info = instance->get_cpu_mem_info();
    std::cout << cpu_mem_info;
    large_block0.clear();    
    large_block1.clear();
    large_block2.clear();
    // large_block2.clear();

    instance->free_cpu_cached_list();
}




int main(int argc, char **argv){
    testing::InitGoogleTest(&argc, argv);
    
    std::cout<<"===MemTracker_test Begine==="<<std::endl;
    // get_mem_tracker();
    RUN_ALL_TESTS();
    return 0;
}