#include<iostream>
#include <torch/csrc/swapper/include/CopyEngine.h>
#include <torch/csrc/swapper/include/MemTracker.h>
#include <torch/csrc/swapper/include/utils/z_debug.h>
#include <gtest/gtest.h>
#include <cuda.h>
#include <cuda_runtime_api.h>
#include <thread>
#include <unistd.h>

using namespace torch::swapper;

auto copy_engine = get_copy_engine();
std::vector<int> candidates = {-1};

void init_copy_engine(){
    copy_engine->set_default_device_id(0);
    copy_engine->set_candidates(candidates);
    copy_engine->init_self();
}

TEST(CopyEngineTest, DataPtrResetByMultiThread){
    int N = (1<<20);
    int* tmp;
    cudaMallocHost(&tmp, N*sizeof(int));
    auto host = allocate(N*sizeof(int), c10::Device(c10::DeviceType::CPU)); // 申请的内存不适宜用cudaMemcpyAsync
    host.set_data_ptr_by_raw_ptr((void*)tmp); 
    auto device = allocate(N*sizeof(int), c10::Device(
        c10::DeviceType::CUDA,
        0
    ));
    int* device_ptr = (int*) device.get();
    int* host_ptr = (int*) host.get();
    for (int i = 0; i < 5; i++) host_ptr[i] = -i;
    cudaMemcpy(device_ptr, host_ptr, N*sizeof(int), cudaMemcpyHostToDevice);
    {
        auto reply = std::make_shared<Reply>();
        reply->device_list = {c10::Device(c10::DeviceType::CPU, -1)};
        reply->size_list = {N*sizeof(int)};
        copy_engine->ready_queues[1].push(
            CopyTask(
                taskType::SWAP_OUT,
                &device,
                N*sizeof(int),
                reply
            )
        );
    }
    device.wait_status_until(c10::DataStatus::SWAPPED);
    EXPECT_EQ(-3, ((int*)device.get())[3]);
}

int main(int argc, char** argv){
    testing::InitGoogleTest(&argc, argv);
    std::cout<<"===CopyEngine_test Begin==="<<std::endl;
    init_copy_engine();
    RUN_ALL_TESTS();
    return 0;
}