#include<iostream>
#include <torch/csrc/swapper/include/CopyEngine.h>
#include <torch/csrc/swapper/include/MemTracker.h>
#include <torch/csrc/swapper/include/utils/z_debug.h>
#include <gtest/gtest.h>
#include <cuda.h>
#include <cuda_runtime_api.h>
#include <thread>
#include <unistd.h>
#include <thread>
#include <functional>

using namespace torch::swapper;


// TEST(MultiThreadDataPtr, ProblemAboutPassDataPtrToOtherThread){
//     int N =32;
//     std::thread t;
//     {
//         auto dataptr = allocate(N*sizeof(int), c10::Device(c10::DeviceType::CPU));
//         int num = -1;
//         Z_DEBUG("local thread: dataptr.get()=%p\n", dataptr.get());
//         t = std::thread(
//             [&num, &dataptr](){
//                 usleep(100);
//                 Z_DEBUG("other thread: dataptr.get()=%p\n", dataptr.get());
//                 Z_DEBUG("num=%d\n", num);
//             }
//         );
//     };
//     t.detach();
// }


TEST(MultiThreadDataPtr, TryToSolvePassDataPtrToOtherThread){
    int N =32;
    std::thread t;
    {
        auto  dataptr = allocate(N*sizeof(int), c10::Device(c10::DeviceType::CPU));
        int num = -1;
        Z_DEBUG("local thread: dataptr.get()=%p\n", dataptr.get());
        
        auto f = [&num](c10::DataPtr ptr){
                    usleep(100);
                    Z_DEBUG("other thread: dataptr.get()=%p\n", ptr.get());
                    Z_DEBUG("num=%d\n", num);
                 };
        // auto f1 = std::bind(f, &dataptr);
        t = std::thread(f, std::move(dataptr));
    };
    t.detach();
}

int main(int argc, char** argv){
    testing::InitGoogleTest(&argc, argv);
    std::cout<<"===Test (DataPtr in Multi Thread Test) Begin==="<<std::endl;
    RUN_ALL_TESTS();
    return 0;
}