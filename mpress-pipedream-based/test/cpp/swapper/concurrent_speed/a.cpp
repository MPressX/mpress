#include <iostream>
#include <cuda.h>
#include <cuda_runtime_api.h>
#include <thread>
#include <unistd.h>

using namespace std;


int main(){
    unsigned long long nBytes = 1<<30;
    cudaStream_t stream;
    cudaEvent_t start, stop;
    printf("haha\n");
    cudaSetDevice(0);
    cudaStreamCreateWithFlags(&stream, cudaStreamNonBlocking);
    cudaEventCreate(&start);
    cudaEventCreate(&stop);


    void* pHost;
    void* pDevice;
    cudaMallocHost(&pHost, nBytes);
    cudaMalloc(&pDevice, nBytes);

    for (int i = 0; i < 10; i++){
        cudaEventRecord(start, stream);
        cudaMemcpyAsync(pDevice, pHost, nBytes, cudaMemcpyHostToDevice, stream);
        cudaEventRecord(stop, stream);
        cudaEventSynchronize(stop);
        float elapsedTime;
        cudaEventElapsedTime(&elapsedTime, start, stop);
        printf("elapsed time: %f ms\n", elapsedTime);
        float speed_GBperS = nBytes/1024/1024/1024/elapsedTime*1000;
        printf("speed: %f GB/s\n", speed_GBperS);
    }





    return 0;
}