#include <iostream>
#include <cuda.h>
#include <cuda_runtime_api.h>
#include <thread>
#include <unistd.h>
#include <vector>
using namespace std;


int main(){
    unsigned long long nBytes = 1<<30;
    vector<int> devices = {0,1,2};
    int N = devices.size();
    vector<cudaStream_t> streams(N);
    vector<cudaEvent_t> starts(N);
    vector<cudaEvent_t> stops(N);
    vector<void*> pHosts(N);
    vector<void*> pDevices(N);
    for (int i = 0; i < N; i++){
        cudaSetDevice(devices[i]);
        cudaStreamCreateWithFlags(&streams[i], cudaStreamNonBlocking);
        cudaEventCreate(&starts[i]);
        cudaEventCreate(&stops[i]);
        cudaMallocHost(&pHosts[i], nBytes);
        cudaMalloc(&pDevices[i], nBytes);
    }

    for (int cnt = 0; cnt < 10; cnt++){
        for (int i = 0; i < N; i++){
            cudaSetDevice(devices[i]);
            cudaEventRecord(starts[i], streams[i]);
        }
        for (int i = 0; i < N; i++){
            cudaSetDevice(devices[i]);
            cudaMemcpyAsync(pDevices[i], pHosts[i], nBytes, cudaMemcpyHostToDevice, streams[i]);
            cudaEventRecord(stops[i], streams[i]);
        }
        for (int i = 0; i < N; i++){
            cudaSetDevice(devices[i]);
            cudaEventSynchronize(stops[i]);
        }
        float elapsedTime;
        cudaEventElapsedTime(&elapsedTime, starts[N-1], stops[N-1]);
        printf("elapsed time: %f ms\n", elapsedTime);
        float speed_GBperS = nBytes/1024/1024/1024/elapsedTime*1000*N;
        printf("speed: %f GB/s\n", speed_GBperS);
    }

    return 0;
}