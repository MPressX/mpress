#include <iostream>
#include <cuda.h>
// #include <cuda_runtime_api.h>
#include <thread>
#include <functional>

using namespace std;

int GPU2CPU(void* host, void* device, size_t nbytes, cudaMemcpyKind cpy_kind, cudaStream_t stream,
            cudaEvent_t event2,
            function<void()> fn){
    cudaEvent_t event;
    cudaEventCreate(&event);
    cudaEventCreate(&event);
    cudaMemcpyAsync(host, device, nbytes, cpy_kind, stream);
    cudaEventRecord(event, stream);
    std::thread tmp{
        [&]{
            cudaEventSynchronize(event);
            fn();
        }
    };
    tmp.detach();
    return 0;
}

int main(){
    int N = 1<<20;
    int* host;
    cudaMallocHost(&host, N*sizeof(int));
    int* data;
    cudaMallocHost(&data, N*sizeof(int));
    int* device;
    cudaMalloc(&device, N*sizeof(int));

    cudaStream_t stream, stream2;
    cudaStreamCreate(&stream);
    cudaStreamCreate(&stream2);

    cudaEvent_t event;
    cudaEventCreate(&event);

    for (int i = 0; i < N; i++) host[i] = 2;
    printf("host:\t\t");
    for (int i = N-10; i < N; i++){
        printf("%d ", host[i]);
    }
    printf("\n");
    cudaMemcpy(device, host, N*sizeof(int), cudaMemcpyHostToDevice);

    // 执行代码
    host[0] = 3;
    GPU2CPU(data, device, sizeof(int)*N, cudaMemcpyDeviceToHost, stream, event, 
            [&](){
                printf("call fn\n");
                data[N-3]=4;
            });

    // free(device);  // 这一句应该与上一句memcpyAsync代码结束后一起做
    cudaMemcpyAsync(device+N-1, host, sizeof(int), cudaMemcpyHostToDevice, stream2); //device释放后 且 上一句异步未完成前，被其他进程/后续代码写入数据
    cudaMemcpy(device+N-2, host, sizeof(int), cudaMemcpyHostToDevice); //device释放后 且 上一句异步未完成前，被其他进程/后续代码写入数据
    
    // 验证结果
    int* finally;
    cudaMallocHost(&finally, N*sizeof(int));
    cudaMemcpy(finally, device, N*sizeof(int), cudaMemcpyDeviceToHost);


    printf("finally:\t");
    for (int i = N-10; i < N; i++){
        printf("%d ", finally[i]);
    }
    printf("\n");
    printf("data:\t\t");
    for (int i = N-10; i < N; i++){
        printf("%d ", data[i]);
    }
    printf("\n");

    
    return 0;
}