#include<iostream>
#include<string>
#include<ctime>
#include<cstdlib>
using namespace std;
class Student{
 	public:
		string Id;
		double Score;
	public:
        Student(){};
		Student(string id,double score){
			Id=id;
			Score=score;
		}
};


class Select{
	private:
		int N;
		int count=0;
		Student stuList[100];
	public:	
		Select(int n){
			N=n;
		}
			
		void read(){
			for(int i=0;i<N;i++){
				cout<<"请输入第"<<i+1<<"位学生的学号和分数"<<endl; 
				string id;
				double score;
				cin>>id>>score;
				Student stu(id,score);
				if(stu.Score>80)
					stuList[count++]=stu;
			}
		}
		
		void printOne(){
			srand(time(0));
	     	int r=rand() % count;
	     	cout<<"抽签出的学生学号和分数分别为："<<endl; 
	    	cout<<stuList[r].Id<<' '<<stuList[r].Score << endl;
			
		}
};


int main(){
	int N;
	cout<<"请输入学生总数"<<endl; 
	cin>>N;
	Select select(N);
	select.read();
	select.printOne();
	
	return 0;
}