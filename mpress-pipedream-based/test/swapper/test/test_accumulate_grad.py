import torch
from torch import nn

a = torch.rand(32,3,224,224).cuda()
a.requires_grad=True

conv = nn.Conv2d(3, 3, kernel_size = 3, padding = 1).cuda()
b = conv(a)
c=a*a

d=b+c

e = torch.sum(d)

e.backward()

from torch import swapper
print(f"a: {swapper.str_infos_about_tensor(a)}")
print(f"conv.weight: {swapper.str_infos_about_tensor(conv.weight)}")

# print(f"a.grad={a.grad}")
