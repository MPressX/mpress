
import colorful as cf

mark_for_swap_in = []
    
def demo7():
    import torch
    N = 2**22
    x = torch.ones([N]).to('cuda')
    x.requires_grad= True
    

    def forward(inputs):
        global mark_for_swap_in
        layer3 = torch.nn.ReLU(inplace=False)
        tmp1 = torch.pow(inputs,2)
        tmp2 = torch.pow(tmp1,2)
        torch.swapper.swap_out_by_layers(outputs=[tmp2]) # 下降16MB
        mark_for_swap_in.append(tmp2)
        tmp3 = torch.pow(tmp2,2)
        torch.swapper.swap_out_by_layers(outputs=[tmp3]) # 下降16MB
        mark_for_swap_in.append(tmp3)
        tmp4 = layer3(tmp3)
        return torch.sum(tmp4)
    
    c = forward(x)
    torch.swapper.swap_in_by_layers(outputs=mark_for_swap_in) # 注释掉这行反向无法运行：检查到数据未准备好
    ret_backward = c.backward()
    print(f"c={c}")
    print(f"grad: {x.grad}")
    
    
        

if __name__ == '__main__':
    demo7()