import torch
from torch import swapper

class Stage0(torch.nn.Module):
    def __init__(self):
        super(Stage0, self).__init__()
        self.layer2 = torch.nn.Conv2d(3, 64, kernel_size=(3,3), stride=(1,1), padding=(1,1))
        self.layer3 = torch.nn.ReLU(inplace=True)
        self.layer4 = torch.nn.Conv2d(64, 64, kernel_size = (3,3), stride=(1,1), padding=(1,1))
        
    def forward(self, input0)->torch.Tensor:
        out0 = input0.clone()
        out2 = self.layer2(out0)
        
        swapper.swap_out_by_layers(targets=[out2.grad_fn], excludes=[out2])
        self.layer4.register_swap_in(targets=[out2.grad_fn])
        out3 = self.layer3(out2)
        out4 = self.layer4(out3)
        swapper.swap_out_by_layers(targets=[out4.grad_fn], excludes=[out4])
        self.layer4.register_swap_in(targets=[out4.grad_fn], is_register_self=True)
        return out4
    
def generate_input(batchsize=32):
    return torch.rand(batchsize,3,224,224).cuda()

def generate_state_dict():
    state = {
        "layer2.weight": torch.rand(64,3,3,3).cuda(),
        "layer2.bias": torch.rand(64).cuda(),
        "layer4.weight": torch.rand(64,64,3,3).cuda(),
        "layer4.bias": torch.rand(64).cuda()
    }
    return state

def generate_gradient(batchsize=32):
    return torch.rand(32,64,224,224).cuda()