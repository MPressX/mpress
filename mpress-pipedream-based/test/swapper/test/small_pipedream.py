from collections import OrderedDict
import torch
from torch import nn
from torch import swapper
import colorful as cf
from vgg0 import Stage0, generate_input, generate_state_dict, generate_gradient
from collections import deque

def static_vars(**kwargs):
    def decorate(func):
        for k in kwargs:
            setattr(func, k, kwargs[k])
        return func
    return decorate

default_device_id = 0
torch.cuda.set_device(default_device_id)
swapper.init_swapper(
    default_device_id=default_device_id,
    cuda_memory_threshold=-1,
)

model:torch.nn.Module = Stage0().cuda()

num_warm_up_minibatches = 3
total_minibatches = 10
num_versions = 4
state_dicts = deque(maxlen=num_versions) # optimizer.queue
# for i in range(num_versions):
#     state_dicts.append(generate_state_dict())

def load_old_params():
    swapper.print_info(targets=[model.layer2.weight])
    model.load_state_dict_v2(state_dicts[0])
    swapper.print_info(targets=[model.layer2.weight])
def load_new_params():
    if len(state_dicts)<num_versions:
        state_dicts.append(generate_state_dict())
    swapper.print_info(targets=[model.layer2.weight])
    model.load_state_dict_v2(state_dicts[-1])
    swapper.print_info(targets=[model.layer2.weight])

tensors = []
@static_vars(counter=0)
def run_forward():
    run_forward.counter+=1
    print(f"Forward {run_forward.counter}")
    load_new_params()
    tensor = {}  
    tensor['input'] = generate_input()
    tensor['output'] = model(tensor['input'])
    tensors.append(tensor)
@static_vars(counter=0)
def run_backward():
    run_backward.counter+=1
    print(f"Backward {run_backward.counter}")
    load_old_params()
    tensor = tensors[0]
    tensor['gradient'] = generate_gradient()
    torch.autograd.backward(
        tensors=(tensor['output'],),
        grad_tensors=(tensor['gradient'],)
    )
@static_vars(counter=0)
def step():
    step.counter+=1
    print(f"Step {step.counter}")
    load_new_params()
    state_dict = model.state_dict()
    
    tensor = tensors.pop(0)
    swapper.swap_in_by_targets(targets=[model.layer2.weight, model.layer4.weight])
    swapper.wait_until_ok(targets=[model.layer2.weight, model.layer4.weight])

    if swapper.get_true_device_index(model.layer2.weight) == -1:
        raise RuntimeError("the weight should be on GPU now")
    if swapper.get_true_device_index(model.layer4.weight) == -1:
        raise RuntimeError("the weight should be on GPU now")

    new_state_dict = {}
    for key, item in model.state_dict().items():
        new_state_dict[key] = item.clone()
        if key.find("weight")>=0:
            swapper.swap_out_by_targets(targets=[item])

    state_dicts.append(new_state_dict)


for i in range(num_warm_up_minibatches):
    run_forward()
for i in range(total_minibatches-num_warm_up_minibatches):
    run_forward()
    run_backward()
    step()
for i in range(num_warm_up_minibatches):
    run_backward()
    step()

    
