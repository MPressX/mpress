import torch
from torch import nn

class Net(nn.Module):
    def __init__(self):
        super(Net, self).__init__()
        self.conv1 = nn.Conv2d(3,6,3,1,1)
        self.relu1 = nn.ReLU()
        self.pool1 = nn.MaxPool2d(2,2)
        self.conv2 = nn.Conv2d(6,9,3,1,1)
        self.relu2 = nn.ReLU()
        self.pool2 = nn.MaxPool2d(2,2)
        self.fc1 = nn.Linear(8*8*9, 120)
        self.relu3 = nn.ReLU()
        self.fc2 = nn.Linear(120,10)

    def forward(self, x):
        out = self.pool1(self.relu1(self.conv1(x)))
        out = self.pool2(self.relu2(self.conv2(out)))
        out = out.view(out.shape[0], -1)
        out = self.relu3(self.fc1(out))
        out = self.fc2(out)
        return out
    
def partial_print(weight):
    print(weight[0][0][0])
    
    
def net_conv1_weight(net):
    from torch import swapper
    t = net.conv1.weight
    print(f"infos about conv1.weight: {swapper.str_infos_about_tensor(t)}")
    partial_print(t)

def dict_conv1_weight(d):
    t = d['conv1.weight']
    partial_print(t)

    
if __name__ == '__main__':
    d = dict()
    import copy
    net:nn.Module = Net()
    state_dict = net.state_dict()
    state_dict = copy.deepcopy(state_dict)
    state_dict['conv1.weight']*=2
    net_conv1_weight(net)
    d[net.conv1.weight] = net.conv1.weight.data
    # net.load_state_dict(state_dict)
    # if net.conv1.weight in d:
    #     print(f"load_state_dict does not affect weight as key in dict")
    # else:
    #     print(f"load_state_dict does affect weight as key in dict")
        
    net.load_state_dict_v2(state_dict)
    if net.conv1.weight in d:
        print(f"load_state_dict_v2 does not affect weight as key in dict")
    else:
        print(f"load_state_dict_v2 does affect weight as key in dict")
    net_conv1_weight(net)
    partial_print(d[net.conv1.weight])