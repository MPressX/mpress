import torch
print(torch.swapper.name())
torch.cuda.set_device(1)
torch.swapper.init_swapper(1)
def print_mem():
    print(f"cuda memory cost: {torch.cuda.memory_allocated()}\n"
          f"cuda memory cached: {torch.cuda.memory_cached()}")

cuda_data = torch.ones(100,100).cuda()
cuda_data_cpy = cuda_data
print("original:\n"
      f"cuda_data={cuda_data}\n"
      f"cuda_data_cpy={cuda_data_cpy}")

print_mem()
torch.swapper.swap_out_by_targets([cuda_data])
torch.swapper.wait_until_ok([cuda_data], 2)
print_mem()
torch.swapper.swap_in_by_targets([cuda_data])
print_mem()

import time
time.sleep(1)

print("finally:\n"
      f"cuda_data={cuda_data}\n"
      f"cuda_data_cpy={cuda_data_cpy}")

