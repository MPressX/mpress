import torch
from torch.autograd.profiler import profile
def demo8():
    
    N = 2**27
    x = torch.ones([N],requires_grad=True).cuda()

    def forward(inputs):
        layer3 = torch.nn.ReLU(inplace=False).cuda()
        
        tmp1 = torch.pow(inputs,2)
        
        # torch.swapper.swap_out_by_layers(outputs=[tmp1]) # 下降16MB
        # mark_for_swap_in.append(tmp1) # 这个会将外面的x也给迁移出去
        tmp2 = torch.pow(tmp1,2) # before pow_grad(), main thread check tmp1 swap complete; check node savedvariables
        
        layer3.register_swap_in([tmp2.grad_fn,tmp2])
        
        tmp3 = torch.pow(tmp2,2)
        torch.swapper.swap_out_by_targets(targets=[tmp1,tmp2])
        
        # tmp2 = torch.relu(tmp1)
        tmp4 = layer3(tmp3)
        
        # layer3.register_swap_in(tmp1) # before layer3_grad start, swap tmp1 in begin
        return torch.sum(tmp4)
    
    
    # with profile(use_cuda=True) as prof:
    c = forward(x)
    c.backward()
    # print(prof.table())
    # prof.export_chrome_trace('./test_swap_in_out.json')

    
    
    # print(f"before swap out: mem={float(torch.cuda.memory_allocated())/1024/1024} MB")
    # torch.swapper.swap_out_by_targets(targets=[x])
    # time.sleep(1)
    # print(f"after swap out: mem={float(torch.cuda.memory_allocated())/1024/1024} MB")
    # torch.swapper.swap_in_by_targets(targets=[x])
    # torch.swapper.wait_until_ok(targets=[x])
    # print(f"after swap in: mem={float(torch.cuda.memory_allocated())/1024/1024} MB")

    


if __name__ == '__main__':
    demo8()