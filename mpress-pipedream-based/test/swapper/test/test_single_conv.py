import torch
from torch import nn
from torch import swapper

def get_mem_in_MB():
    return torch.cuda.memory_allocated()/1024/1024, torch.cuda.max_memory_allocated()/1024/1024, torch.cuda.memory_cached()/1024/1024


def test_cudnn(enabled=True):
    torch._C._set_cudnn_enabled(enabled)
    BS=128
    conv = nn.Conv2d(128, 128, kernel_size=3, padding=1).cuda()
    input_data = torch.rand(BS, 128,112,112).cuda()
    input_data.requires_grad=True
    print(get_mem_in_MB())
    output = conv(input_data)
    print(output.shape, output.dtype)
    loss = torch.sum(output)
    loss.backward()
    torch.cuda.synchronize()
    print(get_mem_in_MB())

if __name__ == '__main__':
    test_cudnn(True)
    test_cudnn(False)