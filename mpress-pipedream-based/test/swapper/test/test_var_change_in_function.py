class A:
    def __init__(self):
        self.cnt = 0
        self.fn = None
    def run(self):
        self.cnt += 1
        def fn(x):
            return x
        from functools import partial
        self.fn = partial(lambda x: fn(x), self.cnt)
    
if __name__ == '__main__':
    a = A()
    a.run()
    fn_1 = a.fn
    b_1=fn_1()
    print(f"b_1({b_1}) should be 1")
    a.run()
    fn_2 = a.fn
    b_1=fn_1()
    b_2 = fn_2()
    print(f"b_1({b_1}) should be 1")
    print(f"b_2({b_2}) should be 2")
    print(f"fn_1={fn_1}, fn_2={fn_2}")