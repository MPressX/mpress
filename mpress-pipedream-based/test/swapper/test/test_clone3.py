from collections import OrderedDict
import torch
from torch import nn
from torch import swapper
import colorful as cf

default_device_id = 0

torch.cuda.set_device(default_device_id)
swapper.init_swapper(
    default_device_id=default_device_id,
    cuda_memory_threshold=-1,
)

class Model(nn.Module):
    def __init__(self):
        super(Model, self).__init__()
        self.layer = torch.nn.Linear(3,2)
        # self.conv = torch.nn.Conv2d( 3,1,3,0)
        
        self.relu = torch.nn.ReLU(inplace=False)
    def forward(self, input):
        x = self.layer(input)
        
        # print(cf.red("self.layer.weight:"), end='') 
        # swapper.print_info(targets=[self.layer.weight])
        
        # swapper.swap_out_by_layers(targets=[x.grad_fn], excludes=[x, self.layer.weight])
        swapper.swap_out_by_layers(targets=[x.grad_fn], excludes=[x])
        self.layer.register_swap_in(targets=[x.grad_fn], is_register_self=True)
        print(f"x.is_cuda={x.is_cuda}")
        # x = self.relu(x)
        x = torch.sum(x)
        return x

model = Model().cuda()


old_state_dict = OrderedDict()
old_state_dict = {
    "layer.weight": torch.tensor([[10.0,10.0,10.0],[10.0,10.0,10.0]]).cuda(),
    "layer.bias": torch.tensor([10.0,10.0]).cuda()
}
new_state_dict = OrderedDict()
new_state_dict = {
    "layer.weight": torch.tensor([[20.0,20.0,0.0],[20.0,10.0,5.0]]).cuda(),
    "layer.bias": torch.tensor([20.0,20.0]).cuda()
}
print(f"before load old state for forward")
swapper.print_info(targets=[model.layer.weight])
model.load_state_dict(old_state_dict)
# model.load_state_dict(old_state_dict)
print(f"after load old state for forward")
swapper.print_info(targets=[model.layer.weight])


x= torch.tensor([[1., 2., 3.],[1.,2.,3.]])
x = x.cuda()
x.requires_grad=True

# y = model(x)
# y.backward()
# print(f"x.grad={x.grad}")
# print(f"model.layer.weight.grad={model.layer.weight.grad}")
# exit()

# x2= torch.tensor([[6., 4., 2.],[3.,2.,1.]])
x2 = torch.tensor([[1., 2., 3.],[1.,2.,3.]])
x2 = x2.cuda()
x2.requires_grad=True
print(f"ready to run forward for y=x")

y=model(x)
print(f"y={y}")
import time
time.sleep(1)
print(f"before load new state for forward")
swapper.print_info(targets=[model.layer.weight])
model.load_state_dict(new_state_dict)
print(f"after load new state for forward")
swapper.print_info(targets=[model.layer.weight])

torch.cuda.synchronize()
print(f"ready to run forward for y2=x2")
y2=model(x2)
print(f"y={y}")
print(f"y2={y2}")

print(f"before load old state for backward")
swapper.print_info(targets=[model.layer.weight])
model(old_state_dict)
print(f"after load old state for backward")
swapper.print_info(targets=[model.layer.weight])
y.backward()
print(f"x={x}")
print(f"x.grad={x.grad}")
print(f"model.layer.weight.grad={model.layer.weight.grad}")

print(f"brefore load new state for backward")
swapper.print_info(targets=[model.layer.weight])
model.load_state_dict(new_state_dict)
print(f"after load new state for backward")
swapper.print_info(targets=[model.layer.weight])
y2.backward()
print(f"x2={x2}")
print(f"x2.grad={x2.grad}")
print(f"model.layer.weight.grad={model.layer.weight.grad}")

