import torch
from torch import nn
import copy

def test_relu():
    relu = nn.ReLU(inplace=True).cuda()
    input = torch.rand(32,3,224,224).cuda()
    output = relu(input)
    print(f"output.shape={output.shape}")
    print(f"relu={relu}")
    v = copy.deepcopy(relu)
    print(f"v={v}")
    

def test_conv():
    conv = nn.Conv2d(3,64,kernel_size=2).cuda()
    input = torch.rand(32,3,224,224).cuda()
    output = conv(input)
    print(f"output.shape={output.shape}")
    print(f"conv={conv}")
    v = copy.deepcopy(conv)
    print(f"v={v}")
    

if __name__ == '__main__':
    # test_conv()
    test_relu()