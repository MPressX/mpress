from torch import swapper

swapper.init_swapper(
        default_device_id=1,
        cuda_memory_threshold=0,
        candidates=[-1],
        speed_matrix=[[11.7,  11.7,   11.7,   11.7,   11.7],
                    [12.8,  356.1,  3,   10.8,   4],
                    [12.8,  10.25,  354.62, 10.79,  11.00],
                    [12.8,  11.18,  3,  353.26, 4],
                    [12.8,  11.20,  11.13,  10.25,  352.67]]
    )
swapper.enable_hybrid_swap()
swapper.reset_advisor_index()
swapper.set_device_status([2,3,4],[100,200,300])