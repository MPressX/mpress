from torch import swapper
import torch


def serialize(t):
    ans = []
    if type(t) is torch.Tensor:
        return [t]
    elif type(t) is list or type(t) is tuple:
        for v in t:
            ans.extend(serialize(v))
    return ans

layer39 = torch.nn.LSTM(2048, 1024)
input = torch.rand(50,1,2048)
input.requires_grad=True
output = layer39(input, None)
found_tensors = swapper.search_metas_by_range(start_tensors=[input], end_tensors=serialize(output))
print(f"found_tensors={found_tensors}")
loss = torch.sum(output[0])
loss.backward()
print(input.grad)
# print(output.shape, loss.shape)