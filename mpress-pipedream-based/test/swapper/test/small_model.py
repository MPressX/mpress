import torch
import torch.nn as nn
import time
class VGG16(nn.Module):
    def __init__(self, num_classes=1000, init_weights=False):
        super(VGG16, self).__init__()
        self.conv64_1 = nn.Conv2d(3, 64, kernel_size = 3, padding = 1, bias=True) 
        # self.relu64_1 = nn.ReLU(True).cuda()
        # self.conv64_2 = nn.Conv2d(64, 64, kernel_size=3, padding=1).cuda()
        # self.relu64_2 = nn.ReLU(True).cuda()
        self.maxpool64 = nn.MaxPool2d(kernel_size=2, stride=2).cuda()
        # self.conv128_1 = nn.Conv2d(64, 128, kernel_size = 3, padding = 1).cuda()
        # self.relu128_1 = nn.ReLU(True).cuda()
        # self.conv128_2 = nn.Conv2d(128, 128, kernel_size=3, padding=1).cuda()
        # self.relu128_2 = nn.ReLU(True).cuda()
        # self.maxpool128 = nn.MaxPool2d(kernel_size=2, stride=2).cuda()
    def forward(self, x):
        x = self.conv64_1(x)
        torch.swapper.swap_out_by_layers(targets=[x.grad_fn],excludes=[x])
        self.maxpool64.register_swap_in(targets=[x.grad_fn])
        x = self.maxpool64(x)
        # self.maxpool64.register_swap_in(targets=[x.grad_fn])
        self.maxpool64.register_swap_in(targets=[x.grad_fn], is_register_self=True)
        # x = self.relu64_1(x)
        # torch.swapper.swap_out_by_layers(targets=[x.grad_fn],excludes=[x])
        # self.relu64_1.register_swap_in(targets=[x.grad_fn])
        # x = self.conv64_2(x)
        # torch.swapper.swap_out_by_layers(targets=[x.grad_fn],excludes=[x])
        # self.conv64_2.register_swap_in(targets=[x.grad_fn])
        # x = self.relu64_2(x)
        # torch.swapper.swap_out_by_layers(targets=[x.grad_fn],excludes=[x])
        # self.relu64_2.register_swap_in(targets=[x.grad_fn])
        # x = self.maxpool64(x)
        # x = self.conv128_1(x)
        # x = self.relu128_1(x)
        # x = self.conv128_2(x)
        # x = self.relu128_2(x)
        # torch.swapper.swap_out_by_layers(targets=[x.grad_fn],excludes=[x])
        # self.relu128_2.register_swap_in(targets=[x.grad_fn])
        # x = self.maxpool128(x)
        # torch.swapper.swap_out_by_layers(targets=[x.grad_fn],excludes=[x])
        # self.maxpool128.register_swap_in(targets=[x.grad_fn])
        x = torch.sum(x)

        return x

net = VGG16().cuda(0)


for _ in range(1000000):
    images = torch.randn(32,3,224,224,dtype=torch.float32,requires_grad=True).cuda(0)

    output = net.forward(images)
    print("[haiqwa python] before sleep, memory allocated: ", torch.cuda.memory_allocated()/(1024.0*1024),"MB")
    time.sleep(2)
    print("[haiqwa python] after sleep, memory allocated: ", torch.cuda.memory_allocated()/(1024.0*1024),"MB")

    output.backward()
    print("[haiqwa python] after backward, memory allocated: ", torch.cuda.memory_allocated()/(1024.0*1024),"MB")
    print("[haiqwa python] memory cached: ", torch.cuda.memory_cached()/(1024.0*1024),"MB")


