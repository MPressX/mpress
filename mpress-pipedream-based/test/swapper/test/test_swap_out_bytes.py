import torch
import torch.nn as nn
import numpy as np 
import torchvision.transforms as transforms
from torch import swapper
swapper.init_swapper(
    
)


class Net(nn.Module):
    def __init__(self):
        super(Net, self).__init__()
        self.conv1 = nn.Conv2d(3,6,3,1,1)
        self.relu1 = nn.ReLU()
        self.pool1 = nn.MaxPool2d(2,2)
        self.conv2 = nn.Conv2d(6,9,3,1,1)
        self.relu2 = nn.ReLU()
        self.pool2 = nn.MaxPool2d(2,2)
        self.fc1 = nn.Linear(8*8*9, 120)
        self.relu3 = nn.ReLU()
        self.fc2 = nn.Linear(120,10)

    def forward(self, x):
        out = self.pool1(self.relu1(self.conv1(x)))
        out = self.pool2(self.relu2(self.conv2(out)))
        out = out.view(out.shape[0], -1)
        out = self.relu3(self.fc1(out))
        swapper.swap_out_by_range(start_tensors=[x], end_tensors=[out], excludes=[out])
        out = self.fc2(out)
        return out



if __name__ == '__main__':
    bs=100
    label = torch.empty(bs, dtype=torch.long).random_(3).cuda()
    input_img = torch.randn(bs,3,32,32).requires_grad_().cuda()
    net = Net().cuda()
    loss_func = nn.CrossEntropyLoss()
    outs = net(input_img)
    torch.cuda.synchronize()
    loss = loss_func(outs, label)
    print(f"loss={loss}")
    print(f"outs.shape=d{outs.shape}")
    from torch import swapper
    swapper.launch_swap_in(srcs=[loss], dsts=[])
    loss.backward()
    print(f"net.conv1.weight.grad={net.conv1.weight.grad}")
    print(f"net.conv1.weight.grad.shape={net.conv1.weight.grad.shape}")
    swapped_bytes = swapper.get_swap_out_bytes()
    print(f"swapped_bytes = {swapped_bytes}")
    swapper.clear_swap_out_bytes()
    swapped_bytes = swapper.get_swap_out_bytes()
    print(f"swapped_bytes = {swapped_bytes}")