import torch
from torch import swapper
from torch.nn.modules import linear
swapper.init_swapper()

class demo_network(torch.nn.Module):
    def __init__(self) -> None:
        super().__init__()
        self.linear1 = torch.nn.Linear(4,4, bias=True)
        self.linear2 = torch.nn.Linear(4,4, bias=True)
    def forward(self, x):
        y = self.linear1(x)
        # print("y: ",y)
        z = self.linear2(y)
        # print("z: ",z)
        # print("linear2 weight: ", self.linear2.weight)
        # print("linear2 bias: ", self.linear2.bias)
        output = y + z
        swapout_tensors = swapper.swap_out_by_range(start_tensors=[y,], end_tensors=[output])
        output = torch.sum(output)
        output.grad_fn.register_swap_in(swapout_tensors)
        return output


x = torch.randn(4,4).cuda()
net = demo_network().cuda()


result = net(x)
result.backward()

result = net(x)
result.backward()

result = net(x)
result.backward()
# print(result)