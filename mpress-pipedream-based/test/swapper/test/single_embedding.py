import torch
import sys
from torch import swapper

def get_mem_in_MB():
    return torch.cuda.memory_allocated()/1024/1024, torch.cuda.max_memory_allocated()/1024/1024, torch.cuda.memory_cached()/1024/1024

def get_info(t:torch.Tensor):
    return f"shape={t.shape}, dtype={t.dtype}, nMiB={sys.getsizeof(t.storage())/1024/1024}"

layer = torch.nn.Embedding(32320, 1024).cuda()
# layer = torch.nn.Embedding(32320, 1024, padding_idx=0).cuda()

swapper.init_swapper()


def train_normal():
    for i in range(3):
        mem_allocated, mem_max_allocated, mem_cached = get_mem_in_MB()
        input0 = torch.randint(low=0, high=2**8, size=[50,1], dtype=torch.int64).cuda()
        output =layer(input0) 
        loss = torch.sum(output)
        loss.backward()
        print(f"step {i}, memory usage: {mem_allocated:0.4f}[{mem_max_allocated:0.4f}]({mem_cached:0.4f})")

def train_swap_by_range():
    for i in range(3):
        mem_allocated, mem_max_allocated, mem_cached = get_mem_in_MB()
        input0 = torch.randint(low=0, high=2**8, size=[50,1], dtype=torch.int64).cuda()
        output =layer(input0) 
        swapped_tensors = swapper.swap_out_by_range(start_tensors=[input0], end_tensors=[output])
        swapper.print_info(targets=[input0, output, swapped_tensors[0], swapped_tensors[1], layer.weight])
        # swapped_tensors[0] is input0
        # swapped_tensors[1] is layer.weight
        layer.register_swap_in(targets=swapped_tensors, is_register_self=True)
        loss = torch.sum(output)
        loss.backward()
        print(f"step {i}, memory usage: {mem_allocated:0.4f}[{mem_max_allocated:0.4f}]({mem_cached:0.4f})")

def train_swap_by_target():
    for i in range(10):
        mem_allocated, mem_max_allocated, mem_cached = get_mem_in_MB()
        input0 = torch.randint(low=0, high=2**8, size=[50,1], dtype=torch.int64).cuda()
        output =layer(input0) 
        swapper.swap_out_by_targets(targets=[layer.weight])
        layer.register_swap_in(targets=[layer.weight], is_register_self=True)
        loss = torch.sum(output)
        loss.backward()
        print(f"step {i}, memory usage: {mem_allocated:0.4f}[{mem_max_allocated:0.4f}]({mem_cached:0.4f})")

def train_find_and_swap():
    for i in range(10):
        mem_allocated, mem_max_allocated, mem_cached = get_mem_in_MB()
        input0 = torch.randint(low=0, high=2**8, size=[50,1], dtype=torch.int64).cuda()
        output =layer(input0) 
        tensors_found = swapper.search_tensors_by_range(start_tensors=[input0], end_tensors=[output])
        swapper.swap_out_by_targets(targets=tensors_found)
        layer.register_swap_in(targets=tensors_found, is_register_self=True)
        loss = torch.sum(output)
        loss.backward()
        print(f"step {i}, memory usage: {mem_allocated:0.4f}[{mem_max_allocated:0.4f}]({mem_cached:0.4f})")

def train_find_and_swap_only_weight():
    for i in range(10):
        mem_allocated, mem_max_allocated, mem_cached = get_mem_in_MB()
        input0 = torch.randint(low=0, high=2**8, size=[50,1], dtype=torch.int64).cuda()
        output =layer(input0) 
        tensors_found = swapper.search_tensors_by_range(start_tensors=[input0], end_tensors=[output])
        tensors_found = [tensors_found[1]]
        swapper.swap_out_by_targets(targets=tensors_found)
        layer.register_swap_in(targets=tensors_found, is_register_self=True)
        loss = torch.sum(output)
        loss.backward()
        print(f"step {i}, memory usage: {mem_allocated:0.4f}[{mem_max_allocated:0.4f}]({mem_cached:0.4f})")
    

def train_only_search_no_swap():
    for i in range(10):
        mem_allocated, mem_max_allocated, mem_cached = get_mem_in_MB()
        input0 = torch.randint(low=0, high=2**8, size=[50,1], dtype=torch.int64).cuda()
        output =layer(input0) 
        tensors_found = swapper.search_tensors_by_range(start_tensors=[input0], end_tensors=[output])
        loss = torch.sum(output)
        loss.backward()
        print(f"step {i}, memory usage: {mem_allocated:0.4f}[{mem_max_allocated:0.4f}]({mem_cached:0.4f})")
    

def train_test_metas():
    for i in range(10):
        mem_allocated, mem_max_allocated, mem_cached = get_mem_in_MB()
        input0 = torch.randint(low=0, high=2**8, size=[50,1], dtype=torch.int64).cuda()
        output =layer(input0) 
        metas_found = swapper.search_metas_by_range(start_tensors=[input0], end_tensors=[output])
        print(f"metas_found={metas_found}")
        mem_allocated, mem_max_allocated, mem_cached = get_mem_in_MB()
        print(f"step {i}, memory usage: {mem_allocated:0.4f}[{mem_max_allocated:0.4f}]({mem_cached:0.4f})")
        swapper.swap_out_by_metas(targets=metas_found)
        layer.register_swap_in(targets=metas_found, is_register_self=True)
        loss = torch.sum(output)
        torch.cuda.synchronize()
        mem_allocated, mem_max_allocated, mem_cached = get_mem_in_MB()
        print(f"step {i}, memory usage: {mem_allocated:0.4f}[{mem_max_allocated:0.4f}]({mem_cached:0.4f})")
        loss.backward()
    

if __name__ == '__main__':
    # train_normal() # 内存稳定
    # train_swap_by_range() # 内存上升
    # train_swap_by_target() # 内存稳定
    # train_find_and_swap() # 内存上升
    # train_find_and_swap_only_weight() # 内存上升
    # train_only_search_no_swap() # 内存上升
    train_test_metas()
