import torch
from torch import swapper
def print_grad(module, grad_inputs, grad_outputs):
    print("grad_inputs: ")
    print(grad_inputs)
    print("grad_outputs: ")
    print(grad_outputs)
    return grad_inputs
    

torch.cuda.set_device(0)
input_tensor = torch.randn(1,1,2,2, requires_grad = True).cuda()
print("input_tensor: ")
print(input_tensor)
max_pooling = torch.nn.MaxPool2d(kernel_size=(2,2))

max_pooling.register_backward_hook(print_grad)


output_tensor = max_pooling(input_tensor)
print("output_tensor: ")
print(output_tensor)

out = torch.sum(output_tensor)
swapper.analyze_operator(output_tensor.grad_fn)


# out.backward()


