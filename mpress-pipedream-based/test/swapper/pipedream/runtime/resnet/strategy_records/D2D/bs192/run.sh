# suitable for bert, first 3 layers to last 3 layers
# export CUDA_VISIBLE_DEVICES=0,2,7,5,6,1,3,4
# export CUDA_VISIBLE_DEVICES=0,1,2,3,4,5,6,7
# 2 links
# export CUDA_VISIBLE_DEVICES=0,3,2,1,5,6,7,4
# 1 link
# export CUDA_VISIBLE_DEVICES=0,1,3,7,5,4,6,2
export CUDA_VISIBLE_DEVICES=0,4,7,6,5,1,2,3


export Z_LEVEL=info

pkill python
bs=192
dir="output/bs${bs}/"
rm -rf ${dir}
mkdir -p ${dir}
mem_limit=30012254720
# mem_limit=0

for id in {0..7}
do
# if [ $id -eq 2 ] 
# then
# mem_limit=30012254720
# else
# mem_limit=0
# fi
echo "id=${id}, mem_limit=${mem_limit}, bs=${bs}"
python main.py --module model.optimized_gpus=8 -b ${bs} --candidates -1 4 5 6 7 --cuda_memory_threshold=${mem_limit} --rank ${id} --config_path model/optimized_gpus=8/mp_conf.json --distributed_backend nccl > ${dir}/stage${id}.log 2>&1 &
done

# copy files to directory
cp ./*.py ${dir}/
cp ./*.json ${dir}/
cp ./run.sh ${dir}/
nvidia-smi > ${dir}/nvidia-smi.log 2>&1
for i in {0..9}
do
sleep 10
nvidia-smi >> ${dir}/nvidia-smi.log 2>&1
done