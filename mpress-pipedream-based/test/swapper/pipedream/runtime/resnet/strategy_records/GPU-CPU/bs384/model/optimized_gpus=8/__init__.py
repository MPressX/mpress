from .stage0 import Stage0
from .stage1 import Stage1
from .stage2 import Stage2
from .stage3 import Stage3
from .stage4 import Stage4
from .stage5 import Stage5
from .stage6 import Stage6
from .stage7 import Stage7

def arch():
    return "resnet152"

# def model(criterion, config):
#     return [
#         (Stage0(config), ["input0"], ["out0", "out1"]),
#         (Stage1(config), ["out0", "out1"], ["out2", "out3"]),
#         (Stage2(config), ["out2", "out3"], ["out4", "out5"]),
#         (Stage3(config), ["out4", "out5"], ["out6", "out7"]),
#         (Stage4(config), ["out6", "out7"], ["out8", "out9"]),
#         (Stage5(config), ["out8", "out9"], ["out10", "out11"]),
#         (Stage6(config), ["out10", "out11"], ["out12", "out13"]),
#         (Stage7(config), ["out12", "out13"], ["out14"]),
#         (criterion, ["out14"], ["loss"])
#     ]
def model(criterion, config):
    return [
        (Stage0(config), ["input0"], ["out0"]),
        (Stage1(config), ["out0"], ["out1"]),
        (Stage2(config), ["out1"], ["out2"]),
        (Stage3(config), ["out2"], ["out3"]),
        (Stage4(config), ["out3"], ["out4"]),
        (Stage5(config), ["out4"], ["out5"]),
        (Stage6(config), ["out5"], ["out6"]),
        (Stage7(config), ["out6"], ["out7"]),
        (criterion, ["out7"], ["loss"])
    ]
# def model(criterion, config):
#     return [
#         (Stage0(config), ["input0"], ["out0", "out1"]),
#         (Stage1(config), ["out0", "out1"], ["out2", "out3"]),
#         (Stage2(config), ["out2", "out3"], ["out4", "out5"]),
#         (Stage3(config), ["out4", "out5"], ["out6", "out7"]),
#         (Stage4(config), ["out6", "out7"], ["out8", "out9"]),
#         (Stage5(config), ["out8", "out9"], ["out10", "out11"]),
#         (Stage6(config), ["out10", "out11"], ["out12", "out13"]),
#         (Stage7(config), ["out12", "out13"], ["out14"]),
#         (criterion, ["out14"], ["loss"])
#     ]
