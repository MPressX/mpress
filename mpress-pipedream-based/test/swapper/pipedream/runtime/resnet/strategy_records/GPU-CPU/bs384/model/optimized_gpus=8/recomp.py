from torch import nn
from torch.utils.checkpoint import checkpoint
from torch import swapper
class Recomp(nn.Module):
    def __init__(self, layer:nn.Module, destinations=[], ratios=[]):
        super(Recomp, self).__init__()
        self.layer = layer
        self.destinations = destinations
        self.ratios = ratios
        if type(self.layer) in [nn.ReLU]:
            self.not_suitable_for_recomp = True
        else:
            self.not_suitable_for_recomp = False
    def forward(self, input):
        if self.not_suitable_for_recomp:
            output =  self.layer(input)
        else:
            output =  checkpoint(self.layer, input)
        if self.destinations:
            # swapper.swap_out_by_range(start_tensors=[input], end_tensors=[output]+[p for p in self.layer.parameters()], destinations=self.destinations, ratios=self.ratios)
            meta_of_input = swapper.tensor_to_meta(input)
            swapper.swap_out_by_metas(targets=[meta_of_input], destinations=self.destinations, ratios=self.ratios)
        return output
class EnableSwap(nn.Module):
    def __init__(self, layer:nn.Module, destinations=[], ratios=[]):
        super(EnableSwap, self).__init__()
        self.layer=layer
        self.destinations=destinations
        self.ratios = ratios
    def forward(self, *args):
        output = self.layer(*args)
        end_tensors = list(output) if type(output) in [tuple, list] else [output]
        swapper.swap_out_by_range(start_tensors=list(args), end_tensors=end_tensors, excludes=end_tensors+[p for p in self.layer.parameters()], destinations=self.destinations, ratios=self.ratios)
        return output