import torch


class Block61(torch.nn.Module):
    def __init__(self):
        super(Block61, self).__init__()
        self.layer78f5 = torch.nn.Conv2d(1024, 256, kernel_size=(1, 1), stride=(1, 1), bias=False)
        self.layer79f5 = torch.nn.BatchNorm2d(256, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
        self.layer2 = torch.nn.ReLU(inplace=False)
        self.layer3 = torch.nn.Conv2d(256, 256, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1), bias=False)
        self.layer4 = torch.nn.BatchNorm2d(256, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
        self.layer5 = torch.nn.ReLU(inplace=True)
        self.layer6 = torch.nn.Conv2d(256, 1024, kernel_size=(1, 1), stride=(1, 1), bias=False)
        self.layer7 = torch.nn.BatchNorm2d(1024, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
        self.layer9 = torch.nn.ReLU(inplace=True)
    def forward(self, out77):
        out78 = self.layer78f5(out77)
        out79 = self.layer79f5(out78)
        out2 = self.layer2(out79)
        out3 = self.layer3(out2)
        out4 = self.layer4(out3)
        out5 = self.layer5(out4)
        out6 = self.layer6(out5)
        out7 = self.layer7(out6)
        out7 = out7 + out77
        out9 = self.layer9(out7)
        return out9
    
class Block62(torch.nn.Module):
    def __init__(self):
        super(Block62, self).__init__()
        self.layer10 = torch.nn.Conv2d(1024, 256, kernel_size=(1, 1), stride=(1, 1), bias=False)
        self.layer11 = torch.nn.BatchNorm2d(256, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
        self.layer12 = torch.nn.ReLU(inplace=True)
        self.layer13 = torch.nn.Conv2d(256, 256, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1), bias=False)
        self.layer14 = torch.nn.BatchNorm2d(256, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
        self.layer15 = torch.nn.ReLU(inplace=True)
        self.layer16 = torch.nn.Conv2d(256, 1024, kernel_size=(1, 1), stride=(1, 1), bias=False)
        self.layer17 = torch.nn.BatchNorm2d(1024, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
        self.layer19 = torch.nn.ReLU(inplace=True)
    def forward(self, out9):
        out10 = self.layer10(out9)
        out11 = self.layer11(out10)
        out12 = self.layer12(out11)
        out13 = self.layer13(out12)
        out14 = self.layer14(out13)
        out15 = self.layer15(out14)
        out16 = self.layer16(out15)
        out17 = self.layer17(out16)
        out17 = out17 + out9
        out19 = self.layer19(out17)
        return out19
        
        
class Block63(torch.nn.Module):
    def __init__(self):
        super(Block63, self).__init__()
        self.layer20 = torch.nn.Conv2d(1024, 256, kernel_size=(1, 1), stride=(1, 1), bias=False)
        self.layer21 = torch.nn.BatchNorm2d(256, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
        self.layer22 = torch.nn.ReLU(inplace=True)
        self.layer23 = torch.nn.Conv2d(256, 256, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1), bias=False)
        self.layer24 = torch.nn.BatchNorm2d(256, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
        self.layer25 = torch.nn.ReLU(inplace=True)
        self.layer26 = torch.nn.Conv2d(256, 1024, kernel_size=(1, 1), stride=(1, 1), bias=False)
        self.layer27 = torch.nn.BatchNorm2d(1024, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
        self.layer29 = torch.nn.ReLU(inplace=True)
    def forward(self, out19):
        out20 = self.layer20(out19)
        out21 = self.layer21(out20)
        out22 = self.layer22(out21)
        out23 = self.layer23(out22)
        out24 = self.layer24(out23)
        out25 = self.layer25(out24)
        out26 = self.layer26(out25)
        out27 = self.layer27(out26)
        out27 = out27 + out19
        out29 = self.layer29(out27)
        return out29

class Block64(torch.nn.Module):
    def __init__(self):
        super(Block64, self).__init__()
        self.layer30 = torch.nn.Conv2d(1024, 256, kernel_size=(1, 1), stride=(1, 1), bias=False)
        self.layer31 = torch.nn.BatchNorm2d(256, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
        self.layer32 = torch.nn.ReLU(inplace=True)
        self.layer33 = torch.nn.Conv2d(256, 256, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1), bias=False)
        self.layer34 = torch.nn.BatchNorm2d(256, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
        self.layer35 = torch.nn.ReLU(inplace=True)
        self.layer36 = torch.nn.Conv2d(256, 1024, kernel_size=(1, 1), stride=(1, 1), bias=False)
        self.layer37 = torch.nn.BatchNorm2d(1024, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
        self.layer39 = torch.nn.ReLU(inplace=True)
    def forward(self, out29):
        out30 = self.layer30(out29)
        out31 = self.layer31(out30)
        out32 = self.layer32(out31)
        out33 = self.layer33(out32)
        out34 = self.layer34(out33)
        out35 = self.layer35(out34)
        out36 = self.layer36(out35)
        out37 = self.layer37(out36)
        out37 = out37 + out29
        out39 = self.layer39(out37)
        return out39
    
class Block65(torch.nn.Module):
    def __init__(self):
        super(Block65, self).__init__()
        self.layer40 = torch.nn.Conv2d(1024, 256, kernel_size=(1, 1), stride=(1, 1), bias=False)
        self.layer41 = torch.nn.BatchNorm2d(256, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
        self.layer42 = torch.nn.ReLU(inplace=True)
        self.layer43 = torch.nn.Conv2d(256, 256, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1), bias=False)
        self.layer44 = torch.nn.BatchNorm2d(256, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
        self.layer45 = torch.nn.ReLU(inplace=True)
        self.layer46 = torch.nn.Conv2d(256, 1024, kernel_size=(1, 1), stride=(1, 1), bias=False)
        self.layer47 = torch.nn.BatchNorm2d(1024, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
        self.layer49 = torch.nn.ReLU(inplace=True)
    def forward(self, out39):
        out40 = self.layer40(out39)
        out41 = self.layer41(out40)
        out42 = self.layer42(out41)
        out43 = self.layer43(out42)
        out44 = self.layer44(out43)
        out45 = self.layer45(out44)
        out46 = self.layer46(out45)
        out47 = self.layer47(out46)
        out47 = out47 + out39
        out49 = self.layer49(out47)
        return out49
        
class Block66(torch.nn.Module):
    def __init__(self):
        super(Block66, self).__init__()
        self.layer50 = torch.nn.Conv2d(1024, 256, kernel_size=(1, 1), stride=(1, 1), bias=False)
        self.layer51 = torch.nn.BatchNorm2d(256, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
        self.layer52 = torch.nn.ReLU(inplace=True)
        self.layer53 = torch.nn.Conv2d(256, 256, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1), bias=False)
        self.layer54 = torch.nn.BatchNorm2d(256, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
        self.layer55 = torch.nn.ReLU(inplace=True)
        self.layer56 = torch.nn.Conv2d(256, 1024, kernel_size=(1, 1), stride=(1, 1), bias=False)
        self.layer57 = torch.nn.BatchNorm2d(1024, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
        self.layer59 = torch.nn.ReLU(inplace=True)
    def forward(self, out49):
        out50 = self.layer50(out49)
        out51 = self.layer51(out50)
        out52 = self.layer52(out51)
        out53 = self.layer53(out52)
        out54 = self.layer54(out53)
        out55 = self.layer55(out54)
        out56 = self.layer56(out55)
        out57 = self.layer57(out56)
        out57 = out57 + out49
        out59 = self.layer59(out57)
        return out59
        
class Block67(torch.nn.Module):
    def __init__(self):
        super(Block67, self).__init__()
        self.layer60 = torch.nn.Conv2d(1024, 256, kernel_size=(1, 1), stride=(1, 1), bias=False)
        self.layer61 = torch.nn.BatchNorm2d(256, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
        self.layer62 = torch.nn.ReLU(inplace=True)
        self.layer63 = torch.nn.Conv2d(256, 256, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1), bias=False)
        self.layer64 = torch.nn.BatchNorm2d(256, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
        self.layer65 = torch.nn.ReLU(inplace=True)
        self.layer66 = torch.nn.Conv2d(256, 1024, kernel_size=(1, 1), stride=(1, 1), bias=False)
        self.layer67 = torch.nn.BatchNorm2d(1024, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
        self.layer69 = torch.nn.ReLU(inplace=True)
    def forward(self, out59):
        out60 = self.layer60(out59)
        out61 = self.layer61(out60)
        out62 = self.layer62(out61)
        out63 = self.layer63(out62)
        out64 = self.layer64(out63)
        out65 = self.layer65(out64)
        out66 = self.layer66(out65)
        out67 = self.layer67(out66)
        out67 = out67 + out59
        out69 = self.layer69(out67)
        return out69
        

class Block68(torch.nn.Module):
    def __init__(self):
        super(Block68, self).__init__()
        self.layer70 = torch.nn.Conv2d(1024, 256, kernel_size=(1, 1), stride=(1, 1), bias=False)
        self.layer71 = torch.nn.BatchNorm2d(256, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
        self.layer72 = torch.nn.ReLU(inplace=True)
        self.layer73 = torch.nn.Conv2d(256, 256, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1), bias=False)
        self.layer74 = torch.nn.BatchNorm2d(256, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
        self.layer75 = torch.nn.ReLU(inplace=True)
        self.layer76 = torch.nn.Conv2d(256, 1024, kernel_size=(1, 1), stride=(1, 1), bias=False)
        self.layer77 = torch.nn.BatchNorm2d(1024, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
        self.layer79 = torch.nn.ReLU(inplace=True)
    def forward(self, out69):
        out70 = self.layer70(out69)
        out71 = self.layer71(out70)
        out72 = self.layer72(out71)
        out73 = self.layer73(out72)
        out74 = self.layer74(out73)
        out75 = self.layer75(out74)
        out76 = self.layer76(out75)
        out77 = self.layer77(out76)
        out77 = out77 + out69
        out79 = self.layer79(out77)
        return out79

class Stage6(torch.nn.Module):
    def __init__(self, config):
        self.config = config
        super(Stage6, self).__init__()
        self.layer1 = Block61()
        self.layer2 = Block62()
        self.layer3 = Block63()
        self.layer4 = Block64()
        self.layer5 = Block65()
        self.layer6 = Block66()
        self.layer7 = Block67()
        self.layer8 = Block68()
        self.wrap_layer()

    
    def wrap_layer(self):
        from torch import nn
        from .recomp import Recomp, EnableSwap
        for name, layer in self._modules.items():
            assert(name.startswith("layer"))
            wrapped_name = name.replace("layer", "wrap")
            id = int(name.replace("layer", ""))
            swap_choice = self.config.activations[id]
            
            if swap_choice.type=='recompute':
                self.__dict__[wrapped_name] = Recomp(layer)
            elif swap_choice.type == 'swap':
                self.__dict__[wrapped_name] = EnableSwap(layer, destinations=swap_choice.destinations, ratios=swap_choice.ratios)
            else:
                self.__dict__[wrapped_name] = layer
            
    def forward(self, x):
        x = self.wrap1(x)
        x = self.wrap2(x)
        x = self.wrap3(x)
        x = self.wrap4(x)
        x = self.wrap5(x)
        x = self.wrap6(x)
        x = self.wrap7(x)
        x = self.wrap8(x)
        return x
