import torch

class Block26(torch.nn.Module):
    def __init__(self):
        super(Block26, self).__init__()
        self.layer48 = torch.nn.Conv2d(512, 256, kernel_size=(1, 1), stride=(1, 1), bias=False)
        self.layer49 = torch.nn.BatchNorm2d(256, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
        self.layer50 = torch.nn.ReLU(inplace=True)
        self.layer51 = torch.nn.Conv2d(256, 256, kernel_size=(3, 3), stride=(2, 2), padding=(1, 1), bias=False)
        self.layer52 = torch.nn.BatchNorm2d(256, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
        self.layer53 = torch.nn.ReLU(inplace=True)
        self.layer54 = torch.nn.Conv2d(256, 1024, kernel_size=(1, 1), stride=(1, 1), bias=False)
        self.layer55 = torch.nn.BatchNorm2d(1024, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
        self.layer56 = torch.nn.Conv2d(512, 1024, kernel_size=(1, 1), stride=(2, 2), bias=False)
        self.layer57 = torch.nn.BatchNorm2d(1024, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
        self.layer59 = torch.nn.ReLU(inplace=True)
    def forward(self, out47):
        out48 = self.layer48(out47)
        out49 = self.layer49(out48)
        out50 = self.layer50(out49)
        out51 = self.layer51(out50)
        out52 = self.layer52(out51)
        out53 = self.layer53(out52)
        out54 = self.layer54(out53)
        out55 = self.layer55(out54)
        out56 = self.layer56(out47)
        out57 = self.layer57(out56)
        out55 = out55 + out57
        out59 = self.layer59(out55)
        return out59

class Block31(torch.nn.Module):
    def __init__(self):
        super(Block31, self).__init__()
        self.layer60f2 = torch.nn.Conv2d(1024, 256, kernel_size=(1, 1), stride=(1, 1), bias=False)
        self.layer2 = torch.nn.BatchNorm2d(256, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
        self.layer3 = torch.nn.ReLU(inplace=True)
        self.layer4 = torch.nn.Conv2d(256, 256, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1), bias=False)
        self.layer5 = torch.nn.BatchNorm2d(256, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
        self.layer6 = torch.nn.ReLU(inplace=True)
        self.layer7 = torch.nn.Conv2d(256, 1024, kernel_size=(1, 1), stride=(1, 1), bias=False)
        self.layer8 = torch.nn.BatchNorm2d(1024, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
        self.layer10 = torch.nn.ReLU(inplace=True)
    def forward(self, out59):
        out60 = self.layer60f2(out59)
        out2 = self.layer2(out60)
        out3 = self.layer3(out2)
        out4 = self.layer4(out3)
        out5 = self.layer5(out4)
        out6 = self.layer6(out5)
        out7 = self.layer7(out6)
        out8 = self.layer8(out7)
        out8 = out8 + out59
        out10 = self.layer10(out8)
        return out10
        
class Block32(torch.nn.Module):
    def __init__(self):
        super(Block32, self).__init__()
        self.layer11 = torch.nn.Conv2d(1024, 256, kernel_size=(1, 1), stride=(1, 1), bias=False)
        self.layer12 = torch.nn.BatchNorm2d(256, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
        self.layer13 = torch.nn.ReLU(inplace=True)
        self.layer14 = torch.nn.Conv2d(256, 256, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1), bias=False)
        self.layer15 = torch.nn.BatchNorm2d(256, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
        self.layer16 = torch.nn.ReLU(inplace=True)
        self.layer17 = torch.nn.Conv2d(256, 1024, kernel_size=(1, 1), stride=(1, 1), bias=False)
        self.layer18 = torch.nn.BatchNorm2d(1024, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
        self.layer20 = torch.nn.ReLU(inplace=True)
    def forward(self, out10):
        out11 = self.layer11(out10)
        out12 = self.layer12(out11)
        out13 = self.layer13(out12)
        out14 = self.layer14(out13)
        out15 = self.layer15(out14)
        out16 = self.layer16(out15)
        out17 = self.layer17(out16)
        out18 = self.layer18(out17)
        out18 = out18 + out10
        out20 = self.layer20(out18)
        return out20
    
class Block33(torch.nn.Module):
    def __init__(self):
        super(Block33, self).__init__()
        self.layer21 = torch.nn.Conv2d(1024, 256, kernel_size=(1, 1), stride=(1, 1), bias=False)
        self.layer22 = torch.nn.BatchNorm2d(256, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
        self.layer23 = torch.nn.ReLU(inplace=True)
        self.layer24 = torch.nn.Conv2d(256, 256, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1), bias=False)
        self.layer25 = torch.nn.BatchNorm2d(256, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
        self.layer26 = torch.nn.ReLU(inplace=True)
        self.layer27 = torch.nn.Conv2d(256, 1024, kernel_size=(1, 1), stride=(1, 1), bias=False)
        self.layer28 = torch.nn.BatchNorm2d(1024, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
        self.layer30 = torch.nn.ReLU(inplace=True)
    def forward(self, out20):
        out21 = self.layer21(out20)
        out22 = self.layer22(out21)
        out23 = self.layer23(out22)
        out24 = self.layer24(out23)
        out25 = self.layer25(out24)
        out26 = self.layer26(out25)
        out27 = self.layer27(out26)
        out28 = self.layer28(out27)
        out28 = out28 + out20
        out30 = self.layer30(out28)
        return out30
    
        
class Block34(torch.nn.Module):
    def __init__(self):
        super(Block34, self).__init__()
        self.layer31 = torch.nn.Conv2d(1024, 256, kernel_size=(1, 1), stride=(1, 1), bias=False)
        self.layer32 = torch.nn.BatchNorm2d(256, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
        self.layer33 = torch.nn.ReLU(inplace=True)
        self.layer34 = torch.nn.Conv2d(256, 256, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1), bias=False)
        self.layer35 = torch.nn.BatchNorm2d(256, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
        self.layer36 = torch.nn.ReLU(inplace=True)
        self.layer37 = torch.nn.Conv2d(256, 1024, kernel_size=(1, 1), stride=(1, 1), bias=False)
        self.layer38 = torch.nn.BatchNorm2d(1024, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
        self.layer40 = torch.nn.ReLU(inplace=True)
    def forward(self, out30):
        out31 = self.layer31(out30)
        out32 = self.layer32(out31)
        out33 = self.layer33(out32)
        out34 = self.layer34(out33)
        out35 = self.layer35(out34)
        out36 = self.layer36(out35)
        out37 = self.layer37(out36)
        out38 = self.layer38(out37)
        out38 = out38 + out30
        out40 = self.layer40(out38)
        return out40
    
class Block35(torch.nn.Module):
    def __init__(self):
        super(Block35, self).__init__()
        self.layer41 = torch.nn.Conv2d(1024, 256, kernel_size=(1, 1), stride=(1, 1), bias=False)
        self.layer42 = torch.nn.BatchNorm2d(256, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
        self.layer43 = torch.nn.ReLU(inplace=True)
        self.layer44 = torch.nn.Conv2d(256, 256, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1), bias=False)
        self.layer45 = torch.nn.BatchNorm2d(256, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
        self.layer46 = torch.nn.ReLU(inplace=True)
        self.layer47 = torch.nn.Conv2d(256, 1024, kernel_size=(1, 1), stride=(1, 1), bias=False)
        self.layer48 = torch.nn.BatchNorm2d(1024, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
        self.layer50 = torch.nn.ReLU(inplace=True)
    def forward(self, out40):
        out41 = self.layer41(out40)
        out42 = self.layer42(out41)
        out43 = self.layer43(out42)
        out44 = self.layer44(out43)
        out45 = self.layer45(out44)
        out46 = self.layer46(out45)
        out47 = self.layer47(out46)
        out48 = self.layer48(out47)
        out48 = out48 + out40
        out50 = self.layer50(out48)
        return out50
        
class Block36(torch.nn.Module):
    def __init__(self):
        super(Block36, self).__init__()
        self.layer51 = torch.nn.Conv2d(1024, 256, kernel_size=(1, 1), stride=(1, 1), bias=False)
        self.layer52 = torch.nn.BatchNorm2d(256, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
        self.layer53 = torch.nn.ReLU(inplace=True)
        self.layer54 = torch.nn.Conv2d(256, 256, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1), bias=False)
        self.layer55 = torch.nn.BatchNorm2d(256, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
        self.layer56 = torch.nn.ReLU(inplace=True)
        self.layer57 = torch.nn.Conv2d(256, 1024, kernel_size=(1, 1), stride=(1, 1), bias=False)
        self.layer58 = torch.nn.BatchNorm2d(1024, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
        self.layer60 = torch.nn.ReLU(inplace=True)
    def forward(self, out50):
        out51 = self.layer51(out50)
        out52 = self.layer52(out51)
        out53 = self.layer53(out52)
        out54 = self.layer54(out53)
        out55 = self.layer55(out54)
        out56 = self.layer56(out55)
        out57 = self.layer57(out56)
        out58 = self.layer58(out57)
        out58 = out58 + out50
        out60 = self.layer60(out58)
        return out60

class Block37(torch.nn.Module):
    def __init__(self):
        super(Block37, self).__init__()
        self.layer61 = torch.nn.Conv2d(1024, 256, kernel_size=(1, 1), stride=(1, 1), bias=False)
        self.layer62 = torch.nn.BatchNorm2d(256, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
        self.layer63 = torch.nn.ReLU(inplace=True)
        self.layer64 = torch.nn.Conv2d(256, 256, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1), bias=False)
        self.layer65 = torch.nn.BatchNorm2d(256, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
        self.layer66 = torch.nn.ReLU(inplace=True)
        self.layer67 = torch.nn.Conv2d(256, 1024, kernel_size=(1, 1), stride=(1, 1), bias=False)
        self.layer68 = torch.nn.BatchNorm2d(1024, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
        self.layer70 = torch.nn.ReLU(inplace=True)
    def forward(self, out60):
        out61 = self.layer61(out60)
        out62 = self.layer62(out61)
        out63 = self.layer63(out62)
        out64 = self.layer64(out63)
        out65 = self.layer65(out64)
        out66 = self.layer66(out65)
        out67 = self.layer67(out66)
        out68 = self.layer68(out67)
        out68 = out68 + out60
        out70 = self.layer70(out68)
        return out70
    
        
        
class Block38(torch.nn.Module):
    def __init__(self):
        super(Block38, self).__init__()
        self.layer71 = torch.nn.Conv2d(1024, 256, kernel_size=(1, 1), stride=(1, 1), bias=False)
        self.layer72 = torch.nn.BatchNorm2d(256, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
        self.layer73 = torch.nn.ReLU(inplace=True)
        self.layer74 = torch.nn.Conv2d(256, 256, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1), bias=False)
        self.layer75 = torch.nn.BatchNorm2d(256, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
        self.layer76 = torch.nn.ReLU(inplace=True)
        self.layer77 = torch.nn.Conv2d(256, 1024, kernel_size=(1, 1), stride=(1, 1), bias=False)
        self.layer2f4 = torch.nn.BatchNorm2d(1024, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
        self.layer4f4 = torch.nn.ReLU(inplace=True)
    def forward(self, out70):
        out71 = self.layer71(out70)
        out72 = self.layer72(out71)
        out73 = self.layer73(out72)
        out74 = self.layer74(out73)
        out75 = self.layer75(out74)
        out76 = self.layer76(out75)
        out77 = self.layer77(out76)
        out2 = self.layer2f4(out77)
        out2 = out2 + out70
        out4 = self.layer4f4(out2)
        return out4
class Stage3(torch.nn.Module):
    def __init__(self, config):
        self.config = config
        super(Stage3, self).__init__()
        self.layer0 = Block26()
        self.layer1 = Block31()
        self.layer2 = Block32()
        self.layer3 = Block33()
        self.layer4 = Block34()
        self.layer5 = Block35()
        self.layer6 = Block36()
        self.layer7 = Block37()
        self.layer8 = Block38()
        self.wrap_layer()

    
    def wrap_layer(self):
        from torch import nn
        from .recomp import Recomp, EnableSwap
        for name, layer in self._modules.items():
            assert(name.startswith("layer"))
            wrapped_name = name.replace("layer", "wrap")
            id = int(name.replace("layer", ""))
            swap_choice = self.config.activations[id]
            
            if swap_choice.type=='recompute':
                self.__dict__[wrapped_name] = Recomp(layer)
            elif swap_choice.type == 'swap':
                self.__dict__[wrapped_name] = EnableSwap(layer, destinations=swap_choice.destinations, ratios=swap_choice.ratios)
            else:
                self.__dict__[wrapped_name] = layer
            
    def forward(self, x):
        x = self.wrap0(x)
        x = self.wrap1(x)
        x = self.wrap2(x)
        x = self.wrap3(x)
        x = self.wrap4(x)
        x = self.wrap5(x)
        x = self.wrap6(x)
        x = self.wrap7(x)
        x = self.wrap8(x)
        return x
