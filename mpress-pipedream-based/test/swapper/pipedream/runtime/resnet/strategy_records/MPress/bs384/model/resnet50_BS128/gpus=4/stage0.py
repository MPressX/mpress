import torch


class Stage0(torch.nn.Module):
    def __init__(self, config):
        self.config = config
        super(Stage0, self).__init__()
        self.layer2 = torch.nn.Conv2d(3, 64, kernel_size=(7, 7), stride=(2, 2), padding=(3, 3), bias=False)
        self.layer3 = torch.nn.BatchNorm2d(64, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
        self.layer4 = torch.nn.ReLU(inplace=True)
        self.layer5 = torch.nn.MaxPool2d(kernel_size=3, stride=2, padding=1, dilation=1, ceil_mode=False)
        self.layer6 = torch.nn.Conv2d(64, 64, kernel_size=(1, 1), stride=(1, 1), bias=False)
        self.layer7 = torch.nn.BatchNorm2d(64, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
        self.layer8 = torch.nn.ReLU(inplace=True)
        self.layer9 = torch.nn.Conv2d(64, 64, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1), bias=False)
        self.layer10 = torch.nn.BatchNorm2d(64, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
        self.layer11 = torch.nn.ReLU(inplace=True)
        self.cast_to_ckpt()
        
    
    def cast_to_ckpt(self):
        from torch import nn
        from .recomp import Recomp
        for key, f in self._modules.items():
            assert(key.startswith("layer"))
            ckpt = key.replace("layer", "ckpt")
            if type(f) in [nn.ReLU]:
                self.__dict__[ckpt] = f
            else:
                self.__dict__[ckpt] = Recomp(f)
        

    def forward(self, input0):
        out0 = input0.clone()
        out2 = self.ckpt2(out0)
        out3 = self.ckpt3(out2)
        out4 = self.ckpt4(out3)
        out5 = self.ckpt5(out4)
        out6 = self.ckpt6(out5)
        out7 = self.ckpt7(out6)
        out8 = self.ckpt8(out7)
        out9 = self.ckpt9(out8)
        out10 = self.ckpt10(out9)
        out11 = self.ckpt11(out10)
        return (out11, out5)
