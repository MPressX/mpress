from torch import nn
from torch.utils.checkpoint import checkpoint
class Recomp(nn.Module):
    def __init__(self, layer:nn.Module):
        super(Recomp, self).__init__()
        self.layer = layer
    def forward(self, *args):
        return checkpoint(self.layer, *args)
    
class EnableSwap(nn.Module):
    def __init__(self, layer:nn.Module, destinations=[], ratios=[]):
        super(EnableSwap, self).__init__()
        self.layer = layer
        self.destinations = destinations
        self.ratios = ratios
    def forward(self, *args):
        