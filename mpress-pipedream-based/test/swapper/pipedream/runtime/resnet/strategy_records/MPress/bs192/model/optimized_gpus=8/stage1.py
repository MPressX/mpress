import torch

class Block12(torch.nn.Module):
    def __init__(self):
        super(Block12, self).__init__()
        self.layer5 = torch.nn.Conv2d(256, 512, kernel_size=(1, 1), stride=(2, 2), bias=False)
        self.layer6 = torch.nn.BatchNorm2d(512, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
        self.layer7 = torch.nn.Conv2d(256, 128, kernel_size=(1, 1), stride=(1, 1), bias=False)
        self.layer8 = torch.nn.BatchNorm2d(128, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
        self.layer9 = torch.nn.ReLU(inplace=True)
        self.layer10 = torch.nn.Conv2d(128, 128, kernel_size=(3, 3), stride=(2, 2), padding=(1, 1), bias=False)
        self.layer11 = torch.nn.BatchNorm2d(128, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
        self.layer12 = torch.nn.ReLU(inplace=True)
        self.layer13 = torch.nn.Conv2d(128, 512, kernel_size=(1, 1), stride=(1, 1), bias=False)
        self.layer14 = torch.nn.BatchNorm2d(512, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
        self.layer16 = torch.nn.ReLU(inplace=True)
    def forward(self, out4):
        out5 = self.layer5(out4)
        out6 = self.layer6(out5)
        out7 = self.layer7(out4)
        out8 = self.layer8(out7)
        out9 = self.layer9(out8)
        out10 = self.layer10(out9)
        out11 = self.layer11(out10)
        out12 = self.layer12(out11)
        out13 = self.layer13(out12)
        out14 = self.layer14(out13)
        out14 = out14 + out6
        out16 = self.layer16(out14)
        return out16
        
class Block13(torch.nn.Module):
    def __init__(self):
        super(Block13, self).__init__()
        self.layer17 = torch.nn.Conv2d(512, 128, kernel_size=(1, 1), stride=(1, 1), bias=False)
        self.layer18 = torch.nn.BatchNorm2d(128, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
        self.layer19 = torch.nn.ReLU(inplace=True)
        self.layer20 = torch.nn.Conv2d(128, 128, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1), bias=False)
        self.layer21 = torch.nn.BatchNorm2d(128, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
        self.layer22 = torch.nn.ReLU(inplace=True)
        self.layer23 = torch.nn.Conv2d(128, 512, kernel_size=(1, 1), stride=(1, 1), bias=False)
        self.layer24 = torch.nn.BatchNorm2d(512, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
        self.layer26 = torch.nn.ReLU(inplace=True)
    def forward(self, out16):
        out17 = self.layer17(out16)
        out18 = self.layer18(out17)
        out19 = self.layer19(out18)
        out20 = self.layer20(out19)
        out21 = self.layer21(out20)
        out22 = self.layer22(out21)
        out23 = self.layer23(out22)
        out24 = self.layer24(out23)
        out24 = out24 + out16
        out26 = self.layer26(out24)
        return out26

class Block14(torch.nn.Module):
    def __init__(self):
        super(Block14, self).__init__()
        self.layer27 = torch.nn.Conv2d(512, 128, kernel_size=(1, 1), stride=(1, 1), bias=False)
        self.layer28 = torch.nn.BatchNorm2d(128, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
        self.layer29 = torch.nn.ReLU(inplace=True)
        self.layer30 = torch.nn.Conv2d(128, 128, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1), bias=False)
        self.layer31 = torch.nn.BatchNorm2d(128, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
        self.layer32 = torch.nn.ReLU(inplace=True)
        self.layer33 = torch.nn.Conv2d(128, 512, kernel_size=(1, 1), stride=(1, 1), bias=False)
        self.layer34 = torch.nn.BatchNorm2d(512, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
        self.layer36 = torch.nn.ReLU(inplace=True)
    def forward(self, out26):
        out27 = self.layer27(out26)
        out28 = self.layer28(out27)
        out29 = self.layer29(out28)
        out30 = self.layer30(out29)
        out31 = self.layer31(out30)
        out32 = self.layer32(out31)
        out33 = self.layer33(out32)
        out34 = self.layer34(out33)
        out34 = out34 + out26
        out36 = self.layer36(out34)
        return out36
    
class Stage1(torch.nn.Module):
    def __init__(self, config):
        self.config = config
        super(Stage1, self).__init__()
        self.layer2 = Block12()
        self.layer3 = Block13()
        self.layer4 = Block14()
        self.wrap_layer()

    
    def wrap_layer(self):
        from torch import nn
        from .recomp import Recomp, EnableSwap
        for name, layer in self._modules.items():
            assert(name.startswith("layer"))
            wrapped_name = name.replace("layer", "wrap")
            id = int(name.replace("layer", ""))
            swap_choice = self.config.activations[id]
            
            if swap_choice.type=='recompute':
                self.__dict__[wrapped_name] = Recomp(layer)
            elif swap_choice.type == 'swap':
                self.__dict__[wrapped_name] = EnableSwap(layer, destinations=swap_choice.destinations, ratios=swap_choice.ratios)
            else:
                self.__dict__[wrapped_name] = layer
            
    def forward(self, x):
        x = self.wrap2(x)
        x = self.wrap3(x)
        x = self.wrap4(x)
        return x