import torch

class Block51(torch.nn.Module):
    def __init__(self):
        super(Block51, self).__init__()
        self.layer75f4 = torch.nn.Conv2d(1024, 256, kernel_size=(1, 1), stride=(1, 1), bias=False)
        self.layer76f4 = torch.nn.BatchNorm2d(256, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
        self.layer77f4 = torch.nn.ReLU(inplace=True)
        self.layer78f4 = torch.nn.Conv2d(256, 256, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1), bias=False)
        self.layer2 = torch.nn.BatchNorm2d(256, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
        self.layer3 = torch.nn.ReLU(inplace=True)
        self.layer4 = torch.nn.Conv2d(256, 1024, kernel_size=(1, 1), stride=(1, 1), bias=False)
        self.layer5 = torch.nn.BatchNorm2d(1024, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
        self.layer7 = torch.nn.ReLU(inplace=True)
    def forward(self, out74):
        out75 = self.layer75f4(out74)
        out76 = self.layer76f4(out75)
        out77 = self.layer77f4(out76)
        out78 = self.layer78f4(out77)
        out2 = self.layer2(out78)
        out3 = self.layer3(out2)
        out4 = self.layer4(out3)
        out5 = self.layer5(out4)
        out5 = out5 + out74
        out7 = self.layer7(out5)
        return out7

    
class Block52(torch.nn.Module):
    def __init__(self):
        super(Block52, self).__init__()
        self.layer8 = torch.nn.Conv2d(1024, 256, kernel_size=(1, 1), stride=(1, 1), bias=False)
        self.layer9 = torch.nn.BatchNorm2d(256, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
        self.layer10 = torch.nn.ReLU(inplace=True)
        self.layer11 = torch.nn.Conv2d(256, 256, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1), bias=False)
        self.layer12 = torch.nn.BatchNorm2d(256, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
        self.layer13 = torch.nn.ReLU(inplace=True)
        self.layer14 = torch.nn.Conv2d(256, 1024, kernel_size=(1, 1), stride=(1, 1), bias=False)
        self.layer15 = torch.nn.BatchNorm2d(1024, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
        self.layer17 = torch.nn.ReLU(inplace=True)
    def forward(self, out7):
        out8 = self.layer8(out7)
        out9 = self.layer9(out8)
        out10 = self.layer10(out9)
        out11 = self.layer11(out10)
        out12 = self.layer12(out11)
        out13 = self.layer13(out12)
        out14 = self.layer14(out13)
        out15 = self.layer15(out14)
        out15 = out15 + out7
        out17 = self.layer17(out15)
        return out17
    
    
class Block53(torch.nn.Module):
    def __init__(self):
        super(Block53, self).__init__()
        self.layer18 = torch.nn.Conv2d(1024, 256, kernel_size=(1, 1), stride=(1, 1), bias=False)
        self.layer19 = torch.nn.BatchNorm2d(256, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
        self.layer20 = torch.nn.ReLU(inplace=True)
        self.layer21 = torch.nn.Conv2d(256, 256, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1), bias=False)
        self.layer22 = torch.nn.BatchNorm2d(256, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
        self.layer23 = torch.nn.ReLU(inplace=True)
        self.layer24 = torch.nn.Conv2d(256, 1024, kernel_size=(1, 1), stride=(1, 1), bias=False)
        self.layer25 = torch.nn.BatchNorm2d(1024, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
        self.layer27 = torch.nn.ReLU(inplace=True)
    def forward(self, out17):
        out18 = self.layer18(out17)
        out19 = self.layer19(out18)
        out20 = self.layer20(out19)
        out21 = self.layer21(out20)
        out22 = self.layer22(out21)
        out23 = self.layer23(out22)
        out24 = self.layer24(out23)
        out25 = self.layer25(out24)
        out25 = out25 + out17
        out27 = self.layer27(out25)
        return out27
    
class Block54(torch.nn.Module):
    def __init__(self):
        super(Block54, self).__init__()
        self.layer28 = torch.nn.Conv2d(1024, 256, kernel_size=(1, 1), stride=(1, 1), bias=False)
        self.layer29 = torch.nn.BatchNorm2d(256, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
        self.layer30 = torch.nn.ReLU(inplace=True)
        self.layer31 = torch.nn.Conv2d(256, 256, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1), bias=False)
        self.layer32 = torch.nn.BatchNorm2d(256, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
        self.layer33 = torch.nn.ReLU(inplace=True)
        self.layer34 = torch.nn.Conv2d(256, 1024, kernel_size=(1, 1), stride=(1, 1), bias=False)
        self.layer35 = torch.nn.BatchNorm2d(1024, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
        self.layer37 = torch.nn.ReLU(inplace=True)
    def forward(self, out27):
        out28 = self.layer28(out27)
        out29 = self.layer29(out28)
        out30 = self.layer30(out29)
        out31 = self.layer31(out30)
        out32 = self.layer32(out31)
        out33 = self.layer33(out32)
        out34 = self.layer34(out33)
        out35 = self.layer35(out34)
        out35 = out35 + out27
        out37 = self.layer37(out35)
        return out37
        
        
class Block55(torch.nn.Module):
    def __init__(self):
        super(Block55, self).__init__()
        self.layer38 = torch.nn.Conv2d(1024, 256, kernel_size=(1, 1), stride=(1, 1), bias=False)
        self.layer39 = torch.nn.BatchNorm2d(256, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
        self.layer40 = torch.nn.ReLU(inplace=True)
        self.layer41 = torch.nn.Conv2d(256, 256, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1), bias=False)
        self.layer42 = torch.nn.BatchNorm2d(256, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
        self.layer43 = torch.nn.ReLU(inplace=True)
        self.layer44 = torch.nn.Conv2d(256, 1024, kernel_size=(1, 1), stride=(1, 1), bias=False)
        self.layer45 = torch.nn.BatchNorm2d(1024, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
        self.layer47 = torch.nn.ReLU(inplace=True)
    def forward(self, out37):
        out38 = self.layer38(out37)
        out39 = self.layer39(out38)
        out40 = self.layer40(out39)
        out41 = self.layer41(out40)
        out42 = self.layer42(out41)
        out43 = self.layer43(out42)
        out44 = self.layer44(out43)
        out45 = self.layer45(out44)
        out45 = out45 + out37
        out47 = self.layer47(out45)
        return out47
        
class Block56(torch.nn.Module):
    def __init__(self):
        super(Block56, self).__init__()
        self.layer48 = torch.nn.Conv2d(1024, 256, kernel_size=(1, 1), stride=(1, 1), bias=False)
        self.layer49 = torch.nn.BatchNorm2d(256, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
        self.layer50 = torch.nn.ReLU(inplace=True)
        self.layer51 = torch.nn.Conv2d(256, 256, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1), bias=False)
        self.layer52 = torch.nn.BatchNorm2d(256, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
        self.layer53 = torch.nn.ReLU(inplace=True)
        self.layer54 = torch.nn.Conv2d(256, 1024, kernel_size=(1, 1), stride=(1, 1), bias=False)
        self.layer55 = torch.nn.BatchNorm2d(1024, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
        self.layer57 = torch.nn.ReLU(inplace=True)
    def forward(self, out47):
        out48 = self.layer48(out47)
        out49 = self.layer49(out48)
        out50 = self.layer50(out49)
        out51 = self.layer51(out50)
        out52 = self.layer52(out51)
        out53 = self.layer53(out52)
        out54 = self.layer54(out53)
        out55 = self.layer55(out54)
        out55 = out55 + out47
        out57 = self.layer57(out55)
        return out57
        
class Block57(torch.nn.Module):
    def __init__(self):
        super(Block57, self).__init__()
        self.layer58 = torch.nn.Conv2d(1024, 256, kernel_size=(1, 1), stride=(1, 1), bias=False)
        self.layer59 = torch.nn.BatchNorm2d(256, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
        self.layer60 = torch.nn.ReLU(inplace=True)
        self.layer61 = torch.nn.Conv2d(256, 256, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1), bias=False)
        self.layer62 = torch.nn.BatchNorm2d(256, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
        self.layer63 = torch.nn.ReLU(inplace=True)
        self.layer64 = torch.nn.Conv2d(256, 1024, kernel_size=(1, 1), stride=(1, 1), bias=False)
        self.layer65 = torch.nn.BatchNorm2d(1024, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
        self.layer67 = torch.nn.ReLU(inplace=True)
    def forward(self, out57):
        out58 = self.layer58(out57)
        out59 = self.layer59(out58)
        out60 = self.layer60(out59)
        out61 = self.layer61(out60)
        out62 = self.layer62(out61)
        out63 = self.layer63(out62)
        out64 = self.layer64(out63)
        out65 = self.layer65(out64)
        out65 = out65 + out57
        out67 = self.layer67(out65)
        return out67
        
        
class Block58(torch.nn.Module):
    def __init__(self):
        super(Block58, self).__init__()
        self.layer68 = torch.nn.Conv2d(1024, 256, kernel_size=(1, 1), stride=(1, 1), bias=False)
        self.layer69 = torch.nn.BatchNorm2d(256, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
        self.layer70 = torch.nn.ReLU(inplace=True)
        self.layer71 = torch.nn.Conv2d(256, 256, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1), bias=False)
        self.layer72 = torch.nn.BatchNorm2d(256, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
        self.layer73 = torch.nn.ReLU(inplace=True)
        self.layer74 = torch.nn.Conv2d(256, 1024, kernel_size=(1, 1), stride=(1, 1), bias=False)
        self.layer75 = torch.nn.BatchNorm2d(1024, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
        self.layer77 = torch.nn.ReLU(inplace=True)
    def forward(self, out67):
        out68 = self.layer68(out67)
        out69 = self.layer69(out68)
        out70 = self.layer70(out69)
        out71 = self.layer71(out70)
        out72 = self.layer72(out71)
        out73 = self.layer73(out72)
        out74 = self.layer74(out73)
        out75 = self.layer75(out74)
        out75 = out75 + out67
        out77 = self.layer77(out75)
        return out77
        


class Stage5(torch.nn.Module):
    def __init__(self, config):
        self.config = config
        super(Stage5, self).__init__()
        self.layer1 = Block51()
        self.layer2 = Block52()
        self.layer3 = Block53()
        self.layer4 = Block54()
        self.layer5 = Block55()
        self.layer6 = Block56()
        self.layer7 = Block57()
        self.layer8 = Block58()
        self.wrap_layer()

    
    def wrap_layer(self):
        from torch import nn
        from .recomp import Recomp, EnableSwap
        for name, layer in self._modules.items():
            assert(name.startswith("layer"))
            wrapped_name = name.replace("layer", "wrap")
            id = int(name.replace("layer", ""))
            swap_choice = self.config.activations[id]
            
            if swap_choice.type=='recompute':
                self.__dict__[wrapped_name] = Recomp(layer)
            elif swap_choice.type == 'swap':
                self.__dict__[wrapped_name] = EnableSwap(layer, destinations=swap_choice.destinations, ratios=swap_choice.ratios)
            else:
                self.__dict__[wrapped_name] = layer
            

    def forward(self, x):
        x = self.wrap1(x)
        x = self.wrap2(x)
        x = self.wrap3(x)
        x = self.wrap4(x)
        x = self.wrap5(x)
        x = self.wrap6(x)
        x = self.wrap7(x)
        x = self.wrap8(x)
        return x
