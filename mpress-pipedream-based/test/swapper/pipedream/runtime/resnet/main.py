
import argparse
from collections import OrderedDict
import importlib
import json
import os
import shutil
import sys
from tarfile import NUL
import time
from tkinter.messagebox import NO

import torch
from torch.autograd import Variable
import torch.nn as nn
import torch.nn.parallel
import torch.backends.cudnn as cudnn
import torch.optim
import torch.utils.data
import torch.utils.data.distributed
import torchvision.transforms as transforms
import torchvision.datasets as datasets
from torch import swapper

sys.path.append("..")
from LayerWiseOptimizerWithStashing.SGD import SGD as LayerWiseSGD
import runtime
from optimizer import OptimizerWithWeightStashing
import sgd

flush = sys.stdout.flush


parser = argparse.ArgumentParser(description='PyTorch ImageNet Training')
parser.add_argument('--distributed_backend', type=str,
                    help='distributed backend to use (gloo|nccl)')
parser.add_argument('--module', '-m', required=True,
                    help='name of module that contains model and tensor_shapes definition')
parser.add_argument('-b', '--batch-size', default=256, type=int,
                    metavar='N', help='mini-batch size (default: 256)')
parser.add_argument('--config_path', default=None, type=str,
                    help="Path of configuration file")
parser.add_argument('--rank', default=None, type=int,
                    help="Rank of worker")

parser.add_argument('--cuda_memory_threshold', default=0, type=int,
                    help='Used for swapper, default is 0')
parser.add_argument("--candidates", nargs='+', type=int, default=[-1], help="set the candidates to send\n"
                                                                            "-1: CPU\n"
                                                                            "default=[-1]\n"
                                                                            "example: --candidates 1 2 3")



# Helper methods.
def is_first_stage():
    return args.stage is None or (args.stage == 0)

def is_last_stage():
    return args.stage is None or (args.stage == (args.num_stages-1))



def get_mem_in_MB():
    return torch.cuda.memory_allocated()/1024/1024, torch.cuda.max_memory_allocated()/1024/1024, torch.cuda.memory_cached()/1024/1024

class AverageMeter(object):
    """Computes and stores the average and current value"""
    def __init__(self):
        self.reset()

    def reset(self):
        self.val = 0
        self.avg = 0
        self.sum = 0
        self.count = 0

    def update(self, val, n=1):
        self.val = val
        self.sum += val * n
        self.count += n
        self.avg = self.sum / self.count

# Synthetic Dataset class.
class SyntheticDataset(torch.utils.data.dataset.Dataset):
    def __init__(self, input_size, length, num_classes=1000):
        self.tensor = Variable(torch.rand(*input_size)).type(torch.FloatTensor)
        self.target = torch.Tensor(1).random_(0, num_classes)[0].type(torch.LongTensor)
        self.length = length

    def __getitem__(self, index):
        return self.tensor, self.target

    def __len__(self):
        return self.length
    
class SwapDst():
    def __init__(self, type, destinations=[], ratios=[]):
        assert(type in ['recompute', 'none', 'swap'])
        assert(len(destinations) == len(ratios))
        self.type = type
        self.destinations = destinations
        self.ratios = ratios

            
            
        
        
class SwapConfig:
    def __init__(self, rank):
        self.rank = rank
        self.optimizer = None
        self.activations = None
        # self.activations[layer_id] = SwapDst(xxx)
        self.weights = None
    
    def generate_activations(self, act):
        from collections import defaultdict
        def none():
            return SwapDst("none")
        dic = defaultdict(none)
        for j in act:
            layer_range = j['layer_range']
            type = j['type']
            destinations = [] if 'destinations' not in j else j['destinations']
            ratios = [] if 'ratios' not in j else j['ratios']
            swap_dst = SwapDst(type, destinations, ratios)
            for layer_id in range(layer_range[0], layer_range[1]):
                dic[layer_id] = swap_dst
        return dic
    
    def from_json_file(self, json_file):
        with open(json_file, 'r', encoding='utf-8') as f:
            content = f.read()
            j = json.loads(content)
            optimizer_swap_config = j['optimizer_swap_config']
            self.optimizer = optimizer_swap_config[f"optimizer{self.rank}"]
            activations = j['activations_swap_config'][f'stage{self.rank}']
            self.activations = self.generate_activations(activations)
            self.weights = j['weight_swap_config'][f'weight{self.rank}']

def train(train_loader, r:runtime.StageRuntime, optimizer:LayerWiseSGD, num_warmup_minibatches):
    global args

    hook = optimizer.get_optimize(enable_swap=False)
    swapper.register_optimize_func(hook)
    
    batch_time = AverageMeter()

    # switch to train mode
    r.train(0)
    if not is_first_stage(): train_loader = None
    r.set_loader(train_loader)
    
    forward_cnt = 0
    backward_cnt = 0
    
    # warmup 
    for i in range(num_warmup_minibatches+1):
        # optimizer.load_params_by_index(i)
        if i!=0:
            optimizer.generate_and_set_new_param(False)
        r.receive_tensors_forward()
        if i == num_warmup_minibatches:
            r.run_prefetch()
        r._run_forward()
        forward_cnt+=1
        r.send_tensors_forward()
    
    end = time.time()
    for i in range(5000):
        if is_last_stage():
            batch_time = time.time() - end
            end = time.time()
            print(f"Throughput: {args.batch_size/batch_time:.3f}, elapsed time {batch_time:.3f}")
            sys.stdout.flush()

        r.receive_tensors_backward()
        optimizer.load_params_by_index(backward_cnt)
        r._run_backward(enable_swap=True, optimizer=optimizer)
        backward_cnt+=1
        r.receive_tensors_forward()
        r.send_tensors_backward()
        r.run_prefetch()
        r._run_forward()
        forward_cnt += 1
        r.send_tensors_forward()
            
    
def main():
    global args
    args = parser.parse_args()
    torch.cuda.set_device(args.rank)
    print(f"args parsed, rank={args.rank}")

    swapper.init_swapper(
        default_device_id=args.rank,
        cuda_memory_threshold=args.cuda_memory_threshold,
        num_cards=8,
        candidates=args.candidates,
        swap_mode=2
    )
    swap_config = SwapConfig(args.rank)
    swap_config.from_json_file("optimize.json")
    print(f"swap_config.optimizer: ", swap_config.optimizer)
    
    # define loss function (criterion)
    criterion = nn.CrossEntropyLoss()

    # create stages of the model
    module = importlib.import_module(args.module)
    args.arch = module.arch()
    model = module.model(criterion, swap_config)
    print(f"model inited")
    import sys; sys.stdout.flush()
    
    input_size = [args.batch_size, 3, 224, 224]
    training_tensor_shapes = {"input0": input_size, "target": [args.batch_size]}
    dtypes = {"input0": torch.int64, "target": torch.int64}
    for (stage, inputs, outputs) in model[:-1]:  # Skip last layer (loss).
        input_tensors = []
        for input in inputs:
            input_tensor = torch.zeros(tuple(training_tensor_shapes[input]),
                                       dtype=torch.float32)
            input_tensors.append(input_tensor)
        with torch.no_grad():
            output_tensors = stage(*tuple(input_tensors))
        if not type(output_tensors) is tuple:
            output_tensors = [output_tensors]
        for output, output_tensor in zip(outputs,
                                         list(output_tensors)):
            training_tensor_shapes[output] = list(output_tensor.size())
            dtypes[output] = output_tensor.dtype

    for key in training_tensor_shapes:
        training_tensor_shapes[key] = tuple(
            training_tensor_shapes[key])
    
    
    # training_tensor_shapes={'input0': (args.batch_size, 3, 224, 224), 'target': (args.batch_size,), 'out0': (args.batch_size, 256, 56, 56), 'out1': (args.batch_size, 512, 28, 28), 'out2': (args.batch_size, 1024, 14, 14), 'out3': (args.batch_size, 1024, 14, 14), 'out4': (args.batch_size, 1024, 14, 14), 'out5': (args.batch_size, 1024, 14, 14), 'out6': (args.batch_size, 1024, 14, 14), 'out7': (args.batch_size, 1000)}
    # dtypes={'input0': torch.int64, 'target': torch.int64, 'out0': torch.float32, 'out1': torch.float32, 'out2': torch.float32, 'out3': torch.float32, 'out4': torch.float32, 'out5': torch.float32, 'out6': torch.float32, 'out7': torch.float32}
    # print(f"WARN: the shapes and dtypes are spceified for resnet152 large")
        
    print(f"training_tensor_shapes={training_tensor_shapes}")
    print(f"dtypes={dtypes}")
    
    inputs_module_destinations = {"input": 0}
    target_tensor_names = {"target"}

    # ===============================================
    # load configurations
    # ===============================================
    configuration_maps = {
        'module_to_stage_map': None,
        'stage_to_rank_map': None,
        'stage_to_depth_map': None
    }
    assert(args.config_path is not None)
    json_config_file = json.load(open(args.config_path, 'r'))
    configuration_maps['module_to_stage_map'] = json_config_file.get("module_to_stage_map", None)
    configuration_maps['stage_to_rank_map'] = json_config_file.get("stage_to_rank_map", None)
    configuration_maps['stage_to_rank_map'] = {
        int(k): v for (k, v) in configuration_maps['stage_to_rank_map'].items()}
    configuration_maps['stage_to_depth_map'] = json_config_file.get("stage_to_depth_map", None)
    print("configuration_maps: ", configuration_maps)
    sys.stdout.flush()
    
    
    
    r = runtime.StageRuntime(
        model=model, distributed_backend=args.distributed_backend,
        fp16=False, loss_scale=1,
        training_tensor_shapes=training_tensor_shapes,
        eval_tensor_shapes=None,
        training_tensor_dtypes=dtypes,
        inputs_module_destinations=inputs_module_destinations,
        target_tensor_names=target_tensor_names,
        configuration_maps=configuration_maps,
        master_addr="localhost", rank=args.rank,
        local_rank=args.rank,
        num_ranks_in_server=1,
        verbose_freq=0,
        model_type=runtime.IMAGE_CLASSIFICATION,
        enable_recompute=False)

    # stage needed to determine if current stage is the first stage
    # num_stages needed to determine if current stage is the last stage
    # num_ranks needed to determine number of warmup_minibatches in case of pipelining
    args.stage = r.stage
    args.num_stages = r.num_stages
    args.num_ranks = r.num_ranks
        
    num_versions = r.num_warmup_minibatches + 1
    
    optimizer = LayerWiseSGD(r.modules(), num_versions)
    optimizer.set_swap_config(swap_config.optimizer)
    
    # Data loading code
    normalize = transforms.Normalize(mean=[0.485, 0.456, 0.406],
                                     std=[0.229, 0.224, 0.225])
    
    # use syntheticdataset
    train_dataset = SyntheticDataset((3, 224, 224), 1000000)
    
    def generate_fake_data(batch_size):
        while True:
            input = torch.rand((batch_size, 3, 224, 224))
            target = torch.randint(0, 999, (batch_size, ))
            yield input, target
    train_loader = generate_fake_data(args.batch_size)
    
    print(f"launch train")
    flush()
    train(train_loader, r, optimizer, r.num_warmup_minibatches)
    
if __name__ == '__main__':
    main()