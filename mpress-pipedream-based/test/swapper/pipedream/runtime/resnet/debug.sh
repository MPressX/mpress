pkill python
bs=32
dir="debug/bs${bs}/"
rm -rf ${dir}
mkdir -p ${dir}
export Z_LEVEL=debug

print_rank=0

for id in {0..7}
do
if [ ${id} -ne ${print_rank} ]; then
python main.py --module model.optimized_gpus=8 -b ${bs} --candidates -1 --cuda_memory_threshold=0 --rank ${id} --config_path model/optimized_gpus=8/mp_conf.json --distributed_backend nccl > ${dir}/stage${id}.log 2>&1 &
fi
done

python main.py --module model.optimized_gpus=8 -b ${bs} --candidates -1 --cuda_memory_threshold=0 --rank ${print_rank} --config_path model/optimized_gpus=8/mp_conf.json --distributed_backend nccl



# copy files to directory
cp ./*.py ${dir}/
cp ./*.json ${dir}/
cp ./run.sh ${dir}/
nvidia-smi > ${dir}/nvidia-smi.log 2>&1
for i in {0..9}
do
sleep 10
nvidia-smi >> ${dir}/nvidia-smi.log 2>&1
done