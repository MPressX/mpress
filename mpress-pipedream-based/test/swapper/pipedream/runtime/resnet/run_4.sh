export Z_LEVEL=warning
for id in {0..3}
do
python main.py --module model.resnet50_BS128.gpus=4 -b 32 --candidates -1 --cuda_memory_threshold=0 --rank ${id} --config_path model/resnet50_BS128/gpus=4/mp_conf.json --distributed_backend nccl > stage${id}.log 2>&1 &
done