export Z_LEVEL=warning

pkill python
bs=128
dir="fig/device_mapping_default/"
rm -rf ${dir}
mkdir -p ${dir}
# mem_limit=30012254720
mem_limit=0

cp optimize.show_dev_map.json optimize.json

for id in {0..7}
do
# if [ $id -eq 2 ] 
# then
# mem_limit=30012254720
# else
# mem_limit=0
# fi
echo "id=${id}, mem_limit=${mem_limit}, bs=${bs}"
python main.py --module model.optimized_gpus=8 -b ${bs} --candidates -1 4 5 6 7 --cuda_memory_threshold=${mem_limit} --rank ${id} --config_path model/optimized_gpus=8/mp_conf.json --distributed_backend nccl > ${dir}/stage${id}.log 2>&1 &
done

# copy files to directory
cp ./*.py ${dir}/
cp ./*.json ${dir}/
cp ./run.sh ${dir}/
nvidia-smi > ${dir}/nvidia-smi.log 2>&1
for i in {0..9}
do
sleep 10
nvidia-smi >> ${dir}/nvidia-smi.log 2>&1
done