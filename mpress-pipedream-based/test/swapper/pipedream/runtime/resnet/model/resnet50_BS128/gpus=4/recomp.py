from torch import nn
from torch.utils.checkpoint import checkpoint
class Recomp(nn.Module):
    def __init__(self, layer:nn.Module):
        super(Recomp, self).__init__()
        self.layer = layer
    def forward(self, *args, **kwargs):
        return checkpoint(self.layer, *args, **kwargs)