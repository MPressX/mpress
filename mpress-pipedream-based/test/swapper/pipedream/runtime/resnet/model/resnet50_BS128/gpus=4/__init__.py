from .resnet50_BS128 import resnet50_BS128
from .stage0 import Stage0
from .stage1 import Stage1
from .stage2 import Stage2
from .stage3 import Stage3

def arch():
    return "resnet50_BS128"

def model(criterion, config):
    return [
        (Stage0(config), ["input0"], ["out1", "out0"]),
        (Stage1(config), ["out1", "out0"], ["out2"]),
        (Stage2(config), ["out2"], ["out3", "out4"]),
        (Stage3(config), ["out3", "out4"], ["out5"]),
        (criterion, ["out5"], ["loss"])
    ]

def full_model():
    return resnet50_BS128()
