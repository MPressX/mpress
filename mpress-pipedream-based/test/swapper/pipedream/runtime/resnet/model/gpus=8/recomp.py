from torch import nn
from torch.utils.checkpoint import checkpoint
from torch import swapper
class Recomp(nn.Module):
    def __init__(self, layer:nn.Module):
        super(Recomp, self).__init__()
        self.layer = layer
    def forward(self, *args):
        return checkpoint(self.layer, *args)
    
class EnableSwap(nn.Module):
    def __init__(self, layer:nn.Module, destinations=[], ratios=[]):
        super(EnableSwap, self).__init__()
        self.layer = layer
        self.destinations = destinations
        self.ratios = ratios
    def forward(self, *args):
        output = self.layer(*args)
        end_tensors = list(output) if type(output) in [tuple, list] else [output]
        swapper.swap_out_by_range(start_tensors=list(args), end_tensors=end_tensors, excludes=end_tensors+[p for p in self.layer.parameters()], destinations=self.destinations, ratios=self.ratios)
        return output