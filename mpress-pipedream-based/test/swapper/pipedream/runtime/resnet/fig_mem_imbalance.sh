export Z_LEVEL=warning

pkill python

# Pipedream's recommended model slicing scheme () can train up to 112 BS. We fine-tuned 
# the model to train only up to 96 without swap enabled. The overall memory usage is 
# the same. 
# You can also follow PipeDreams' README (https://github.com/msr-fiddle/pipedream) to 
# generate a slicing scheme based on profiling. Same overall memory usage is expected.
bs=96
dir="output/bs${bs}/"
rm -rf ${dir}
mkdir -p ${dir}
# mem_limit=30012254720
mem_limit=0

for id in {0..7}
do
echo "id=${id}, mem_limit=${mem_limit}, bs=${bs}"
python main.py --module model.gpus=8 -b ${bs} --candidates -1  --cuda_memory_threshold=${mem_limit} --rank ${id} --config_path model/gpus=8/mp_conf.json --distributed_backend nccl > ${dir}/stage${id}.log 2>&1 &
done

# copy files to directory
cp ./*.py ${dir}/
cp ./*.json ${dir}/
cp ./run.sh ${dir}/
nvidia-smi > ${dir}/nvidia-smi.log 2>&1
for i in {0..9}
do
sleep 10
nvidia-smi >> ${dir}/nvidia-smi.log 2>&1
done