import sys
file = sys.argv[1]

def analyze(line:str):
    if line.startswith("Throughput:") and line.find("elapsed time")>0:
        p = len("Throughput:")
        q = line.find(",", p)
        throughput = float(line[p:q])
        p = line.find("elapsed time") + len("elapsed time")
        elapsed_time = float(line[p:])
        return (throughput, elapsed_time)
    return None
        

imgs = []
elapsed_times = []
with open(file, 'r') as f:
    for line in f.readlines():
        res = analyze(line)
        if res:
            throughput, elapsed_time = res
            if elapsed_time <= 0.001:
                print(f"invalid res={res}")
                continue
            img = throughput * elapsed_time
            imgs.append(img)
            elapsed_times.append(elapsed_time)
    
print(f"len(imgs) = {len(imgs)}")
assert(len(imgs) >= 30)
p = -20
print(f'imgs={imgs[p:]}')
print(f'elapsed_times={elapsed_times[p:]}')
throughput = sum(imgs[p:])/sum(elapsed_times[p:])
print(f"throughput = {throughput}")

        