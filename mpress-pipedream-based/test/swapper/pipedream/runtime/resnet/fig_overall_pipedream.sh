pkill python

bar_type="PipeDream"

batch_size_list=("64")

for batch_size in ${batch_size_list}; do
    strategy_record_folder=strategy_records/$bar_type/bs$batch_size
    cp -r $strategy_record_folder/* ./
    bash run.sh &
    sleep 100
    pkill python
    echo "$bar_type:bs$batch_size done" 
done