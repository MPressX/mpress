import os 

SCHEDULER_DIR="./scheduler"

def get_total_nbytes_each_card():
    # 11 G as example (local)
    return 11111*1024*1024 

def read_config(device_num:int):
    configs = []
    for i in range(device_num):
        filename = os.path.join(SCHEDULER_DIR, f"info{i}.txt")
        try:
            with open(filename, 'r') as f:
                s = f.read()
                import json
                info = json.loads(s)
                configs.append(info)
        except:
            print(f"{filename} not found")
            return None
    return configs

def fetch_config_until_stable(device_num):
    # to read several times until stable
    total_max_memory = 0
    CHANGE_THRESHOLD = 0.1*1024*1024*1024 # 0.1G内浮动算稳定
    iterations = [0 for i in range(device_num)]
    while True:
        import time
        time.sleep(1)
        configs = read_config(device_num)
        if configs: 
            new_iterations = [config['iteration'] for config in configs]
            iteration_diff = [new-old for new,old in zip(new_iterations, iterations)]
            if 0 in iteration_diff: # 有stage没有更新状态
                continue
            new_max_memory = 0
            for config in configs:
                new_max_memory += config['max_memory']
            if -CHANGE_THRESHOLD <= new_max_memory - total_max_memory <= CHANGE_THRESHOLD:
                break
            total_max_memory = new_max_memory
    total_memory = get_total_nbytes_each_card()
    new_configs = []
    for config in configs:
        new_config = dict()
        new_config['total_memory'] = total_memory
        new_config['spare_memory'] = total_memory - config['cached_memory']
        new_config['swapped_nbytes'] = config['nbytes_swapped_all']
        new_configs.append(new_config)
    return new_configs
    
    

def get_speed_matrix():
    from torch.utils.link_speed import get_speed_matrix
    speed_matrix = get_speed_matrix("./output_config/network_profiling.json")
    return speed_matrix

def make_decisions(configs):
    candidates = []
    allocated = []
    device_num = len(configs)
    total_spare_memory = 0
    total_swapped_nbytes = 0
    for config in configs:
        spare_memory = config['spare_memory']
        swapped_nbytes = config['swapped_nbytes']
        total_spare_memory += spare_memory
        total_swapped_nbytes += swapped_nbytes
    # for i in range(device_num):
    #     config = configs[i]
    #     swapped_nbytes = config['swapped_nbytes']
    #     ratio = swapped_nbytes + total_swapped_nbytes
    for i in range(device_num-1):
        candidates.append([device_num-1])
        allocated.append([configs[-1]['spare_memory']/(device_num-1)]) # -1对应-1，表示无限制
    candidates.append([])
    allocated.append([])

    j = dict()
    for i in range(device_num):
        sub_j = {
            "candidates": candidates[i],
            "allocated": allocated[i]
        }
        j[i] = sub_j
    return j

def write_decisions(decisions):
    import json
    filename = os.path.join(SCHEDULER_DIR, "decisions.json")
    with open(filename, 'w') as f:
        f.write(json.dumps(decisions, indent=4))

def run(device_num:int):
    # os.system(f"rm -rf {SCHEDULER_DIR}")
    # os.system(f"mkdir {SCHEDULER_DIR}")
    configs = fetch_config_until_stable(device_num)

    j = make_decisions(configs)
    print(j)
    write_decisions(j)
            

if __name__ == '__main__':
    run(4)