import os
import setproctitle
import subprocess
import time
setproctitle.setproctitle("RunALLPipedream")
os.environ['Z_LEVEL'] = 'fatal'
output = "aws_resnet50"
if not os.path.exists(output):
    os.system('mkdir ' +output)
def generate_conf(module, batchsize, candidates, cuda_memory_threshold, config_json):
    return {
        "module": module,
        "batchsize": batchsize,
        "candidates": candidates,
        "cuda_memory_threshold": cuda_memory_threshold,
        "config_path": module.replace('.','/')+"/"+config_json,
        "title": f"{module}_{batchsize}_{candidates.replace(' ','')}_{cuda_memory_threshold}_{config_json[:config_json.find('.')]}".replace("-1","H")
    }
def generate_running_scripts(conf):
    with open("./run_tmp_aws.sh", 'w') as f:
        f.write("rm stage*.log\n")
        for i in range(4):
            if i == 1:
                f.write(f"python main_with_runtime.py --module {conf['module']} -b {conf['batchsize']} "
                    f"--candidates {conf['candidates']} --cuda_memory_threshold={conf['cuda_memory_threshold']} "
                    f"--data_dir /home/ubuntu/imagenet_dataset --rank {i} --local_rank {i} --master_addr localhost "
                    f"--config_path {conf['config_path']} --distributed_backend nccl > stage{i}.log 2>&1 &\n")
            else :  
                f.write(f"python main_with_runtime.py --module {conf['module']} -b {conf['batchsize']} "
                    f"--candidates {conf['candidates']} --cuda_memory_threshold=0 "
                    f"--data_dir /home/ubuntu/imagenet_dataset --rank {i} --local_rank {i} --master_addr localhost "
                    f"--config_path {conf['config_path']} --distributed_backend nccl > stage{i}.log 2>&1 &\n")


confs = []
# confs.append(generate_conf("models.swap_resnet50.ToHost_bs96_mem9.gpus=4", 32, "-1", 15032385536, "mp_conf.json"))
# confs.append(generate_conf("models.swap_resnet50.ToHost_bs96_mem9.gpus=4", 64, "-1", 15032385536, "mp_conf.json"))
# confs.append(generate_conf("models.swap_resnet50.ToHost_bs96_mem9.gpus=4", 96, "-1", 15032385536, "mp_conf.json"))
# confs.append(generate_conf("models.swap_resnet50.ToHost_bs96_mem9.gpus=4", 128, "-1", 15032385536, "mp_conf.json"))
# confs.append(generate_conf("models.swap_resnet50.ToHost_bs96_mem9.gpus=4", 160, "-1", 15032385536, "mp_conf.json"))
# confs.append(generate_conf("models.swap_resnet50.ToHost_bs96_mem9.gpus=4", 192, "-1", 15032385536, "mp_conf.json"))

# confs.append(generate_conf("models.swap_resnet50.ToHost_bs96_mem9.gpus=4", 32, "0 2 3", 15032385536, "mp_conf.json"))
# confs.append(generate_conf("models.swap_resnet50.ToHost_bs96_mem9.gpus=4", 64, "0 2 3",15032385536, "mp_conf.json"))
# confs.append(generate_conf("models.swap_resnet50.ToHost_bs96_mem9.gpus=4", 96, "0 2 3",15032385536, "mp_conf.json"))
# confs.append(generate_conf("models.swap_resnet50.ToHost_bs96_mem9.gpus=4", 128,"0 2 3", 15032385536, "mp_conf.json"))
# confs.append(generate_conf("models.swap_resnet50.ToHost_bs96_mem9.gpus=4", 160,"0 2 3", 15032385536, "mp_conf.json"))
# confs.append(generate_conf("models.swap_resnet50.ToHost_bs96_mem9.gpus=4", 192,"0 2 3", 15032385536, "mp_conf.json"))

confs.append(generate_conf("models.noswap_resnet50.ToHost_bs96_mem9.gpus=4", 32, "-1", 0, "mp_conf.json"))
# confs.append(generate_conf("models.noswap_resnet50.ToHost_bs96_mem9.gpus=4", 64, "-1", 0, "mp_conf.json"))
# confs.append(generate_conf("models.noswap_resnet50.ToHost_bs96_mem9.gpus=4", 96, "-1", 0, "mp_conf.json"))

# confs.append(generate_conf("models.swap_resnet50.ToHost_bs96_mem9.gpus=4", 32, "-1", 0, "mp_conf.json"))
# confs.append(generate_conf("models.swap_resnet50.ToHost_bs96_mem9.gpus=4", 32, "-1", 0, "mp_conf.json"))
# confs.append(generate_conf("models.swap_resnet50.ToHost_bs96_mem9.gpus=4", 32, "-1", 0, "mp_conf.json"))
# confs.append(generate_conf("models.swap_resnet50.ToHost_bs96_mem9.gpus=4", 32, "-1", 0, "mp_conf.json"))
# confs.append(generate_conf("models.aws_repartition_resnet50.vgg16.gpus=4_swap_only_1", 104, "1 3", 0, "mp_conf_t.json"))

nsys_start_time = 150
nsys_duration_time = 10
                 
for conf in confs:
    generate_running_scripts(conf)
    cmd_nsys = f"nsys profile -f true -o {conf['title']} -y {nsys_start_time} -d {nsys_duration_time} bash run_tmp_aws.sh"
    conf_dir_path = os.path.join(output, conf['title'])
    if os.path.exists(conf_dir_path):
        # print(f"skip {conf_dir_path}")
        os.system(f"rm -rf {conf_dir_path}")
    #     continue
    # else:
    os.system(f"mkdir {conf_dir_path}")
    subprocess.Popen(cmd_nsys, shell=True)
    time.sleep(nsys_start_time+nsys_duration_time+10)
    os.system("pkill python")
    os.system(f"mv stage*.log {conf_dir_path}/")
    os.system(f"mv *.qdrep {conf_dir_path}/")
    
# for swap_mode in confs:
#     for candidates in confs[swap_mode]:
#         for bs in batch_size:
#             conf_dir_name = "2To"+candidates.replace(" ","")+"_BS"+str(bs)
#             conf_dir_path = os.path.join(output,conf_dir_name)
#             if os.path.exists(conf_dir_path):
#                 continue
#             else :
#                 print(conf_dir_path)
#                 os.system('mkdir '+conf_dir_path)
#                 modify_run(swap_mode,bs,candidates)
#                 os.system("pkill python")
#                 cmdstr="nsys profile -f true -o "+conf_dir_name+" -y 50 -d 10  bash run_tmp.sh" 
#                 cmdstr=f"nsys profile -f true -o {conf_dir_name} -y {nsys_start_time} -d {nsys_duration_time}  bash run_tmp.sh" 
#                 # subprocess.Popen(cmdstr,shell=True,stdout=subprocess.PIPE)
#                 subprocess.Popen(cmdstr,shell=True)
#                 time.sleep(nsys_start_time+nsys_duration_time+10)
#                 os.system("pkill python")
#                 os.system('mv vgg*.log '+ conf_dir_path+"/")
#                 os.system('mv *.qdrep '+ conf_dir_path+"/")
#                 # exit()
            
             
            


