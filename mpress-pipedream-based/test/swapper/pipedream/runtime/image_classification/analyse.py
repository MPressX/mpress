import os
import re

def analyze_file(file_path, bs):
    memory_buffereds = []
    training_speeds = []
    if not os.path.isfile(file_path):
        return 0,0
    with open(file_path, 'r') as f:
        for line in f.readlines():
            line = line.strip()
            if line.startswith("[memory consumption]"):
                p = line.find("(")
                q = line.find(")")
                memory_buffereds.append(float(line[p+1:q]))
            elif line.startswith("[training speed]"):
                p = line.find(":")
                training_speeds.append(float(line[p+1:]))
    try :
        memory_buffered = memory_buffereds[-1]
    except :
        return 0, 0
    training_speeds = training_speeds[4:]  
    elapsed_times = [bs/v for v in training_speeds] 
    # print(sum(elapsed_times))
    if sum(elapsed_times)==0:
        return 0,0
    training_speed = len(elapsed_times)*bs / sum(elapsed_times)     
    return memory_buffered, training_speed
        
def analyze_folder(folder_path, bs=32):
    status = []
    for i in range(4):
        filename = f"stage{i}.log"
        filepath = os.path.join(folder_path, filename)
        mem, spd = analyze_file(filepath, bs)
        status.append((mem, spd))
        print(f"Exp {folder_path}, Stage {i}, Mem Buffered={mem} MB, Spd = {spd:0.2f} imgs/sec")
    print()
    return status





analyse_dir = "/home/ubuntu/pytorch_swap/test/swapper/pipedream/runtime/image_classification/aws_resnet50"
# analyse_dir = "/data/sdc/pipedream.gnmt/runtime/image_classification/output"
if __name__ == '__main__':
    for res in os.listdir(analyse_dir):
        res_path = os.path.join(analyse_dir,res)
        print(f"path={res_path}")
        # bs = int(res.split('_')[-6]) if res.endswith("_t") else int(res.split('_')[-5])
        # bs = int(res[-2:])
        try:
            bs = re.findall('4_(.*?)_H',res)[0]
        except:
            bs = re.findall('4_(.*?)_023',res)[0]
        print(f"bs={bs}")
        analyze_folder(res_path,int(bs))
    # analyze_file("output/1To23_BS64/vgg0.log")
    # analyze_folder("swap_only_1_32/gpu", 32)
    # analyze_folder("swap_only_1_32/gpu_t", 32)
    # analyze_folder("swap_only_1_32/host", 32)
    # analyze_folder("swap_only_1_32/noSwap", 32)
    # analyze_folder("output/1To23_BS96")
    # analyze_folder("output/1ToH_BS96")
    # # analyze_folder("output/1ToNull_BS96")
    # analyze_folder("output/1To23_BS64")
    # analyze_folder("output/1ToH_BS64")
    # analyze_folder("output/1ToNull_BS64")
    