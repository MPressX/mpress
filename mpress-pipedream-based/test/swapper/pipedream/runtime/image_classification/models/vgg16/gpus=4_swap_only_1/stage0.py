# Copyright (c) Microsoft Corporation.
# Licensed under the MIT license.

import torch
from torch import swapper
from collections import deque
from .swap_in_forward import swap_conv, swap_maxpool, swap_linear



class Stage0(torch.nn.Module):
    def __init__(self):
        super(Stage0, self).__init__()
        self.layer2 = torch.nn.Conv2d(3, 64, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1))
        self.layer3 = torch.nn.ReLU(inplace=True)
        self.layer4 = torch.nn.Conv2d(64, 64, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1))
        self._initialize_weights()
        

    def forward(self, input0):
        self.metas_to_swap = []
        self.metas_to_prefetch = []
        
        gpu_running = input0.is_cuda
        out0 = input0.clone()
        out2 = self.layer2(out0)
        # swap_conv(gpu_running, out2, self.layer2, grad_fn_list)
        out3 = self.layer3(out2)
        out4 = self.layer4(out3)
        # swap_conv(gpu_running, out4, self.layer4,grad_fn_list)
        
        # if gpu_running:
        #     self.grad_fn_queue.append(grad_fn_list)       
        return out4

    def _initialize_weights(self):
        for m in self.modules():
            if isinstance(m, torch.nn.Conv2d):
                torch.nn.init.kaiming_normal_(m.weight, mode='fan_out', nonlinearity='relu')
                if m.bias is not None:
                    torch.nn.init.constant_(m.bias, 0)
            elif isinstance(m, torch.nn.BatchNorm2d):
                torch.nn.init.constant_(m.weight, 1)
                torch.nn.init.constant_(m.bias, 0)
            elif isinstance(m, torch.nn.Linear):
                torch.nn.init.normal_(m.weight, 0, 0.01)
                torch.nn.init.constant_(m.bias, 0)
