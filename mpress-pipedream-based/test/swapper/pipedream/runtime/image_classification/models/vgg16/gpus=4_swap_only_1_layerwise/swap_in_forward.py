from torch import swapper
import torch
def swap_conv(gpu_running:bool, tensor:torch.Tensor, layer:torch.nn.Module,grad_fn_list:list):
    if gpu_running:
        swapper.swap_out_by_layers(targets=[tensor.grad_fn], excludes=[tensor, layer.weight])
        grad_fn_list.append(tensor.grad_fn)
def swap_maxpool(gpu_running:bool, tensor:torch.Tensor,grad_fn_list:list):
    if gpu_running:
        swapper.swap_out_by_layers(targets=[tensor.grad_fn], excludes=[tensor])
        grad_fn_list.append(tensor.grad_fn)
swap_linear = swap_conv        