# Copyright (c) Microsoft Corporation.
# Licensed under the MIT license.

import torch
from .swap_in_forward import swap_conv, swap_maxpool
from collections import deque
from torch import swapper

class Stage1(torch.nn.Module):
    def __init__(self):
        super(Stage1, self).__init__()
        self.layer1 = torch.nn.ReLU(inplace=True)
        self.layer2 = torch.nn.MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
        self.layer3 = torch.nn.Conv2d(64, 128, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1))
        self.layer4 = torch.nn.ReLU(inplace=True)
        self.layer5 = torch.nn.Conv2d(128, 128, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1))
        self.layer6 = torch.nn.ReLU(inplace=True)
        self.layer7 = torch.nn.MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
        self.layer8 = torch.nn.Conv2d(128, 256, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1))
        self.layer9 = torch.nn.ReLU(inplace=True)
        self._initialize_weights()


    def forward(self, input0):
        self.metas_to_swap = []
        self.metas_to_prefetch = []
        
        gpu_running = input0.is_cuda
        out0 = input0.clone()
        out1 = self.layer1(out0)
        out2 = self.layer2(out1)
        if gpu_running:
            swapper.swap_out_by_layers(targets=[out2.grad_fn], excludes=[out2])
            self.layer9.register_swap_in(targets=[out2.grad_fn])
        out3 = self.layer3(out2)
        if gpu_running:
            swapper.swap_out_by_layers(targets=[out3.grad_fn], excludes=[out3, self.layer3.weight])
            self.layer9.register_swap_in(targets=[out3.grad_fn])
            self.metas_to_swap.append(swapper.tensor_to_meta(self.layer3.weight))
        out4 = self.layer4(out3)
        out5 = self.layer5(out4)
        if gpu_running:
            swapper.swap_out_by_layers(targets=[out5.grad_fn], excludes=[out5, self.layer5.weight])
            self.layer9.register_swap_in(targets=[out5.grad_fn])
            self.metas_to_swap.append(swapper.tensor_to_meta(self.layer5.weight))
        out6 = self.layer6(out5)
        out7 = self.layer7(out6)
        if gpu_running:
            swapper.swap_out_by_layers(targets=[out7.grad_fn], excludes=[out7])
            self.layer9.register_swap_in(targets=[out7.grad_fn])
        out8 = self.layer8(out7)
        if gpu_running:
            metas = swapper.swap_out_by_range(start_tensors=[out7], end_tensors=[out8], excludes=[self.layer8.weight])
            self.metas_to_prefetch.extend(metas)
            meta_of_layer8_weight = swapper.tensor_to_meta(self.layer8.weight)
            self.metas_to_prefetch.append(meta_of_layer8_weight)
            self.metas_to_swap.append(meta_of_layer8_weight)
        out9 = self.layer9(out8)
        
        return out9

    def _initialize_weights(self):
        for m in self.modules():
            if isinstance(m, torch.nn.Conv2d):
                torch.nn.init.kaiming_normal_(m.weight, mode='fan_out', nonlinearity='relu')
                if m.bias is not None:
                    torch.nn.init.constant_(m.bias, 0)
            elif isinstance(m, torch.nn.BatchNorm2d):
                torch.nn.init.constant_(m.weight, 1)
                torch.nn.init.constant_(m.bias, 0)
            elif isinstance(m, torch.nn.Linear):
                torch.nn.init.normal_(m.weight, 0, 0.01)
                torch.nn.init.constant_(m.bias, 0)
