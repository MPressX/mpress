# Copyright (c) Microsoft Corporation.
# Licensed under the MIT license.

import torch
from .swap_in_forward import swap_conv, swap_maxpool, swap_linear
from collections import deque

class Stage3(torch.nn.Module):
    def __init__(self):
        super(Stage3, self).__init__()
        self.layer1 = torch.nn.MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
        self.layer2 = torch.nn.Conv2d(512, 512, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1))
        self.layer3 = torch.nn.ReLU(inplace=True)
        self.layer4 = torch.nn.Conv2d(512, 512, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1))
        self.layer5 = torch.nn.ReLU(inplace=True)
        self.layer6 = torch.nn.Conv2d(512, 512, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1))
        self.layer7 = torch.nn.ReLU(inplace=True)
        self.layer8 = torch.nn.MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
        self.layer11 = torch.nn.Linear(in_features=25088, out_features=4096, bias=True)
        self.layer12 = torch.nn.ReLU(inplace=True)
        self.layer13 = torch.nn.Dropout(p=0.5)
        self.layer14 = torch.nn.Linear(in_features=4096, out_features=4096, bias=True)
        self.layer15 = torch.nn.ReLU(inplace=True)
        self.layer16 = torch.nn.Dropout(p=0.5)
        self.layer17 = torch.nn.Linear(in_features=4096, out_features=1000, bias=True)
        self._initialize_weights()
        
        self.grad_fn_queue  = deque()

    def forward(self, input0):
        grad_fn_list= []
        gpu_running = input0.is_cuda
        out0 = input0.clone()
        out1 = self.layer1(out0)
        swap_maxpool(gpu_running, out1,grad_fn_list)
        out2 = self.layer2(out1)
        swap_conv(gpu_running, out2, self.layer2,grad_fn_list)
        out3 = self.layer3(out2)
        out4 = self.layer4(out3)
        swap_conv(gpu_running, out4, self.layer4,grad_fn_list)
        out5 = self.layer5(out4)
        out6 = self.layer6(out5)
        swap_conv(gpu_running, out6, self.layer6,grad_fn_list)
        out7 = self.layer7(out6)
        out8 = self.layer8(out7)
        swap_maxpool(gpu_running, out8,grad_fn_list)
        out9 = out8.size(0)
        out10 = out8.view(out9, -1)
        out11 = self.layer11(out10)
        swap_linear(gpu_running, out11, self.layer11,grad_fn_list)
        out12 = self.layer12(out11)
        out13 = self.layer13(out12)
        out14 = self.layer14(out13)
        swap_linear(gpu_running, out14, self.layer14,grad_fn_list)
        out15 = self.layer15(out14)
        out16 = self.layer16(out15)
        out17 = self.layer17(out16)
        
        if gpu_running:
            self.grad_fn_queue.append(grad_fn_list) 
        return out17

    def _initialize_weights(self):
        for m in self.modules():
            if isinstance(m, torch.nn.Conv2d):
                torch.nn.init.kaiming_normal_(m.weight, mode='fan_out', nonlinearity='relu')
                if m.bias is not None:
                    torch.nn.init.constant_(m.bias, 0)
            elif isinstance(m, torch.nn.BatchNorm2d):
                torch.nn.init.constant_(m.weight, 1)
                torch.nn.init.constant_(m.bias, 0)
            elif isinstance(m, torch.nn.Linear):
                torch.nn.init.normal_(m.weight, 0, 0.01)
                torch.nn.init.constant_(m.bias, 0)
