read_dir(){
    for file in `ls $1`
    do
        if [ -d $1"/"$file ]
        then
            read_dir $1"/"$file
        else
            if [ $file != "modify_inplace.sh" ] 
            then
                echo $1"/"$file
                filename=$1"/"$file
                echo $filename
                sed -i 's/inplace=True=True/inplace=True/g'  $filename 
            fi
        fi
    done 
}
read_dir $1