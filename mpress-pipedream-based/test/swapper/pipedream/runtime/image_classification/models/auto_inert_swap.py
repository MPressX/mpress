import re

from torch import var
files =[]
files.append("/data/sdc/pytorch.gnmt/test/swapper/pipedream/runtime/image_classification/models/aws_repartition_resnet50/ToHost_bs96_mem9/gpus=4/stage1.py")

def parseXY(line):
    return re.findall("\d+",line)

inert_dict={}
var_retained = set()
    

def get_inert_swap(filename):
    with open(filename,"r") as f:
        lines=reversed(f.readlines())
        for line in lines:
            if " + " in line :
                for v in parseXY(line) :
                    var_retained.add(v)    
                inert_dict[line] = None
            elif "self.layer" in line :
                vars = parseXY(line)
                excludeVar=set()
                for v in vars[2:]:
                    if v in var_retained :
                        excludeVar.add(v)
                if len(excludeVar) == 0:
                    inert_str = "        swap_out(out"+str(vars[0])+", self.layer"+str(vars[1])+")\n"
                elif len(excludeVar) == 1:
                    inert_str = "        swap_out(out"+str(vars[0])+", self.layer"+str(vars[1])+",out"+str(v)+")\n"
                else:
                    raise Exception(" len(excludeVar is not equal to 0 and 1") 
                inert_dict[line] = inert_str
            elif "def forward(self," in line:
                break
            
            else:
                inert_dict[line] = None
    return inert_dict
                
                           
def rewritefile(inert_dict,original_lines,file):
    new_lines=[]
    new_lines.append("from torch import swapper\n")
    for l in original_lines :
        new_lines.append(l)
    for l  in reversed(list(inert_dict.keys())):
        new_lines.append(l)
        if inert_dict[l] :
            new_lines.append(inert_dict[l] )
    with open(file,"w") as f:
        for l in new_lines:
            f.write(l)        
    
def getOriginaline(filename):
    lines=[]
    with open(filename,"r") as f:
        ori_lines = f.readlines()
        for i, l in enumerate(ori_lines):
            if "self._initialize_weights()" in l:
                last_layer=str(re.findall("\d+",ori_lines[i-1])[0])
            if "def forward" in l :
                lines.append(l)
                lines.append("        self.metas_to_swap = [] \n\
        self.metas_to_prefetch = []\n\
        gpu_running = input1.is_cuda\n\
        def swap_out(output:torch.Tensor, layer:torch.nn.Module, excluded_tensor=None):\n\
            if not gpu_running:\n\
                return\n\
            if isinstance(layer, torch.nn.Conv2d) or isinstance(layer, torch.nn.BatchNorm2d):\n\
                if excluded_tensor is not None:\n\
                    swapper.swap_out_by_layers(targets=[output.grad_fn], excludes=[output, layer.weight, excluded_tensor])\n\
                else:\n\
                    swapper.swap_out_by_layers(targets=[output.grad_fn], excludes=[output, layer.weight])\n\
                self.metas_to_swap.append(swapper.tensor_to_meta(layer.weight))\n\
            else:\n\
                if excluded_tensor is not None:\n\
                    swapper.swap_out_by_layers(targets=[output.grad_fn], excludes=[output, excluded_tensor])\n\
                else:\n\
                    swapper.swap_out_by_layers(targets=[output.grad_fn], excludes=[output])\n\
            self.layer"+last_layer+".register_swap_in(targets=[output.grad_fn])\n")
                break
            else : 
                lines.append(l)
    return lines         
        


for f in files:
    original_lines = getOriginaline(f)
    inert_dict = get_inert_swap(f)
    rewritefile(inert_dict,original_lines,f)
    