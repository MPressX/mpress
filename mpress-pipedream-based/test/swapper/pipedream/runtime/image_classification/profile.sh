rm vgg*.log
python main_with_runtime.py --module models.vgg16.gpus=4_swap_only_1 -b 96 --data_dir /home/ubuntu/imagenet_dataset --rank 0 --local_rank 0 --master_addr localhost --config_path models/vgg16/gpus=4_swap_only_1/mp_conf.json --distributed_backend gloo 2>&1 >> vgg0.log &
python main_with_runtime.py --module models.vgg16.gpus=4_swap_only_1 -b 96 --data_dir /home/ubuntu/imagenet_dataset --rank 1 --local_rank 1 --master_addr localhost --config_path models/vgg16/gpus=4_swap_only_1/mp_conf.json --distributed_backend gloo 2>&1 >> vgg1.log &
python main_with_runtime.py --module models.vgg16.gpus=4_swap_only_1 -b 96 --data_dir /home/ubuntu/imagenet_dataset --rank 2 --local_rank 2 --master_addr localhost --config_path models/vgg16/gpus=4_swap_only_1/mp_conf.json --distributed_backend gloo 2>&1 >> vgg2.log &
python main_with_runtime.py --module models.vgg16.gpus=4_swap_only_1 -b 96 --data_dir /home/ubuntu/imagenet_dataset --rank 3 --local_rank 3 --master_addr localhost --config_path models/vgg16/gpus=4_swap_only_1/mp_conf.json --distributed_backend gloo 2>&1 >> vgg3.log &


# rm vgg*.log
# python main_with_runtime.py --module models.vgg16.gpus=4_straight -b 96 --data_dir /home/ubuntu/imagenet_dataset --rank 0 --local_rank 0 --master_addr localhost --config_path models/vgg16/gpus=4_straight/mp_conf.json --distributed_backend gloo 2>&1 >> vgg0.log &
# python main_with_runtime.py --module models.vgg16.gpus=4_straight -b 96 --data_dir /home/ubuntu/imagenet_dataset --rank 1 --local_rank 1 --master_addr localhost --config_path models/vgg16/gpus=4_straight/mp_conf.json --distributed_backend gloo 2>&1 >> vgg1.log &
# python main_with_runtime.py --module models.vgg16.gpus=4_straight -b 96 --data_dir /home/ubuntu/imagenet_dataset --rank 2 --local_rank 2 --master_addr localhost --config_path models/vgg16/gpus=4_straight/mp_conf.json --distributed_backend gloo 2>&1 >> vgg2.log &
# python main_with_runtime.py --module models.vgg16.gpus=4_straight -b 96 --data_dir /home/ubuntu/imagenet_dataset --rank 3 --local_rank 3 --master_addr localhost --config_path models/vgg16/gpus=4_straight/mp_conf.json --distributed_backend gloo 2>&1 >> vgg3.log &