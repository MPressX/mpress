# Copyright (c) Microsoft Corporation.
# Licensed under the MIT license.


from .stage0 import Stage0
from .stage1 import Stage1
from .stage2 import Stage2
from .stage3 import Stage3

def arch():
    return "bert"

def model(config, criterion):
    return [
        (Stage0(config), ["input_ids0", "attention_mask0", "token_type_ids0"], ["hidden_states0", "attention_mask0"]),
        (Stage1(config), ["hidden_states0", "attention_mask0"], ["hidden_states1", "attention_mask1"]),
        (Stage2(config), ["hidden_states1", "attention_mask1"], ["hidden_states2", "attention_mask2"]),
        (Stage3(config), ["hidden_states2", "attention_mask2"], ["hidden_states3"]),
        (criterion, ["hidden_states3"], ["loss"])
    ]

