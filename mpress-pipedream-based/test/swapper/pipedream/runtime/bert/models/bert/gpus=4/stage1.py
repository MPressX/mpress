# Copyright (c) Microsoft Corporation.
# Licensed under the MIT license.
import copy

import torch
from torch.utils.checkpoint import checkpoint
from torch import swapper

from .modeling import BertLayer



class Stage1(torch.nn.Module):
    def __init__(self, config):
        super(Stage1, self).__init__()
        layer = BertLayer(config=config)
        self.layer = torch.nn.ModuleList([copy.deepcopy(layer) for _ in range(config.num_hidden_layers)])
        self.recomputation = config.recomputation
        self.swap = False
    def forward(self, hidden_states, attention_mask):
        self.metas_to_swap = []
        self.metas_to_prefetch = []
        # hidden_states = self.layer(hidden_states, attention_mask)
        for _, layer_module in enumerate(self.layer):
            if self.recomputation:
                hidden_states = checkpoint(layer_module, hidden_states, attention_mask, enable_swap=self.swap)
            else:
                hidden_states = layer_module(hidden_states, attention_mask, self.swap)
        return hidden_states, attention_mask

    def _initialize_weights(self):
        pass
