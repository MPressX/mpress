# Copyright (c) Microsoft Corporation.
# Licensed under the MIT license.
import copy

import torch
from torch.utils.checkpoint import checkpoint
from .modeling import BertLayer



class Stage2(torch.nn.Module):
    def __init__(self, config):
        super(Stage2, self).__init__()
        layer = BertLayer(config=config)
        self.layer = torch.nn.ModuleList([copy.deepcopy(layer) for _ in range(config.num_hidden_layers2)])
        self.activations_swap_config = config.activations_swap_config
    def forward(self, hidden_states, attention_mask):
        self.metas_to_swap = []
        self.metas_to_prefetch = []
        # hidden_states = self.layer(hidden_states, attention_mask)
        for id, layer_module in enumerate(self.layer):
            swap_choice = self.activations_swap_config[id]
            if swap_choice.type=='recompute':
                hidden_states = checkpoint(layer_module, hidden_states, attention_mask, enable_swap=True)
            elif swap_choice.type == 'swap':
                hidden_states = layer_module(hidden_states, attention_mask, True, swap_choice.destinations, swap_choice.ratios)
            else:
                hidden_states = layer_module(hidden_states, attention_mask, False)
       
        return hidden_states, attention_mask

    def _initialize_weights(self):
        pass