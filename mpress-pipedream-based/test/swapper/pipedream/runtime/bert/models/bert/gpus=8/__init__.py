# Copyright (c) Microsoft Corporation.
# Licensed under the MIT license.


from .stage0 import Stage0
from .stage1 import Stage1
from .stage2 import Stage2
from .stage3 import Stage3
from .stage4 import Stage4
from .stage5 import Stage5
from .stage6 import Stage6
from .stage7 import Stage7

def arch():
    return "bert"

def model(config, criterion):
    return [
        (Stage0(config), ["input_ids0", "attention_mask0", "token_type_ids0"], ["hidden_states0", "attention_mask0"]),
        (Stage1(config), ["hidden_states0", "attention_mask0"], ["hidden_states1", "attention_mask1"]),
        (Stage2(config), ["hidden_states1", "attention_mask1"], ["hidden_states2", "attention_mask2"]),
        (Stage3(config), ["hidden_states2", "attention_mask2"], ["hidden_states3", "attention_mask3"]),
        (Stage4(config), ["hidden_states3", "attention_mask3"], ["hidden_states4", "attention_mask4"]),
        (Stage5(config), ["hidden_states4", "attention_mask4"], ["hidden_states5", "attention_mask5"]),
        (Stage6(config), ["hidden_states5", "attention_mask5"], ["hidden_states6", "attention_mask6"]),
        (Stage7(config), ["hidden_states6", "attention_mask6"], ["hidden_states7"]),
        (criterion, ["hidden_states7"], ["loss"])
    ]

