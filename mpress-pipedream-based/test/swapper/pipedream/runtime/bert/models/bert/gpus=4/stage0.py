# Copyright (c) Microsoft Corporation.
# Licensed under the MIT license.
import copy

import torch
from torch.utils.checkpoint import checkpoint
from torch import swapper

from .modeling import BertEmbeddings
from .modeling import BertLayer

class Stage0(torch.nn.Module):
    def __init__(self, config):
        super(Stage0, self).__init__()
        self.embeddings = BertEmbeddings(config)
        layer = BertLayer(config=config)
        self.layer = torch.nn.ModuleList([copy.deepcopy(layer) for _ in range(config.num_hidden_layers)])
        self.recomputation = config.recomputation
        self.swap = False
    def forward(self, input_ids, attention_mask=None, token_type_ids=None):
        self.metas_to_swap = []
        self.metas_to_prefetch = []
        if attention_mask is None:
            attention_mask = torch.ones_like(input_ids)
        if token_type_ids is None:
            token_type_ids = torch.zeros_like(input_ids)
        extended_attention_mask = attention_mask.unsqueeze(1).unsqueeze(2)
        extended_attention_mask = extended_attention_mask.to(dtype=next(self.parameters()).dtype) # fp16 compatibility
        extended_attention_mask = (1.0 - extended_attention_mask) * -10000.0

        embedding_output = self.embeddings(input_ids, token_type_ids)
        if self.swap:
            swapper.swap_out_by_range(start_tensors=[input_ids, token_type_ids], end_tensors=[embedding_output], excludes=[embedding_output]+[p for p in self.embeddings.parameters()])
            for p in self.embeddings.parameters():
                self.metas_to_swap.append(swapper.tensor_to_meta(p))

        hidden_states = embedding_output
        for _, layer_module in enumerate(self.layer):
            if self.recomputation:
                hidden_states = checkpoint(layer_module, hidden_states, extended_attention_mask, enable_swap=self.swap)
            else:
                hidden_states = layer_module(hidden_states, extended_attention_mask, self.swap)

        return hidden_states, extended_attention_mask

    def _initialize_weights(self):
        pass
