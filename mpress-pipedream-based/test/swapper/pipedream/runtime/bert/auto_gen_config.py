import json
import argparse

parser = argparse.ArgumentParser(description="Automatically generate bert configuration file")
parser.add_argument("--model", type=str, required=True)
parser.add_argument('--recompute', action='store_true',
                    help='Recompute tensors in backward pass')
args = parser.parse_args()

if "recompute" in args.model:
    args.recompute = True
    args.model=args.model.replace("_recompute","")
else:
    args.recompute = False
config_map={
    "bert_large_balance":{
        "hidden_size": 1024,
        "num_hidden_layers0": 3,
        "num_hidden_layers1": 3,
        "num_hidden_layers2": 3,
        "num_hidden_layers3": 3,
        "num_hidden_layers4": 3,
        "num_hidden_layers5": 3,
        "num_hidden_layers6": 3,
        "num_hidden_layers7": 3,
        "swap_layers0": 3,
        "swap_layers1": 3,
        "swap_layers2": 3,
        "swap_layers3": 3,
        "swap_layers4": 3,
        "swap_layers5": 3,
        "swap_layers6": 3,
        "swap_layers7": 3,
    },
    "bert_large_memory":{
        "hidden_size": 1024,
        "num_hidden_layers0": 1,
        "num_hidden_layers1": 1,
        "num_hidden_layers2": 2,
        "num_hidden_layers3": 2,
        "num_hidden_layers4": 3,
        "num_hidden_layers5": 3,
        "num_hidden_layers6": 5,
        "num_hidden_layers7": 7,
        "swap_layers0": 1,
        "swap_layers1": 1,
        "swap_layers2": 2,
        "swap_layers3": 2,
        "swap_layers4": 3,
        "swap_layers5": 3,
        "swap_layers6": 5,
        "swap_layers7": 7,
    },
    "bert_base_balance":{
        "num_attention_heads": 12,
        "intermediate_size": 3072,
        "hidden_size": 768,
        "num_hidden_layers0": 1,
        "num_hidden_layers1": 2,
        "num_hidden_layers2": 2,
        "num_hidden_layers3": 2,
        "num_hidden_layers4": 2,
        "num_hidden_layers5": 1,
        "num_hidden_layers6": 1,
        "num_hidden_layers7": 1,
        "swap_layers0": 0,
        "swap_layers1": 0,
        "swap_layers2": 0,
        "swap_layers3": 0,
        "swap_layers4": 0,
        "swap_layers5": 0,
        "swap_layers6": 0,
        "swap_layers7": 0,
    },
    "bert_base_memory":{
        "num_attention_heads": 12,
        "intermediate_size": 3072,
        "hidden_size": 768,
        "num_hidden_layers0": 0,
        "num_hidden_layers1": 1,
        "num_hidden_layers2": 1,
        "num_hidden_layers3": 1,
        "num_hidden_layers4": 1,
        "num_hidden_layers5": 2,
        "num_hidden_layers6": 3,
        "num_hidden_layers7": 3,
        "swap_layers0": 0,
        "swap_layers1": 0,
        "swap_layers2": 0,
        "swap_layers3": 0,
        "swap_layers4": 0,
        "swap_layers5": 0,
        "swap_layers6": 0,
        "swap_layers7": 0,
    },
    "bert_48_1_balance":{
        "hidden_size": 1024,
        "num_hidden_layers0": 6,
        "num_hidden_layers1": 6,
        "num_hidden_layers2": 6,
        "num_hidden_layers3": 6,
        "num_hidden_layers4": 6,
        "num_hidden_layers5": 6,
        "num_hidden_layers6": 6,
        "num_hidden_layers7": 6,
        "swap_layers0": 3,
        "swap_layers1": 2,
        "swap_layers2": 1,
        "swap_layers3": 0,
        "swap_layers4": 0,
        "swap_layers5": 0,
        "swap_layers6": 0,
        "swap_layers7": 0,
    },
    "bert_48_1_memory":{
        "hidden_size": 1024,
        "num_hidden_layers0": 2,
        "num_hidden_layers1": 3,
        "num_hidden_layers2": 3,
        "num_hidden_layers3": 4,
        "num_hidden_layers4": 5,
        "num_hidden_layers5": 7,
        "num_hidden_layers6": 10,
        "num_hidden_layers7": 14,
        "swap_layers0": 2,
        "swap_layers1": 3,
        "swap_layers2": 3,
        "swap_layers3": 4,
        "swap_layers4": 5,
        "swap_layers5": 7,
        "swap_layers6": 10,
        "swap_layers7": 14,
    },
    "bert_48_2_balance":{
        "hidden_size": 2048,
        "num_hidden_layers0": 6,
        "num_hidden_layers1": 6,
        "num_hidden_layers2": 6,
        "num_hidden_layers3": 6,
        "num_hidden_layers4": 6,
        "num_hidden_layers5": 6,
        "num_hidden_layers6": 6,
        "num_hidden_layers7": 6,
        "swap_layers0": 4,
        "swap_layers1": 3,
        "swap_layers2": 2,
        "swap_layers3": 2,
        "swap_layers4": 0,
        "swap_layers5": 0,
        "swap_layers6": 0,
        "swap_layers7": 0,
    },
    "bert_48_2_balance_optimized":{
        "hidden_size": 2048,
        "num_hidden_layers0": 5,
        "num_hidden_layers1": 6,
        "num_hidden_layers2": 6,
        "num_hidden_layers3": 6,
        "num_hidden_layers4": 6,
        "num_hidden_layers5": 6,
        "num_hidden_layers6": 6,
        "num_hidden_layers7": 7,
        "swap_layers0": 4,
        "swap_layers1": 3,
        "swap_layers2": 2,
        "swap_layers3": 2,
        "swap_layers4": 0,
        "swap_layers5": 0,
        "swap_layers6": 0,
        "swap_layers7": 0,
    },
    "bert_48_2_memory":{
        "hidden_size": 2048,
        "num_hidden_layers0": 2,
        "num_hidden_layers1": 3,
        "num_hidden_layers2": 3,
        "num_hidden_layers3": 4,
        "num_hidden_layers4": 5,
        "num_hidden_layers5": 7,
        "num_hidden_layers6": 10,
        "num_hidden_layers7": 14,
        "swap_layers0": 2,
        "swap_layers1": 3,
        "swap_layers2": 3,
        "swap_layers3": 4,
        "swap_layers4": 5,
        "swap_layers5": 7,
        "swap_layers6": 10,
        "swap_layers7": 14,
    },
    "bert_64_1_balance":{
        "hidden_size": 2048,
        "num_hidden_layers0": 8,
        "num_hidden_layers1": 8,
        "num_hidden_layers2": 8,
        "num_hidden_layers3": 8,
        "num_hidden_layers4": 8,
        "num_hidden_layers5": 8,
        "num_hidden_layers6": 8,
        "num_hidden_layers7": 8,
        "swap_layers0": 6,
        "swap_layers1": 6,
        "swap_layers2": 5,
        "swap_layers3": 4,
        "swap_layers4": 3,
        "swap_layers5": 1,
        "swap_layers6": 0,
        "swap_layers7": 0,
    },
    "bert_64_2_balance":{
        "hidden_size": 3072,
        "num_hidden_layers0": 8,
        "num_hidden_layers1": 8,
        "num_hidden_layers2": 8,
        "num_hidden_layers3": 8,
        "num_hidden_layers4": 8,
        "num_hidden_layers5": 8,
        "num_hidden_layers6": 8,
        "num_hidden_layers7": 8,
        "swap_layers0": 8,
        "swap_layers1": 7,
        "swap_layers2": 7,
        "swap_layers3": 6,
        "swap_layers4": 5,
        "swap_layers5": 3,
        "swap_layers6": 0,
        "swap_layers7": 0,
    },
    "bert_6B_balance_original":{
        "hidden_size": 1824,
        "num_hidden_layers0": 30,
        "num_hidden_layers1": 30,
        "num_hidden_layers2": 30,
        "num_hidden_layers3": 30,
        "num_hidden_layers4": 30,
        "num_hidden_layers5": 30,
        "num_hidden_layers6": 30,
        "num_hidden_layers7": 30
    },
    "bert_6B_balance":{
        "hidden_size": 1824,
        "num_hidden_layers0": 30,
        "num_hidden_layers1": 30,
        "num_hidden_layers2": 30,
        "num_hidden_layers3": 30,
        "num_hidden_layers4": 30,
        "num_hidden_layers5": 30,
        "num_hidden_layers6": 30,
        "num_hidden_layers7": 30
    }
}


base_config = {
    "attention_probs_dropout_prob": 0.1,
    "hidden_act": "gelu",
    "hidden_dropout_prob": 0.1,
    "initializer_range": 0.02,
    "max_position_embeddings": 384,

    "type_vocab_size": 2,
    "vocab_size": 30522,

    "num_attention_heads": 16,
    "intermediate_size": 4096,
    "recomputation": False
}
if args.recompute:
    base_config["recomputation"]=True
base_config.update(config_map[args.model])
if args.recompute:
    args.model+='_recompute'
with open(args.model+'.json', 'w') as f:
    f.write(json.dumps(base_config, indent=4))


