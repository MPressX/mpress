export Z_LEVEL=info
export CUDA_VISIBLE_DEVICES=0,2,7,5,6,1,3,4
bash init_config.sh
# export CUDA_VISIBLE_DEVICES=4,6,3,1,2,5,0,7

# test_list=("bert_large_balance" "bert_large_balance_recompute" "bert_large_memory" \
#             "bert_48_1_balance_recompute" "bert_48_1_memory" \
#             "bert_48_2_balance_recompute" "bert_48_2_memory" \
#             "bert_64_1_balance_recompute")

pkill python
mode=2
test_list=("bert_48_1_balance")
dir="fig/device_mapping_optimal/"

function get_configuration(){
    echo "--module models.bert.gpus=8 \
    --mode ${mode} \
    --mem 28012254720 \
    --batch_size 12 \
    --num_epochs 1 \
    --candidates -1 3 4 5 6 7 \
    --data_dir /data/DNN_Dataset/bert/squad/train-v1.1.json \
    --max_seq_length 384 \
    --doc_stride 128 \
    --master_addr localhost \
    --rank ${1} \
    --local_rank ${1} \
    --master_addr localhost \
    --pipeline_config_path models/bert/gpus=8/mp_conf.json \
    --bert_config_path ${bert_test}.json \
    --distributed_backend nccl \
    --lfrequency 1"
}

for bert_test in ${test_list[@]};do
    python auto_gen_config.py --model ${bert_test}
    if [ $? -eq 0 ]; then
        if [ ! -d ${dir} ]; then
            mkdir -p ${dir}
        fi
        python main.py `get_configuration 0` > ${dir}/bert0.log  2>&1 &
        python main.py `get_configuration 1` > ${dir}/bert1.log  2>&1 &
        python main.py `get_configuration 2` > ${dir}/bert2.log  2>&1 &
        python main.py `get_configuration 3` > ${dir}/bert3.log  2>&1 &
        python main.py `get_configuration 4` > ${dir}/bert4.log  2>&1 &
        python main.py `get_configuration 5` > ${dir}/bert5.log  2>&1 &
        python main.py `get_configuration 6` > ${dir}/bert6.log  2>&1 &
        python main.py `get_configuration 7` > ${dir}/bert7.log  2>&1 &
        sleep 360
        mv ${bert_test}.json ${dir}/
        cp run.sh ${dir}/
        cp main.py ${dir}/
        cp optimize.json ${dir}/
        pkill python
        echo "${bert_test}_mode${mode} done"
    else
        echo "Raise an error"
    fi
done





