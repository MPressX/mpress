import argparse
import importlib
import copy
import json
import sys
import time


import torch
from torch import nn
from torch.nn import CrossEntropyLoss
from torch.optim import optimizer
import torch.distributed as dist

from get_dataset import *

from torch import swapper

sys.path.append("..")
import runtime
import adam
from optimizer import OptimizerWithWeightStashing
from LayerWiseOptimizerWithStashing.Adam import Adam as LayerWiseAdam
from LayerWiseOptimizerWithStashing import LayerWiseOptimizerWithStashing
from torch.utils.link_speed import get_speed_matrix

GB=1024*1024*1024

parser = argparse.ArgumentParser(description='PipeDream Bert Training')



# model configuration
parser.add_argument('--bert_config_path', default=None, type=str,
                    help="Path of configuration file for bert")
parser.add_argument('--module', required=True,
                    help='name of module that contains model and tensor_shapes definition')
parser.add_argument("--max_seq_length", default=384, type=int,
                        help="The maximum total input sequence length after WordPiece tokenization. Sequences "
                             "longer than this will be truncated, and sequences shorter than this will be padded.")
parser.add_argument("--doc_stride", default=128, type=int,
                    help="When splitting up a long document into chunks, how much stride to take between chunks.")
parser.add_argument("--max_query_length", default=64, type=int,
                    help="The maximum number of tokens for the question. Questions longer than this will "
                            "be truncated to this length.")



# traning configuration
parser.add_argument('--data_dir', type=str,
                    help='path to dataset')
parser.add_argument('--batch_size', default=256, type=int,
                    metavar='N', help='mini-batch size (default: 256)')
parser.add_argument("--num_epochs", default=3, type=int,
                        help="Total number of training epochs to perform.")
parser.add_argument("--lfrequency", default=1, type=int, 
                        help="the frequency of log; by default, would print in every iteration")

# swap configuration
parser.add_argument("--candidates", nargs='+', type=int, default=[-1], help="set the candidates to send\n"
                                                                            "-1: CPU\n"
                                                                            "default=[-1]\n"
                                                                            "example: --candidates 1 2 3")
parser.add_argument("--mode", type=int, default=0, help="[mode 0] no swap\n"
                                                        "[mode 1] swap to candidates\n"
                                                        "[mode 2] swap by advisor\n"
                                                        "default=0")
parser.add_argument("--mem",type=int,default=0*1024*1024*1024,help="set memory limitation; only valid if set mode=1; default=0")


# distributed training configuration
parser.add_argument('--pipeline_config_path', default=None, type=str,
                    help="Path of configuration file for pipedream")
parser.add_argument('--distributed_backend', type=str,
                    help='distributed backend to use (gloo|nccl)')
parser.add_argument('--rank', default=None, type=int,
                    help="Rank of worker")
parser.add_argument('--local_rank', default=0, type=int,
                    help="Local rank of worker")
parser.add_argument('--master_addr', default=None, type=str,
                    help="IP address of master (machine with rank 0)")



# recomputation
parser.add_argument('--recompute', action='store_true',
                    help='Recompute tensors in backward pass')

class SwapOptimize:
    def __init__(self):
        self.enable_swap = None
        self.weight_swapping_ratio = None
        self.hybrid_swap = None
    def from_json_file(self, json_file):
        with open(json_file, "r", encoding='utf-8') as reader:
            text = reader.read()
        json_object = json.loads(text)
        for key, value in json_object.items():
            self.__dict__[key] = value

from enum import Enum
class SwapType(Enum):
    swap = 1
    recompute = 2
    none = 3

class SwapDst():
    def __init__(self, destinations, ratios=None):
        if destinations == "recompute":
            self.type = "recompute"
        elif destinations == "none":
            self.type = "none"
        else:
            self.type = "swap"
            self.destinations = destinations
            self.ratios = ratios
            assert(len(destinations) == len(ratios))
            
        
class SwapConfig:
    def __init__(self, rank):
        self.rank = rank
        self.optimizer = None
        self.activations = None
        # self.activations[layer_id] = SwapDst(xxx)
        self.weights = None
    
    def generate_activations(self, act):
        from collections import defaultdict
        def none():
            return SwapDst("none")
        dic = defaultdict(none)
        for j in act:
            destinations = j['destinations']
            ratios = None if 'ratios' not in j else j['ratios']
            layer_range = j['layer_range']
            swap_dst = SwapDst(destinations, ratios)
            for layer_id in range(layer_range[0], layer_range[1]):
                dic[layer_id] = swap_dst
        return dic
    
    def from_json_file(self, json_file):
        with open(json_file, 'r', encoding='utf-8') as f:
            content = f.read()
            j = json.loads(content)
            optimizer_swap_config = j['optimizer_swap_config']
            self.optimizer = optimizer_swap_config[f"optimizer{self.rank}"]
            activations = j['activations_swap_config'][f'stage{self.rank}']
            self.activations = self.generate_activations(activations)
            self.weights = j['weight_swap_config'][f'weight{self.rank}']

class BertConfig(object):
    """Configuration class to store the configuration of a `BertModel`.
    """
    def __init__(self,
                 vocab_size_or_config_json_file,
                 hidden_size=768,
                 num_hidden_layers=12,
                 num_attention_heads=12,
                 intermediate_size=3072,
                 hidden_act="gelu",
                 hidden_dropout_prob=0.1,
                 attention_probs_dropout_prob=0.1,
                 max_position_embeddings=512,
                 type_vocab_size=2,
                 initializer_range=0.02):
        """Constructs BertConfig.

        Args:
            vocab_size_or_config_json_file: Vocabulary size of `inputs_ids` in `BertModel`.
            hidden_size: Size of the encoder layers and the pooler layer.
            num_hidden_layers: Number of hidden layers in the Transformer encoder.
            num_attention_heads: Number of attention heads for each attention layer in
                the Transformer encoder.
            intermediate_size: The size of the "intermediate" (i.e., feed-forward)
                layer in the Transformer encoder.
            hidden_act: The non-linear activation function (function or string) in the
                encoder and pooler. If string, "gelu", "relu" and "swish" are supported.
            hidden_dropout_prob: The dropout probabilitiy for all fully connected
                layers in the embeddings, encoder, and pooler.
            attention_probs_dropout_prob: The dropout ratio for the attention
                probabilities.
            max_position_embeddings: The maximum sequence length that this model might
                ever be used with. Typically set this to something large just in case
                (e.g., 512 or 1024 or 2048).
            type_vocab_size: The vocabulary size of the `token_type_ids` passed into
                `BertModel`.
            initializer_range: The sttdev of the truncated_normal_initializer for
                initializing all weight matrices.
        """
        self.activations_swap_config = None
        if isinstance(vocab_size_or_config_json_file, str):
            with open(vocab_size_or_config_json_file, "r", encoding='utf-8') as reader:
                json_config = json.loads(reader.read())
            for key, value in json_config.items():
                self.__dict__[key] = value
        elif isinstance(vocab_size_or_config_json_file, int):
            self.vocab_size = vocab_size_or_config_json_file
            self.hidden_size = hidden_size
            self.num_hidden_layers = num_hidden_layers
            self.num_attention_heads = num_attention_heads
            self.hidden_act = hidden_act
            self.intermediate_size = intermediate_size
            self.hidden_dropout_prob = hidden_dropout_prob
            self.attention_probs_dropout_prob = attention_probs_dropout_prob
            self.max_position_embeddings = max_position_embeddings
            self.type_vocab_size = type_vocab_size
            self.initializer_range = initializer_range
        else:
            raise ValueError("First argument must be either a vocabulary size (int)"
                             "or the path to a pretrained model config file (str)")

    @classmethod
    def from_dict(cls, json_object):
        """Constructs a `BertConfig` from a Python dictionary of parameters."""
        config = BertConfig(vocab_size_or_config_json_file=-1)
        for key, value in json_object.items():
            config.__dict__[key] = value
        return config

    @classmethod
    def from_json_file(cls, json_file):
        """Constructs a `BertConfig` from a json file of parameters."""
        with open(json_file, "r", encoding='utf-8') as reader:
            text = reader.read()
        return cls.from_dict(json.loads(text))

    def __repr__(self):
        return str(self.to_json_string())

    def to_dict(self):
        """Serializes this instance to a Python dictionary."""
        output = copy.deepcopy(self.__dict__)
        return output

    def to_json_string(self):
        """Serializes this instance to a JSON string."""
        return json.dumps(self.to_dict(), indent=2, sort_keys=True) + "\n"

class BertLoss(nn.Module):
    def __init__(self, config) -> None:
        super(BertLoss,self).__init__()
        self.qa_outputs = nn.Linear(config.hidden_size, 2)
    def forward(self, hidden_states, start_positions, end_positions):
        logits = self.qa_outputs(hidden_states)
        start_logits, end_logits = logits.split(1, dim=-1)
        start_logits = start_logits.squeeze(-1)
        end_logits = end_logits.squeeze(-1)
        if len(start_positions.size()) > 1:
                start_positions = start_positions.squeeze(-1)
        if len(end_positions.size()) > 1:
            end_positions = end_positions.squeeze(-1)
        ignored_index = start_logits.size(1)
        start_positions.clamp_(0, ignored_index)
        end_positions.clamp_(0, ignored_index)

        loss_fct = CrossEntropyLoss(ignore_index=ignored_index)
        start_loss = loss_fct(start_logits, start_positions)
        end_loss = loss_fct(end_logits, end_positions)
        total_loss = (start_loss + end_loss) / 2
        return total_loss

class AverageMeter(object):
    """Computes and stores the average and current value"""
    def __init__(self):
        self.reset()

    def reset(self):
        self.val = 0
        self.avg = 0
        self.sum = 0
        self.count = 0

    def update(self, val, n=1):
        self.val = val
        self.sum += val * n
        self.count += n
        self.avg = self.sum / self.count

def train(train_loader,r:runtime.StageRuntime, optimizer:LayerWiseOptimizerWithStashing, num_warmup_minibatches):
    global args

    hook = optimizer.get_optimize(enable_swap=True)
    swapper.register_optimize_func(hook)


    batch_time = AverageMeter()
    swap_data_size = AverageMeter()
    r.train(0)
    r.set_loader(train_loader)

    forward_cnt = 0
    backward_cnt = 0

    swapper.clear_swap_out_bytes()
    for i in range(num_warmup_minibatches+1):
        # optimizer.load_params_by_index(i)
        if i!=0:
            optimizer.generate_and_set_new_param(True)
        r.receive_tensors_forward()
        if i == num_warmup_minibatches:
            r.run_prefetch()
        r._run_forward()
        forward_cnt+=1
        r.send_tensors_forward()
        swap_data_size.update(swapper.get_swap_out_bytes())
    print(f"Total swap volumn: {swap_data_size.sum/(1024*1024*1024):.3f}GB")
    swap_data_size.reset()
    sys.stdout.flush()
    swapper.clear_swap_out_bytes()
    # 7400
    end = time.time()
    for epoch in range(args.num_epochs):
        print(f"Epoch: {epoch}")
        
        for i in range(5000):
            if args.stage == (args.num_stages-1):
                batch_time.update(time.time() - end)
                end = time.time()
                if i % args.lfrequency == 0:
                    
                    print(
                        f"Throughput: {args.batch_size/batch_time.avg:.3f}\t"
                        f"Memory Cached: {torch.cuda.memory_cached()/(1024*1024)}MB\t"
                        f"Swap volumn per iter: {swap_data_size.avg/(1024*1024*1024):.3f}GB")
                    batch_time.reset()
                    swap_data_size.reset()
                    sys.stdout.flush()
            else:
                if i % args.lfrequency == 0:
                    print(
                        f"Memory Cached: {torch.cuda.memory_cached()/(1024*1024):.3f}MB\t"
                        f"Total swap-tensor size: {swap_data_size.avg/(1024*1024*1024):.3f}GB")
                    swap_data_size.reset()
                    sys.stdout.flush()
            swapper.clear_swap_out_bytes()
            # swapper.reset_advisor_index()
            # swapper.enable_hybrid_swap()
            r.receive_tensors_backward()
            optimizer.load_params_by_index(backward_cnt)
            r._run_backward(enable_swap=True, optimizer=optimizer)
            backward_cnt+=1
            r.receive_tensors_forward()
            r.send_tensors_backward()

            r.run_prefetch()

            r._run_forward()
            forward_cnt += 1
            r.send_tensors_forward()
            swap_data_size.update(swapper.get_swap_out_bytes())
        r.set_loader(train_loader)
    print("Pipeline training finished!")

def main():
    global args
    args = parser.parse_args()
    print(f"max_seq_length = {args.max_seq_length}")
    sys.stdout.flush()
    torch.cuda.set_device(args.local_rank)
    

    # ===============================================
    # load configurations
    # ===============================================
    configuration_maps = {
        'module_to_stage_map': None,
        'stage_to_rank_map': None,
        'stage_to_depth_map': None
    }
    if args.pipeline_config_path is not None:
        json_config_file = json.load(open(args.pipeline_config_path, 'r'))
        configuration_maps['module_to_stage_map'] = json_config_file.get("module_to_stage_map", None)
        configuration_maps['stage_to_rank_map'] = json_config_file.get("stage_to_rank_map", None)
        configuration_maps['stage_to_rank_map'] = {
            int(k): v for (k, v) in configuration_maps['stage_to_rank_map'].items()}
        configuration_maps['stage_to_depth_map'] = json_config_file.get("stage_to_depth_map", None)
    swap_config = SwapConfig(args.local_rank)
    swap_config.from_json_file("optimize.json")
    print("configuration_maps: ", configuration_maps)
    print(f"swap_config.optimizer: ", swap_config.optimizer)
    sys.stdout.flush()
        
    # ===============================================
    # initialize model
    # ===============================================
    bert_config = BertConfig.from_json_file(args.bert_config_path)
    bert_config.activations_swap_config = swap_config.activations
    
    criterion = BertLoss(bert_config)
    module = importlib.import_module(args.module)
    args.arch = module.arch()
    model = module.model(bert_config, criterion)
    # num_params = sum(p.numel() for p in model.parameters())
    # print(f"num_params = {num_params}")
    sys.stdout.flush()
    
    # ===============================================
    # generate dataset
    # ===============================================
    # train_loader=None
    # if args.rank == 0:
    #     train_dataset = get_training_data(args)
    #     train_loader = torch.utils.data.DataLoader(
    #         train_dataset, batch_size=args.batch_size, shuffle=True,
    #         num_workers=3, pin_memory=True, drop_last=True)

 
    def generate_fake_data(bs, seq_len, vocab_size):
        attention_mask = torch.ones((bs, seq_len), dtype=torch.long)
        token_type_ids = torch.zeros((bs, seq_len), dtype=torch.long)
        while True:
            start_positions = torch.randint(seq_len, (bs,), dtype=torch.long)
            input_ids = torch.randint(vocab_size, (bs, seq_len), dtype=torch.long)
            end_positions = torch.randint(seq_len, (bs,), dtype=torch.long)
            yield input_ids, attention_mask, token_type_ids, start_positions, end_positions
    train_loader = None
    if args.rank == 0:
        train_loader = generate_fake_data(args.batch_size, bert_config.max_position_embeddings, bert_config.vocab_size)

    
    


    # ===============================================
    # inference input data shape
    # ===============================================
    training_tensor_shapes = {
                                "input_ids0": [args.batch_size, args.max_seq_length], 
                                "attention_mask0": [args.batch_size, args.max_seq_length],
                                "token_type_ids0":[args.batch_size, args.max_seq_length],
                                "start_positions":[args.batch_size],
                                "end_positions":[args.batch_size],
                                }
    dtypes = {
                "input_ids0": torch.long, 
                "attention_mask0": torch.long, 
                "token_type_ids0":torch.long, 
                "start_positions":torch.long, 
                "end_positions": torch.long}


    inputs_module_destinations = {
                                    "input_ids0": 0, 
                                    "attention_mask0": 0, 
                                    "token_type_ids0": 0}
    target_tensor_names = {"start_positions", "end_positions"}

    for (stage, inputs, outputs) in model[:-1]:  # Skip last layer (loss).
        input_tensors = []
        for input in inputs:
            input_tensor = torch.zeros(tuple(training_tensor_shapes[input]),
                                       dtype=dtypes[input]).cuda()
            input_tensors.append(input_tensor)
        with torch.no_grad():
            stage=stage.cuda()
            output_tensors = stage(*tuple(input_tensors))
            # stage.swap=bert_config.swap
            stage=stage.cpu()
        if not type(output_tensors) is tuple:
            output_tensors = [output_tensors]
        for output, output_tensor in zip(outputs,
                                         list(output_tensors)):
            training_tensor_shapes[output] = list(output_tensor.size())
            dtypes[output] = output_tensor.dtype
    input_tensors = []
    
    # training_tensor_shapes={'input_ids0': [args.batch_size, args.max_seq_length], 'attention_mask0': [args.batch_size, 1, 1, args.max_seq_length], 'token_type_ids0': [args.batch_size, args.max_seq_length], 'start_positions': [args.batch_size], 'end_positions': [args.batch_size], 'hidden_states0': [args.batch_size, args.max_seq_length, 1824], 'hidden_states1': [args.batch_size, args.max_seq_length, 1824], 'attention_mask1': [args.batch_size, 1, 1, args.max_seq_length], 'hidden_states2': [args.batch_size, args.max_seq_length, 1824], 'attention_mask2': [args.batch_size, 1, 1, args.max_seq_length], 'hidden_states3': [args.batch_size, args.max_seq_length, 1824], 'attention_mask3': [args.batch_size, 1, 1, args.max_seq_length], 'hidden_states4': [args.batch_size, args.max_seq_length, 1824], 'attention_mask4': [args.batch_size, 1, 1, args.max_seq_length], 'hidden_states5': [args.batch_size, args.max_seq_length, 1824], 'attention_mask5': [args.batch_size, 1, 1, args.max_seq_length], 'hidden_states6': [args.batch_size, args.max_seq_length, 1824], 'attention_mask6': [args.batch_size, 1, 1, args.max_seq_length], 'hidden_states7': [args.batch_size, args.max_seq_length, 1824]}
    # dtypes={'input_ids0': torch.int64, 'attention_mask0': torch.float32, 'token_type_ids0': torch.int64, 'start_positions': torch.int64, 'end_positions': torch.int64, 'hidden_states0': torch.float32, 'hidden_states1': torch.float32, 'attention_mask1': torch.float32, 'hidden_states2': torch.float32, 'attention_mask2': torch.float32, 'hidden_states3': torch.float32, 'attention_mask3': torch.float32, 'hidden_states4': torch.float32, 'attention_mask4': torch.float32, 'hidden_states5': torch.float32, 'attention_mask5': torch.float32, 'hidden_states6': torch.float32, 'attention_mask6': torch.float32, 'hidden_states7': torch.float32}
    # print(f"Warning: shapes and dtypes is specified for bert-6B")
    
    print(f"training_tensor_shapes={training_tensor_shapes}")
    print(f"dtypes={dtypes}")
    sys.stdout.flush()

    # ===============================================
    # initialize pipedream runtime
    # ===============================================

    r = runtime.StageRuntime(
        model=model, distributed_backend=args.distributed_backend,
        fp16=False, loss_scale=1,
        training_tensor_shapes=training_tensor_shapes,
        eval_tensor_shapes=None,
        training_tensor_dtypes=dtypes,
        inputs_module_destinations=inputs_module_destinations,
        target_tensor_names=target_tensor_names,
        configuration_maps=configuration_maps,
        master_addr=args.master_addr, rank=args.rank,
        local_rank=args.local_rank,
        num_ranks_in_server=1,
        verbose_freq=0,
        model_type=runtime.BERT_MODEL,
        enable_recompute=args.recompute)
    model=None
    args.stage = r.stage
    args.num_stages = r.num_stages
    args.num_ranks = r.num_ranks
    num_warmup_minibatches = r.num_warmup_minibatches
    print("stage: ", r.stage)
    print("num_stages: ", r.num_stages)
    print("num_ranks: ", r.num_ranks)
    print("num_warmup_minibatches: ", num_warmup_minibatches)
    

    # ===============================================
    # initialize pipedream optimizer
    # ===============================================
    num_versions = r.num_warmup_minibatches + 1
    
    assert(args.mode>0)
    optimizer = LayerWiseAdam(r.modules(),num_versions)
    # optimizer.set_swapped_iter(swap_optimize.weight_swapping_ratio[args.local_rank])
    optimizer.set_swap_config(swap_config.optimizer)

    # ===============================================
    # initialize torchswapper
    # ===============================================
    speed_matrix = get_speed_matrix("output/network_profiling.json")
    
    # `candidates` in init_swapper must include `candidates` in set_device_status
    swapper.init_swapper(
                        default_device_id=args.local_rank,
                        cuda_memory_threshold=args.mem,
                        num_cards=r.num_ranks,
                        swap_mode=args.mode,
                        candidates=args.candidates,
                        speed_matrix=speed_matrix
                        )


    # ===============================================
    # begin to train
    # ===============================================
    assert(args.mode>0)
    train(train_loader=train_loader,r=r, optimizer=optimizer, num_warmup_minibatches=num_warmup_minibatches)

main()