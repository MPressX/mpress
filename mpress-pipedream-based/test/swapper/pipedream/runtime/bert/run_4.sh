export Z_LEVEL=error

# bert_test="bert_48_1_balance"
# bert_test="bert_48_1_memory"

# bert_test="bert_48_2_balance"
# bert_test="bert_48_2_memory"

bert_test="bert-base-uncased"
# bert_test="bert_large_memory"
# bert_test="bert_large_balance"


function get_configuration(){
    echo "--module models.bert.gpus=4 \
    --mode 2 \
    --mem 9663676416 \
    --batch_size 12 \
    --num_epochs 1 \
    --candidates -1 1 2 3 4 \
    --data_dir /data/DNN_Dataset/bert/squad/train-v1.1.json \
    --max_seq_length 384 \
    --doc_stride 128 \
    --master_addr localhost \
    --rank ${1} \
    --local_rank ${1} \
    --master_addr localhost \
    --pipeline_config_path models/bert/gpus=4/mp_conf.json \
    --bert_config_path models/bert/gpus=4/${bert_test}.json \
    --distributed_backend nccl \
    --lfrequency 10"
    
}

python main.py `get_configuration 0` > bert0.log  2>&1 &
python main.py `get_configuration 1` > bert1.log  2>&1 &
python main.py `get_configuration 2` > bert2.log  2>&1 &
python main.py `get_configuration 3` > bert3.log  2>&1 &