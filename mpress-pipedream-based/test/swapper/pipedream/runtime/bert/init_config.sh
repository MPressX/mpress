export CUDA_VISIBLE_DEVICES=0,2,7,5,6,1,3,4
mkdir -p output
cd ../../../../../tools/link_speed/
cd bandwidthTest
make
cd ../p2pBandwidthLatencyTest
make
cd ..
python get_link_speed.py
cp output/* ../../test/swapper/pipedream/runtime/bert/output/
