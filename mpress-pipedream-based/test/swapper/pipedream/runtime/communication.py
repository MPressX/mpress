# Copyright (c) Microsoft Corporation.
# Licensed under the MIT license.

import os
import threading
import torch
import torch.distributed as dist
import sys

import threadsafe_counter
import threadsafe_queue


NCCL='nccl'
GLOO='gloo'


class CommunicationHandler(object):
    """ Handles communication between stages.

    For stages on different machines, use send/recv.
    For stages on same machine, use broadcast.
    """
    def __init__(self, master_addr, master_port, rank,
                 local_rank, num_ranks_in_server,
                 world_size, fp16, backend,
                 training_tensor_shapes,
                 training_tensor_dtypes):
        """ Set up process groups.

        Note: To turn off broadcasting, set num_ranks_in_server = 1.
        """
        self.rank = rank
        self.local_rank = local_rank
        self.backend = backend
        self.num_ranks_in_server = num_ranks_in_server
        self.world_size = world_size
        self.fp16 = fp16
        self.training_tensor_shapes=training_tensor_shapes
        self.training_tensor_dtypes=training_tensor_dtypes
        assert num_ranks_in_server > 0

        # Initialize the distributed environment.
        os.environ['MASTER_ADDR'] = master_addr
        os.environ['MASTER_PORT'] = str(master_port)
        dist.init_process_group(backend, rank=rank, world_size=world_size)
        assert dist.get_world_size() == self.world_size
        print("Finished initializing process group; backend: %s, rank: %d, "
              "world_size: %d" % (backend, rank, world_size))
        self.groups = dict()
        for i in range(world_size):
            for j in range(i):
                self.groups[(j,i)] = dist.new_group([j,i])
                self.groups[(i,j)] = self.groups[(j,i)]
        print("after new_group 0,1")

    def send(self, tensor_name:str, dst:int, tensor:torch.Tensor):
        assert tensor.is_cuda
        contiguous_tensor=tensor.detach().clone()
        dist.broadcast(
            tensor=contiguous_tensor.contiguous(),
            src=self.local_rank,
            group=self.groups[(self.local_rank, dst)]
        )
        
    def recv(self, tensor_name:str, src:int, set_requires_grad=False):
        tensor_shape = self.training_tensor_shapes[tensor_name]
        tensor_dtype = self.training_tensor_dtypes[tensor_name]
        tensor = torch.zeros(tensor_shape, dtype=tensor_dtype, device='cuda')
        dist.broadcast(
            tensor=tensor,
            src=src,
            group=self.groups[(src,self.local_rank)]
        )
        if set_requires_grad and tensor.dtype==torch.float32:
            tensor = tensor.requires_grad_()
        assert tensor.is_cuda
        return tensor
