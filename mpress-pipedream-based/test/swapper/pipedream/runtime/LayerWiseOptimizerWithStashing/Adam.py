from . import LayerWiseOptimizerWithStashing
from torch import swapper
import torch
import math

class Adam(LayerWiseOptimizerWithStashing):
    def __init__(self, modules, num_versions,
                 learning_rate=1e-3, betas=(0.9, 0.999), eps=1e-8,
                 weight_decay=0):
        super(Adam, self).__init__(modules, num_versions)
        if not 0.0 <= learning_rate:
            raise ValueError("Invalid learning rate: {}".format(learning_rate))
        if not 0.0 <= eps:
            raise ValueError("Invalid epsilon value: {}".format(eps))
        if not 0.0 <= betas[0] < 1.0:
            raise ValueError("Invalid beta parameter at index 0: {}".format(betas[0]))
        if not 0.0 <= betas[1] < 1.0:
            raise ValueError("Invalid beta parameter at index 1: {}".format(betas[1]))
        self.betas = betas
        self.lr = learning_rate
        self.eps = eps
        self.weight_decay = weight_decay
        
    @staticmethod
    def optimize(self, param):
        id = swapper.get_id(param.data)
        if id not in self.states: return 1

        states = self.states[id]
        weight_old = param.data
        weight_new = states['next_weight']
        grad= param.grad.data
        if 'exp_avg' not in states:
            states['step'] = 0
            states['exp_avg'] = torch.zeros_like(weight_old)
            states['exp_avg_sq'] = torch.zeros_like(weight_old)
            swapper.record_state(weight_old, states['exp_avg'])
            swapper.record_state(weight_old, states['exp_avg_sq'])
        exp_avg, exp_avg_sq = states['exp_avg'], states['exp_avg_sq']
        beta1, beta2 = self.betas
        states['step'] += 1
        if self.weight_decay!=0:
            grad.add_(self.weight_decay, weight_new)
        exp_avg.mul_(beta1).add_(1-beta1, grad)
        exp_avg_sq.mul_(beta2).addcmul_(1-beta2, grad, grad)

        denom = exp_avg_sq.sqrt().add_(self.eps)

        bias_correction1 = 1-beta1**states['step']
        bias_correction2 = 1-beta2**states['step']
        step_size = self.lr*math.sqrt(bias_correction2)/bias_correction1
        # 用weight_new来计算，更新后的weight赋值给weight_old
        weight_old.copy_(weight_new)
        weight_old.addcdiv_(-step_size, exp_avg, denom)
        return 0

    def nbytes_swapped_states_one_version(self, threshold=0):
        return self.nbytes_swapped_weights_one_version(threshold)*2
    