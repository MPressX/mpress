from . import LayerWiseOptimizerWithStashing
from torch import swapper

class SGD(LayerWiseOptimizerWithStashing):
    def __init__(self, modules, num_versions, learning_rate=0.01):
        super(SGD, self).__init__(modules, num_versions)
        self.lr = learning_rate

    @staticmethod
    def optimize(self, param):
        id = swapper.get_id(param.data)
        # id = 0
        if id in self.states:
            # print(f"optimize: id in dicts: {id}")
            weight_old = param.data
            states = self.states[id]
            weight_new = states['next_weight']
            grad = param.grad.data
            weight_old.copy_(grad)
            weight_old.mul_(-self.lr)
            weight_old.add_(weight_new)
            return 0 # 是个普通的weight
        else:
            # print(f"optimize: id not in dicts: {id}")
            # print(f"find param not in states, id={id}")
            return 1 # 是通过receive_tensors_forward接受到的数据

