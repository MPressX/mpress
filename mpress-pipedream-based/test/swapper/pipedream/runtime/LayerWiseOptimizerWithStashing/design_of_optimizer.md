```python
from typing import Dict
def receive_forward(): pass
def run_forward(): pass
def send_forward(): pass
def receive_backward(): pass
def run_backward(): pass
def send_backward(): pass
def load_old_param(): pass
def load_new_param(): pass
def generate_param(): pass

class LayerWiseOptimizerWithStashing():
    def __init__(self, num_versions_of_weight):
        self.state=dict()
    def step(self, param):
        param_old = param
        param_new = self.state[param]['next_weight']
        gradient = param_old.grad
        update = calc(gradient)
        param_old = param_new + update
    def load_by_index(index:int):pass

num_gpus=4
local_rank:int

warmup_iterations = num_gpus-local_rank
num_versions_of_weight = warmup_iterations

optimizer = LayerWiseOptimizerWithStashing(num_versions_of_weight)

forward_cnt = 0
backward_cnt = 0
    
for i in range(warmup_iterations):
    optimizer.load_by_index(forward_cnt)
    receive_forward() 
    run_forward()
    forward_cnt += 1
    send_forward()

for _ in range(100):
    optimizer.load_by_index(backward_cnt)

    launch_swap_in()

    receive_backward()
    
    run_backward():
        processing layer[N]: 
            W[N] = Update(W2[N], W[N].grad)
            W[N].grad = None
            swap_out(W2[N])
        ...
        Layer[1]: W[1] = Update(W2[1], W[1].grad)

    backward_cnt += 1
    # optimizer.load_by_index(forward_cnt) # forward_cnt = backward_cnt + warmup_iterations
    receive_forward()
    send_backward():
        swap_out(Activations)
        not swap_out(Weights) # this weight is used in next backward
    run_forward()
    forward_cnt += 1
    send_forward()
```