import traceback
import copy
from torch import swapper
import torch

class LayerWiseOptimizerWithStashing():
    def __init__(self, modules, num_versions):
        # swapper.print_str(f"num_versions={num_versions}")
        self.modules = modules
        self.num_versions = num_versions
        self.initialize_queue()
        self.states = dict()
    

    def get_params(self, clone=False):
        state_dicts = []
        for module in self.modules:
            state_dict = module.state_dict()
            if clone:
                for key in state_dict:
                    # print(f"key={key}, device={state_dict[key].device}")
                    # import sys;sys.stdout.flush()
                    state_dict[key] = copy.deepcopy(state_dict[key])
            state_dicts.append(state_dict)
        return state_dicts

    def set_params(self, state_dicts):
        for (state_dict, module) in zip(state_dicts, self.modules):
            cur_state_dict = module.state_dict()
            for key in state_dict:
                # Don't update running_mean and running_var; these should
                # accumulate normally.
                # mask might have a different shape, so don't copy it to
                # the module this way.
                if "running_" in key or "mask" in key:
                    state_dict[key] = cur_state_dict[key]
            module.load_state_dict_v2(state_dict)
            for key in state_dict:
                if "mask" in key:
                    attribute_names = key.split('.')
                    attribute = module
                    for attribute_name in attribute_names:
                        attribute = getattr(attribute, attribute_name)
                    # NOTE: Do we need to clone here?
                    attribute = state_dict[key]
    
    def set_swap_config(self, swap_config):
        self.swap_config = swap_config
    # def set_swapped_iter(self, t=1):
    #     if 0<=t<=1:
    #         # 按比例
    #         import math
    #         self.swapped_num = math.ceil(self.num_weights_in_one_iter * t)
    #     elif t > 1:
    #         # 按个数
    #         self.swapped_num = math.ceil(t)
    #     else:
    #         raise ValueError

    def initialize_queue(self):
        self.queue = [self.get_params(clone=False)]
        self.num_weights_in_one_iter = 0
        for state_dict in self.queue[-1]:
            self.num_weights_in_one_iter += len(state_dict)
        # 默认全部swap
        self.swapped_num = self.num_weights_in_one_iter
            
        # for i in range(1, self.num_versions):
        #     self.queue.append(self.get_params(clone=True))
            
    def load_params_by_index(self, index):
        if self.num_versions > 1:
            self.set_params(self.queue[index%self.num_versions])

    def generate_and_set_new_param(self, enable_swap=False):
        if len(self.queue) >= self.num_versions:
            raise RuntimeError    
        if len(self.queue) == 1:
            cnt = 0
            for state_dict in self.queue[-1]:
                for k in state_dict:
                    cnt += 1        
                    id = swapper.get_id(state_dict[k].data)
                    self.states[id] = dict()
                    self.states[id]['swap_id'] = cnt
                    # print(f"id={id}, swap_id={self.states[id]['swap_id']}")
        self.queue.append(self.get_params(clone=True))
        # self.set_params(self.queue[-1])
        self.load_params_by_index(len(self.queue)-1)
        
        for state_dict_2, state_dict_1 in zip(self.queue[-2], self.queue[-1]):
            for key in state_dict_2:
                id1 = swapper.get_id(state_dict_1[key].data)
                id2 = swapper.get_id(state_dict_2[key].data)
                if id1 != id2:
                    assert(id1 not in self.states)
                    self.states[id1] = dict()
                    self.states[id1]['next_weight'] = state_dict_2[key].data
                    swapper.record_state(weight=state_dict_1[key].data, 
                                            state=state_dict_2[key].data)
                    self.states[id1]['swap_id'] = self.states[id2]['swap_id']
                else:
                    self.states[id1]['swap_id'] = -1
        if len(self.queue) == self.num_versions:
            for state_dict_2, state_dict_1 in zip(self.queue[-1], self.queue[0]):
                for key in state_dict_2:
                    id1 = swapper.get_id(state_dict_1[key].data)
                    id2 = swapper.get_id(state_dict_2[key].data)
                    if id1 != id2:
                        self.states[id1]['next_weight'] = state_dict_2[key].data
                        swapper.record_state(weight=state_dict_1[key].data, 
                                                state=state_dict_2[key].data)
                    
        # swap out older weight
        if enable_swap:
            print("swap out old weight")
            import sys;sys.stdout.flush()
            meta_list = []
            for state_dict in self.queue[-2]:
                for key,v in state_dict.items():
                    id = swapper.get_id(state_dict[key].data)
                    if self.states[id]['swap_id'] <= self.swapped_num:
                        meta_list.append(swapper.tensor_to_meta(v.data))
            swapper.swap_out_by_metas(targets=meta_list, destinations=[-1], ratios=[1])          
        
            
    @staticmethod
    def optimize(self, param):
        raise NotImplementedError

    def nbytes_swapped_weights_one_version(self, threshold=0):
        """tells weights need how many nbytes
        """
        ans = 0
        for state_dict in self.queue[-1]:
            for key in state_dict:
                id = swapper.get_id(state_dict[key].data)
                if self.states[id]['swap_id'] <= self.swapped_num:
                    nbytes = state_dict[key].element_size() * state_dict[key].nelement()
                    if nbytes >= threshold:
                        ans += nbytes
        return ans

    def nbytes_swapped_states_one_version(self, threshold=0):
        raise NotImplementedError
    
    def nbytes_swapped_one_version(self, threshold=0):
        return self.nbytes_swapped_states_one_version() + self.nbytes_swapped_weights_one_version()
                


    def get_optimize(self, zero_grad=True, enable_swap=False):
        def wrapped_optimizer(param):
            try:
                # print(f"param={param}")
                # return
                ret = self.optimize(self, param)
                if ret==0:
                    if zero_grad:
                        param.grad=None
                    if enable_swap:
                        id = swapper.get_id(param.data)
                        states:dict = self.states[id]
                        swap_id = states['swap_id']
                        current_ratio_point = swap_id / self.num_weights_in_one_iter
                        destinations, ratios = None, None
                        for j in self.swap_config:
                            ratio_range = j['ratio_range']
                            if ratio_range[0] <= current_ratio_point < ratio_range[1]:
                                destinations = j['destinations']
                                ratios = j['ratios']
                                break
                        if destinations is None:
                            return
                        print(f"swap states of tensor with id={id}, destinations={destinations}, ratios={ratios}")
                        if self.num_versions>1:
                            meta_list = []
                            for k,v in states.items():
                                if isinstance(v, torch.Tensor):
                                    meta_list.append(swapper.tensor_to_meta(v))
                                else:
                                    pass
                            if meta_list:
                                swapper.swap_out_by_metas(targets=meta_list, destinations=destinations, ratios=ratios)
                else:
                    pass
            except:
                print(f"LayerWiseOptimizerWithStashing optimize failed, Error_msg={traceback.format_exc()}")
        return wrapped_optimizer
                