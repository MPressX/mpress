node2 -- Conv2d(3, 64, kernel_size=(7, 7), stride=(2, 2), padding=(3, 3), bias=False) -- forward_compute_time=18.279, backward_compute_time=7.940, activation_size=411041792.0, parameter_size=37632.000 -- stage_id=0
node3 -- BatchNorm2d(64, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True) -- forward_compute_time=3.842, backward_compute_time=0.183, activation_size=411041792.0, parameter_size=512.000 -- stage_id=0
node4 -- ReLU(inplace=True) -- forward_compute_time=2.309, backward_compute_time=0.730, activation_size=411041792.0, parameter_size=0.000 -- stage_id=0
node5 -- MaxPool2d(kernel_size=3, stride=2, padding=1, dilation=1, ceil_mode=False) -- forward_compute_time=2.443, backward_compute_time=0.535, activation_size=102760448.0, parameter_size=0.000 -- stage_id=0
node6 -- Conv2d(64, 64, kernel_size=(1, 1), stride=(1, 1), bias=False) -- forward_compute_time=47.661, backward_compute_time=20.560, activation_size=102760448.0, parameter_size=16384.000 -- stage_id=0
node7 -- BatchNorm2d(64, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True) -- forward_compute_time=26.515, backward_compute_time=0.156, activation_size=102760448.0, parameter_size=512.000 -- stage_id=0
node8 -- ReLU(inplace=True) -- forward_compute_time=49.680, backward_compute_time=0.104, activation_size=102760448.0, parameter_size=0.000 -- stage_id=0
node9 -- Conv2d(64, 64, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1), bias=False) -- forward_compute_time=8.091, backward_compute_time=0.318, activation_size=102760448.0, parameter_size=147456.000 -- stage_id=0
node10 -- BatchNorm2d(64, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True) -- forward_compute_time=1.539, backward_compute_time=0.164, activation_size=102760448.0, parameter_size=512.000 -- stage_id=0
node11 -- ReLU(inplace=True) -- forward_compute_time=0.603, backward_compute_time=0.149, activation_size=102760448.0, parameter_size=0.000 -- stage_id=0
node12 -- Conv2d(64, 256, kernel_size=(1, 1), stride=(1, 1), bias=False) -- forward_compute_time=2.197, backward_compute_time=0.249, activation_size=411041792.0, parameter_size=65536.000 -- stage_id=1
node13 -- BatchNorm2d(256, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True) -- forward_compute_time=3.723, backward_compute_time=0.194, activation_size=411041792.0, parameter_size=2048.000 -- stage_id=1
node14 -- Conv2d(64, 256, kernel_size=(1, 1), stride=(1, 1), bias=False) -- forward_compute_time=2.271, backward_compute_time=0.275, activation_size=411041792.0, parameter_size=65536.000 -- stage_id=1
node15 -- BatchNorm2d(256, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True) -- forward_compute_time=8.455, backward_compute_time=0.262, activation_size=411041792.0, parameter_size=2048.000 -- stage_id=1
node16 -- Add(inplace) -- forward_compute_time=0.000, backward_compute_time=0.000, activation_size=411041792.0, parameter_size=0.000 -- stage_id=1
node17 -- ReLU(inplace=True) -- forward_compute_time=5.652, backward_compute_time=0.107, activation_size=411041792.0, parameter_size=0.000 -- stage_id=1
node18 -- Conv2d(256, 64, kernel_size=(1, 1), stride=(1, 1), bias=False) -- forward_compute_time=46.877, backward_compute_time=0.249, activation_size=102760448.0, parameter_size=65536.000 -- stage_id=1
node19 -- BatchNorm2d(64, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True) -- forward_compute_time=46.600, backward_compute_time=0.164, activation_size=102760448.0, parameter_size=512.000 -- stage_id=1
node20 -- ReLU(inplace=True) -- forward_compute_time=11.764, backward_compute_time=0.108, activation_size=102760448.0, parameter_size=0.000 -- stage_id=1
node21 -- Conv2d(64, 64, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1), bias=False) -- forward_compute_time=11.605, backward_compute_time=0.329, activation_size=102760448.0, parameter_size=147456.000 -- stage_id=1
node22 -- BatchNorm2d(64, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True) -- forward_compute_time=12.503, backward_compute_time=0.169, activation_size=102760448.0, parameter_size=512.000 -- stage_id=1
node23 -- ReLU(inplace=True) -- forward_compute_time=12.834, backward_compute_time=0.114, activation_size=102760448.0, parameter_size=0.000 -- stage_id=1
node24 -- Conv2d(64, 256, kernel_size=(1, 1), stride=(1, 1), bias=False) -- forward_compute_time=11.023, backward_compute_time=0.623, activation_size=411041792.0, parameter_size=65536.000 -- stage_id=1
node25 -- BatchNorm2d(256, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True) -- forward_compute_time=3.727, backward_compute_time=0.248, activation_size=411041792.0, parameter_size=2048.000 -- stage_id=1
node26 -- Add(inplace) -- forward_compute_time=0.000, backward_compute_time=0.000, activation_size=411041792.0, parameter_size=0.000 -- stage_id=1
node27 -- ReLU(inplace=True) -- forward_compute_time=5.648, backward_compute_time=0.112, activation_size=411041792.0, parameter_size=0.000 -- stage_id=1
node28 -- Conv2d(256, 64, kernel_size=(1, 1), stride=(1, 1), bias=False) -- forward_compute_time=51.097, backward_compute_time=15.922, activation_size=102760448.0, parameter_size=65536.000 -- stage_id=1
node29 -- BatchNorm2d(64, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True) -- forward_compute_time=11.807, backward_compute_time=0.171, activation_size=102760448.0, parameter_size=512.000 -- stage_id=1
node30 -- ReLU(inplace=True) -- forward_compute_time=12.538, backward_compute_time=0.110, activation_size=102760448.0, parameter_size=0.000 -- stage_id=1
node31 -- Conv2d(64, 64, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1), bias=False) -- forward_compute_time=14.837, backward_compute_time=11.890, activation_size=102760448.0, parameter_size=147456.000 -- stage_id=1
node32 -- BatchNorm2d(64, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True) -- forward_compute_time=11.704, backward_compute_time=0.168, activation_size=102760448.0, parameter_size=512.000 -- stage_id=1
node33 -- ReLU(inplace=True) -- forward_compute_time=44.444, backward_compute_time=0.118, activation_size=102760448.0, parameter_size=0.000 -- stage_id=1
node34 -- Conv2d(64, 256, kernel_size=(1, 1), stride=(1, 1), bias=False) -- forward_compute_time=2.211, backward_compute_time=19.427, activation_size=411041792.0, parameter_size=65536.000 -- stage_id=1
node35 -- BatchNorm2d(256, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True) -- forward_compute_time=3.715, backward_compute_time=0.272, activation_size=411041792.0, parameter_size=2048.000 -- stage_id=1
node36 -- Add(inplace) -- forward_compute_time=0.000, backward_compute_time=0.000, activation_size=411041792.0, parameter_size=0.000 -- stage_id=1
node37 -- ReLU(inplace=True) -- forward_compute_time=5.644, backward_compute_time=0.114, activation_size=411041792.0, parameter_size=0.000 -- stage_id=1
node38 -- Conv2d(256, 128, kernel_size=(1, 1), stride=(1, 1), bias=False) -- forward_compute_time=47.677, backward_compute_time=25.332, activation_size=205520896.0, parameter_size=131072.000 -- stage_id=1
node39 -- BatchNorm2d(128, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True) -- forward_compute_time=12.039, backward_compute_time=0.163, activation_size=205520896.0, parameter_size=1024.000 -- stage_id=1
node40 -- ReLU(inplace=True) -- forward_compute_time=11.527, backward_compute_time=0.500, activation_size=205520896.0, parameter_size=0.000 -- stage_id=1
node41 -- Conv2d(128, 128, kernel_size=(3, 3), stride=(2, 2), padding=(1, 1), bias=False) -- forward_compute_time=11.895, backward_compute_time=13.934, activation_size=51380224.0, parameter_size=589824.000 -- stage_id=1
node42 -- BatchNorm2d(128, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True) -- forward_compute_time=12.207, backward_compute_time=0.153, activation_size=51380224.0, parameter_size=1024.000 -- stage_id=1
node43 -- ReLU(inplace=True) -- forward_compute_time=45.515, backward_compute_time=0.107, activation_size=51380224.0, parameter_size=0.000 -- stage_id=1
node44 -- Conv2d(128, 512, kernel_size=(1, 1), stride=(1, 1), bias=False) -- forward_compute_time=4.622, backward_compute_time=0.248, activation_size=205520896.0, parameter_size=262144.000 -- stage_id=1
node45 -- BatchNorm2d(512, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True) -- forward_compute_time=2.027, backward_compute_time=0.191, activation_size=205520896.0, parameter_size=4096.000 -- stage_id=1
node46 -- Conv2d(256, 512, kernel_size=(1, 1), stride=(2, 2), bias=False) -- forward_compute_time=3.541, backward_compute_time=16.097, activation_size=205520896.0, parameter_size=524288.000 -- stage_id=1
node47 -- BatchNorm2d(512, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True) -- forward_compute_time=1.989, backward_compute_time=0.212, activation_size=205520896.0, parameter_size=4096.000 -- stage_id=1
node48 -- Add(inplace) -- forward_compute_time=0.000, backward_compute_time=0.000, activation_size=205520896.0, parameter_size=0.000 -- stage_id=1
node49 -- ReLU(inplace=True) -- forward_compute_time=2.822, backward_compute_time=0.098, activation_size=205520896.0, parameter_size=0.000 -- stage_id=2
node50 -- Conv2d(512, 128, kernel_size=(1, 1), stride=(1, 1), bias=False) -- forward_compute_time=21.977, backward_compute_time=0.230, activation_size=51380224.0, parameter_size=262144.000 -- stage_id=2
node51 -- BatchNorm2d(128, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True) -- forward_compute_time=23.086, backward_compute_time=0.152, activation_size=51380224.0, parameter_size=1024.000 -- stage_id=2
node52 -- ReLU(inplace=True) -- forward_compute_time=8.294, backward_compute_time=0.100, activation_size=51380224.0, parameter_size=0.000 -- stage_id=2
node53 -- Conv2d(128, 128, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1), bias=False) -- forward_compute_time=41.436, backward_compute_time=0.636, activation_size=51380224.0, parameter_size=589824.000 -- stage_id=2
node54 -- BatchNorm2d(128, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True) -- forward_compute_time=6.001, backward_compute_time=0.155, activation_size=51380224.0, parameter_size=1024.000 -- stage_id=2
node55 -- ReLU(inplace=True) -- forward_compute_time=11.482, backward_compute_time=0.110, activation_size=51380224.0, parameter_size=0.000 -- stage_id=2
node56 -- Conv2d(128, 512, kernel_size=(1, 1), stride=(1, 1), bias=False) -- forward_compute_time=12.916, backward_compute_time=0.252, activation_size=205520896.0, parameter_size=262144.000 -- stage_id=2
node57 -- BatchNorm2d(512, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True) -- forward_compute_time=8.266, backward_compute_time=0.388, activation_size=205520896.0, parameter_size=4096.000 -- stage_id=2
node58 -- Add(inplace) -- forward_compute_time=0.000, backward_compute_time=0.000, activation_size=205520896.0, parameter_size=0.000 -- stage_id=2
node59 -- ReLU(inplace=True) -- forward_compute_time=4.308, backward_compute_time=0.097, activation_size=205520896.0, parameter_size=0.000 -- stage_id=2
node60 -- Conv2d(512, 128, kernel_size=(1, 1), stride=(1, 1), bias=False) -- forward_compute_time=23.634, backward_compute_time=0.229, activation_size=51380224.0, parameter_size=262144.000 -- stage_id=2
node61 -- BatchNorm2d(128, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True) -- forward_compute_time=14.207, backward_compute_time=0.153, activation_size=51380224.0, parameter_size=1024.000 -- stage_id=2
node62 -- ReLU(inplace=True) -- forward_compute_time=7.066, backward_compute_time=0.099, activation_size=51380224.0, parameter_size=0.000 -- stage_id=2
node63 -- Conv2d(128, 128, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1), bias=False) -- forward_compute_time=5.844, backward_compute_time=0.277, activation_size=51380224.0, parameter_size=589824.000 -- stage_id=2
node64 -- BatchNorm2d(128, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True) -- forward_compute_time=4.160, backward_compute_time=0.161, activation_size=51380224.0, parameter_size=1024.000 -- stage_id=2
node65 -- ReLU(inplace=True) -- forward_compute_time=15.458, backward_compute_time=0.104, activation_size=51380224.0, parameter_size=0.000 -- stage_id=2
node66 -- Conv2d(128, 512, kernel_size=(1, 1), stride=(1, 1), bias=False) -- forward_compute_time=12.530, backward_compute_time=0.254, activation_size=205520896.0, parameter_size=262144.000 -- stage_id=2
node67 -- BatchNorm2d(512, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True) -- forward_compute_time=3.487, backward_compute_time=0.219, activation_size=205520896.0, parameter_size=4096.000 -- stage_id=2
node68 -- Add(inplace) -- forward_compute_time=0.000, backward_compute_time=0.000, activation_size=205520896.0, parameter_size=0.000 -- stage_id=2
node69 -- ReLU(inplace=True) -- forward_compute_time=2.836, backward_compute_time=0.104, activation_size=205520896.0, parameter_size=0.000 -- stage_id=2
node70 -- Conv2d(512, 128, kernel_size=(1, 1), stride=(1, 1), bias=False) -- forward_compute_time=29.077, backward_compute_time=9.260, activation_size=51380224.0, parameter_size=262144.000 -- stage_id=2
node71 -- BatchNorm2d(128, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True) -- forward_compute_time=9.817, backward_compute_time=0.159, activation_size=51380224.0, parameter_size=1024.000 -- stage_id=2
node72 -- ReLU(inplace=True) -- forward_compute_time=5.722, backward_compute_time=0.108, activation_size=51380224.0, parameter_size=0.000 -- stage_id=2
node73 -- Conv2d(128, 128, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1), bias=False) -- forward_compute_time=15.648, backward_compute_time=9.420, activation_size=51380224.0, parameter_size=589824.000 -- stage_id=2
node74 -- BatchNorm2d(128, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True) -- forward_compute_time=2.676, backward_compute_time=0.155, activation_size=51380224.0, parameter_size=1024.000 -- stage_id=2
node75 -- ReLU(inplace=True) -- forward_compute_time=11.153, backward_compute_time=0.108, activation_size=51380224.0, parameter_size=0.000 -- stage_id=2
node76 -- Conv2d(128, 512, kernel_size=(1, 1), stride=(1, 1), bias=False) -- forward_compute_time=3.071, backward_compute_time=13.250, activation_size=205520896.0, parameter_size=262144.000 -- stage_id=2
node77 -- BatchNorm2d(512, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True) -- forward_compute_time=3.375, backward_compute_time=0.247, activation_size=205520896.0, parameter_size=4096.000 -- stage_id=2
node78 -- Add(inplace) -- forward_compute_time=0.000, backward_compute_time=0.000, activation_size=205520896.0, parameter_size=0.000 -- stage_id=2
node79 -- ReLU(inplace=True) -- forward_compute_time=2.813, backward_compute_time=0.113, activation_size=205520896.0, parameter_size=0.000 -- stage_id=2
node80 -- Conv2d(512, 256, kernel_size=(1, 1), stride=(1, 1), bias=False) -- forward_compute_time=31.189, backward_compute_time=14.534, activation_size=102760448.0, parameter_size=524288.000 -- stage_id=2
node81 -- BatchNorm2d(256, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True) -- forward_compute_time=9.549, backward_compute_time=0.162, activation_size=102760448.0, parameter_size=2048.000 -- stage_id=2
node82 -- ReLU(inplace=True) -- forward_compute_time=6.195, backward_compute_time=0.125, activation_size=102760448.0, parameter_size=0.000 -- stage_id=2
node83 -- Conv2d(256, 256, kernel_size=(3, 3), stride=(2, 2), padding=(1, 1), bias=False) -- forward_compute_time=29.901, backward_compute_time=10.924, activation_size=25690112.0, parameter_size=2359296.000 -- stage_id=2
node84 -- BatchNorm2d(256, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True) -- forward_compute_time=3.931, backward_compute_time=0.173, activation_size=25690112.0, parameter_size=2048.000 -- stage_id=2
node85 -- ReLU(inplace=True) -- forward_compute_time=14.222, backward_compute_time=0.108, activation_size=25690112.0, parameter_size=0.000 -- stage_id=3
node86 -- Conv2d(256, 1024, kernel_size=(1, 1), stride=(1, 1), bias=False) -- forward_compute_time=6.176, backward_compute_time=0.765, activation_size=102760448.0, parameter_size=1048576.000 -- stage_id=3
node87 -- BatchNorm2d(1024, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True) -- forward_compute_time=1.238, backward_compute_time=0.185, activation_size=102760448.0, parameter_size=8192.000 -- stage_id=3
node88 -- Conv2d(512, 1024, kernel_size=(1, 1), stride=(2, 2), bias=False) -- forward_compute_time=10.291, backward_compute_time=13.719, activation_size=102760448.0, parameter_size=2097152.000 -- stage_id=3
node89 -- BatchNorm2d(1024, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True) -- forward_compute_time=1.192, backward_compute_time=0.245, activation_size=102760448.0, parameter_size=8192.000 -- stage_id=3
node90 -- Add(inplace) -- forward_compute_time=0.000, backward_compute_time=0.000, activation_size=102760448.0, parameter_size=0.000 -- stage_id=3
node91 -- ReLU(inplace=True) -- forward_compute_time=1.396, backward_compute_time=0.096, activation_size=102760448.0, parameter_size=0.000 -- stage_id=3
node92 -- Conv2d(1024, 256, kernel_size=(1, 1), stride=(1, 1), bias=False) -- forward_compute_time=15.623, backward_compute_time=0.224, activation_size=25690112.0, parameter_size=1048576.000 -- stage_id=3
node93 -- BatchNorm2d(256, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True) -- forward_compute_time=14.885, backward_compute_time=0.157, activation_size=25690112.0, parameter_size=2048.000 -- stage_id=3
node94 -- ReLU(inplace=True) -- forward_compute_time=2.420, backward_compute_time=0.102, activation_size=25690112.0, parameter_size=0.000 -- stage_id=3
node95 -- Conv2d(256, 256, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1), bias=False) -- forward_compute_time=23.900, backward_compute_time=0.255, activation_size=25690112.0, parameter_size=2359296.000 -- stage_id=3
node96 -- BatchNorm2d(256, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True) -- forward_compute_time=5.608, backward_compute_time=0.149, activation_size=25690112.0, parameter_size=2048.000 -- stage_id=3
node97 -- ReLU(inplace=True) -- forward_compute_time=8.957, backward_compute_time=0.102, activation_size=25690112.0, parameter_size=0.000 -- stage_id=3
node98 -- Conv2d(256, 1024, kernel_size=(1, 1), stride=(1, 1), bias=False) -- forward_compute_time=10.633, backward_compute_time=0.269, activation_size=102760448.0, parameter_size=1048576.000 -- stage_id=3
node99 -- BatchNorm2d(1024, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True) -- forward_compute_time=3.577, backward_compute_time=0.200, activation_size=102760448.0, parameter_size=8192.000 -- stage_id=3
node100 -- Add(inplace) -- forward_compute_time=0.000, backward_compute_time=0.000, activation_size=102760448.0, parameter_size=0.000 -- stage_id=3
node101 -- ReLU(inplace=True) -- forward_compute_time=3.189, backward_compute_time=0.097, activation_size=102760448.0, parameter_size=0.000 -- stage_id=3
node102 -- Conv2d(1024, 256, kernel_size=(1, 1), stride=(1, 1), bias=False) -- forward_compute_time=13.718, backward_compute_time=0.229, activation_size=25690112.0, parameter_size=1048576.000 -- stage_id=3
node103 -- BatchNorm2d(256, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True) -- forward_compute_time=5.393, backward_compute_time=0.151, activation_size=25690112.0, parameter_size=2048.000 -- stage_id=3
node104 -- ReLU(inplace=True) -- forward_compute_time=2.708, backward_compute_time=0.104, activation_size=25690112.0, parameter_size=0.000 -- stage_id=3
node105 -- Conv2d(256, 256, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1), bias=False) -- forward_compute_time=2.970, backward_compute_time=0.263, activation_size=25690112.0, parameter_size=2359296.000 -- stage_id=3
node106 -- BatchNorm2d(256, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True) -- forward_compute_time=2.860, backward_compute_time=0.157, activation_size=25690112.0, parameter_size=2048.000 -- stage_id=3
node107 -- ReLU(inplace=True) -- forward_compute_time=6.437, backward_compute_time=0.106, activation_size=25690112.0, parameter_size=0.000 -- stage_id=3
node108 -- Conv2d(256, 1024, kernel_size=(1, 1), stride=(1, 1), bias=False) -- forward_compute_time=5.068, backward_compute_time=0.529, activation_size=102760448.0, parameter_size=1048576.000 -- stage_id=3
node109 -- BatchNorm2d(1024, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True) -- forward_compute_time=4.524, backward_compute_time=0.220, activation_size=102760448.0, parameter_size=8192.000 -- stage_id=3
node110 -- Add(inplace) -- forward_compute_time=0.000, backward_compute_time=0.000, activation_size=102760448.0, parameter_size=0.000 -- stage_id=3
node111 -- ReLU(inplace=True) -- forward_compute_time=2.011, backward_compute_time=0.098, activation_size=102760448.0, parameter_size=0.000 -- stage_id=3
node112 -- Conv2d(1024, 256, kernel_size=(1, 1), stride=(1, 1), bias=False) -- forward_compute_time=15.056, backward_compute_time=0.225, activation_size=25690112.0, parameter_size=1048576.000 -- stage_id=3
node113 -- BatchNorm2d(256, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True) -- forward_compute_time=2.656, backward_compute_time=0.154, activation_size=25690112.0, parameter_size=2048.000 -- stage_id=3
node114 -- ReLU(inplace=True) -- forward_compute_time=2.045, backward_compute_time=0.101, activation_size=25690112.0, parameter_size=0.000 -- stage_id=3
node115 -- Conv2d(256, 256, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1), bias=False) -- forward_compute_time=2.943, backward_compute_time=0.266, activation_size=25690112.0, parameter_size=2359296.000 -- stage_id=3
node116 -- BatchNorm2d(256, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True) -- forward_compute_time=2.426, backward_compute_time=0.154, activation_size=25690112.0, parameter_size=2048.000 -- stage_id=3
node117 -- ReLU(inplace=True) -- forward_compute_time=5.985, backward_compute_time=0.103, activation_size=25690112.0, parameter_size=0.000 -- stage_id=3
node118 -- Conv2d(256, 1024, kernel_size=(1, 1), stride=(1, 1), bias=False) -- forward_compute_time=6.974, backward_compute_time=0.727, activation_size=102760448.0, parameter_size=1048576.000 -- stage_id=3
node119 -- BatchNorm2d(1024, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True) -- forward_compute_time=4.525, backward_compute_time=0.210, activation_size=102760448.0, parameter_size=8192.000 -- stage_id=3
node120 -- Add(inplace) -- forward_compute_time=0.000, backward_compute_time=0.000, activation_size=102760448.0, parameter_size=0.000 -- stage_id=3
node121 -- ReLU(inplace=True) -- forward_compute_time=1.406, backward_compute_time=0.104, activation_size=102760448.0, parameter_size=0.000 -- stage_id=3
node122 -- Conv2d(1024, 256, kernel_size=(1, 1), stride=(1, 1), bias=False) -- forward_compute_time=10.776, backward_compute_time=0.220, activation_size=25690112.0, parameter_size=1048576.000 -- stage_id=3
node123 -- BatchNorm2d(256, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True) -- forward_compute_time=2.348, backward_compute_time=0.159, activation_size=25690112.0, parameter_size=2048.000 -- stage_id=3
node124 -- ReLU(inplace=True) -- forward_compute_time=1.322, backward_compute_time=0.104, activation_size=25690112.0, parameter_size=0.000 -- stage_id=3
node125 -- Conv2d(256, 256, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1), bias=False) -- forward_compute_time=2.943, backward_compute_time=0.535, activation_size=25690112.0, parameter_size=2359296.000 -- stage_id=3
node126 -- BatchNorm2d(256, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True) -- forward_compute_time=2.762, backward_compute_time=0.151, activation_size=25690112.0, parameter_size=2048.000 -- stage_id=3
node127 -- ReLU(inplace=True) -- forward_compute_time=2.251, backward_compute_time=0.104, activation_size=25690112.0, parameter_size=0.000 -- stage_id=3
node128 -- Conv2d(256, 1024, kernel_size=(1, 1), stride=(1, 1), bias=False) -- forward_compute_time=6.812, backward_compute_time=0.611, activation_size=102760448.0, parameter_size=1048576.000 -- stage_id=3
node129 -- BatchNorm2d(1024, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True) -- forward_compute_time=7.890, backward_compute_time=0.212, activation_size=102760448.0, parameter_size=8192.000 -- stage_id=3
node130 -- Add(inplace) -- forward_compute_time=0.000, backward_compute_time=0.000, activation_size=102760448.0, parameter_size=0.000 -- stage_id=3
node131 -- ReLU(inplace=True) -- forward_compute_time=1.406, backward_compute_time=0.092, activation_size=102760448.0, parameter_size=0.000 -- stage_id=3
node132 -- Conv2d(1024, 256, kernel_size=(1, 1), stride=(1, 1), bias=False) -- forward_compute_time=14.531, backward_compute_time=9.291, activation_size=25690112.0, parameter_size=1048576.000 -- stage_id=3
node133 -- BatchNorm2d(256, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True) -- forward_compute_time=2.760, backward_compute_time=0.155, activation_size=25690112.0, parameter_size=2048.000 -- stage_id=3
node134 -- ReLU(inplace=True) -- forward_compute_time=1.304, backward_compute_time=0.131, activation_size=25690112.0, parameter_size=0.000 -- stage_id=3
node135 -- Conv2d(256, 256, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1), bias=False) -- forward_compute_time=2.879, backward_compute_time=7.296, activation_size=25690112.0, parameter_size=2359296.000 -- stage_id=3
node136 -- BatchNorm2d(256, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True) -- forward_compute_time=2.886, backward_compute_time=0.146, activation_size=25690112.0, parameter_size=2048.000 -- stage_id=3
node137 -- ReLU(inplace=True) -- forward_compute_time=4.638, backward_compute_time=0.099, activation_size=25690112.0, parameter_size=0.000 -- stage_id=3
node138 -- Conv2d(256, 1024, kernel_size=(1, 1), stride=(1, 1), bias=False) -- forward_compute_time=5.232, backward_compute_time=10.140, activation_size=102760448.0, parameter_size=1048576.000 -- stage_id=3
node139 -- BatchNorm2d(1024, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True) -- forward_compute_time=6.545, backward_compute_time=0.223, activation_size=102760448.0, parameter_size=8192.000 -- stage_id=3
node140 -- Add(inplace) -- forward_compute_time=0.000, backward_compute_time=0.000, activation_size=102760448.0, parameter_size=0.000 -- stage_id=3
node141 -- ReLU(inplace=True) -- forward_compute_time=2.459, backward_compute_time=0.104, activation_size=102760448.0, parameter_size=0.000 -- stage_id=3
node142 -- Conv2d(1024, 512, kernel_size=(1, 1), stride=(1, 1), bias=False) -- forward_compute_time=18.417, backward_compute_time=11.738, activation_size=51380224.0, parameter_size=2097152.000 -- stage_id=3
node143 -- BatchNorm2d(512, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True) -- forward_compute_time=3.516, backward_compute_time=0.177, activation_size=51380224.0, parameter_size=4096.000 -- stage_id=3
node144 -- ReLU(inplace=True) -- forward_compute_time=2.032, backward_compute_time=0.470, activation_size=51380224.0, parameter_size=0.000 -- stage_id=3
node145 -- Conv2d(512, 512, kernel_size=(3, 3), stride=(2, 2), padding=(1, 1), bias=False) -- forward_compute_time=14.582, backward_compute_time=12.564, activation_size=12845056.0, parameter_size=9437184.000 -- stage_id=3
node146 -- BatchNorm2d(512, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True) -- forward_compute_time=0.689, backward_compute_time=0.157, activation_size=12845056.0, parameter_size=4096.000 -- stage_id=3
node147 -- ReLU(inplace=True) -- forward_compute_time=0.915, backward_compute_time=0.107, activation_size=12845056.0, parameter_size=0.000 -- stage_id=3
node148 -- Conv2d(512, 2048, kernel_size=(1, 1), stride=(1, 1), bias=False) -- forward_compute_time=5.219, backward_compute_time=0.241, activation_size=51380224.0, parameter_size=4194304.000 -- stage_id=3
node149 -- BatchNorm2d(2048, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True) -- forward_compute_time=5.060, backward_compute_time=0.189, activation_size=51380224.0, parameter_size=16384.000 -- stage_id=3
node150 -- Conv2d(1024, 2048, kernel_size=(1, 1), stride=(2, 2), bias=False) -- forward_compute_time=21.891, backward_compute_time=9.070, activation_size=51380224.0, parameter_size=8388608.000 -- stage_id=3
node151 -- BatchNorm2d(2048, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True) -- forward_compute_time=1.009, backward_compute_time=0.218, activation_size=51380224.0, parameter_size=16384.000 -- stage_id=3
node152 -- Add(inplace) -- forward_compute_time=0.000, backward_compute_time=0.000, activation_size=51380224.0, parameter_size=0.000 -- stage_id=3
node153 -- ReLU(inplace=True) -- forward_compute_time=0.697, backward_compute_time=0.096, activation_size=51380224.0, parameter_size=0.000 -- stage_id=3
node154 -- Conv2d(2048, 512, kernel_size=(1, 1), stride=(1, 1), bias=False) -- forward_compute_time=22.527, backward_compute_time=0.558, activation_size=12845056.0, parameter_size=4194304.000 -- stage_id=3
node155 -- BatchNorm2d(512, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True) -- forward_compute_time=4.184, backward_compute_time=0.148, activation_size=12845056.0, parameter_size=4096.000 -- stage_id=3
node156 -- ReLU(inplace=True) -- forward_compute_time=3.549, backward_compute_time=0.094, activation_size=12845056.0, parameter_size=0.000 -- stage_id=3
node157 -- Conv2d(512, 512, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1), bias=False) -- forward_compute_time=4.031, backward_compute_time=0.773, activation_size=12845056.0, parameter_size=9437184.000 -- stage_id=3
node158 -- BatchNorm2d(512, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True) -- forward_compute_time=6.094, backward_compute_time=0.147, activation_size=12845056.0, parameter_size=4096.000 -- stage_id=3
node159 -- ReLU(inplace=True) -- forward_compute_time=0.563, backward_compute_time=0.099, activation_size=12845056.0, parameter_size=0.000 -- stage_id=3
node160 -- Conv2d(512, 2048, kernel_size=(1, 1), stride=(1, 1), bias=False) -- forward_compute_time=4.855, backward_compute_time=0.242, activation_size=51380224.0, parameter_size=4194304.000 -- stage_id=3
node161 -- BatchNorm2d(2048, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True) -- forward_compute_time=3.253, backward_compute_time=0.206, activation_size=51380224.0, parameter_size=16384.000 -- stage_id=3
node162 -- Add(inplace) -- forward_compute_time=0.000, backward_compute_time=0.000, activation_size=51380224.0, parameter_size=0.000 -- stage_id=3
node163 -- ReLU(inplace=True) -- forward_compute_time=0.754, backward_compute_time=0.104, activation_size=51380224.0, parameter_size=0.000 -- stage_id=3
node164 -- Conv2d(2048, 512, kernel_size=(1, 1), stride=(1, 1), bias=False) -- forward_compute_time=9.868, backward_compute_time=14.120, activation_size=12845056.0, parameter_size=4194304.000 -- stage_id=3
node165 -- BatchNorm2d(512, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True) -- forward_compute_time=2.814, backward_compute_time=0.156, activation_size=12845056.0, parameter_size=4096.000 -- stage_id=3
node166 -- ReLU(inplace=True) -- forward_compute_time=2.749, backward_compute_time=0.102, activation_size=12845056.0, parameter_size=0.000 -- stage_id=3
node167 -- Conv2d(512, 512, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1), bias=False) -- forward_compute_time=4.304, backward_compute_time=16.011, activation_size=12845056.0, parameter_size=9437184.000 -- stage_id=3
node168 -- BatchNorm2d(512, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True) -- forward_compute_time=0.793, backward_compute_time=0.160, activation_size=12845056.0, parameter_size=4096.000 -- stage_id=3
node169 -- ReLU(inplace=True) -- forward_compute_time=0.746, backward_compute_time=0.105, activation_size=12845056.0, parameter_size=0.000 -- stage_id=3
node170 -- Conv2d(512, 2048, kernel_size=(1, 1), stride=(1, 1), bias=False) -- forward_compute_time=4.851, backward_compute_time=16.889, activation_size=51380224.0, parameter_size=4194304.000 -- stage_id=3
node171 -- BatchNorm2d(2048, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True) -- forward_compute_time=1.999, backward_compute_time=0.441, activation_size=51380224.0, parameter_size=16384.000 -- stage_id=3
node172 -- Add(inplace) -- forward_compute_time=0.000, backward_compute_time=0.000, activation_size=51380224.0, parameter_size=0.000 -- stage_id=3
node173 -- ReLU(inplace=True) -- forward_compute_time=1.963, backward_compute_time=0.111, activation_size=51380224.0, parameter_size=0.000 -- stage_id=3
node174 -- AdaptiveAvgPool2d(output_size=(1, 1)) -- forward_compute_time=5.006, backward_compute_time=0.079, activation_size=1048576.0, parameter_size=0.000 -- stage_id=3
node175 -- Size(0) -- forward_compute_time=0.000, backward_compute_time=0.000, activation_size=4.0, parameter_size=0.000 -- stage_id=3
node176 -- View(-1) -- forward_compute_time=0.000, backward_compute_time=0.000, activation_size=1048576.0, parameter_size=0.000 -- stage_id=3
node177 -- Linear(in_features=2048, out_features=1000, bias=True) -- forward_compute_time=2.605, backward_compute_time=0.927, activation_size=512000.0, parameter_size=8196000.000 -- stage_id=3
node1 -- Input0 -- forward_compute_time=0.000, backward_compute_time=0.000, activation_size=0.0, parameter_size=0.000 -- stage_id=0
	node1 -- node2
	node2 -- node3
	node3 -- node4
	node4 -- node5
	node5 -- node6
	node6 -- node7
	node7 -- node8
	node8 -- node9
	node9 -- node10
	node10 -- node11
	node11 -- node12
	node12 -- node13
	node5 -- node14
	node14 -- node15
	node13 -- node16
	node15 -- node16
	node16 -- node17
	node17 -- node18
	node18 -- node19
	node19 -- node20
	node20 -- node21
	node21 -- node22
	node22 -- node23
	node23 -- node24
	node24 -- node25
	node25 -- node26
	node17 -- node26
	node26 -- node27
	node27 -- node28
	node28 -- node29
	node29 -- node30
	node30 -- node31
	node31 -- node32
	node32 -- node33
	node33 -- node34
	node34 -- node35
	node35 -- node36
	node27 -- node36
	node36 -- node37
	node37 -- node38
	node38 -- node39
	node39 -- node40
	node40 -- node41
	node41 -- node42
	node42 -- node43
	node43 -- node44
	node44 -- node45
	node37 -- node46
	node46 -- node47
	node45 -- node48
	node47 -- node48
	node48 -- node49
	node49 -- node50
	node50 -- node51
	node51 -- node52
	node52 -- node53
	node53 -- node54
	node54 -- node55
	node55 -- node56
	node56 -- node57
	node57 -- node58
	node49 -- node58
	node58 -- node59
	node59 -- node60
	node60 -- node61
	node61 -- node62
	node62 -- node63
	node63 -- node64
	node64 -- node65
	node65 -- node66
	node66 -- node67
	node67 -- node68
	node59 -- node68
	node68 -- node69
	node69 -- node70
	node70 -- node71
	node71 -- node72
	node72 -- node73
	node73 -- node74
	node74 -- node75
	node75 -- node76
	node76 -- node77
	node77 -- node78
	node69 -- node78
	node78 -- node79
	node79 -- node80
	node80 -- node81
	node81 -- node82
	node82 -- node83
	node83 -- node84
	node84 -- node85
	node85 -- node86
	node86 -- node87
	node79 -- node88
	node88 -- node89
	node87 -- node90
	node89 -- node90
	node90 -- node91
	node91 -- node92
	node92 -- node93
	node93 -- node94
	node94 -- node95
	node95 -- node96
	node96 -- node97
	node97 -- node98
	node98 -- node99
	node99 -- node100
	node91 -- node100
	node100 -- node101
	node101 -- node102
	node102 -- node103
	node103 -- node104
	node104 -- node105
	node105 -- node106
	node106 -- node107
	node107 -- node108
	node108 -- node109
	node109 -- node110
	node101 -- node110
	node110 -- node111
	node111 -- node112
	node112 -- node113
	node113 -- node114
	node114 -- node115
	node115 -- node116
	node116 -- node117
	node117 -- node118
	node118 -- node119
	node119 -- node120
	node111 -- node120
	node120 -- node121
	node121 -- node122
	node122 -- node123
	node123 -- node124
	node124 -- node125
	node125 -- node126
	node126 -- node127
	node127 -- node128
	node128 -- node129
	node129 -- node130
	node121 -- node130
	node130 -- node131
	node131 -- node132
	node132 -- node133
	node133 -- node134
	node134 -- node135
	node135 -- node136
	node136 -- node137
	node137 -- node138
	node138 -- node139
	node139 -- node140
	node131 -- node140
	node140 -- node141
	node141 -- node142
	node142 -- node143
	node143 -- node144
	node144 -- node145
	node145 -- node146
	node146 -- node147
	node147 -- node148
	node148 -- node149
	node141 -- node150
	node150 -- node151
	node149 -- node152
	node151 -- node152
	node152 -- node153
	node153 -- node154
	node154 -- node155
	node155 -- node156
	node156 -- node157
	node157 -- node158
	node158 -- node159
	node159 -- node160
	node160 -- node161
	node161 -- node162
	node153 -- node162
	node162 -- node163
	node163 -- node164
	node164 -- node165
	node165 -- node166
	node166 -- node167
	node167 -- node168
	node168 -- node169
	node169 -- node170
	node170 -- node171
	node171 -- node172
	node163 -- node172
	node172 -- node173
	node173 -- node174
	node174 -- node175
	node174 -- node176
	node175 -- node176
	node176 -- node177