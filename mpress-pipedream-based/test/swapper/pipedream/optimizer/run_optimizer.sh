python optimizer_graph_hierarchical.py -f ../profiler/image_classification/profiles/resnet50/graph.txt  -n 4   \
    --straight_pipeline \
    --activation_compression_ratio 1 \
    -o "aws_repartition_resnet50/"$1
python convert_graph_to_model.py -f "aws_repartition_resnet50/"$1/gpus=4.txt -n $1 -a $1 \
    -o "../runtime/image_classification/models/aws_repartition_resnet50/"$1"/gpus=4" --stage_to_num_ranks 0:1,1:1,2:1,3:1


# argparser of `convert_graph_to_model.py`
# parser.add_argument('-f', "--profile_filename", required=True,
#                     help="Input profile filename")
# parser.add_argument("--model_template_filename", default="templ
#                     help="Model template filename")
# parser.add_argument("--init_template_filename", default="templa
#                     help="__init__.py template filename")
# parser.add_argument("--conf_template_filename", default="templa
#                     help="Conf template filename")
# parser.add_argument("--stage_to_num_ranks_map", type=str, defau
#                     help="Stage split")
# parser.add_argument('-n', "--model_name", required=True,
#                     help="Name of model class")
# parser.add_argument('-a', "--arch", required=True,
#                     help="Human-readable architecture name")
# parser.add_argument('-o', "--output_directory", required=True,
#                     help="Full path of output model directory")


# python optimizer_graph_hierarchical.py -f ../profiler/image_classification/profiles/resnet50/graph.txt  -n 4   \
#     --straight_pipeline \
#     --activation_compression_ratio 1 \
#     -o resnet50_BS64_StreamSync

# python convert_graph_to_model.py -f resnet50_BS64_StreamSync/gpus=4.txt -n resnet50_BS64_StreamSync -a resnet50_BS64_StreamSync \
#     -o ../runtime/image_classification/models/resnet50_BS64_StreamSync/gpus=4 --stage_to_num_ranks 0:1,1:1,2:1,3:1