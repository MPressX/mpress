export Z_LEVEL=fatal
# bash launch_local.sh 0ToHost "-1" 1 96 9663676416 resnet50
# bash launch_local.sh 0ToHost "-1" 1 128 9663676416 resnet50
# bash launch_local.sh 0ToHost "-1" 1 160 9663676416 resnet50
# bash launch_local.sh 0ToHost "-1" 1 196 9663676416 resnet50
# bash launch_local.sh 0ToHost "-1" 1 224 9663676416 resnet50
# bash launch_local.sh 0ToHost "-1" 1 256 9663676416 resnet50
# bash launch_local.sh 0ToHost "-1" 1 288 9663676416 resnet50
# bash launch_local.sh 0ToHost "-1" 1 320 9663676416 resnet50
# bash launch_local.sh 0ToHost "-1" 1 352 9663676416 resnet50

# bash launch_local.sh noswap "-1" 0 32 0 resnet50
# bash launch_local.sh noswap "-1" 0 64 0 resnet50
# bash launch_local.sh noswap "-1" 0 96 0 resnet50

# bash launch_local.sh noswap "-1" 0 128 0 resnet50
# bash launch_local.sh noswap "-1" 0 160 0 resnet50

# bash launch_local.sh 0ToHost "-1" 1 32 9663676416 resnet50
# bash launch_local.sh 0ToHost "-1" 1 64 9663676416 resnet50

# CUDA_VISIBLE_DEVICES=3,1,2,0 bash run_profiler.sh To120 "1 2 3" 1 32 9 resnet50
# CUDA_VISIBLE_DEVICES=3,1,2,0 bash run_profiler.sh To120 "1 2 3" 1 64 9 resnet50
# CUDA_VISIBLE_DEVICES=3,1,2,0 bash run_profiler.sh To120 "1 2 3" 1 96 9 resnet50
# CUDA_VISIBLE_DEVICES=3,1,2,0 bash run_profiler.sh To120 "1 2 3" 1 128 9 resnet50
# CUDA_VISIBLE_DEVICES=3,1,2,0 bash run_profiler.sh To120 "1 2 3" 1 160 9 resnet50



# # no swap
# bash run_profiler.sh noswap "-1" 0 32 0 resnet50
# bash run_profiler.sh noswap "-1" 0 64 0 resnet50
# bash run_profiler.sh noswap "-1" 0 96 0 resnet50

# swap to host
bash run_profiler.sh ToHost "-1" 1 32  9 resnet50
bash run_profiler.sh ToHost "-1" 1 64  9 resnet50
bash run_profiler.sh ToHost "-1" 1 96  9 resnet50
bash run_profiler.sh ToHost "-1" 1 128 9 resnet50

/data/sdc/pytorch.gnmt/test/swapper/pipedream/runtime/image_classification/models/modify_inplace.sh /data/sdc/pytorch.gnmt/test/swapper/pipedream/runtime/image_classification/models/


# bash run_profiler.sh 0ToHost "-1" 1 64  9663676416 resnet50
# bash run_profiler.sh 0ToHost "-1" 1 96  9663676416 resnet50
# bash run_profiler.sh 0ToHost "-1" 1 128 9663676416 resnet50
# bash run_profiler.sh 0ToHost "-1" 1 160 15032385536 resnet152
# bash run_profiler.sh 0ToHost "-1" 1 192 15032385536 resnet152
# bash run_profiler.sh 0ToHost "-1" 1 224 15032385536 resnet152
# bash run_profiler.sh 0ToHost "-1" 1 256 15032385536 resnet152
# bash run_profiler.sh 0ToHost "-1" 1 288 15032385536 resnet152
# bash run_profiler.sh 0ToHost "-1" 1 320 15032385536 resnet152
# bash run_profiler.sh 0ToHost "-1" 1 352 15032385536 resnet152
# bash run_profiler.sh 0ToHost "-1" 1 384 15032385536 resnet152
# bash run_profiler.sh 0ToHost "-1" 1 416 15032385536 resnet152
# bash run_profiler.sh 0ToHost "-1" 1 448 15032385536 resnet152
# bash run_profiler.sh 0ToHost "-1" 1 480 15032385536 resnet152

# swap to other GPUs

# CUDA_VISIBLE_DEVICES=3,1,2,0 bash launch_local.sh 3To120 "1 2 3" 1 32 15032385536 resnet152
# CUDA_VISIBLE_DEVICES=3,1,2,0 bash launch_local.sh 3To120 "1 2 3" 1 64 15032385536 resnet152
# CUDA_VISIBLE_DEVICES=3,1,2,0 bash launch_local.sh 3To120 "1 2 3" 1 96 15032385536 resnet152
# CUDA_VISIBLE_DEVICES=3,1,2,0 bash launch_local.sh 3To120 "1 2 3" 1 128 15032385536 resnet152
# CUDA_VISIBLE_DEVICES=3,1,2,0 bash launch_local.sh 3To120 "1 2 3" 1 160 15032385536 resnet152
# CUDA_VISIBLE_DEVICES=3,1,2,0 bash launch_local.sh 3To120 "1 2 3" 1 192 15032385536 resnet152
# CUDA_VISIBLE_DEVICES=3,1,2,0 bash launch_local.sh 3To120 "1 2 3" 1 224 15032385536 resnet152
# CUDA_VISIBLE_DEVICES=3,1,2,0 bash launch_local.sh 3To120 "1 2 3" 1 256 15032385536 resnet152
# CUDA_VISIBLE_DEVICES=3,1,2,0 bash launch_local.sh 3To120 "1 2 3" 1 288 15032385536 resnet152
# CUDA_VISIBLE_DEVICES=3,1,2,0 bash launch_local.sh 3To120 "1 2 3" 1 320 15032385536 resnet152
# CUDA_VISIBLE_DEVICES=3,1,2,0 bash launch_local.sh 3To120 "1 2 3" 1 352 15032385536 resnet152
# CUDA_VISIBLE_DEVICES=3,1,2,0 bash launch_local.sh 3To120 "1 2 3" 1 384 15032385536 resnet152
# CUDA_VISIBLE_DEVICES=3,1,2,0 bash launch_local.sh 3To120 "1 2 3" 1 416 15032385536 resnet152
# CUDA_VISIBLE_DEVICES=3,1,2,0 bash launch_local.sh 3To120 "1 2 3" 1 448 15032385536 resnet152
# CUDA_VISIBLE_DEVICES=3,1,2,0 bash launch_local.sh 3To120 "1 2 3" 1 480 15032385536 resnet152

