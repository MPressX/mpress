
# python main.py -a resnet50 -b 64 --mode 0 --candidates -1 --num_cards 4 --mem 0 --config_path "models2/resnet/output/network_profiling.json" --data_dir /data/DNN_Dataset/imagenet/full
# exit 1



export title=$1
if [ "$2" = "" ]; then
    candidates="-1"
else
    candidates=$2
fi
if [ "$3" = "" ]; then
    mode=0
else
    mode=$3
fi
if [ "$4" = "" ]; then
    batch_size=32
else
    batch_size=$4
fi
if [ "$5" = "" ]; then
    mem=0
    memB=0
else
    mem=$5
    memB=$[ $mem * 1024 * 1024 * 1024 ]
fi
if [ "$6" = "" ]; then
    model=resnet50
else
    model=$6
fi



num_cards=4

config_path="models2/resnet/output/network_profiling.json"
data_dir="/data/DNN_Dataset/imagenet/full"



args="-a ${model} -b ${batch_size} --mode ${mode} \
        --candidates ${candidates} --num_cards ${num_cards} \
        --mem ${memB} \
        --config_path ${config_path} \
        --data_dir ${data_dir}"


echo args=$args

if [ $mem -lt 0 ]
then
       mem=INF
fi     

export title=${title}_bs${batch_size}_mem${mem}
echo $title

rm -rf profiles/resnet50
python main.py ${args} 

cd ../../optimizer
bash run_optimizer.sh $title
cd ../profiler/image_classification
