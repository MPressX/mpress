#include <vector>
#include <iostream>
#include <functional>
#include <string>
#include <tuple>
using namespace std;

#define GPU_MEM_CAP 31
#define NUM_CARDS 8
typedef vector<vector<int>> matrix;

vector<int> mem_used = {43, 40, 37, 34, 28, 25, 22, 19};
vector<int> dev_map={0,1,2,3,4,5,6,7};

matrix empty_matrix(int rows=NUM_CARDS, int cols=NUM_CARDS) {
    matrix m(rows, vector<int>(cols, 0));
    return m;
}
string str_line(vector<int>& line){
    string s="";
    for(int i=0; i<line.size(); i++){
        s+=to_string(line[i]);
        if (i!=line.size()-1) s+="\t";
    }
    return s;
}

string str_matrix(matrix& m){
    string s = "";
    for (int i = 0; i < m.size(); i++) {
        s+=str_line(m[i]);
        if (i!=m.size()-1) s+="\n";
    }
    return s;
}

matrix get_nvlink_matrix(){
    vector<int> link_2 = {0,3,2,1,5,6,7,4,0};
    vector<int> link_1 = {0,1,3,7,5,4,6,2,0};
    auto m = empty_matrix();
    for (int i = 0; i < 8; i++){
        m[link_2[i]][link_2[i+1]] = 2;
        m[link_2[i+1]][link_2[i]] = 2;
        m[link_1[i]][link_1[i+1]] = 1;
        m[link_1[i+1]][link_1[i]] = 1;
    }
    return m;
}
matrix nvlink_matrix = get_nvlink_matrix();

int get_num_links(int src, int dst){
    return nvlink_matrix[dev_map[src]][dev_map[dst]];
}
int get_speed(int src, int dst){
    int num_links = get_num_links(src, dst);
    if (num_links == 2) return 50;
    if (num_links == 1) return 25;
    return 12;
}
int get_mem_used(int gpu_id){
    return mem_used[gpu_id];
}

vector<vector<int>> distribute_mem_to_candidates(int mem, int num_candidates){
    // assert(num_candidates > 0);
    vector<vector<int>> ans;
    if (mem == 0) {
        ans.push_back(vector<int>(num_candidates, 0));
        return ans;
    }
    if (num_candidates==1){
        ans.push_back(vector<int>(1, mem));
        return ans;
    }
    for (int last_alloc_mem = 0; last_alloc_mem<=mem; last_alloc_mem++){
        auto previous_ans = distribute_mem_to_candidates(mem-last_alloc_mem, num_candidates-1);
        for (auto&v:previous_ans){
            v.push_back(last_alloc_mem);
            ans.push_back(v);
        }
    }
    return ans;
}

vector<vector<tuple<int,int,int>>> distribute_spare_mem(int gpu_id){
    // ans[solution_id][candidate_id] = [src, dst, mem]
    vector<vector<tuple<int,int,int>>> ans;
    int spare_amount = GPU_MEM_CAP - get_mem_used(gpu_id);
    vector<int> nbh_with_overmem;
    for (int i = 0; i < NUM_CARDS; i++){
        if (get_num_links(gpu_id, i)>0 && get_mem_used(i)>GPU_MEM_CAP){
            nbh_with_overmem.push_back(i);
        }
    }
    if (nbh_with_overmem.size()==0){
        return ans;
    }
    auto ret = distribute_mem_to_candidates(spare_amount, nbh_with_overmem.size());
    for (auto&v:ret){
        vector<tuple<int,int,int>> tmp;
        for (int i = 0; i < v.size(); i++){
            if (v[i]>0) tmp.push_back(make_tuple(gpu_id, nbh_with_overmem[i], v[i]));
        }
        ans.push_back(tmp);
    }
    return ans;
}

vector<vector<int>> permutations(vector<int> devs){
    vector<vector<int>> v;
    if (devs.size()==1){
        v.push_back(devs);
        return v;
    }
    for (int i = 0; i < devs.size(); i++){
        vector<int> v_i = devs;
        v_i.erase(v_i.begin()+i);
        vector<vector<int>> v_i_perm = permutations(v_i);
        for (int j = 0; j < v_i_perm.size(); j++){
            v_i_perm[j].insert(v_i_perm[j].begin(), devs[i]);
            v.push_back(v_i_perm[j]);
        }
    }
}

vector<vector<int>> product(vector<vector<int>> a){
    vector<vector<int>> ans;
    if (a.size()==1){
        for (auto v: a[0]){
            ans.push_back({v});
        }
        return ans;
    }
    auto sub_ans = product(vector<vector<int>>(a.begin(), a.end()-1));
    auto&last_vector = a[a.size()-1];
    for (auto last: last_vector){
        for (auto v: sub_ans){
            v.push_back(last);
            ans.push_back(v);
        }
    }
    return ans;
}

tuple<double, vector<tuple<int,int,int>>> get_score(vector<vector<vector<tuple<int,int,int>>>>& dists, vector<int>& indices){
    double score = 0;
    vector<tuple<int,int,int>> actual_scheme;

    vector<tuple<int,int,int>> nv2;
    vector<tuple<int,int,int>> nv1;
    for (int i = 0; i < indices.size(); i++){
        auto index = indices[i];
        auto&t1 = dists[i][index];
        for (auto&v:t1){
            // auto [ src, dst, mem ] = v;
            int src = get<0>(v); int dst = get<1>(v); int mem = get<2>(v);

            if (get_num_links(src, dst)==2){
                nv2.push_back(v);
            }
            else{
                nv1.push_back(v);
            }
        }
    }
    vector<int> mem_overred(NUM_CARDS, 0);
    for (int i = 0; i < NUM_CARDS; i++) mem_overred[i] = get_mem_used(i) - GPU_MEM_CAP;
    for (auto&tmp:nv2){
        int src = get<0>(tmp); int dst = get<1>(tmp); int mem = get<2>(tmp);
        auto actual_mem = min(mem, mem_overred[dst]);
        if (actual_mem>0){
            double swap_cost = (double)actual_mem/get_speed(src,dst);
            actual_scheme.push_back(make_tuple(src, dst, actual_mem));
            mem_overred[dst] -= actual_mem;
            score = score + actual_mem - swap_cost;
        }
    }
    for (auto&tmp:nv1){
        int src = get<0>(tmp); int dst = get<1>(tmp); int mem = get<2>(tmp);
        auto actual_mem = min(mem, mem_overred[dst]);
        if (actual_mem>0){
            double swap_cost = (double)actual_mem/get_speed(src,dst);
            actual_scheme.push_back(make_tuple(src, dst, actual_mem));
            mem_overred[dst] -= actual_mem;
            score = score + actual_mem - swap_cost;
        }
    }
    return make_tuple(score, actual_scheme);
}

tuple<double, vector<tuple<int,int,int>>> get_score(vector<tuple<int,int,int>> schemes){
    vector<vector<vector<tuple<int,int,int>>>> dists = {{schemes}};
    // dists.push_back({});
    vector<int> indices={0};
    return get_score(dists, indices);
}

double max_score = 0;
vector<tuple<int,int,int>> best_scheme;
vector<int> best_dev_map;

void find_best_device_mapping(){
    // Search time can be greatly optimized by using pruning and multithreading. 
    vector<int> spare_id_list;
    for (int i = 0; i < NUM_CARDS; i++){
        if (get_mem_used(i)<GPU_MEM_CAP){
            spare_id_list.push_back(i);
        }
    }
    cout<<"spare_id_list: "<<str_line(spare_id_list)<<endl;
    auto perm7s = permutations(vector<int>{1,2,3,4,5,6});
    for (auto& perm: perm7s){
        perm.insert(perm.begin(), 0);
        perm.push_back(7);
        // cout<<str_line(perm)<<endl;
        dev_map = perm;
        cout<<"searching for dev_mapping: " << str_line(dev_map)<<endl;
        // dists[0]: distributions of first spare gpu's mem
        vector<vector<vector<tuple<int,int,int>>>> dists; 
        for (auto& spare_id:spare_id_list){
            auto dist = distribute_spare_mem(spare_id);
            if (dist.size()>0){
                dists.push_back(dist);
            }
        }
        if (dists.size()==0) continue;
        // cout<<"dists[i].size: "; for (auto&v:dists) cout<<v.size()<<" "; cout<<endl;
        vector<vector<int>> dist_members_range;
        for (auto&v:dists){
            auto tmp = vector<int>(v.size(), 0);
            for (int i = 0; i < v.size(); i++) tmp[i] =i;
            dist_members_range.push_back(tmp);
        }
        auto dist_enumerate_indices = product(dist_members_range);
        // cout<<"dist_enumerate_indices.size: " << dist_enumerate_indices.size()<<endl;
        for (auto&indices:dist_enumerate_indices){
            auto tmp = get_score(dists, indices);
            double score = get<0>(tmp);
            vector<tuple<int,int,int>> actual_scheme = get<1>(tmp);
            if (score > max_score){
                max_score = score;
                best_scheme = actual_scheme;
                best_dev_map = dev_map;

            }
        }
        cout<<"max_score: "<<max_score<<endl;
        cout<<"best_dev_map: "<<str_line(dev_map)<<endl;
        cout<<"best_scheme: ";
        for (auto&tmp:best_scheme){
            int src = get<0>(tmp); int dst = get<1>(tmp); int mem = get<2>(tmp);
            cout<<"("<<src<<","<<dst<<","<<mem<<") ";
        }
        cout<<endl;
    }
}

void test(){
    cout<<"nvlink_matrix:"<<endl;
    cout<<str_matrix(nvlink_matrix)<<endl;
    dev_map = {0,1,6,7,2,3,4,5}; 
    auto ret = distribute_spare_mem(7);
    for (auto&list:ret){
        for (auto&v:list){
            cout<<"("<<get<0>(v)<<","<<get<1>(v)<<","<<get<2>(v)<<") ";
        }
        cout<<endl;
    }
}

int main(){
    find_best_device_mapping();
    // test();
    return 0;
}