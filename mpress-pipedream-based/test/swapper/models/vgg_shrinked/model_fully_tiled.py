from typing import List
import colorful as cf
import torch
import torch.nn as nn

class VGG16(nn.Module):
    def __init__(self, num_classes=1000, init_weights=False, enable_swap=True):
        super(VGG16, self).__init__()
        self.enable_swap = enable_swap
        self.swap = self.swap_true if self.enable_swap else self.swap_false
        self.conv64_1 = nn.Conv2d(3, 64, kernel_size = 3, padding = 1).cuda()
        self.relu64_1 = nn.ReLU(True).cuda()
        self.conv64_2 = nn.Conv2d(64, 64, kernel_size=3, padding=1).cuda()
        self.relu64_2 = nn.ReLU(True).cuda()
        self.maxpool64 = nn.MaxPool2d(kernel_size=2, stride=2).cuda()
        self.conv128_1 = nn.Conv2d(64, 128, kernel_size = 3, padding = 1).cuda()
        self.relu128_1 = nn.ReLU(True).cuda()
        self.conv128_2 = nn.Conv2d(128, 128, kernel_size=3, padding=1).cuda()
        self.relu128_2 = nn.ReLU(True).cuda()
        self.maxpool128 = nn.MaxPool2d(kernel_size=2, stride=2).cuda()
        self.conv256_1 = nn.Conv2d(128, 256, kernel_size = 3, padding = 1).cuda()
        self.relu256_1 = nn.ReLU(True).cuda()
        self.conv256_2 = nn.Conv2d(256, 256, kernel_size=3, padding=1).cuda()
        self.relu256_2 = nn.ReLU(True).cuda()
        self.conv256_3 = nn.Conv2d(256, 256, kernel_size=3, padding=1).cuda()
        self.relu256_3 = nn.ReLU(True).cuda()
        self.maxpool256 = nn.MaxPool2d(kernel_size=2, stride=2).cuda()
        self.conv512_1 = nn.Conv2d(256, 512, kernel_size = 3, padding = 1).cuda()
        self.relu512_1 = nn.ReLU(True).cuda()
        self.conv512_2 = nn.Conv2d(512, 512, kernel_size=3, padding=1).cuda()
        self.relu512_2 = nn.ReLU(True).cuda()
        self.conv512_3 = nn.Conv2d(512, 512, kernel_size=3, padding=1).cuda()
        self.relu512_3 = nn.ReLU(True).cuda()
        self.maxpool512_1 = nn.MaxPool2d(kernel_size=2, stride=2).cuda()
        self.conv512_4 = nn.Conv2d(512, 512, kernel_size=3, padding=1).cuda()
        self.relu512_4 = nn.ReLU(True).cuda()
        self.conv512_5 = nn.Conv2d(512, 512, kernel_size=3, padding=1).cuda()
        self.relu512_5 = nn.ReLU(True).cuda()
        self.conv512_6 = nn.Conv2d(512, 512, kernel_size=3, padding=1).cuda()
        self.relu512_6 = nn.ReLU(True).cuda()
        self.maxpool512_2 = nn.MaxPool2d(kernel_size=2, stride=2).cuda()
        self.dropout_1 = nn.Dropout(p=0.5).cuda()
        self.linear_1 = nn.Linear(512*7*7, 4096).cuda()
        self.relu_1 = nn.ReLU(True).cuda()
        self.dropout_2 = nn.Dropout(p=0.5).cuda()
        self.linear_2 = nn.Linear(4096, 4096).cuda()
        self.relu_2 = nn.ReLU(True).cuda()
        self.linear_3 = nn.Linear(4096, num_classes).cuda()

        self.forward = self.forward_failed
    
    def swap_true(self, output):
        torch.swapper.swap_out_by_layers(targets=[output.grad_fn], excludes=[output])
        self.conv128_1.register_swap_in(targets=[output.grad_fn])
    def swap_false(self, output):
        return
    
    def forward_failed(self, x):
        x = self.conv64_1(x)
        self.swap(x)
        x = self.maxpool64(x)
        self.swap(x)
        x = self.conv128_1(x)
        return x
    
    def forward_stuck(self, x):
        x = self.conv64_1(x)
        self.swap(x)
        x = self.maxpool64(x)
        torch.swapper.swap_out_by_layers(targets=[x.grad_fn], excludes=[x])
        self.maxpool64.register_swap_in(targets=[x.grad_fn], is_register_self=True)
        # self.swap(x)
        return x


