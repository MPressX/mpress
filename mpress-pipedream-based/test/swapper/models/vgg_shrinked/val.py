
from torchvision import transforms
from torchvision import datasets
from torch.utils.data import DataLoader
from model_fully_tiled import VGG16
from torch import nn
from torch import optim
import torch

import colorful as cf

val_data_path = '/data/DNN_Dataset/imagenet/full/val'
val_dataset = datasets.ImageFolder(
    root = val_data_path,
    transform = transforms.Compose([
        transforms.Resize((224,224)),
        transforms.ToTensor(),
        transforms.Normalize( (0.5,0.5,0.5), (0.5,0.5,0.5) )
    ])
)
val_loader = DataLoader(val_dataset, batch_size=16, shuffle=True)


model = VGG16().cuda()

with torch.no_grad():
    acc = 0.0
    for step, val_data in enumerate(val_loader, start=0):
        input, label = val_data
        input = input.cuda()
        label = label.cuda()
        out = model(input)
        pred = torch.max(out, dim=1)[1]
        acc += torch.eq(pred, label).sum().item()
        print(f"step={step+1}, acc={acc}")
    val_acc = acc/len(val_dataset)
    print(f"val_acc={val_acc}")
        
