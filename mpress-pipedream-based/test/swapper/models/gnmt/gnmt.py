# Copyright (c) Microsoft Corporation.
# Licensed under the MIT license.

import torch
from seq2seq.models.encoder import EmuBidirLSTM
from seq2seq.models.decoder import RecurrentAttention
from seq2seq.models.decoder import Classifier
from torch import swapper


def serialize(t):
    ans = []
    if type(t) is torch.Tensor:
        return [t]
    elif type(t) is list or type(t) is tuple:
        for v in t:
            ans.extend(serialize(v))
    return ans

class GNMTGenerated(torch.nn.Module):
    def __init__(self):
        super(GNMTGenerated, self).__init__()
        self.layer11 = torch.nn.Embedding(32320, 1024, padding_idx=0) # Embedding
        self.layer12 = torch.nn.Embedding(32320, 1024, padding_idx=0)
        self.layer13 = EmuBidirLSTM(1024, 1024)
        self.layer14 = torch.nn.Dropout(p=0.2)
        self.layer15 = torch.nn.LSTM(2048, 1024)
        self.layer17 = torch.nn.Dropout(p=0.2)
        self.layer18 = torch.nn.LSTM(1024, 1024)
        self.layer21 = torch.nn.Dropout(p=0.2)
        self.layer22 = torch.nn.LSTM(1024, 1024)
        self.layer25 = RecurrentAttention(1024, 1024, 1024)
        self.layer28 = torch.nn.Dropout(p=0.2)
        self.layer30 = torch.nn.LSTM(2048, 1024)
        self.layer32 = torch.nn.Dropout(p=0.2)
        self.layer34 = torch.nn.LSTM(2048, 1024)
        self.layer37 = torch.nn.Dropout(p=0.2)
        self.layer39 = torch.nn.LSTM(2048, 1024)
        self.layer42 = Classifier(1024, 32320)

    def forward(self, input0, input1, input2):
        # print(f"input0.shape={input0.shape}, input0.dtype={input0.dtype}")
        # print(f"input1.shape={input1.shape}, input1.dtype={input1.dtype}")
        # print(f"input2.shape={input2.shape}, input2.dtype={input2.dtype}")
        out0 = input0.clone()
        out1 = input1.clone()
        out2 = input2.clone()
        out3 = [None, None, None, None]  # out3 is hidden, might need to be initialized differently.
        out4 = out3[3]
        out5 = out3[2]
        out6 = out3[1]
        out7 = out3[0]
        out12 = self.layer12(out0) # Embedding
        tmp = swapper.swap_out_by_range(start_tensors=[out0], end_tensors=[out12])
        self.layer42.register_swap_in(targets=tmp)
        out13 = self.layer13(out12, out1)
        out14 = self.layer14(out13) # Dropout
        swapper.swap_out_by_layers(targets=[out14.grad_fn], excludes=[out14])
        self.layer42.register_swap_in(targets=[out14.grad_fn])
        out15 = self.layer15(out14) # LSTM
        tmp = swapper.swap_out_by_range(start_tensors=[out14], end_tensors=serialize(out15))
        self.layer42.register_swap_in(targets=tmp)
        out16 = out15[0]
        out17 = self.layer17(out16) # Dropout
        swapper.swap_out_by_layers(targets=[out17.grad_fn], excludes=[out17])
        self.layer42.register_swap_in(targets=[out17.grad_fn])
        out18 = self.layer18(out17) # LSTM
        tmp = swapper.swap_out_by_range(start_tensors=[out17], end_tensors=serialize(out18))
        self.layer42.register_swap_in(targets=tmp)
        out19 = out18[0]
        out20 = out19 + out16
        out21 = self.layer21(out20) # Dropout
        swapper.swap_out_by_layers(targets=[out21.grad_fn], excludes=[out21])
        self.layer42.register_swap_in(targets=[out21.grad_fn])
        out22 = self.layer22(out21) # LSTM
        tmp = swapper.swap_out_by_range(start_tensors=[out21], end_tensors=serialize(out22))
        self.layer42.register_swap_in(targets=tmp)
        out23 = out22[0]
        out24 = out23 + out20
        swapper.swap_out_by_targets(targets=[out23,out20])
        self.layer42.register_swap_in(targets=[swapper.tensor_to_meta(out23), swapper.tensor_to_meta(out20)])
        out11 = self.layer11(out2) # Embedding
        tmp = swapper.swap_out_by_range(start_tensors=[out2], end_tensors=[out11])
        self.layer42.register_swap_in(targets=tmp)
        out25 = self.layer25(out11, out7, out24, out1) # RecurrentAttention
        tmp = swapper.swap_out_by_range(start_tensors=[out11, out24, out1], end_tensors=serialize(out25)) # bugs rasies
        self.layer42.register_swap_in(targets=tmp)
        out27 = out25[0]
        out28 = self.layer28(out27) # Dropout
        swapper.swap_out_by_layers(targets=[out28.grad_fn], excludes=[out28])
        self.layer42.register_swap_in(targets=[out28.grad_fn])
        out26 = out25[2]
        out29 = torch.cat([out28, out26], 2)  # 这底层是怎么操作的，out29是分离的吗？
        out30 = self.layer30(out29, out6) # LSTM
        tmp = swapper.swap_out_by_range(start_tensors=[out29], end_tensors=serialize(out30))
        self.layer42.register_swap_in(targets=tmp)
        out31 = out30[0]
        out32 = self.layer32(out31) # Dropout
        swapper.swap_out_by_layers(targets=[out32.grad_fn], excludes=[out32])
        self.layer42.register_swap_in(targets=[out32.grad_fn])
        out33 = torch.cat([out32, out26], 2)
        out34 = self.layer34(out33, out5) # LSTM
        tmp = swapper.swap_out_by_range(start_tensors=[out33], end_tensors=serialize(out34))
        self.layer42.register_swap_in(targets=tmp)
        out35 = out34[0]
        out36 = out35 + out31
        swapper.swap_out_by_targets(targets=[out35,out31])
        self.layer42.register_swap_in(targets=[swapper.tensor_to_meta(out35), swapper.tensor_to_meta(out31)])
        out37 = self.layer37(out36) # Dropout
        swapper.swap_out_by_layers(targets=[out37.grad_fn], excludes=[out37])
        self.layer42.register_swap_in(targets=[out37.grad_fn])
        out38 = torch.cat([out37, out26], 2)
        # print(f"out38.shape={out38.shape}, out38.type={out38.dtype}")
        # print(f"out4.shape={out4.shape}, out4.type={out4.dtype}")
        out39 = self.layer39(out38, out4) # LSTM
        tmp = swapper.swap_out_by_range(start_tensors=[out38], end_tensors=serialize(out39))
        self.layer42.register_swap_in(targets=tmp)
        out40 = out39[0]
        out41 = out40 + out36 # AddBackward0并没有存储任务Variable，所以参与计算的中间变量很可能就不见了
        print(f"out41.shape={out41.shape}, out41.dtype={out41.dtype}")
        out42 = self.layer42(out41)
        return out42

    def _initialize_weights(self):
        for m in self.modules():
            if isinstance(m, torch.nn.Conv2d):
                torch.nn.init.kaiming_normal_(m.weight, mode='fan_out', nonlinearity='relu')
                if m.bias is not None:
                    torch.nn.init.constant_(m.bias, 0)
            elif isinstance(m, torch.nn.BatchNorm2d):
                torch.nn.init.constant_(m.weight, 1)
                torch.nn.init.constant_(m.bias, 0)
            elif isinstance(m, torch.nn.Linear):
                torch.nn.init.normal_(m.weight, 0, 0.01)
                torch.nn.init.constant_(m.bias, 0)
