import torch
import gnmt
from seq2seq.utils import build_gnmt_criterion
from seq2seq.data.tokenizer import Tokenizer
import os
from torch import swapper

swapper.init_swapper()


def get_mem_in_MB():
    return torch.cuda.memory_allocated()/1024/1024, torch.cuda.max_memory_allocated()/1024/1024, torch.cuda.memory_cached()/1024/1024


data_dir="/data/DNN_Dataset/wmt/"
VOCAB_FNAME="vocab.bpe.32000"
PAD=0
magic_number = 50
batch_size = 1

training_tensor_shapes={'input0': [magic_number, batch_size], 'input1': [batch_size], 'input2': [magic_number, batch_size], 'target': [magic_number], 'target_length': [1]}
dtypes={'input0': torch.int64, 'input1': torch.int64, 'input2': torch.int64, 'target': torch.int64, 'target_length': torch.int32}

def generate_data():
    input0 = torch.randint(low=0, high=2**8, size=training_tensor_shapes['input0'], dtype=dtypes['input0']).cuda()
    input1 = torch.randint(low=0, high=2**8, size=training_tensor_shapes['input1'], dtype=dtypes['input1']).cuda()
    input2 = torch.randint(low=0, high=2**8, size=training_tensor_shapes['input2'], dtype=dtypes['input2']).cuda()
    target = torch.randint(low=0, high=2**8, size=training_tensor_shapes['target'], dtype=dtypes['target']).cuda()
    # target_length = torch.randint(low=0, high=2**8, size=training_tensor_shapes['target_length'], dtype=dtypes['target_length']).cuda()
    input1[0] = magic_number
    # target_length[0] = magic_number
    return input0, input1, input2, target
model = gnmt.GNMTGenerated().cuda()
tokenizer = Tokenizer(os.path.join("/data/DNN_Dataset/wmt/", VOCAB_FNAME))
criterion = build_gnmt_criterion(
    vocab_size=tokenizer.vocab_size, padding_idx=PAD, smoothing=0.1)


for i in range(100):
    input0, input1, input2, target = generate_data()
    out = model(input0, input1, input2)
    loss = criterion(out, target)
    torch.cuda.synchronize()
    mem_allocated, mem_max_allocated, mem_cached = get_mem_in_MB()
    print(f"step {i}, memory usage: {mem_allocated:0.4f}[{mem_max_allocated:0.4f}]({mem_cached:0.4f})")
    loss.backward()
    # print(out.shape, out.dtype)
    # print(loss.shape, loss.dtype)
    # print(loss)
    # print(model.layer11.weight.grad)