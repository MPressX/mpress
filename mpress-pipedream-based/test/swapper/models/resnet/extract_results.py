import os
import json
TARGET_CONFIG='config/bert-base-uncased.json'
SAVED_TRAINING_CONFIG='config/training-config.json'

def extract_config(log_path:str):
    _, log_name = os.path.split(log_path)
    conf = dict()
    title, mode, batchsize, mem, model = tuple(log_name.split('_'))
    conf['title'] = title
    conf['model'] = int(mode[4:])
    conf['batchsize'] = int(batchsize[9:])
    conf['mem'] = int(mem[3:])
    conf['model'] = model[5:-8]
    return conf


def extract_iteration_time(log_path:str):
    warmup_iterations = 3
    max_diff_ratio = 0.1
    training_log = log_path
    if not os.path.exists(training_log):
        return 0
    time_records = []
    with open(training_log, 'r') as f:
        for line in f.readlines():
            p = line.find("end-start=")
            if p < 0: continue
            q = line.find("forward=")
            if q < 0: continue
            time_per_iteration = float(line[p+len("end-start="):q-1])
            time_records.append(time_per_iteration)
    if len(time_records) <= warmup_iterations:
        return 0
    time_records = time_records[warmup_iterations:]
    ans = sum(time_records) / len(time_records)
    # just check it
    for v in time_records:
        diff_ratio = (v-ans)/ans
        if not -max_diff_ratio < diff_ratio < max_diff_ratio:
            print(f"Warning: in [{training_log}], average time={ans:0.2f}s, one training time={v:0.2f}s, diff_ratio is {diff_ratio:0.2f}")
    return ans
        
    

    

def extract_dir(dir:str):
    for v in os.listdir(dir):
        p = os.path.join(dir,v)
        if p.endswith("key.log"):
            conf = extract_config(p)
            conf['time_per_iteration'] = extract_iteration_time(p)
            conf['log_path'] = p
            print(f"{conf['batchsize']} {conf['title']}  {conf['time_per_iteration']}")
            # print(json.dumps(conf, indent=4))
            

if __name__ == '__main__':
    extract_dir("./output")