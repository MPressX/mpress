import torch
from torch import nn

class Test(nn.Module):
    def __init__(self):
        super(Test,self).__init__()
        layer=nn.ModuleList()
        layer.append(nn.Conv2d(3,64,kernel_size=7, stride=2, padding=3, bias=False))
        layer.append(nn.ReLU(inplace=True))
        layer.append(nn.MaxPool2d(kernel_size=3, stride=2, padding=1))
        self.layer = layer

    def forward(self, x):
        x1 = self.layer[0](x)
        x2 = self.layer[1](x1)
        x3 = self.layer[2](x2)
        return x3

class Test2(nn.Module):
    def __init__(self):
        super(Test2,self).__init__()
        self.conv=nn.Conv2d(3,64,kernel_size=7, stride=2, padding=3, bias=False)
        self.relu=nn.ReLU(inplace=True)
        self.maxpool=nn.MaxPool2d(kernel_size=3, stride=2, padding=1)

    def forward(self, x):
        x1 = self.conv(x)
        x2 = self.relu(x1)
        x3 = self.maxpool(x2)
        return x3

class Test3(nn.Module):
    def __init__(self):
        super(Test3,self).__init__()
        self.map=nn.ModuleDict()
        self.map["conv"]=nn.Conv2d(3,64,kernel_size=7, stride=2, padding=3, bias=False)
        self.map["relu"]=nn.ReLU(inplace=True)
        self.map["maxpool"]=nn.MaxPool2d(kernel_size=3, stride=2, padding=1)

    def forward(self, x):
        x1 = self.map["conv"](x)
        x2 = self.map["relu"](x1)
        x3 = self.map["maxpool"](x2)
        return x3

if __name__ == '__main__':
    t = Test3()
    input = torch.rand(32,3,224,224)
    input.requires_grad=True
    y = t(input)
    print(f"input={input}")
    print(f"y={y}")
    z = torch.sum(y)
    z.backward()
    for key in t.state_dict():
        print(key)
    exit()

        