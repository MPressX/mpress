export title=$1
if [ "$2" = "" ]; then
    candidates="-1"
else
    candidates=$2
fi
if [ "$3" = "" ]; then
    mode=0
else
    mode=$3
fi
if [ "$4" = "" ]; then
    batch_size=32
else
    batch_size=$4
fi
if [ "$5" = "" ]; then
    mem=0
else
    mem=$5
fi
if [ "$6" = "" ]; then
    model=resnet50
else
    model=$6
fi
epoch=1
steps=100000
default_device_id=0


num_cards=4
sleep_time=0
config_path="output/network_profiling.json"

args="--mode=${mode} --batch_size=${batch_size} --epoch=${epoch}\
       --steps=${steps} --mem=${mem} --default_device_id=${default_device_id}\
       --candidates ${candidates} --num_cards=${num_cards}\
       --sleep_time=${sleep_time} --config_path=${config_path} --model=${model}"

echo args=$args

if [ $mem -lt 0 ]
then
       mem=INF
fi     
export title=${title}_mode${mode}_batchsize${batch_size}_mem${mem}_model${model}

rm -rf train.log
# nsys profile -f true -o $title -y 40 -d 20 python train.py ${args} > ${title}.log 2>&1
# python train.py ${args} > ${title}.log 2>&1 &
python train.py ${args} &
sleep 30s
pkill python

mkdir -p output
mv ${title}.* output/
mv train.log output/${title}-key.log
