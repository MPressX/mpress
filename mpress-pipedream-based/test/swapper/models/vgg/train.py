
from torchvision import transforms
from torchvision import datasets
from torch.utils.data import DataLoader
from model_fully_tiled import VGG16
from torch import dtype, nn
from torch import optim
import torch
import json
import argparse
from argparse import  RawTextHelpFormatter
from torch import swapper
from torch.utils.link_speed import get_speed_matrix

from torch.swapper.optim import SGD
from torch.swapper.optim import Adam

parse = argparse.ArgumentParser(description="vgg swapper training", formatter_class=RawTextHelpFormatter)

parse.add_argument("--mode", type=int, default=0, help="[mode 0] no swap\n"
                                                       "[mode 1] swap to candidates\n"
                                                       "[mode 2] swap by advisor\n"
                                                       "default=0")
parse.add_argument("--batch_size", type=int, default=96,help="set batch size; default=96")
parse.add_argument("--use_cudnn", type=int, default=1, help="cudnn/thnn; default=1(use cudnn)")
parse.add_argument("--epoch", type=int, default=1, help="set the epoch times during the whole training; default=1")
parse.add_argument("--steps", type=int, default=10, help="set step num in each epoch; default=10")
parse.add_argument("--mem",type=int,default=0*1024*1024*1024,help="set memory limitation; only valid if set mode=1; default=0")
parse.add_argument("--default_device_id", type=int, default=0, help="set default gpu to run the program; default=0")
parse.add_argument("--candidates", nargs='+', type=int, default=[-1], help="set the candidates to send\n"
                                                                            "-1: CPU\n"
                                                                            "candidate>=0: CUDA[candidate]\n"
                                                                            "default=[-1]")
parse.add_argument("--num_cards", type=int, default=4, help="the number of available GPUs; default=4")
parse.add_argument("--sleep_time", type=float, default=0, help="sleep time(s) after forward to see whether memory drops; default=0")
parse.add_argument("--config_path", type=str, default="", help="the path to the network profiling path, produced by get_link_speed.py; default=''")
parse.add_argument("--enable_optimizer_layerwise", type=int, default=0, help="use layerwise optimizer")
args = parse.parse_args()

torch.cuda.set_device(args.default_device_id)

if args.use_cudnn==0:
    torch._C._set_cudnn_enabled(False)
print(f"use_cudnn={args.use_cudnn}")
    

def get_mem_in_MB():
    return torch.cuda.memory_allocated()/1024/1024, torch.cuda.max_memory_allocated()/1024/1024, torch.cuda.memory_cached()/1024/1024

def log_to_file(s):
    with open("train.log", 'a') as f:
        f.write(s+'\n')

speed_matrix = get_speed_matrix(args.config_path)
log_to_file(f"speed_to_matrix={speed_matrix}")
swapper.init_swapper(
    default_device_id=args.default_device_id,
    cuda_memory_threshold=args.mem,
    num_cards = args.num_cards,
    swap_mode = args.mode,
    candidates = args.candidates,
    speed_matrix = speed_matrix
    )
import time
global_start = time.time()

if args.mode==0:
    model = VGG16(enable_swap=False).cuda()
else:
    model = VGG16(enable_swap=True).cuda()

loss_fn = nn.CrossEntropyLoss().cuda()

if args.enable_optimizer_layerwise:
    sgd = SGD(lr_=0.1)
    sgd_optimizer = sgd.get_optimize()
    swapper.register_optimize_func(sgd_optimizer)
    # adam = Adam()
    # adam_optimzier = adam.get_optimize()
    # swapper.register_optimize_func(adam_optimzier)
else:
    opt = optim.Adam(model.parameters(), lr=0.0001)

        

for epoch in range(args.epoch):
    model.train() # open Dropout
    print(f"[Epoch {epoch+1}] begins")
    import time
    
    
    input = torch.randn(args.batch_size,3,224,224,dtype=torch.float32).cuda(args.default_device_id)
    label = torch.randint(0,10,(args.batch_size,),dtype=torch.long).cuda(args.default_device_id)
    for step in range(args.steps):
        print(f"step={step}")
        # time.sleep(0.5)
        torch.cuda.synchronize()
        torch.cuda.nvtx.range_push("forward")
        swapper.forward_begin()
        start = time.time()
        occupied, allocated, cached = get_mem_in_MB(); log_to_file(f"step={step}: before forward, memory cost(MB) = {allocated}({cached})[{occupied}]")
        output = model(input)
        loss = loss_fn(output, label)
        print(f"loss={loss}")
        torch.cuda.synchronize()
        torch.cuda.nvtx.range_pop()
        end_of_forward = time.time()
        occupied, allocated, cached = get_mem_in_MB(); log_to_file(f"step={step}: after forward, memory cost(MB) = {allocated}({cached})[{occupied}]")
        time.sleep(args.sleep_time)
        occupied, allocated, cached = get_mem_in_MB(); log_to_file(f"step={step}: after sleep, memory cost(MB) = {allocated}({cached})[{occupied}]")
        # opt.zero_grad()
        torch.cuda.nvtx.range_push("backward")
        swapper.backward_begin()
        loss.backward()
        occupied, allocated, cached = get_mem_in_MB(); log_to_file(f"step={step}: after backward, memory cost(MB) = {allocated}({cached})[{occupied}]")
        torch.cuda.synchronize()
        torch.cuda.nvtx.range_pop()
        end = time.time()
        if not args.enable_optimizer_layerwise:
            opt.step()
        log_to_file(f"[step {step+1}]\tstart={start-global_start}\tend={end-global_start}\tend-start={end-start}\tforward={end_of_forward-start}")
        if step==1:
            torch.cuda.reset_max_memory_allocated()
    