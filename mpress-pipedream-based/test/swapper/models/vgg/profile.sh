export title=$1

mode=1
batch_size=96
mem=`expr 0 \* 1024 \* 1024 \* 1024`

arguments="--mode=${mode} --batch_size=${batch_size} --mem=${mem}"
rm -rf train.log
nsys profile -o ${title}_${mode}_${batch_size}_${mem} -y 60 -d 15 python train.py ${arguments}
mv train.log ${title}_${mode}_${batch_size}_${mem}.log
