export title=$1
export Z_LEVEL=warn
mode=1
batch_size=64
epoch=1
steps=1000
mem=`expr 10 \* 1024 \* 1024 \* 1024`
default_device_id=0
candidates="1"
num_cards=4
sleep_time=0
config_path="output/network_profiling.json"
enable_optimizer_layerwise=1

args="--mode=${mode} --batch_size=${batch_size} --epoch=${epoch}\
       --steps=${steps} --mem=${mem} --default_device_id=${default_device_id}\
       --candidates ${candidates} --num_cards=${num_cards}\
       --sleep_time=${sleep_time} --config_path=${config_path}\
       --enable_optimizer_layerwise=${enable_optimizer_layerwise}"

echo args=$args

export title=${title}_mode${mode}_batchsize${batch_size}_mem${mem}

rm -rf train.log
# nsys profile -f true -o $title -y 40 -d 5 python train.py ${args}
python train.py ${args}


mkdir -p output
mv ${title}.* output/
mv train.log output/${title}.log
cp launch_profiling_on_local.sh output/${title}.laucnh.sh
