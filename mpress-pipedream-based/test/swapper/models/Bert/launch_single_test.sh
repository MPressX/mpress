

export title=$1
export batch_size=$2
export mem=$3
export mode=$4
export candidates=$5
export num_cards=$6
export sleep_seconds=$7

echo title=${title}
echo batch_size=${batch_size}
echo mem=${mem}
echo mode=${mode}
echo candidates=${candidates}
echo num_cards=${num_cards}
echo sleep_seconds=${sleep_seconds}


python run_squad.py \
  --bert_model bert-base-uncased \
  --do_train \
  --do_predict \
  --do_lower_case \
  --train_file $SQUAD_DIR/train-v1.1.json \
  --predict_file $SQUAD_DIR/dev-v1.1.json \
  --train_batch_size ${batch_size} \
  --learning_rate 3e-5 \
  --num_train_epochs 2.0 \
  --max_seq_length 384 \
  --doc_stride 128 \
  --output_dir /tmp/debug_squad/ \
  --mem ${mem} \
  --mode ${mode} \
  --candidates ${candidates} \
  --enable_optimizer_layerwise 1 \
  --num_cards ${num_cards} > ${title}.log 2>&1 &

pid=$!
echo pid=$pid
for ((i=0;i<$sleep_seconds;i++));
do
    pid_exist=$(ps aux | awk '{print $2}' | grep -w $pid)
    if [ ! $pid_exist ]; then
        echo $pid not exist
        break
    fi
    sleep 1
    # echo i=$i, pid_exist=${pid_exist}
done
echo i=$i
kill -9 $pid


dir=output/$title
rm -rf $dir
mkdir -p $dir
cp -r ./*.py $dir/
cp -r ./*.sh $dir/
cp -r ./config $dir/
mkdir -p $dir/pytorch_pretrained_bert
cp -r ./pytorch_pretrained_bert/*.py  $dir/pytorch_pretrained_bert/
mv ./${title}.log $dir/
mv ./train.log $dir/ # 记录显存占用以及训练速度