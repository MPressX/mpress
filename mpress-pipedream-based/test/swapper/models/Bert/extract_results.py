import os
import json
TARGET_CONFIG='config/bert-base-uncased.json'
SAVED_TRAINING_CONFIG='config/training-config.json'

def extract_config(dir_path:str):
    conf = dict()
    bert_config_path = os.path.join(dir_path, TARGET_CONFIG)
    saved_training_config = os.path.join(dir_path, SAVED_TRAINING_CONFIG)
    with open(bert_config_path, 'r') as f:
        j1 = json.loads(f.read())
    with open(saved_training_config, 'r') as f:
        j2 = json.loads(f.read())
    conf = {**j1, **j2}
    return conf

def extract_iteration_time(dir_path:str):
    warmup_iterations = 3
    max_diff_ratio = 0.1
    _, dir_name = os.path.split(dir_path)
    training_log = os.path.join(dir_path, "train.log")
    if not os.path.exists(training_log):
        return 0
    time_records = []
    with open(training_log, 'r') as f:
        for line in f.readlines():
            p = line.find("end-start=")
            if p < 0: continue
            q = line.find("forward=")
            if q < 0: continue
            time_per_iteration = float(line[p+len("end-start="):q-1])
            time_records.append(time_per_iteration)
    if len(time_records) <= warmup_iterations:
        return 0
    time_records = time_records[warmup_iterations:]
    ans = sum(time_records) / len(time_records)
    # just check it
    for v in time_records:
        diff_ratio = (v-ans)/ans
        if not -max_diff_ratio < diff_ratio < max_diff_ratio:
            print(f"Warning: in [{training_log}], average time={ans:0.2f}s, one training time={v:0.2f}s, diff_ratio is {diff_ratio:0.2f}")
    return ans
        
    

    

def extract_dir(dir:str):
    for v in os.listdir(dir):
        p = os.path.join(dir,v)
        if os.path.isdir(p):
            conf = extract_config(p)
            conf['time_per_iteration'] = extract_iteration_time(p)
            print(f"{conf['num_hidden_layers']} {conf['swap_info']} {conf['time_per_iteration']}")
            # print(json.dumps(conf, indent=4))
            

if __name__ == '__main__':
    extract_dir("./output")