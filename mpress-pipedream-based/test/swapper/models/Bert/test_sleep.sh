sleep_sec=5
pid=31772
for ((i=0;i<$sleep_sec;i++));
do
    pid_exist=$(ps aux | awk '{print $2}' | grep -w $pid)
    if [ ! $pid_exist ]; then
        echo $pid not exist
        break
    fi
    echo i=$i, pid_exist=${pid_exist}
done