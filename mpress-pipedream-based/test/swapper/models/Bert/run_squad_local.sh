export CUDA_VISIBLE_DEVICES=0,1,2,3
export Z_LEVEL=debug
export Z_COLOR=1
export SQUAD_DIR=/data/DNN_Dataset/bert/squad

# for debug, args=
# run_squad.py --bert_model bert-base-uncased --do_train --do_predict --do_lower_case --train_file /data/DNN_Dataset/bert/squad/train-v1.1.json --predict_file /data/DNN_Dataset/bert/squad/dev-v1.1.json --train_batch_size 12 --learning_rate 3e-5 --num_train_epochs 2.0 --max_seq_length 384 --doc_stride 128 --output_dir /tmp/debug_squad/

args="--bert_model bert-base-uncased \
  --do_train \
  --do_predict \
  --do_lower_case \
  --train_file $SQUAD_DIR/train-v1.1.json \
  --predict_file $SQUAD_DIR/dev-v1.1.json \
  --train_batch_size 12 \
  --learning_rate 3e-5 \
  --num_train_epochs 2.0 \
  --max_seq_length 384 \
  --doc_stride 128 \
  --output_dir /tmp/debug_squad/ \
  --mem 9663676416 \
  --mode 1 \
  --candidates -1 \
  --num_cards 4 \
  --default_device_id 0"

nsys profile -f true -o bert -y 60 -d 15 python run_squad.py ${args}

