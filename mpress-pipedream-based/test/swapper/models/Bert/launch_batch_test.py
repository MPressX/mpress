import json
from typing import List
BASE_CONFIG='config/standard/bert-base-uncased.json'
TARGET_CONFIG='config/bert-base-uncased.json'
SAVED_TRAINING_CONFIG='config/training-config.json'

import os

os.environ['CUDA_VISIBLE_DEVICES']="2,1,0"
os.environ['Z_LEVEL']="warn"
os.environ['Z_COLOR']="1"
os.environ['SQUAD_DIR']="/data/DNN_Dataset/bert/squad"


def get_swap_info(candidates, mode):
    swap_info = ""
    if mode == 0:
        swap_info = "0ToNull"
    elif mode==1:
        candidates = ["H" if str(v)=='-1' else str(v) for v in candidates]
        candidates = "".join(candidates)
        swap_info = f"0To{candidates}"
    elif mode==2:
        candidates = ["H" if str(v)=='-1' else str(v) for v in candidates]
        candidates = "".join(candidates)
        swap_info = f"auto0To{candidates}"
    else:
        raise RuntimeError(f"Current not support other mode({mode})")
    return swap_info
    

def launch_single_test(title:str, batch_size:int, num_hidden_layers:int, mem:int, 
                       mode:int, candidates:List[int], num_cards:int, sleep_seconds:int):
    with open(BASE_CONFIG, 'r') as f:
        j = json.loads(f.read())
        j['num_hidden_layers'] = num_hidden_layers
        with open(TARGET_CONFIG, 'w') as f:
            f.write(json.dumps(j, indent=4))
    swap_info = get_swap_info(candidates, mode)
    title = f"{title}_{swap_info}_BS{batch_size}_numHiddenLayers{num_hidden_layers}_mem{mem}_mode{mode}_numCards{num_cards}"
    config = {
        "title":title,
        "batch_size":batch_size,
        "num_hidden_layers":num_hidden_layers,
        "mem":mem,
        "mode":mode,
        "candidates":candidates,
        "swap_info":swap_info,
        "num_cards":num_cards,
        "sleep_seconds":sleep_seconds
    }
    with open(SAVED_TRAINING_CONFIG, 'w') as f:
        f.write(json.dumps(config, indent=4))

    candidates = [str(v) for v in candidates]
    candidates = " ".join(candidates)
    print(f"title={title}")
    cmd = f'bash ./launch_single_test.sh {title} {batch_size} {mem} {mode} "{candidates}" {num_cards} {sleep_seconds} python'
    print(f"executing cmd={cmd}")
    import subprocess
    output = subprocess.call(cmd, shell=True)
    print(f"output={output}")
    # f = os.popen(cmd)
    # print(f.read())
    print(f"finishing cmd={cmd}")


if __name__ == '__main__':
    # launch_single_test("TestOriginalAdam", 12, 12, 0, 0, [-1], 4, 3600)
    # launch_single_test("TestLayerWiseAdam", 12, 12, 0, 0, [-1], 4, 3600)
    # launch_single_test("NoSwap", 12, 18, 0, 0, [-1], 4, 120)
    # launch_single_test("GPUSwap", 12, 12, 9*1024*1024*1024, 1, [1,2,3], 4, 60)
    # launch_single_test("GPUSwap", 12, 18, 9*1024*1024*1024, 1, [1,2,3], 4, 120)
    # launch_single_test("GPUSwap", 12, 24, 9*1024*1024*1024, 1, [1,2,3], 4, 120)
    # launch_single_test("GPUSwap", 12, 30, 9*1024*1024*1024, 1, [1,2,3], 4, 240)
    # launch_single_test("GPUSwap", 12, 36, 9*1024*1024*1024, 1, [1,2,3], 4, 240)
    # launch_single_test("GPUSwap", 12, 42, 9*1024*1024*1024, 1, [1,2,3], 4, 480)
    # launch_single_test("GPUSwap", 12, 48, 9*1024*1024*1024, 1, [1,2,3], 4, 360)

    launch_single_test("HostSwap", 12, 12, 9*1024*1024*1024, 1, [-1], 3, 180)
    # launch_single_test("HostSwap", 12, 18, 9*1024*1024*1024, 1, [-1], 4, 120)
    # launch_single_test("HostSwap", 12, 24, 9*1024*1024*1024, 1, [-1], 4, 120)
    # launch_single_test("HostSwap", 12, 30, 9*1024*1024*1024, 1, [-1], 4, 240)
    # launch_single_test("HostSwap", 12, 36, 9*1024*1024*1024, 1, [-1], 4, 240)
    # launch_single_test("HostSwap", 12, 42, 9*1024*1024*1024, 1, [-1], 4, 480)
    # launch_single_test("HostSwap", 12, 48, 9*1024*1024*1024, 1, [-1], 4, 480)
    # launch_single_test("HostSwap", 12, 54, 9*1024*1024*1024, 1, [-1], 4, 360)
    # launch_single_test("HostSwap", 12, 60, 9*1024*1024*1024, 1, [-1], 4, 360)
    # launch_single_test("HostSwap", 12, 66, 9*1024*1024*1024, 1, [-1], 4, 360)
    # launch_single_test("AutoSwap", 12, 44, 9*1024*1024*1024, 2, [-1,1,2], 4, 360)

    