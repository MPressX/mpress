current_dir=$(pwd)
mkdir -p output
cd ../../../../
cd tools/link_speed/
echo $(pwd)
cd bandwidthTest
make
cd ../p2pBandwidthLatencyTest
make
cd ..
python get_link_speed.py
cp output/* ${current_dir}/output/