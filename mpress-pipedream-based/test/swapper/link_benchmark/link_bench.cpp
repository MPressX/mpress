#include <iostream>
#include <cuda.h>
#include <cuda_runtime_api.h>
#include <thread>
#include <unistd.h>
#include <vector>
#include <chrono>
#include <fstream>
#include <string>

using namespace std;
const string output_filename = "./link_speed.csv";


float bench(int src, vector<int> dsts, unsigned long long src_bytes, vector<unsigned long long> dsts_bytes){
    // print args:
    cout<<"directions: " << src<<"->[";
    for (int i = 0; i < dsts.size()-1; i++) cout << dsts[i] << ",";
    cout<<dsts[dsts.size()-1]<<"]"<<endl;
    cout<<"nbytes: "<<src_bytes<<"->[";
    for (int i = 0; i < dsts_bytes.size()-1; i++) cout << dsts_bytes[i] << ",";
    cout<<dsts_bytes[dsts_bytes.size()-1]<<"]"<<endl;
    // get offset
    vector<unsigned long long> offset;
    offset.push_back(0);
    for (int i = 1; i < dsts_bytes.size(); i++){
        offset.push_back(offset[i-1] + dsts_bytes[i-1]);
    }

    vector<int> devices = dsts;
    for (auto&v:devices){
        if (v==-1) v = src;
    }
    int N = dsts.size();
    // create streams, events
    vector<cudaStream_t> streams(N);
    vector<cudaEvent_t> starts(N);
    vector<cudaEvent_t> stops(N);
    for (int i = 0; i < N; i++){
        cudaSetDevice(devices[i]);
        cudaStreamCreateWithFlags(&streams[i], cudaStreamNonBlocking);
    }

    // alloc source memory
    void* pSrc;
    cudaSetDevice(src);
    cudaMalloc(&pSrc, src_bytes);

    // alloc dst memory
    vector<void*> pDsts(dsts.size());
    for (int i = 0; i < dsts.size(); i++){
        cudaSetDevice(devices[i]);
        if (dsts[i] < 0){
            cudaMallocHost(&pDsts[i], dsts_bytes[i]);
        }
        else{
            cudaMalloc(&pDsts[i], dsts_bytes[i]);
        }
    }
    float elapsedTime, speed_GBperS;
    // copy src to dst
    for (int cnt = 0; cnt < 10; cnt++){
        auto start = std::chrono::high_resolution_clock::now();
        for (int i = 0; i < N; i++){
            cudaSetDevice(devices[i]);
            if (dsts[i] < 0){
                cudaMemcpyAsync(pDsts[i], (uint8_t*)pSrc+offset[i], dsts_bytes[i], cudaMemcpyDeviceToHost, streams[i]);
            }
            else{
                cudaMemcpyPeerAsync(pDsts[i], dsts[i], (uint8_t*)pSrc+offset[i], src, dsts_bytes[i], streams[i]);
            }
        }
        for (int i = 0; i < N; i++){
            cudaSetDevice(devices[i]);
            cudaStreamSynchronize(streams[i]);
        }
        auto stop = std::chrono::high_resolution_clock::now();
        elapsedTime = std::chrono::duration<float, std::milli>(stop - start).count();
        printf("elapsed time: %f ms\n", elapsedTime);
        speed_GBperS = (double)src_bytes/1024/1024/1024/elapsedTime*1000;
        printf("speed: %f GB/s\n", speed_GBperS);
    }

    ofstream fout(output_filename, ios::app);
    fout<<src<<",";
    for (auto&v:dsts) fout<<v<<" ";
    fout<<","<<src_bytes/1024/1024<<","<<speed_GBperS<<endl;
    fout.close();

    // free memory
    cudaSetDevice(src);
    cudaFree(pSrc);
    for (int i = 0; i < dsts.size(); i++){
        cudaSetDevice(devices[i]);
        if (dsts[i] < 0){
            cudaFreeHost(pDsts[i]);
        }
        else{
            cudaFree(pDsts[i]);
        }
    }
    // destroy streams
    for (int i = 0; i < N; i++){
        cudaSetDevice(devices[i]);
        cudaStreamDestroy(streams[i]);
    }
}


int main(int argc, char** argv){
    int src = atoi(argv[1]);
    int N = atoi(argv[2]);
    vector<int> dsts(N);
    for (int i = 0; i < N; i++){
        dsts[i] = atoi(argv[3+i]);
    }
    vector<float> ratios(N);
    for (int i = 0; i < N; i++){
        ratios[i] = atof(argv[3+N+i]);
    }
    float ratio_sum = 0;
    for (auto&v:ratios) ratio_sum += v;
    for (auto&v:ratios) v /= ratio_sum;
    for (unsigned long long  mbytes = 1; mbytes <= 128; mbytes*=2){
        unsigned long long src_bytes = mbytes * 1024 * 1024;
        vector<unsigned long long> dsts_bytes(N);
        dsts_bytes[N-1] = src_bytes;
        for (int i = 0; i < N-1; i++){
            dsts_bytes[i] = src_bytes * ratios[i];
            dsts_bytes[N-1] -= dsts_bytes[i];
        }
        bench(src, dsts, src_bytes, dsts_bytes);
    }
}