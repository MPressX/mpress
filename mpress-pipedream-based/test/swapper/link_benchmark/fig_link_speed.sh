make
FILE="./link_speed.csv"
# clean and init FILE
rm -rf ${FILE}
echo "src, dsts, MiB, speed(GB/s)" > ${FILE}

# PCIe
./link_bench.o 0 1 -1 1
# NV2
./link_bench.o 0 1 3 2
# NV4
./link_bench.o 0 2 3 4 2 2 
# NV6
./link_bench.o 0 4 3 4 1 2 2 2 1 1