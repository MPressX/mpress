import torch
from torch import swapper
from argparse import ArgumentParser
from torch.utils.link_speed import get_speed_matrix


import time
MB = 1024*1024

parser = ArgumentParser(description='Bandwidth testing')
parser.add_argument("--candidates", nargs='+', type=int, default=[-1], help="set the candidates to send\n"
                                                                            "-1: CPU\n"
                                                                            "default=[-1]\n"
                                                                            "example: --candidates 1 2 3")
args = parser.parse_args()

speed_matrix = get_speed_matrix("output/network_profiling.json")
swapper.init_swapper(
                        default_device_id=0,
                        cuda_memory_threshold=0,
                        num_cards=4,
                        swap_mode=1,
                        candidates=args.candidates,
                        speed_matrix=speed_matrix
                        )
print("="*20)

data_list = [torch.zeros(int(1 * MB / 4),device=torch.device("cuda:0"),dtype=torch.float32)]
for i in range(11):
    exp = 1 << i
    data_size = int(exp * MB / 4)
    data_list.append(torch.zeros(data_size,device=torch.device("cuda:0"),dtype=torch.float32))


for data in data_list:
    swapper.swap_out_by_targets([data])
    swapper.wait_until_ok([data], status=2)
    

