#!/bin/bash
Test_Task=$1


SHELL_FOLDER=$(cd "$(dirname "$0")";pwd)
TEST_EXEC_1="${SHELL_FOLDER}/torch/bin/MemTracker_test"
TEST_EXEC_2="${SHELL_FOLDER}/torch/bin/CopyEngine_test"
TEST_EXEC_3="${SHELL_FOLDER}/torch/bin/MultiThreadDataPtr"
TEST_EXEC_4="${SHELL_FOLDER}/torch/bin/Advisor_test"
TEST_EXEC_5="${SHELL_FOLDER}/torch/bin/Transmission_test"
if [ 0"$TORCH_LIBS" = "0" ]; then
  export TORCH_LIBS="${SHELL_FOLDER}/build/lib.linux-x86_64-3.6/torch/lib"
  export LD_LIBRARY_PATH="${TORCH_LIBS}:${LD_LIBRARY_PATH}"
else
  echo ""
fi


if [ "$Test_Task" = "" ]; then
  ${TEST_EXEC_1}
  ${TEST_EXEC_2}
  ${TEST_EXEC_3}
  ${TEST_EXEC_4}
  ${TEST_EXEC_5}
  echo "TEST ALL"
elif [ $Test_Task -eq 1 ]; then
  echo "TEST 1"
  ${TEST_EXEC_1}
elif [ $Test_Task -eq 2 ]; then
  echo "TEST 2"
  ${TEST_EXEC_2}
elif [ $Test_Task -eq 3 ]; then
  echo "TEST 3"
  ${TEST_EXEC_3}
elif [ $Test_Task -eq 4 ]; then
  echo "TEST 4"
  ${TEST_EXEC_4}
elif [ $Test_Task -eq 5 ]; then
  echo "TEST 5"
  ${TEST_EXEC_5}
else
  echo "TEST NONE"
fi

